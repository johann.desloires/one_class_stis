import os
import numpy as np
import pandas as pd
import pickle
import os
import rasterio
import earthpy.spatial as es
import geopandas as gpd
#################################################################################################"
def GetRandomTheiaFile(folder_theia, band_name='B2'):
    '''
    Find a random tif images from the folders downloaded to get informations for the rasterization.
    Args:
        folder_theia (str): Name of the folder from the git repo cloned
        band_name (str) : band name used as reference (here, B2 for 10 meters images)
    Returns:

    '''
    path_folder = [os.path.join(folder_theia, k)
                   for k in os.listdir(folder_theia)
                   if ~np.any([x in k for x in ['zip', 'cfg', 'json', 'md', 'py', 'tmp']])]

    path_random_band = [os.path.join(path_folder[0], k) for k in os.listdir(path_folder[0])
                        if np.all([x in k for x in ['FRE']]) and k.split('_')[-1] == band_name + '.tif']

    return path_random_band[0]



####################################################################################################################
def get_cloud_mask():

    path = '/media/DATA/johann/PUL/TileHG/Sentinel2/'
    dates = pd.read_csv(os.path.join(path,'dates.csv'))
    b2 = pickle.load(open(os.path.join(path,'bands_pickle/dictionary_B2.pickle'),'rb'))
    mask_r10 = b2['CLM']
    file_reference = GetRandomTheiaFile(folder_theia = '/media/DATA/johann/PUL/TileHG/Sentinel2/theia_download',
                                        band_name='B2')

    id_bands = [ind for ind, k in enumerate(b2['Cloud_Percent']) if (k < 0.5 and '2019' in b2['dates'][ind])]
    print(len(id_bands))

    # Read metadata of first file
    with rasterio.open(file_reference) as src0:
        meta = src0.meta
        meta['nodata'] = None
        meta['dtype'] = 'uint8'
        meta['count'] = len(id_bands)

    with rasterio.open(os.path.join(path, 'mask_R10.tif'), 'w', **meta) as dst_clm:
        id = 0
        for num_band in id_bands:
            id +=1
            m = mask_r10[num_band,]
            dst_clm.write_band(id,m.astype(np.uint8))

    ################################################################

    mask_data = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/intersection_hg_tile.shp'
    extent = gpd.read_file(mask_data)
    es.crop_all([os.path.join(path, 'mask_R10.tif')],
                '/media/DATA/johann/PUL/TileHG/Sentinel2/',
                extent, overwrite=True, all_touched=True, verbose=True)  #

get_cloud_mask()
