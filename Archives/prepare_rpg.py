import geopandas as gpd
import os
import matplotlib.pyplot as plt
import pandas as pd
import split_AOI
import numpy as np
#Departments
os.chdir('/home/s999379/tmp/pycharm_project_923/data/')


def prepare_RPG_tile():
    rpg = gpd.read_file('./RPG_2019_HG')
    intersection_tile = gpd.read_file("./tile_cut_S2.gpkg")

    intersection_tile = intersection_tile[intersection_tile.DN == -10000]
    intersection_tile['ID'] =[1,2,3,4]
    intersection_tile = intersection_tile[intersection_tile.ID == 4]

    categories = {'1': 'Soft wheat',
                  '2': 'Corn',
                  '3': 'Barley',
                  '4': 'Other cereals',
                  '5': 'Rapeseed',
                  '6': 'Sunflower',
                  '7': 'Oilseeds',
                  '8': 'Proteaginous',
                  '9': 'Fiber plantes',
                  #'10': '',
                  '11': 'Frozen',
                  #'12': '',
                  #'13' : '',
                  '14': 'Rice',
                  '15': 'Legumes grains',
                  '16': 'Feed',
                  '17': 'Meadows',
                  '18': 'Meadows',
                  '19': 'Meadows',
                  '20': 'Orchards',
                  '21': 'Orchards',
                  '22': 'Orchards',
                  '23': 'Orchards',
                  '24': 'Cultures Maraichaires',
                  '25': 'Legumes or flowers',
                  '26': 'Sugar cane',
                  #'27': '',
                  '28': 'Various'
                  }
    for key,value in categories.items():
        categories[key] = [value]

    df_categories = pd.DataFrame(categories).T
    df_categories.reset_index(inplace=True)
    df_categories.columns = ['CODE_GROUP','CATEGORY']

    df_categories = df_categories.sort_index(axis = 0)
    rpg = pd.DataFrame(rpg)
    df_categories['CODE_GROUP'] = df_categories['CODE_GROUP'].astype(int)
    rpg['CODE_GROUP'] = rpg['CODE_GROUP'].astype(int)

    rpg = pd.merge(rpg,df_categories, on = 'CODE_GROUP',how ='left')
    rpg = gpd.GeoDataFrame(rpg,geometry = rpg.geometry)

    rpg.crs = 'epsg:2154'
    rpg = rpg.to_crs('epsg:32631')
    rpg_ =gpd.sjoin(rpg,intersection_tile, how = 'inner')
    rpg_.to_file(driver='ESRI Shapefile', filename='./rpg_intersection_tile_hg')

    #rpg_['results'] = rpg_.geometry.apply(lambda x : intersection_tile.geometry.intersection(x))
    #Construct a boolean associated
    #booleans = np.array([(1-k.is_empty) for k in rpg_.results])
    #rpg_['check'] = booleans
    #rpg_.check = gdf.check.astype(bool)
    #rpg_.to_file(driver = 'ESRI Shapefile', filename = './rpg_tile_hg')

    return(rpg_)

#rpg_ = prepare_RPG_tile()
rpg_ = gpd.read_file('./rpg_intersection_tile_hg/rpg_intersection_tile_hg.shp')
rpg_.CATEGORY
rpg_[['CODE_GROUP','CATEGORY']].drop_duplicates()

len(rpg_.CODE_GROUP.unique())
len(rpg_.CODE_GROUP.unique())
rpg_['surfaces'] = [(k.area)*1e-6 for k in rpg_.geometry]
surface = pd.DataFrame(rpg_.groupby(['CATEGORY'])['surfaces'].agg('sum'))
surface.reset_index(inplace = True)
counting = pd.DataFrame(rpg_[['CODE_GROUP','CATEGORY']].groupby(['CATEGORY']).count())
counting.reset_index(inplace = True)
total = pd.merge(surface,counting, on = 'CATEGORY',how = 'left')
total.surfaces = total.surfaces.apply(lambda x : round(x,2))
print(total.to_latex(index=False))
np.sum(total['surfaces'])

total['Class'] = ''
total.loc[total.CATEGORY.isin(['Barley','Corn','Oilseeds','Other cereals','Rapeseed','Soft wheat','Sunflower']),'Class'] = 'Cereals & Oilseeds'
total.loc[total.CATEGORY.isin(['Meadows','Frozen']),'Class'] = 'Uncultivated'
total.loc[total.CATEGORY.isin(['Cultures Maraichaires','Proteaginous',
                               'Legumes grains',
                               'Legumes or flowers',
                               'Fiber plantes']),'Class'] = 'Vegetables'

total.loc[total.CATEGORY.isin(['Feed']),'Class'] = 'Feed'

total.loc[total.CATEGORY.isin(['Orchards']),'Class'] = 'Orchards'

surface_tot = pd.DataFrame(total.groupby(['Class'])['surfaces'].agg('sum'))
surface_tot.reset_index(inplace = True)
counting_tot = pd.DataFrame(total.groupby(['Class'])['CODE_GROUP'].agg('sum'))
counting_tot.reset_index(inplace = True)
total_tot = pd.merge(surface_tot,counting_tot, on = 'Class',how = 'left')
total_tot.surfaces = total_tot.surfaces.apply(lambda x : round(x,2))
print(total_tot.to_latex(index=False))

rpg_ = pd.merge(rpg_,total[['Class','CATEGORY']], on = 'CATEGORY', how = 'left')

rpg_.to_file(driver = 'ESRI Shapefile', filename = './rpg_category_tile_hg')
##################################################################################################



associations = rpg_[['CODE_CULTU','CODE_GROUP','CATEGORY','SUB-CATEGO','surfaces']].drop_duplicates()
associations['CROP'] = ""

association_cereales = associations[associations.CATEGORY.isin(['CEREALES','OLEAGINEUX','PROTEAGINEUX','SEMENCES'])][['CODE_CULTU','CODE_GROUP','CATEGORY','CROP']].drop_duplicates()
association_cereales[association_cereales.CROP =='']

associations.loc[associations.CODE_CULTU.isin(['BTH','ORH','BDH','TTH','EPE','CZH','SGH','AVH','LIH','LIP','CHT']),['CROP']] = "Winter crops"
associations.loc[associations.CODE_CULTU.isin(['MID','MIE','MIS','TRN','SOJ','MLT',"SOG",'SRS','CGO','CGF']),['CROP']] = "Summer crops"
associations.loc[associations.CODE_CULTU.isin(['BDP','BTP','ORP','AVP','CZP']),['CROP']] = "Spring crops"
associations.loc[associations.CODE_CULTU.isin(['CAG','MCR','ORP']),['CROP']] = "Undefined cereals"

associations.loc[associations.CATEGORY.isin(['PROTEAGINEUX',' LEGUMES-GRAINS','LEGUMES-FLEURS']),['CROP']] = "Market gardening"
associations.loc[associations.CATEGORY.isin(['Herbe','GEL']),['CROP']] = "Meadows"
associations.loc[associations.CATEGORY.isin(['ARBORICULTURE','FRUITS-COQUES']),['CROP']] = "Orchards"
associations.loc[associations.CATEGORY.isin(['FOURRAGE']) & associations.CODE_CULTU.isin(['SM']) ,['CROP']] = "FOURRAGE"

associations[['CROP']].groupby('CROP').count()
associations.groupby(['CATEGORY'])['surfaces'].agg('sum')


#oleagineux + cereales
#just foret dense
#classe bati TOPO => dillatation (morphological) et erosion
#bati plus petit => 10m gommé => faire dilatation : reste connecté puis erosion : tache urbanisée
#classe eau BDD TOPO souvent 4/5 classes. OSO Jordi classification large echelle
#lecture papiers + shapefile + formation

