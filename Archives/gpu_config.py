wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub
sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
sudo apt-get update

sudo apt-get install libcudnn7_7.4.2.24-1+cuda10.1
sudo apt-get install libcudnn7-dev=7.4.2.24-1-1+cuda10.1

#https://gist.github.com/kmhofmann/cee7c0053da8cc09d62d74a6a4c1c5e4
sudo apt install nvidia-cuda-toolkit
# sudo find /usr -name libcudnn*
#path /usr/lib/cuda/lib64/libcudnn.so.7
#LD_LIBRARY_PATH=/usr/lib/cuda/lib64/
#https://github.com/tensorflow/tensorflow/issues/20271

if [ -z $LD_LIBRARY_PATH ]; then
  LD_LIBRARY_PATH=/usr/lib/cuda/lib64/:/usr/lib/cuda/lib64/
else
  LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/cuda/lib64/:/usr/lib/cuda/lib64/
fi

export LD_LIBRARY_PATH
