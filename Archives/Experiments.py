import rasterio
import numpy as np
import os
import pandas as pd
from sklearn.model_selection import train_test_split, GroupShuffleSplit
from sklearn.preprocessing import MinMaxScaler
import random
from varname import nameof
import matplotlib.pyplot as plt
import pickle

origin_path = '/media/DATA/johann/PUL/TileHG/'
#origin_path = './data/'
os.chdir(origin_path)
path_images = './Sentinel2/GEOTIFFS/'


def make_directory(path) :
    if os.path.exists(path) is False :
        os.makedirs(path)

def save_numpy(path,array,name_array):
    with open(os.path.join(path, name_array + '.npy'), 'wb') as f :
        np.save(f, array)


file_bands = []
band_names = ['B2', 'B3', 'B4','B5','B6','B7','B8','B8A','B11','B12','NDVI','GNDVI','NDWI']

for band in band_names:
    for k in os.listdir(path_images) :
        if band in k.split('_'):
            file_bands.append(os.path.join(path_images, k))

def reformat_labels(target,file_reference):

    target_to_mask = rasterio.open(target)
    meta = target_to_mask.meta
    target_to_mask = target_to_mask.read(1)

    raster_ref = rasterio.open(file_reference)
    raster_ref = raster_ref.read(1)
    mask = (raster_ref == 0)

    arr0 = np.ma.array(target_to_mask,
                       dtype=np.int16,
                       mask=mask,
                       fill_value=0)
    target_to_mask = arr0.filled()

    with rasterio.open(target, 'w', **meta) as dst:
        dst.write_band(1, target_to_mask)


targets = ['./Sentinel2/GEOTIFFS/Erosion_Class_ID_crop.tif',
           './Sentinel2/GEOTIFFS/Erosion_Label_Code_crop.tif']

reformat_labels(targets[0],file_bands[0])
reformat_labels(targets[1],file_bands[0])

dates = pd.read_csv('./Sentinel2/dates.csv')



###############################################################################################################

labelling = pd.read_csv('./FinalDBPreprocessed/DATABASE_SAMPLED/Labelling.csv')

def prepare_training_set():
    array_target1 = rasterio.open(targets[0])
    array_target1 = array_target1.read(1)

    h,w = array_target1.shape
    array_target1 = array_target1.flatten()

    array_target2 = rasterio.open(targets[1])
    array_target2 = array_target2.read(1)
    array_target2 = array_target2.flatten()

    filter_observation = np.where(array_target1>0)

    x = np.linspace(0, h,h).astype(np.int16)
    y = np.linspace(0,w,w).astype(np.int16)
    xv, yv = np.meshgrid(x, y)

    dictionary_bands = {}
    dictionary_meta_info = {}
    dictionary_meta_info['xcoordinates_objects'] = xv.flatten()[filter_observation]
    dictionary_meta_info['ycoordinates_objects'] = yv.flatten()[filter_observation]
    dictionary_meta_info['Class_ID'] = array_target1[filter_observation]
    dictionary_meta_info['Label_ID'] = array_target2[filter_observation]
    dictionary_meta_info['bands'] = file_bands
    dictionary_meta_info['dates'] = list(dates.dates)

    #scaling
    for index_band in file_bands :
        dictionary_meta_info[index_band] = {}
        #index_band = file_bands[0]
        band_name = index_band.split('/')[-1].split('_')[-2]
        print(band_name)
        band = rasterio.open(index_band)

        array_time = []
        bands_filtered = []

        for array_index in range(1, band.count) :
            band_read = band.read(array_index)
            #plt.imshow(band_read, vmin=100, vmax=1000)
            #plt.show()
            array_time.append(band_read)

            band_read = band_read.flatten()
            band_read = band_read[filter_observation]
            bands_filtered.append(band_read)

        #Get statistics from the image
        array_time = np.stack(array_time, axis=0)
        if band_name not in band_names :
            lb = -1
        else:
            lb = 0
        dictionary_meta_info[index_band]['max'] = np.max(array_time[array_time>lb])
        dictionary_meta_info[index_band]['min'] = np.min(array_time[array_time>lb])
        dictionary_meta_info[index_band]['mean'] = np.mean(array_time[array_time>lb])
        dictionary_meta_info[index_band]['median'] = np.median(array_time[array_time>lb])
        dictionary_meta_info[index_band]['std'] = np.std(array_time[array_time>lb])

        bands_filtered = np.stack(bands_filtered,axis = 1)
        dictionary_bands[band_name] = bands_filtered


    dictionary_bands['Class_ID'] = array_target1[filter_observation]
    dictionary_bands['Label_Code'] = array_target2[filter_observation]

    bands_concatenated = []

    for key in dictionary_bands.keys() :
        print(key)
        if key not in ['Class_ID', 'Label_Code'] :
            cols = [key + '_' + dates.dates[id_] for id_ in range(dates.shape[0])]
            df_array = pd.DataFrame(dictionary_bands[key], columns=cols)
            print(df_array.shape)
        else :
            cols = [key]
            df_array = pd.DataFrame(dictionary_bands[key], columns=cols)
            print(df_array.shape)

        bands_concatenated.append(df_array)

    bands_concatenated = pd.concat(bands_concatenated, axis=1)

    bands_concatenated = pd.merge(bands_concatenated,
                                  labelling[['Class', 'Label_Code']].drop_duplicates(),
                                  on=['Label_Code'], how='left')


    x_cols = [k for k in bands_concatenated.columns if np.any([x in k for x in band_names]) ]
    array_bands = np.array(bands_concatenated[x_cols])
    array_bands = array_bands.reshape(dates.shape[0],bands_concatenated.shape[0],len(band_names))

    bands_concatenated.to_csv('./FinalDBPreprocessed/training_set.csv', index=False)
    save_numpy('./FinalDBPreprocessed/', array_bands, nameof(array_bands))

    with open('./Sentinel2/dictionary_meta_info.pickle', 'wb') as d :
        pickle.dump(dictionary_meta_info, d, protocol=pickle.HIGHEST_PROTOCOL)

#prepare_training_set()

#############################################################################################
bands_concatenated = pd.read_csv('./FinalDBPreprocessed/training_set.csv')


cols_random = bands_concatenated.columns[0]
bands_concatenated = bands_concatenated[bands_concatenated[cols_random] > 0]

def experiments(band_names,class_interest = 'Forest'):


    meta_info = pickle.load(open('./Sentinel2/dictionary_meta_info.pickle', 'rb'))
    labels = ['Class_ID', 'Label_Code','Class']

    bands_concatenated_scaled = bands_concatenated.copy()

    for band in band_names :
        cols_subset = [x for x in list(bands_concatenated.columns) if band in x.split('_')]
        key_mata = [x for x in meta_info.keys() if band in x.split('_')][0]
        bands_concatenated_scaled[cols_subset] = (bands_concatenated[cols_subset] - meta_info[key_mata]['min'])/(meta_info[key_mata]['max'] + meta_info[key_mata]['min'])

    ##
    path_experiments = './FinalDBPreprocessed/Experiments'
    if os.path.exists(path_experiments) is False :
        os.makedirs(path_experiments)


    path_class = os.path.join('./FinalDBPreprocessed/Experiments', ''.join(class_interest.split('/')))
    make_directory(path_class)

    number_experiments = 10

    # Take n_first polygons
    nb_P = [20,40,60, 80,100]#list(range(120,220,20))
    print(nb_P)

    class_ids_selected = {}
    for i in range(1,number_experiments+1):
        class_ids_selected[i] = {}
        for n in nb_P:
            class_ids_selected[i][n] = {}
            class_ids_selected[i][n]['P'] = []
            class_ids_selected[i][n]['U'] = []
            class_ids_selected[i][n]['T'] = []

    previous_nb_p = 0

    for n in nb_P:
        print(n)
        output_path_experiment_n = os.path.join(path_class, str(n) + '_P')
        make_directory(output_path_experiment_n)

        for i in range(1,number_experiments+1):

            print(i)
            train_inds, test_inds = next(
                GroupShuffleSplit(test_size=.5, n_splits=2,random_state = i).split(bands_concatenated, groups=bands_concatenated['Class_ID']))

            bands_concatenated_train = bands_concatenated_scaled.iloc[train_inds, :]
            bands_concatenated_test = bands_concatenated_scaled.iloc[test_inds, :]

            path_experiments_i = os.path.join(output_path_experiment_n, str(i))
            make_directory(path_experiments_i)

            labels = bands_concatenated_train.loc[bands_concatenated_train.Class.isin([class_interest]),:]

            if previous_nb_p == 0:
                class_ids = labels.Class_ID.unique()
                rdm_class = random.sample(list(class_ids), 20)
                class_ids_selected[i][n]['P'].extend(rdm_class)

            else:
                previous_polygons = class_ids_selected[i][previous_nb_p]['P'].copy()
                class_ids_selected[i][n]['P'] = previous_polygons
                class_ids = labels.loc[~labels.Class_ID.isin(previous_polygons),:].Class_ID.unique()

                rdm_class = random.sample(list(class_ids), 20)
                class_ids_selected[i][n]['P'].extend(rdm_class)

            class_ids_U = list(bands_concatenated_train.loc[~bands_concatenated_train.Class_ID.isin(class_ids_selected[i][n]['P']), :].Class_ID.unique())
            class_ids_selected[i][n]['U'].extend(class_ids_U)
            class_ids_T = list(bands_concatenated_test.Class_ID.unique())
            class_ids_selected[i][n]['T'].extend(class_ids_T)



            print('There are ' + str(len(class_ids_selected[i][n]['P'])) + ' polygons selected')

            x_var = [k for k in bands_concatenated.columns
                     if k.split('_')[0] in ['B2','B3','B4','B8']]

            #Positive
            P = bands_concatenated_train[bands_concatenated_train.Class_ID.isin(class_ids_selected[i][n]['P'])]
            #P.to_csv(os.path.join(path_experiments_i,'Positive.csv'))
            X_P = np.array(P[x_var])
            print(X_P.shape)
            save_numpy(path_experiments_i, X_P,nameof(X_P))
            Y_P = np.array(P[['Label_Code','Class_ID']])
            save_numpy(path_experiments_i, Y_P,nameof(Y_P))

            #Unlabeled
            U = bands_concatenated_train[~bands_concatenated_train.Class_ID.isin(class_ids_selected[i][n]['P'])]
            #U.to_csv(os.path.join(path_experiments_i,'Unlabeled.csv'))
            X_U = np.array(U[x_var])
            print(X_U.shape)
            save_numpy(path_experiments_i, X_U, nameof(X_U))
            Y_U = np.array(U[['Label_Code','Class_ID']])
            save_numpy(path_experiments_i, Y_U, nameof(Y_U))

            #Test
            #bands_concatenated_test.to_csv(os.path.join(path_experiments_i,'Test.csv'))
            X_T = np.array(bands_concatenated_test[x_var])
            print(X_T.shape)
            save_numpy(path_experiments_i, X_T, nameof(X_T))
            Y_T = np.array(bands_concatenated_test[['Label_Code','Class_ID']])
            save_numpy(path_experiments_i, Y_T, nameof(Y_T))

        previous_nb_p = n

    with open(os.path.join(path_class,'dictionary_sample_selection.pickle'), 'wb') as d :
        pickle.dump(class_ids_selected, d, protocol=pickle.HIGHEST_PROTOCOL)


experiments(band_names,class_interest = 'Cereals/Oilseeds')
experiments(band_names,class_interest = 'Forest')
experiments(band_names,class_interest = 'Built')