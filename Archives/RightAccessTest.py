import rasterio
import numpy as np
import os
import pandas as pd
from sklearn.model_selection import train_test_split, GroupShuffleSplit
from sklearn.preprocessing import MinMaxScaler
import random
from varname import nameof
import matplotlib.pyplot as plt
import pickle

origin_path = '/media/DATA/johann/PUL/TileHG/'
#origin_path = './data/'
os.chdir(origin_path)
path_images = './Sentinel2/GEOTIFFS/'


def make_directory(path) :
    if os.path.exists(path) is False :
        os.makedirs(path)

def save_numpy(path,array,name_array):
    with open(os.path.join(path, name_array + '.npy'), 'wb') as f :
        np.save(f, array)


file_bands = []
band_names = ['B2', 'B3', 'B4','B5','B6','B7','B8','B8A','B11','B12','NDVI','GNDVI','NDWI']

for band in band_names:
    for k in os.listdir(path_images) :
        if band in k.split('_'):
            file_bands.append(os.path.join(path_images, k))

def reformat_labels(target,file_reference):

    target_to_mask = rasterio.open(target)
    meta = target_to_mask.meta
    target_to_mask = target_to_mask.read(1)

    raster_ref = rasterio.open(file_reference)
    raster_ref = raster_ref.read(1)
    mask = (raster_ref == 0)

    arr0 = np.ma.array(target_to_mask,
                       dtype=np.int16,
                       mask=mask,
                       fill_value=0)
    target_to_mask = arr0.filled()

    with rasterio.open(target, 'w', **meta) as dst:
        dst.write_band(1, target_to_mask)


targets = ['./Sentinel2/GEOTIFFS/Erosion_Class_ID_crop.tif',
           './Sentinel2/GEOTIFFS/Erosion_Label_Code_crop.tif']

reformat_labels(targets[0],file_bands[0])
reformat_labels(targets[1],file_bands[0])