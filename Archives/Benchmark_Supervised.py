import sys

sys.path.insert(0, '/home/johann/Topography/Supervised/')
sys.path.append('../')  #

import time

import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import pickle
import pandas as pd
from Supervised import models
from Autoencoder import utils
import numpy as np
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score

origin_path = '/media/DATA/johann/PUL/TileHG/'
os.chdir(origin_path)

path = './FinalDBPreprocessed/Experiments/MeadowsUncultivated/'


####################################################################################
#Our approach
classifier_GRU = models.GRUClassifImproved(nunits=32, fcunits=32,  # 64, 32
                                           dropout_rate=0.2, l2_reg=0.0)

TempCNN = models.TempCNN(ndim = 64, n_filters=16)

###########################
#Teacher
metrics = {}
#metrics['teacher'] = []
metrics['teacher_student'] = []
metrics['teacher_student_XT'] = []

metrics['teacher_only'] = []
metrics['teacher_only_X_T'] = []

metrics['student_only'] = []
metrics['student_only_X_T'] = []

type_ = 'VAE'
name_model = 'GRU_V5_tanh_64_64_Student_AE'

for n_sample in range(20,120,20):#:
    print(str(n_sample))
    for i_exp in range(1,11,2):
        print(i_exp)
        path_save = os.path.join(path, 'Models/' + type_ + '/N_' + \
                                 str(n_sample) + '_Exp_' + str(i_exp) + \
                                 '/PUL_NN/' + name_model + '_alpha_0.5')
        start = time.time()
        if os.path.exists(os.path.join(path_save, 'dictionary_results.pickle')):
            X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path,
                                                                          n_sample=n_sample,
                                                                          i_exp=i_exp)

            path_AE = os.path.join(path, 'Models/' + type_ + '/N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')
            start = time.time()
            dico_tanh, filename, model = utils.read_AE_file(path_out=path_AE,
                                                            hidden_activation='tanh',
                                                            embedding_activation='None',
                                                            output_activation='None',
                                                            name_optimizer='Adam_10e-4',
                                                            name_loss='huber',
                                                            prefix='VAEGRU_V2_32')

            predictions_T = model.predict(X_T.reshape(X_T.shape[0], 25, 6), batch_size=256)
            end= time.time()
            time_transfo = end - start
            print('time transfo ' + str(time_transfo))
            #####################################################################################
            path_student = os.path.join(path, 'Models/' + type_ + '/N_' + \
                                     str(n_sample) + '_Exp_' + str(i_exp) + \
                                     '/PUL_NN/' + name_model + '_alpha_1')

            if os.path.exists(os.path.join(path_student, 'dictionary_results.pickle')):
                # Student only
                dictionary_student = pickle.load(open(os.path.join(path, 'Models/' + type_ + '/N_' + \
                                                                   str(n_sample) + '_Exp_' + str(i_exp) + \
                                                                   '/PUL_NN/' + name_model + '_alpha_1/dictionary_results.pickle'),
                                                      'rb'))
        
                metric = pd.DataFrame(dictionary_student['metrics'])
                metric['Sample_size'] = n_sample
                metric['i_exp'] = i_exp
                metric['alpha'] = "1"
                metrics['student_only'].append(metric)

                # TRANSFORMED DATA
                start = time.time()
                classifier_GRU.load_weights(os.path.join(path, 'Models/' + type_ + '/N_' + \
                                                         str(n_sample) + '_Exp_' + str(i_exp) + \
                                                         '/PUL_NN/' + name_model + '_alpha_1/' + name_model + '_alpha_1_student'))
        
                preds = classifier_GRU.predict(predictions_T, batch_size=256)
                preds = np.argmax(preds, axis=1)
                end = time.time()
                time_model = end - start
                print('time model ' + str(time_model))
        
                metric = dict(f1_score=[f1_score(y_t,
                                                 preds, average='weighted')],
                                       accuracy=[accuracy_score(y_t, preds)],
                                       kappa=[cohen_kappa_score(y_t, preds)])
        
                metric = pd.DataFrame(metric)
                metric['Sample_size'] = n_sample
                metric['i_exp'] = i_exp
                metric['time_transf'] = time_transfo
                metric['time_model'] = time_model
                metrics['student_only_X_T'].append(metric)

            ################################################################################################################
            # TEACHER ONLY
            #ORIGINAL DATA
            classifier_GRU.load_weights(os.path.join(path, 'Models/' + type_ + '/N_' + \
                                                     str(n_sample) + '_Exp_' + str(i_exp) + \
                                                     '/PUL_NN/' + name_model + '_alpha_0.5/' + name_model + '_alpha_0.5_teacher_CE'))

            preds = classifier_GRU.predict(X_T.reshape(X_T.shape[0], 25, 6), batch_size=256)
            preds = np.argmax(preds, axis=1)

            metrics_teacher = dict(f1_score=[f1_score(y_t,
                                                      preds, average='weighted')],
                                   accuracy=[accuracy_score(y_t, preds)],
                                   kappa=[cohen_kappa_score(y_t, preds)])

            metric_teacher = pd.DataFrame(metrics_teacher)
            metric_teacher['Sample_size'] = n_sample
            metric_teacher['i_exp'] = i_exp
            print(metric_teacher)
            metrics['teacher_only'].append(metric_teacher)

            # TRANSFORMED DATA
            classifier_GRU.load_weights(os.path.join(path, 'Models/' + type_ + '/N_' + \
                                                     str(n_sample) + '_Exp_' + str(i_exp) + \
                                                     '/PUL_NN/' + name_model + '_alpha_0.5/' + name_model + '_alpha_0.5_teacher_CE'))

            preds = classifier_GRU.predict(predictions_T, batch_size=256)
            preds = np.argmax(preds, axis=1)

            metrics_teacher = dict(f1_score=[f1_score(y_t,
                                                      preds, average='weighted')],
                                   accuracy=[accuracy_score(y_t, preds)],
                                   kappa=[cohen_kappa_score(y_t, preds)])

            metric_teacher = pd.DataFrame(metrics_teacher)
            metric_teacher['Sample_size'] = n_sample
            metric_teacher['i_exp'] = i_exp
            print(metric_teacher)
            metrics['teacher_only_X_T'].append(metric_teacher)


            for alpha in [0.5]:#[0.1, 0.3, 0.5, 0.7, 0.9]:
                print(alpha)
                '''
                ################################################################################################################
                classifier_GRU.load_weights(os.path.join(path, 'Models/' + type_ + '/N_' + \
                                                         str(n_sample) + '_Exp_' + str(i_exp) + \
                                                         '/PUL_NN/' + name_model + '_alpha_' + str(alpha) + '/' + name_model + '_alpha_' + str(alpha) + '_teacher'))
    
                preds = classifier_GRU.predict(X_T.reshape(X_T.shape[0], 25, 6),batch_size = 32)
                preds = np.argmax(preds,axis = 1)
    
                metrics_teacher = dict(f1_score=[f1_score(y_t,
                                                          preds, average='weighted')],
                                       accuracy=[accuracy_score(y_t, preds)],
                                       kappa = [cohen_kappa_score(y_t, preds)])
    
    
                metric_teacher = pd.DataFrame(metrics_teacher)
                metric_teacher['Sample_size'] = n_sample
                metric_teacher['i_exp'] = i_exp
                metric_teacher['alpha'] = 'i_exp'
                metric_teacher['alpha'] = alpha
                metrics['teacher'].append(metric_teacher)
                print(metric_teacher)
                
    
                #ORGINAL DATA
                preds = classifier_GRU.predict(X_T.reshape(X_T.shape[0], 25, 6),
                                               batch_size=32)
                preds = np.argmax(preds, axis=1)
    
                metrics_teacher = dict(f1_score=[f1_score(y_t,
                                                          preds, average='weighted')],
                                       accuracy=[accuracy_score(y_t, preds)],
                                       kappa=[cohen_kappa_score(y_t, preds)])
    
                metric_teacher = pd.DataFrame(metrics_teacher)
                metric_teacher['Sample_size'] = n_sample
                metric_teacher['i_exp'] = i_exp
                metrics['teacher_only'].append(metric_teacher)
                print(metrics_teacher)
                '''

                ################################################################################################################
                #Student
                ##Original data
                dictionary_student = pickle.load(open(os.path.join(path, 'Models/' + type_ + '/N_' + \
                                                                   str(n_sample) + '_Exp_' + str(i_exp) + \
                                                                   '/PUL_NN/' + name_model + '_alpha_' + str(alpha) + '/dictionary_results.pickle'), 'rb'))

                metric = pd.DataFrame(dictionary_student['metrics'])
                metric['Sample_size'] = n_sample
                metric['i_exp'] = i_exp
                metric['alpha'] = alpha
                metrics['teacher_student'].append(metric)
                '''
                # TRANSFORMED DATA
                classifier_GRU.load_weights(os.path.join(path, 'Models/' + type_ + '/N_' + \
                                                         str(n_sample) + '_Exp_' + str(i_exp) + \
                                                         '/PUL_NN/' + name_model + '_alpha_' + str(alpha) + '/' + name_model + '_alpha_' + str(alpha) + '_student'))
    
                preds = classifier_GRU.predict(predictions_T, batch_size=32)
                preds = np.argmax(preds, axis=1)
    
                metrics_teacher = dict(f1_score=[f1_score(y_t,
                                                          preds, average='weighted')],
                                       accuracy=[accuracy_score(y_t, preds)],
                                       kappa=[cohen_kappa_score(y_t, preds)])
    
                metric = pd.DataFrame(metrics_teacher)
                metric['Sample_size'] = n_sample
                metric['i_exp'] = i_exp
                metrics['teacher_student_X_T'].append(metric)
                '''
        else:
            pass


end = time.time()
a = end - start

with open(os.path.join(os.path.join(path,'Models/' + type_ + '/GRU_V5_tanh_64_64_metrics.pickle')),'wb') as d:
    pickle.dump(metrics, d, protocol=pickle.HIGHEST_PROTOCOL)


####################################################################################
###Competitors
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np




def plot_results(numeric_values):
    ax = numeric_values.plot()

    markers = ['*', 'X',  'o', 'v', '^','P']
    for i, line in enumerate(ax.get_lines()):
        line.set_marker(markers[i])

    # adding legend
    ax.legend(ax.get_lines(), numeric_values.columns , bbox_to_anchor=(1.05, 1), loc='upper left')#, loc='best')
    plt.tight_layout()
    plt.ylim(ymin = 0, ymax = 1)
    plt.show()

origin_path = '/media/DATA/johann/PUL/TileHG/'
os.chdir(origin_path)
path = './FinalDBPreprocessed/Experiments/CerealsOilseeds/'

metric = 'kappa'
dictionary_out = pickle.load(open(os.path.join(path, 'Models/' + str(0.5) + '/dictionary_output_' + metric + '.pickle'), 'rb'))


print(dictionary_out['table'].to_latex())

#gru_results = pickle.load(open(os.path.join(os.path.join(path,'Models/AE/GRU_V1_tanh_64_64_metrics.pickle')),'rb'))
gru_results = pickle.load(open(os.path.join(os.path.join(path,'Models/AE/GRU_V1_tanh_64_64_metrics.pickle')),'rb'))
list(gru_results.keys())


gru_results_student = gru_results['teacher_student']
gru_results_student = pd.concat(gru_results_student)
gru_results_student = gru_results_student.drop('i_exp',axis = 1).groupby(['Sample_size','alpha']).agg('mean')
gru_results_student.reset_index(inplace = True)
gru_results_student['type'] = 'teacher_student'

gru_results_teacher = gru_results['teacher_only']
gru_results_teacher = pd.concat(gru_results_teacher)
gru_results_teacher = gru_results_teacher.drop('i_exp',axis = 1).groupby(['Sample_size']).agg('mean')
gru_results_teacher.reset_index(inplace = True)
gru_results_teacher['type'] = 'teacher_only'

gru_results_teacher_only = gru_results['teacher_only_X_T']
gru_results_teacher_only = pd.concat(gru_results_teacher_only)
gru_results_teacher_only = gru_results_teacher_only.drop('i_exp',axis = 1).groupby(['Sample_size']).agg('mean')
gru_results_teacher_only.reset_index(inplace = True)
gru_results_teacher_only['type'] = 'teacher_only_XT'
print(gru_results_teacher_only.drop(['type'],axis = 1).to_latex())

gru_results_student_only = gru_results['student_only']
gru_results_student_only = pd.concat(gru_results_student_only)
gru_results_student_only = gru_results_student_only.drop('i_exp',axis = 1).groupby('Sample_size').agg('mean')
gru_results_student_only.reset_index(inplace = True)
gru_results_student_only['alpha'] = 1
gru_results_student_only['type'] = 'student_only'

gru_results_student_only_X_T = gru_results['student_only_X_T']
gru_results_student_only_X_T = pd.concat(gru_results_student_only_X_T)
gru_results_student_only_X_T = gru_results_student_only_X_T.drop('i_exp',axis = 1).groupby(['Sample_size']).agg('mean')
gru_results_student_only_X_T.reset_index(inplace = True)
gru_results_student_only_X_T['alpha'] = 1
gru_results_student_only_X_T['type'] = 'student_only_XT'

cols = list(gru_results_student.columns)



###################################################

##########################

gru_results_teacher['alpha'] = 0
gru_results_teacher_only['alpha'] = 0

df_subset = pd.concat([gru_results_student[cols],
                gru_results_student_only[cols],
                gru_results_student_only_X_T[cols],
                gru_results_teacher[cols],
                gru_results_teacher_only[cols]
                ],
               axis = 0)

'''
df_subset = df_subset[df_subset.alpha.isin([0,0.5,1])]
import seaborn as sns
fig, ax1 = plt.subplots(1,1)
g = sns.lineplot(data=df_subset,
                 x="Sample_size",
                 y=index,
                 hue="type")


# EDIT:
# Removed 'ax' from T.W.'s answer here aswell:
box = g.get_position()
g.set_position([box.x0, box.y0, box.width * 0.85, box.height]) # resize position

# Put a legend to the right side
g.legend(loc='center right', bbox_to_anchor=(1.35, 0.5), ncol=1)

plt.show()
'''





#####################################################################################
R = 0.5
path_out = os.path.join(path, "Models/") + str(R)
os.listdir(path_out)
metric_name = 'f1_score'
ratio = {'hold_out_ratio': R}
dictionary_out = pickle.load(open(os.path.join(path_out, 'dictionary_output_' + metric_name + '.pickle'), 'rb'))

numeric_values = dictionary_out['plot']
numeric_values = numeric_values.iloc[[0,2,4],:]
numeric_values['RF'] = numeric_values['RF'].astype(np.float)
numeric_values['ENSEMBLE'] = numeric_values['ENSEMBLE'].astype(np.float)
numeric_values['OCSVM'] = numeric_values['OCSVM'].astype(np.float)

numeric_values['Teacher'] = gru_results_teacher[metric_name].values
numeric_values['Teacher_XT'] = gru_results_teacher_only[metric_name].values
numeric_values['Student'] = gru_results_student_only[metric_name].values
numeric_values['Student_XT'] = gru_results_student_only_X_T[metric_name].values
numeric_values['Teacher-Student'] = gru_results_student[metric_name].values

numeric_values = numeric_values.drop(['RF','ENSEMBLE','OCSVM'],axis = 1)

#plot_results(numeric_values)
print(numeric_values.to_latex())

m = dictionary_out['plot']

m['RF'] = m['RF'].astype(np.float)
m['ENSEMBLE'] = m['ENSEMBLE'].astype(np.float)
m['OCSVM'] = m['OCSVM'].astype(np.float)
#m['Student'] = metrics_agg[metric_name]

m.plot()
plt.show()

######################
#########################################################################


#alpha 0.5 (classical case)
gru_results_teacher_only['alpha'] = 0
#gru_results_student_only['alpha'] = 1

index = 'accuracy'

df_all = pd.concat([gru_results_student[cols],
                    gru_results_teacher_only[cols]
                ],
               axis = 0)

import seaborn as sns
fig, ax1 = plt.subplots(1,1)
g = sns.lineplot(data=df_all,
                 x="Sample_size",
                 y=index,
                 hue="type",
                 style="alpha")

# EDIT:
# Removed 'ax' from T.W.'s answer here aswell:
box = g.get_position()
g.set_position([box.x0, box.y0, box.width * 0.85, box.height]) # resize position

# Put a legend to the right side
g.legend(loc='center right', bbox_to_anchor=(1.35, 0.5), ncol=1)

plt.show()

