
import pickle
from osgeo import gdal
import rasterio
import numpy as np
import geopandas as gpd
import earthpy.spatial as es
import gdal
import os
import pandas as pd
import subprocess
import matplotlib.pyplot as plt

# subprocess.call([r'C:/Users/s999379/Documents/OTB-7.2.0-Win64/OTB-7.2.0-Win64/otbenv.bat'])
# import otbApplication as otb

# Open bands
path = '/media/DATA/johann/PUL/TileHG/'
#path_theia = './Sentinel2/theia_download'
os.getcwd()
#path = './data/'
os.chdir(path)

#path_theia = '/home/s999379/theia_download'
path_theia = './Sentinel2/theia_download'

def Open_array_info(filename=''):
    """
    Opening a tiff info, for example size of array, projection and transform matrix.
    Keyword Arguments:
    filename -- 'C:/file/to/path/file.tif' or a gdal file (gdal.Open(filename))
        string that defines the input tiff file or gdal file
    """
    f = gdal.Open(r"%s" % filename)
    if f is None:
        print('%s does not exists' % filename)
    else:
        geo_out = f.GetGeoTransform()
        proj = f.GetProjection()
        size_X = f.RasterXSize
        size_Y = f.RasterYSize
        f = None
    return (geo_out, proj, size_X, size_Y)


def getrandom_reference_tif(path_theia = './theia_download',band_name = 'B2') :
    path_folder = [os.path.join(path_theia,k)
                   for k in os.listdir(path_theia)
                   if ~np.any([x in k for x in ['zip','cfg','json','md','py','tmp']])]
    #only on remote machine (bug)
    #path_folder = [os.path.join(k,os.listdir(k)[0]) for k in path_folder]

    path_random_band = [os.path.join(path_folder[0],k) for k in os.listdir(path_folder[0])
                        if np.all([x in k for x in ['FRE']]) and k.split('_')[-1] == band_name + '.tif']

    return path_random_band[0]

path_out = './Sentinel2/GEOTIFFS/'

def delete_folder():

    os.getcwd()

    if os.path.exists(path_out) is False:
        os.mkdir(path_out)
    else:
        script = "sudo rm -rf " + path_out
        subprocess.call(script, shell=True)




bands = ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12']


def write_tiff_from_dict(path,path_out,path_theia,
                         bands= ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12']):

    for band in bands:
        #band = 'B4'
        print(band)
        band_array = pickle.load(open('./Sentinel2/bands_pickle/dictionary_' + band + '.pickle', 'rb'))
        #os.remove('./Sentinel2/bands_pickle/dictionary_' + band + '.pickle')
        band_values = band_array['data']
        band_values = band_values.astype(np.int16)

        band_cloud = band_array['CLM']
        band_cloud = band_cloud.astype(np.int16)

        file_reference = getrandom_reference_tif(path_theia,band_name = band)
        geo, proj, size_X, size_Y = Open_array_info(file_reference)
        # filter array

        ##################################

        # Read metadata of first file
        with rasterio.open(file_reference) as src0:
            meta = src0.meta
            meta['nodata'] = 0.0

        # Update meta to reflect the number of layers
        id_bands = [ind for ind, k in enumerate(band_array['Cloud_Percent']) if k < 0.5]
        meta.update(count=len(id_bands)+1)

        with rasterio.open(os.path.join(path_out, 'stack_' + band + '.tif'), 'w', **meta) as dst:
            for id, time_index in enumerate(id_bands):
                dst.write_band(id + 1, band_values[time_index, :, :].astype(np.int16))

        rsuffix = '20m' if geo[1] == 20 else '10m'

        #if os.path.exists(os.path.join(path, 'stack_' + rsuffix + '.tif')) is False:
        meta['nodata'] = None
        with rasterio.open(os.path.join(path_out, 'stack_' + rsuffix + '.tif'), 'w', **meta) as dst_clm:
            for id, num_band in enumerate(id_bands):
                dst_clm.write_band(id + 1, band_cloud[num_band, :, :].astype(np.int16))

        del band_cloud
        del band_values
        del band_array

        if os.path.exists(os.path.join('./Sentinel2', 'dates.csv')) is False:
            dates = list(band_array['dates'])
            dates = [dates[id_] for id_ in id_bands]
            dates = pd.DataFrame(dates)
            dates.columns = ['dates']
            dates['dates'] = dates['dates'].apply(lambda x : pd.to_datetime(x, format='%Y%m%d'))
            dates.to_csv(os.path.join('./Sentinel2', 'dates.csv'),index = False)
            del dates

#write_tiff_from_dict(path,path_out,path_theia,bands= ['B5', 'B6'])
write_tiff_from_dict(path,path_out,path_theia,bands= ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12'])

################################################################################
#name='10m'
#raster = rasterio.open(os.path.join(path_out, 'stack_' + name + '.tif'))
#raster = raster.read(20)
#raster.shape
#plt.imshow(raster,vmin = 100,vmax = 1000)
#plt.show()
################################################################################
#Crop stacked array
os.getcwd()
extent = gpd.read_file(
    './FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/intersection_hg_tile.shp')

raster_paths =  './Sentinel2/GEOTIFFS/'

paths_list = [os.path.join(raster_paths,tif_file) for tif_file in list(os.listdir(raster_paths)) if  tif_file.split('.')[-1] == 'tif'
              and  ~ np.any([x in tif_file for x in ['crop']])]

len(paths_list)
output_crop = './Sentinel2/GEOTIFFS/'

es.crop_all(paths_list, output_crop, extent, overwrite=True, all_touched=True, verbose=True) #

import subprocess
#remove previous images
for path in paths_list:
    script = "sudo rm " + path
    subprocess.call(script, shell=True)

path_class = './FinalDBPreprocessed/DATABASE_SAMPLED/Erosion_Class_ID.tif'
path_labels = './FinalDBPreprocessed/DATABASE_SAMPLED/Erosion_Label_Code.tif'


paths_output = [path_class,path_labels]

es.crop_all(paths_output, output_crop, extent, overwrite=True, all_touched=True, verbose=True)


#works as well
#exec(open("./GapFilling.py").read())

################################################################################
raster = rasterio.open(os.path.join(output_crop, 'stack_' + 'B4' + '_crop.tif'))
raster = raster.read(20)
raster.shape
plt.imshow(raster,vmin = 100,vmax = 1000)
plt.show()