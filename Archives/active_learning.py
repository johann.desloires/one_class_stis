import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import time
import os
import numpy as np #do numpy 1.19.5

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf


import numpy as np
import os


tf.keras.backend.set_floatx('float32')

import Archives.supervised_training as supervised_training
from Autoencoder import utils as utils
import pandas as pd
from importlib import reload

import Supervised

from Supervised import Consistency, pretraining
from importlib import reload
import random
import tslearn
from tslearn import clustering
################################################################################################
path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/MeadowsUncultivated'
n_sample = 60
i_exp = 1
X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                              n_sample=n_sample,
                                                              i_exp=i_exp)

##############################################################################################
# path_VAE = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')
path_file = 'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp)
path_AE = os.path.join(path_experiment,
                       os.path.join(path_file, 'Reconstruction'))

dico_tanh, filename, model = utils.read_AE_file(path_out=path_AE,
                                                hidden_activation='tanh',
                                                embedding_activation='None',
                                                output_activation='None',
                                                name_optimizer='Adam_10e-4',
                                                name_loss='huber',
                                                prefix='AEGRU_V1_16')

reconstruction_df_tanh = dico_tanh['Reconstruction']
reconstruction_df_tanh['binary'] = y_u
reconstruction_df_tanh['True_class'] = Y_U[:, 0]

index_RP = np.where(reconstruction_df_tanh['Huber'] <= np.median(
    reconstruction_df_tanh['Huber']) - 0.5 *np.std(reconstruction_df_tanh['Huber']))[0]  # - 0.5 * np.std(reconstruction_df_tanh['Huber']))
len(index_RP)
index_RN = np.where(reconstruction_df_tanh['Huber'] > np.median(reconstruction_df_tanh['Huber']) + 0.5 *np.std(reconstruction_df_tanh['Huber']))[0]
len(index_RN)

def check_counting(y_u,index):
    from collections import Counter
    cnt = Counter()
    for word in y_u[index,]:
        cnt[word] += 1
    return cnt

check_counting(y_u,index_RP)
check_counting(y_u, index_RN)

dico={}
index_RN_ = index_RN.copy()
i=0

X_RN = X_U[index_RN,]
km = clustering.TimeSeriesKMeans(n_clusters=3, metric="euclidean",
                                 random_state=0,n_jobs=-1).fit(X_RN)
km.cluster_centers_.shape
from collections import Counter
preds = km.predict(X_RN)
cnt = Counter()
for word in preds:
    cnt[word] += 1

dico_RN = {}
for i in set(list(preds)):
    index_ = [i for i, e in enumerate(list(preds)) if e == 1]
    rdm = random.sample(index_,int(X_P.shape[0]))
    dico_RN[i] = X_RN[rdm,]






X_RN_2 = X_U[rdm_neg,]

index_RN = index_RN[0][np.isin(index_RN,rdm_neg)[0]]

rdm_pos= random.sample(list(index_RP[0]), int(X_P.shape[0]))
X_RP = X_U[index_RP,]






x_train = np.concatenate([X_P, X_RN], axis=0)
x_train = x_train.reshape(x_train.shape[0], 25, 6)

'''
if len(list(index_RP[0]))>int(x_train.shape[0]) *2:
    rdm_U = random.sample(list(index_RP[0]), int(x_train.shape[0]) *2)
else:
    rdm_U = index_RP
'''

X_U_update = X_U[list(index_RP[0]),]
X_U_update = X_U_update.reshape(X_U_update.shape[0], 25, 6)

y_train = np.concatenate([np.zeros(X_P.shape[0]) + 1,
                          np.zeros(X_RN.shape[0])],
                         axis=0)

x_test = X_T.reshape(X_T.shape[0], 25, 6)
############################################################################################
from Supervised import models as models

classifier_teacher = models.FCGRU_Model(nunits=32, fcunits=32,
                                        dropout_rate=0.2, l2_reg=0.0)

classifier_student = models.FCGRU_Model(nunits=32, fcunits=32,
                                        dropout_rate=0.2, l2_reg=0.0)

encoder_input = tf.keras.layers.Input(shape=(25, 6))
classifier_teacher._set_inputs(encoder_input)
classifier_student._set_inputs(encoder_input)
######################################################################
classifier_teacher.build(input_shape=(64, encoder_input.shape[1], encoder_input.shape[2]))
classifier_student.build(input_shape=(64, encoder_input.shape[1], encoder_input.shape[2]))