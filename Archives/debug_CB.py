import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import os
import numpy as np #do numpy 1.19.5



gpu = True

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.8
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import Supervised.models as models
import Archives.supervised_training as supervised_training
from Autoencoder import utils as utils
from utils import plot_histogram

#################################################################################
#Path where we have the experiment
path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds'
n_sample=60
i_exp=1

##############################################################################################
X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment = path_experiment,
                                                              n_sample=n_sample,
                                                              i_exp = i_exp)

##############################################################################################

path_AE = os.path.join(path_experiment,'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')

dico_tanh, filename, model = utils.read_AE_file(path_out = path_AE,
                                         hidden_activation='tanh',
                                         embedding_activation='None',
                                         output_activation='None',
                                         name_optimizer='Adam_10e-4',
                                         name_loss='mae',
                                         prefix = 'AEGRU_V2_128')


reconstruction_df_tanh = dico_tanh['Reconstruction']
reconstruction_df_tanh['binary'] = y_u
reconstruction_df_tanh['True_class'] = Y_U[:, 0]

summary = plot_histogram(reconstruction_df_tanh,X_P.shape[0],hue='binary')

output_NN = 'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/PUL_NN'

#################################################################################################
##Option 1 : use the code as it is
name_model = 'GRU_V2_CB_' + 'debug'

pu_process = supervised_training.PUL_NN(reconstruction_df_tanh,
                                        n_pixels=500,
                                        n_iteration=10,
                                        path_experiment=path_experiment,
                                        path_folder_model=output_NN)

classifier_GRU = models.GRUClassifImproved(nunits=64, fcunits=64,  # 64, 32
                                           dropout_rate=0.5)

pu_process.initialize_output(n_sample=n_sample,
                             i_exp=i_exp,
                             name_model=name_model)

pu_process.RunClassifier_ConfirmationBias(classifier_GRU, nb_epoch=100,
                                          optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                                          loss=tf.keras.losses.CategoricalCrossentropy(),
                                          input_shape=(25, 6))


#######################################################################################################
##Option 2 : decompose the script
n_largest = reconstruction_df_tanh.nlargest(X_P.shape[0], 'Reconstruction_error')

dictionary_PUL = dict(X_RN=X_U[n_largest.index, :],
                           X_RP=X_P.copy(),
                           X_P=X_P.copy(),
                           X_U=np.delete(X_U.copy(), n_largest.index, axis=0),
                           X_T=X_T,
                           y_t=y_t,
                           ids_RN=np.array(n_largest.index),
                           ids_RP=np.array([]),
                           predictions = [],
                           metrics=dict(f1_score=[],
                                        kappa=[],
                                        accuracy=[]))


classifier = models.GRUClassifImproved(nunits=8, fcunits=8,  # 64, 32
                                           dropout_rate=0.5)
n_iteration =5
nb_epoch=0
#scores_U = (reconstruction_df_tanh['Reconstruction_error'] - np.min(reconstruction_df_tanh['Reconstruction_error'] )) / (np.max(reconstruction_df['Reconstruction_error']) - np.min(reconstruction_df['Reconstruction_error'] ))
#scores_U = 1- scores_U
n_pixels = 300
path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds'
path_folder_model = output_NN = 'Models/AE/' + 'N_' + str(60) + '_Exp_' + str(1) + '/PUL_NN'

import tensorflow as tf

import numpy as np
import os
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score
import pickle
import copy
tf.keras.backend.set_floatx('float32')


def RunClassifier_ConfirmationBias(
                                   classifier,
                                   optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                                   loss = tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                                   nb_epoch=500,
                                   input_shape = (25,6)):

    path_file = os.path.join(path_experiment, path_folder_model)
    ############################################################$##########
    encoder_input = tf.keras.layers.Input(shape=input_shape)
    classifier._set_inputs(encoder_input)
    ######################################################################
    X_T = dictionary_PUL['X_T'].copy()

    if len(encoder_input.shape) > 2:
        X_T = X_T.reshape(X_T.shape[0], encoder_input.shape[1], encoder_input.shape[2])
        classifier.build(input_shape=(32, encoder_input.shape[1], encoder_input.shape[2]))
    else:
        classifier.build(input_shape=(32, encoder_input.shape[1]))

    weights_init = classifier.get_weights()
    ######################################################################
    dictionary_PUL["ids_RP"] = []
    dictionary_PUL["ids_RN"] = []

    for i in range(n_iteration + 1):
        classifier.set_weights(weights_init)

        filepath = os.path.join(path_file,name_model)

        x_train = np.concatenate(
            [dictionary_PUL['X_RP'], dictionary_PUL['X_RN']], axis=0)

        print('x_train shape ' + str(x_train.shape[0]))
        X_U = np.concatenate([dictionary_PUL['X_U'].copy(),
                              dictionary_PUL['X_RN'].copy()], #Add reliable negative from autoencoder
                             axis = 0)

        #si on remet tout à plat : on repart du meme point initial? quel est l'interet d'ajouter plus d'obs?
        #on reprédit juste ce qu'on a pris dans le unlabeled. Si des nouvelles probas sont plus élevées, on substitut
        print('X_U shape ' + str(X_U.shape[0]))
        if len(encoder_input.shape)>2:
            x_train = x_train.reshape(x_train.shape[0],encoder_input.shape[1],encoder_input.shape[2])
            X_U = X_U.reshape(X_U.shape[0], encoder_input.shape[1], encoder_input.shape[2])

        supervised_training.fit_model(model=classifier,
                                      x_train=x_train,
                                      y_train=np.concatenate([np.zeros(dictionary_PUL['X_RP'].shape[0]) + 1,
                                                              np.zeros(dictionary_PUL['X_RN'].shape[0])],
                                                             axis=0),
                                      x_test = X_T,
                                      y_test = dictionary_PUL['y_t'],
                                      optimizer=copy.deepcopy(optimizer),
                                      loss=copy.deepcopy(loss),
                                      nb_epoch=1,
                                      filepath=filepath,
                                      filename=name_model + '_iteration_' + str(i))

        # Predict over unlabeled data
        predictions = []
        n_iterations =  supervised_training.get_iteration(X_U, 512)
        for batch in range(n_iterations):
            if batch %200==0:
                print(batch*512)
            batch_X =  supervised_training.get_batch(X_U, batch, 512)
            batch_X = tf.convert_to_tensor(batch_X, dtype=tf.float32)
            shape = [batch_X.shape[0]]
            shape.extend(list(input_shape))
            batch_X = tf.reshape(batch_X, shape)
            probas = classifier.call(batch_X.numpy(), is_training=False)
            probas = probas[:, 1]
            predictions.append(probas)

        predictions = tf.concat(predictions, axis=0).numpy()
        dictionary_PUL['predictions'].append(predictions)

        #blending = scores_U * predictions
        # Get the n biggest and lowest probabilities for RN and RP
        #probas = probas.flatten()
        index_sort = np.argsort(predictions)#blending

        # Reliable positive samples : take the same initial positives and add new unlabeled
        # some observations from X_U should be in X_RP from previous step : model identify them as well?
        index_RP = index_sort[-((n_pixels) * (i + 1)) : ]

        dictionary_PUL["ids_RP"].append([index_RP])

        new_RP = X_U[index_RP]
        new_RP = new_RP.reshape(new_RP.shape[0], -1)

        dictionary_PUL["X_RP"] = np.concatenate([dictionary_PUL["X_P"],
                                                 new_RP],
                                                axis=0)
        print(dictionary_PUL["X_RP"].shape)

        # Reliable negative samples
        index_delete = index_sort[:(dictionary_PUL["X_P"].shape[0] + n_pixels * (i+1))]

        # New reliable negatives
        new_RN = X_U[index_delete]
        new_RN = new_RN.reshape(new_RN.shape[0], -1)
        dictionary_PUL["X_RN"] = new_RN
        print('reliable negative ' + str(dictionary_PUL["X_RN"].shape))

        dictionary_PUL["ids_RN"].append([index_delete])

        #Delete reliable negative from X_U (added at the beginning of the loop)
        # Delete reliable negative from X_U (added at the beginning of the loop)
        new_XU = np.delete(X_U, index_delete, axis=0)
        new_XU = new_XU.reshape(new_XU.shape[0], -1)
        dictionary_PUL['X_U'] = new_XU
        print('unlabeled without RN ' + str(dictionary_PUL["X_U"].shape))

        # Get binary probabilistic classifier
        predictions = []
        n_iterations = supervised_training.get_iteration(X_T, 512)
        for batch in range(n_iterations):
            if batch % 200 == 0:
                print(batch * 512)
            batch_X = supervised_training.get_batch(X_T, batch, 512)
            batch_X = tf.convert_to_tensor(batch_X, dtype=tf.float32)
            shape = [batch_X.shape[0]]
            shape.extend(list(input_shape))
            batch_X = tf.reshape(batch_X, shape)
            probas = classifier.call(batch_X.numpy(), is_training=False)
            probas = np.argmax(probas, axis=1)
            predictions.append(probas)

        predictions = tf.concat(predictions, axis=0).numpy()

        dictionary_PUL['metrics']['f1_score'].append(f1_score(dictionary_PUL['y_t'],
                                                              predictions, average='weighted'))
        print(dictionary_PUL['metrics']['f1_score'])
        dictionary_PUL['metrics']['accuracy'].append(accuracy_score(dictionary_PUL['y_t'],
                                                                         predictions))

        dictionary_PUL['metrics']['kappa'].append(cohen_kappa_score(dictionary_PUL['y_t'],
                                                                         predictions))

        tf.keras.backend.clear_session()
        tf.compat.v1.reset_default_graph()

    key_to_del = ['X_T','X_U','X_P','X_RN','X_RP']
    for key in key_to_del:
        del dictionary_PUL[key]

    with open(os.path.join(os.path.join(filepath,'dictionary_results.pickle')),'wb') as d:
        pickle.dump(dictionary_PUL, d, protocol=pickle.HIGHEST_PROTOCOL)