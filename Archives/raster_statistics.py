
import os
import zipfile
import numpy as np
from osgeo import gdal
from osgeo import ogr
from osgeo import gdalconst
import geopandas as gpd
from  osgeo.gdal import *
from osgeo import gdal, ogr
import matplotlib.pyplot as plt
import pickle
import pandas as pd
import datetime
os.getcwd()

import numpy as np
import statsmodels.api as sm # recommended import according to the docs
import matplotlib.pyplot as plt
import skimage.morphology

#clip raster
#https://stackoverflow.com/questions/50035578/clip-raster-into-numpy-arrays-based-on-shapefile-or-polygons

os.chdir('/home/johann/DATA/johann/PUL/TileHG')

###############################################################################################
###############################################################################################

band_10m = pickle.load(open('./Sentinel2/bands_pickle/dictionary_' + 'B8' + '.pickle', 'rb'))
#band_20m = pickle.load(open('./FinalDBPreprocessed/SENTINEL2/dictionary_' + 'B11' + '.pickle', 'rb'))

index = 2
perc = round(band_10m['Cloud_Percent'][index] * 100,2)
plt.imshow(band_10m['data'][index,])
plt.title('B8 with ' + str(perc) + '% clouds')
plt.colorbar()
plt.show()

#Time series of clouds
df_clouds = pd.DataFrame([band_10m['dates'],band_10m['Cloud_Percent'],]).T
df_clouds.columns = ['date','CLM10']
df_clouds['date'] =df_clouds['date'].apply(lambda x : datetime.datetime.strptime(x,'%Y%m%d'))
plt.plot(df_clouds.date,df_clouds.CLM10)
plt.title('Cloud percentage R10')
plt.xticks(rotation=20)
plt.show()

#Cumulative distribution
sample = df_clouds.CLM10
ecdf = sm.distributions.ECDF(sample)

x = np.linspace(min(sample), max(sample))
y = ecdf(x)
y = y*df_clouds.shape[0]
y = y.astype(int)
plt.step(x, y)
plt.title('Number of images available per clouds percentage')
plt.show()

plt.imshow(mask_array_20)
plt.colorbar()
plt.imshow((dictionary_files[key]['BANDS']['B8'] - dictionary_files[key]['BANDS']['B4'])/(dictionary_files[key]['BANDS']['B8'] + dictionary_files[key]['BANDS']['B4']))

plt.imshow(dictionary_files[key]['BANDS']['B8'])
plt.colorbar()
plt.show()

plt.imshow(dictionary_files[key]['MASKS']['R1'])
plt.colorbar()
plt.show()


###############################################################################################
###############################################################################################
Dir = './FinalDBPreprocessed/DATABASE_SAMPLED/'


def Open_tiff_array(filename='', band=''):
    """
    Opening a tiff array.
    Keyword Arguments:
    filename -- 'C:/file/to/path/file.tif' or a gdal file (gdal.Open(filename))
        string that defines the input tiff file or gdal file
    band -- integer
        Defines the band of the tiff that must be opened.
    """
    f = gdal.Open(filename)
    if f is None:
        print('%s does not exists' %filename)
    else:
        if band == '':
            band = 1
        Data = f.GetRasterBand(band).ReadAsArray()
        Data = Data.astype(np.float32)
    return(Data)


label_code = Open_tiff_array(filename= os.path.join(Dir,'Label_Code/DATABASE_SAMPLED_Label_Code.tif'))
data = gpd.read_file('./FinalDBPreprocessed/DATABASE_SAMPLED/DATABASE_SAMPLED.shp')

data_ids = data[['Class','Class_ID','Label_Code','geometry']].drop_duplicates()
data_ids.to_csv('./FinalDBPreprocessed/DATABASE_SAMPLED/Labelling.csv',index = False)

associations = data[['Label_Code','Class']].drop_duplicates()
associations['Label_Code'] = associations['Label_Code'].astype(int)

codes = list(set(list(label_code.flatten())))
codes = codes[1:]
erosion = True

dictionary_stats = {}

for code,class_ in zip(associations.Label_Code,associations.Class):

    mask = (label_code == code).astype(int)

    disk = skimage.morphology.disk(1)
    mask = skimage.morphology.binary_erosion(mask, disk)
    stat = np.count_nonzero(mask > 0)
    name = associations.loc[associations.Label_Code.isin([code]),['Class']]
    name_str = name['Class'].unique()[0]
    dictionary_stats[name_str] =  [stat]



df = pd.DataFrame(dictionary_stats).T
df.reset_index(inplace = True)
df.columns = ['Class','Pixel count']
df['surface_pixel'] = (df['Pixel count'] * 100)*1e-6
df['surface_pixel'] = round(df['surface_pixel'],2)
df = pd.merge(df,associations[['Label_Code','Class']],on ='Class',how = 'left')

df = df[['Label_Code','Class','Pixel count','surface_pixel']]
df = df.sort_values(by=['Label_Code'])
df.reset_index(inplace = True, drop = True)
print(df.to_latex())



np.sum(df[ 'Pixel count'])