
import geopandas as gpd
import pandas as pd
import os
#####################################################
#Foret > 5000m2, taix recouvrement arbre >10% sur une surface de 20m et abre w=5m ; exception vergers
#couvert arboré
#lande > 5000m2, taix recouvrement arbre >10% sur une surface de 20m, pas de trace agricole. (ex. parcours extensifs, friches, terrains vacances, garrigues et masquis non boisé
#Attention, différentes natures peuvent aussi se retrouver dans le RPG (orchards, ...)
#Existe aussi forets : Forêt fermée de conifères, Forêt fermée de feuillus, Forêt fermée mixte, Forêt ouverte
# et Zone arborée (plus globale, essence quelconque~, rbre (monothème) | Bois (monothème) | Bosquet (monothème) | Forêt (monothème) |
# Pépinière (monothème) | Verger (monothème) | Haie (monothème) | Rangée d’arbres (monothème) | Zone
# arborée (monothème))
#vegetation = gpd.read_file("./F_VEGETATION/ZONE_VEGETATION.shp")
#- toutes les surfaces d’eau de plus de 20 m de long non souterraines,
#- tous les cours d’eau de plus de 7,5 m de large, non souterrains,
#- les zones inondables périphériques (zone périphérique d’un lac de barrage, d’un étang à niveau variable) de plus
#de 20 m de large (dans ce cas, l’attribut Persistance="Intermittent"),
#- tous les bassins à ciel ouvert de plus de 200 m²,
#- tous les bassins des stations d’épuration ou de décantation,
#- les bassins ayant totalement perdu leur fonction (anciennes zones de marais salants ou de cultures ostréicoles),
#- tous les cours d’eau temporaires (écoulement saisonnier ou occasionnel) non souterrains de plus de 7,5 m de
#large,

###################FORET#############################
#os.chdir('/home/s999379/tmp/pycharm_project_923/data/FinalDB')
os.chdir('C:/Users/s999379/OneDrive - Syngenta/Documents/Topograhpy/data/FinalDB')
bati_indifferencie = gpd.read_file("./BATI/BATI_INDIFFERENCIE/BATI_INDIFFERENCIE_32631_intersection/BATI_INDIFFERENTIEL_32631_INTERSECTION.dbf.shp")
bati_indifferencie['Class'] = 'BATI'
bati_industriel = gpd.read_file("./BATI/BATI_INDUSTRIEL/Bati_INDUSTRIEL_32363_Intersection/Bati_INDUSTRIEL_32363_Intersection.shp")
bati_industriel['Class'] = 'BATI'
cs_light = gpd.read_file("./BATI/CONSTR_LEGERE/CONSTRUCTION_LEGERE_32631_INTERSECTION/CONSTRUCTION_LEGERE_32631_INTERSECTION.shp")
cs_light['Class'] = 'BATI'

concatenated_batis = pd.concat([bati_indifferencie[['Class','geometry']],bati_industriel[['Class','geometry']],cs_light[['Class','geometry']]],axis = 0)
concatenated_batis = concatenated_batis.dissolve('Class')

concatenated_batis.to_file(driver = 'ESRI Shapefile', filename = './BATI/BATI_READY')

bati_all = gpd.read_file("./BATI/BATI_ALL.shp")

eau = gpd.read_file('./SURFACE_EAU/Surface_eau_32361_intersection/Surface_eau_32361_intersection.shp')
eau['Class'] = 'Eau'
eau = eau.dissolve('Class')

eau.to_file(driver = 'ESRI Shapefile', filename = './SURFACE_EAU/Surface_eau_dissolved')


