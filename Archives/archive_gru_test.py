
encoder_input = tf.keras.layers.Input(shape=(25,6))
classifier_GRU._set_inputs(encoder_input)
classifier_GRU.build(input_shape=(8, encoder_input.shape[1], encoder_input.shape[2]))
weights_init = classifier_GRU.get_weights()

classifier_GRU.set_weights(weights_init) # <- get and set wg
classifier_GRU.save_weights(os.path.join(path_file,name_model + '/init'))
classifier_GRU.load_weights(os.path.join(path_file,name_model + '/init'))
classifier_GRU.predict_on_batch(X_T.reshape(X_T.shape[0],25,6)).shape


'''
ex = X_T[:32,:]
ex = ex.reshape(ex.shape[0], encoder_input.shape[1], encoder_input.shape[2])
del X_T
classifier_GRU.optimizer = None
classifier_GRU.compiled_loss = None
classifier_GRU.compiled_metrics = None
atten_model_json = classifier_GRU.to_json()
classifier_GRU.save_weights(os.path.join(path_file,
                              name_model + '/init.h5'))
classifier = tf.keras.models.load_model(os.path.join(path_file,name_model + '/init.h5'))


classifier = tf.keras.models.load_model(os.path.join(path_file,name_model + '/init'))


'''
config = classifier_GRU.get_config()
custom_objects = {"CustomLayer": models.FCGRU}
with tf.keras.utils.custom_object_scope(custom_objects):
    new_model = tf.keras.Model.from_config(config)



with tf.keras.utils.custom_object_scope(custom_objects):
    new_model = tf.keras.models.clone_model(classifier_GRU)

new_model = tf.keras.Model.from_config(config)



classifier_GRU.save(os.path.join(path_file,
                              name_model + '/init'),
                 save_format='tf', overwrite=True, )

loaded_1 = tf.keras.models.load_model(
    os.path.join(path_file,name_model + '/init'))



loaded_1 = tf.keras.models.load_model(
    os.path.join(path_file,name_model + '/init'),
    custom_objects={"classifier_GRU": classifier_GRU}
)


configs = classifier_GRU.get_config
layer_FCGRU = classifier_GRU.layers[0]


classifier = tf.keras.models.load_model(os.path.join(path_file,name_model + '/init'))



new_model = tf.keras.models.load_model('model.h5', custom_objects={'FCGRU': configs})

# serialize weights to HDF5

print("Saved model to disk")

# Different part of your code or different file
# load json and create model
json_file = open('classifier_GRU.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("classifier_GRU.h5")
print("Loaded model from disk")

