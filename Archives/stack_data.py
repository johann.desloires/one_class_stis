import os
import zipfile
import numpy as np
# noinspection PyInterpreter
from osgeo import gdal
from osgeo import ogr
from osgeo import gdalconst
import geopandas as gpd
from  osgeo.gdal import *
from osgeo import gdal, ogr
import matplotlib.pyplot as plt
import pickle
import pandas as pd
import datetime
import skimage.morphology #pip install scikit-image
import shutil
#conda install gdal
#conda install rasterio
from subprocess import call
import rasterio
os.chdir('/home/johann/DATA/johann/PUL/TileHG')
#os.chdir('./data')
#path_download = '/home/s999379/theia_download'
path_download = './Sentinel2/theia_download/'


os.getcwd()

def Open_array_info(filename=''):
    """
    Opening a tiff info, for example size of array, projection and transform matrix.
    Keyword Arguments:
    filename -- 'C:/file/to/path/file.tif' or a gdal file (gdal.Open(filename))
        string that defines the input tiff file or gdal file
    """
    f = gdal.Open(r"%s" %filename)
    if f is None:
        print ('%s does not exists' %filename)
    else:
        geo_out = f.GetGeoTransform()
        proj = f.GetProjection()
        size_X = f.RasterXSize
        size_Y = f.RasterYSize
        f = None
    return(geo_out, proj, size_X, size_Y)


def Open_tiff_array(filename='', band=''):
    """
    Opening a tiff array.
    Keyword Arguments:
    filename -- 'C:/file/to/path/file.tif' or a gdal file (gdal.Open(filename))
        string that defines the input tiff file or gdal file
    band -- integer
        Defines the band of the tiff that must be opened.
    """
    f = gdal.Open(filename)
    if f is None:
        print('%s does not exists' %filename)
    else:
        if band == '':
            band = 1
        Data = f.GetRasterBand(band).ReadAsArray()
        Data = Data.astype(np.unint16)
    return(Data)


def Vector_to_Raster(Dir, shp, attribute,reference_raster_data_name):
    """
    This function creates a raster of a shp file

    Keyword arguments:
    Dir --
        str: path to the basin folder
    shapefile_name -- 'C:/....../.shp'
        str: Path from the shape file
    reference_raster_data_name -- 'C:/....../.tif'
        str: Path to an example tiff file (all arrays will be reprojected to this example)
    """



    geo, proj, size_X, size_Y=Open_array_info(reference_raster_data_name)

    x_min = geo[0]
    x_max = geo[0] + size_X * geo[1]
    y_min = geo[3] + size_Y * geo[5]
    y_max = geo[3]
    pixel_size = geo[1]

    # Filename of the raster Tiff that will be created
    Dir_Basin_Shape = os.path.join(Dir,attribute)
    if not os.path.exists(Dir_Basin_Shape):
        os.mkdir(Dir_Basin_Shape)

    Basename = os.path.basename(shp)
    Dir_Raster_end = os.path.join(Dir_Basin_Shape, os.path.splitext(Basename)[0]+ '_' + attribute + '.tif')

    # Open the data source and read in the extent
    source_ds = ogr.Open(shp)
    source_layer = source_ds.GetLayer()

    # Create the destination data source
    x_res = int(round((x_max - x_min) / pixel_size))
    y_res = int(round((y_max - y_min) / pixel_size))

    # Create tiff file
    target_ds = gdal.GetDriverByName('GTiff').Create(Dir_Raster_end, x_res, y_res, 1, gdal.GDT_UInt16, ['COMPRESS=LZW'])
    target_ds.SetGeoTransform(geo)
    srse = osr.SpatialReference()
    srse.SetWellKnownGeogCS(proj)
    target_ds.SetProjection(srse.ExportToWkt())
    band = target_ds.GetRasterBand(1)
    target_ds.GetRasterBand(1).SetNoDataValue(0)
    band.Fill(0)

    # Rasterize the shape and save it as band in tiff file
    gdal.RasterizeLayer(target_ds, [1], source_layer, None, None, [1], ['ATTRIBUTE=' +attribute])
    target_ds = None

    # Open array
    Raster_out= Open_tiff_array(Dir_Raster_end)

    return(Raster_out)




def getrandom_reference_tif(path_theia = './theia_download',band_name = 'B2') :
    path_folder = [os.path.join(path_theia,k)
                   for k in os.listdir(path_theia)
                   if ~np.any([x in k for x in ['zip','cfg','json','md','py','tmp']])]
    #only on remote machine
    #path_folder = [os.path.join(k,os.listdir(k)[0]) for k in path_folder]

    path_random_band = [os.path.join(path_folder[0],k) for k in os.listdir(path_folder[0])
                        if np.all([x in k for x in ['FRE']]) and k.split('_')[-1] == band_name + '.tif']

    return path_random_band[0]


def binary_erosion(array):
    mask = (array>0).astype(np.uint16)
    disk = skimage.morphology.disk(1)
    mask = skimage.morphology.binary_erosion(mask, disk)

    arr0 = np.ma.array(array,
                       dtype=np.int16,
                       mask=(1-mask).astype(bool),
                       fill_value=0)
    array = arr0.filled()

    return(array)
###########################################################################################


reference_raster_data_name_10 = getrandom_reference_tif(path_theia = path_download,band_name = 'B2')

mask_array_10 = Vector_to_Raster('./FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/',
                                 './FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/intersection_hg_tile.shp',
                                 'DN',
                                 reference_raster_data_name_10)

reference_raster_data_name_20 = getrandom_reference_tif(path_theia = path_download,band_name = 'B5')

mask_array_20 = Vector_to_Raster('./FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/',
                                 './FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/intersection_hg_tile.shp',
                                 'DN',
                                 reference_raster_data_name_20)


########################################################################################

Dir = './FinalDBPreprocessed/DATABASE_SAMPLED/'
shp = './FinalDBPreprocessed/DATABASE_SAMPLED/DATABASE_SAMPLED.shp'

with rasterio.open(reference_raster_data_name_10) as src0 :
    meta = src0.meta
    meta['nodata'] = 0.0

class_array = Vector_to_Raster(Dir, shp, 'Class_ID',reference_raster_data_name_10)
class_array = binary_erosion(class_array)

label_array = Vector_to_Raster(Dir, shp, 'Label_Code',reference_raster_data_name_10)
label_array = binary_erosion(label_array)


with rasterio.open(os.path.join(Dir, 'Erosion_Class_ID.tif'), 'w', **meta) as dst :
        dst.write_band(1, class_array)

with rasterio.open(os.path.join(Dir, 'Erosion_Label_Code.tif'), 'w', **meta) as dst :
    dst.write_band(1, label_array)

#/DATA/johann/PUL/TileHG/Sentinel2

###########################################################################################

folders = [k for k in os.listdir(path_download) if
           np.any([x in k for x in ['SENTINEL']]) and ~np.any([x in k for x in ['.zip','tmp']])]

dates = [k.split('_')[1].split('-')[0] for k in folders]
dates_sorted = np.argsort(dates)
folders = [folders[i] for i in dates_sorted]


bands = ['B2','B3','B4', 'B5', 'B6','B7','B8','B8A','B11','B12']
resx =  [10, 10, 10, 20, 20, 20, 10, 20, 20, 20]
mask_data_10 = (mask_array_10 > 0)
mask_data_20 = (mask_array_20 > 0)

dictionary_meta_info = {}

folders_to_remove = []

for band in bands:
    #band = 'B8'
    print(band)
    index_band = np.where(np.array(bands) == band)[0][0]
    mask_polygon = mask_data_10 if resx[index_band] == 10 else mask_data_20

    # Concatenate arrays into a dictionary
    dictionary_bands = {}
    dictionary_bands['data'] = []
    dictionary_bands['dates'] = []
    dictionary_bands['CLM'] = []
    dictionary_bands['Cloud_Percent'] = []
    dictionary_meta_info[band] = {}

    count = 0

    for folder in folders:
        count += 1
        if count % 20 == 0:
            print(count)

        ##GET THE BAND
        path_list_bands = os.path.join(path_download+'/',folder)

        list_bands = os.listdir(path_list_bands)

        image = [k for k in list_bands
                 if (np.any([x in k for x in ['FRE']]) and k.split('_')[-1].split('.')[0] in [band]) ]

        path_band = os.path.join(path_list_bands,image[0])
        array = Open_tiff_array(path_band)

        arr0 = np.ma.array(array,
                           dtype=np.float32,
                           mask=(1-mask_polygon).astype(bool),
                           fill_value=-1)

        array = arr0.filled()
        array += 1

        if (np.count_nonzero(array > 0) / np.count_nonzero(mask_polygon)) < 0.7:
            path_list_bands = os.path.join(path_download,folder)
            script = "sudo rm -rf " + path_list_bands
            call(script,shell=True)
            folders.remove(folder)
            if folder not in folders_to_remove:
                folders_to_remove.append(folders_to_remove)
            print((np.count_nonzero(array > 0) / np.count_nonzero(mask_polygon)))
            pass
        else:
            ##GET THE CLOUD
            ref = 'R1' if resx[index_band] == 10 else 'R2'
            file_cloud =  [k for k in os.listdir(os.path.join(path_list_bands,'MASKS'))  \
                           if (np.any([x in k for x in ['CLM']]) and \
                               np.any([x in k for x in [ref]]))]

            path_cloud = os.path.join(os.path.join(path_list_bands,'MASKS'), file_cloud[0])
            mask = Open_tiff_array(path_cloud)
            mask = mask>0

            arr0 = np.ma.array(array,
                               dtype=np.uint16,
                               mask=mask,
                               fill_value=0)

            array = arr0.filled()

            clp =  1 - (np.count_nonzero(array) / np.count_nonzero(mask_polygon))

            dictionary_bands['data'].append(array)
            dictionary_bands['CLM'].append(mask)
            dictionary_bands['Cloud_Percent'].append(clp)
            date = folder.split('_')[1].split('-')[0]
            dictionary_bands['dates'].append(date)

    dictionary_bands['data'] = np.stack(dictionary_bands['data'])
    dictionary_bands['CLM'] = np.stack(dictionary_bands['CLM'])
    if not os.path.exists('./Sentinel2/bands_pickle/'):
        os.makedirs('./Sentinel2/bands_pickle/')
    with open('./Sentinel2/bands_pickle/dictionary_' + band + '.pickle', 'wb') as d:
        pickle.dump(dictionary_bands, d, protocol=pickle.HIGHEST_PROTOCOL)


#if we want VIS : must do through tif images !!
