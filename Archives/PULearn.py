import pulearn
import os
import numpy as np
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import precision_recall_fscore_support, cohen_kappa_score,accuracy_score, f1_score
import pickle

from pulearn import (
    ElkanotoPuClassifier,
    # WeightedElkanotoPuClassifier,
)


origin_path = '/media/DATA/johann/PUL/TileHG/'
#origin_path = './data/'
os.chdir(origin_path)
path_experiment = './FinalDBPreprocessed/Experiments/Built'

paths_experiments = ['./FinalDBPreprocessed/Experiments/Built',
                     './FinalDBPreprocessed/Experiments/Forest',
                     './FinalDBPreprocessed/Experiments/CerealsOilseeds']


def accuracy(y_true, y_pred) :


    cnf_mat = confusion_matrix(y_true, y_pred)

    Acc = 1.0*(cnf_mat[1][1]+cnf_mat[0][0])/len(y_true)
    Sens = 1.0*cnf_mat[1][1]/(cnf_mat[1][1]+cnf_mat[1][0])
    Spec = 1.0*cnf_mat[0][0]/(cnf_mat[0][0]+cnf_mat[0][1])

    fpr, tpr, thresholds = roc_curve(y_true[:,0], y_pred[:,0])
    Auc = auc(fpr, tpr)

    Sens = recall_score(y, y_, average='macro')
    Prec = precision_score(y, y_, average='macro')
    F1 = f1_score(y, y_, average='weighted')
    Support = precision_recall_fscore_support(y, y_, beta=0.5, average=None)
    return Sens, Prec, F1, cnf_mat


nb_P = [20,40,60,80,100]
number_experiments = 10

for path_experiment in paths_experiments:
    dictionary_predictions = {}

    for i in range(1, number_experiments+1) :
        dictionary_predictions[i] = {}
        for n in nb_P :
            dictionary_predictions[i][n] = {}
            dictionary_predictions[i][n]['ypred_pu'] = []
            dictionary_predictions[i][n]['ypred_rf'] = []
            dictionary_predictions[i][n]['ytest'] = []
            dictionary_predictions[i][n]['IndexP'] = []

    for n_tree in [100,200,400]:

        for n in nb_P:
            print(n)
            case = str(n) + '_P/'
            for iteration in range(1,10 + 1):
                print(iteration)

                path_to_read = os.path.join(path_experiment,case + str(iteration))

                X_P = np.load(os.path.join(path_to_read,'X_P.npy'), mmap_mode='r')
                permut_P = np.random.permutation(X_P.shape[0])
                X_P = X_P[permut_P,:]
                Y_P = np.load(os.path.join(path_to_read,'Y_P.npy'), mmap_mode='r')
                Y_P = Y_P[permut_P, :]

                value_class = Y_P[0,0]
                ypos = np.zeros(Y_P.shape[0]) +1

                X_U = np.load(os.path.join(path_to_read,'X_U.npy'), mmap_mode='r')
                permut_U = np.random.permutation(X_U.shape[0])
                X_U = X_U[permut_U, :]
                Y_U = np.load(os.path.join(path_to_read,'Y_U.npy'), mmap_mode='r')
                Y_U = Y_U[permut_U, :]
                yneg = np.zeros(Y_U.shape[0]) - 1

                x_train = np.concatenate([X_P,X_U],axis = 0)
                labels_train = np.concatenate([Y_P,Y_U],axis = 0)
                y_train = np.concatenate([ypos,yneg],axis = 0)

                #x_train, labels_train,y_train = shuffle(x_train, labels_train,y_train, random_state=0)

                X_T = np.load(os.path.join(path_to_read,'X_T.npy'), mmap_mode='r')
                Y_T = np.load(os.path.join(path_to_read,'Y_T.npy'), mmap_mode='r')
                ytest = np.zeros(Y_T.shape[0])
                ytest[np.where(Y_T[:,0] == value_class)] += 1
                #ytest[np.where(Y_T[:,0] != value_class)] += 0


                estimator = RandomForestClassifier(
                    n_estimators=n_tree,
                    criterion='gini',
                    bootstrap=True,
                    n_jobs=-1,
                )

                pu_estimator = ElkanotoPuClassifier(estimator)
                print(pu_estimator)
                pu_estimator.fit(x_train, y_train)

                y_pred_pu = pu_estimator.predict(X_T)
                y_pred_pu[y_pred_pu<0] = 0

                precision_pu, recall_pu, f1_score_pu, _ = precision_recall_fscore_support(
                    ytest, y_pred_pu)

                #cohen_kappa_score(ytest, y_pred_pu)
                print("F1 score: {}".format(f1_score_pu[1]))
                print("Precision: {}".format(precision_pu[1]))
                print("Recall: {}".format(recall_pu[1]))

                estimator.fit(x_train, y_train)
                print(estimator)
                y_pred = estimator.predict(X_T)
                y_pred[y_pred<0] = 0

                precision, recall, f1_score, _ = precision_recall_fscore_support(
                    ytest, y_pred,labels=[1,0])

                print("F1 score: {}".format(f1_score[1]))
                print("Precision: {}".format(precision[1]))
                print("Recall: {}".format(recall[1]))

                dictionary_predictions[iteration][n]['ytest'].append(ytest)
                dictionary_predictions[iteration][n]['ypred_pu'].append(y_pred_pu)
                dictionary_predictions[iteration][n]['ypred_rf'].append(y_pred)
                dictionary_predictions[iteration][n]['IndexP'].append(np.where(Y_T[:,0] == value_class))


        with open(os.path.join(path_experiment,'dictionary_predictionsRF_' + str(n_tree) + '.pickle'), 'wb') as d :
            pickle.dump(dictionary_predictions, d, protocol=pickle.HIGHEST_PROTOCOL)


n_tree = 200
rf_200 = pickle.load(open(os.path.join(path_experiment,'dictionary_predictionsRF_' + str(n_tree) + '.pickle' ),'rb'))

dictionary_scores_200 = {}

for i in range(1, 2+1) :
    dictionary_scores_200[i] = {}
    for n in nb_P :

        ytest = rf_200[i][n]['ytest'][0]
        y_pred_pu = rf_200[i][n]['ypred_pu'][0]
        y_pred = rf_200[i][n]['ypred_rf'][0]

        precision, recall, f1_score_, _ = precision_recall_fscore_support(
            ytest, y_pred_pu,labels=[1,0])


        dictionary_scores_200[i][n] = {}
        dictionary_scores_200[i][n]['f1_score'] = f1_score(ytest, y_pred_pu,average="weighted")
        dictionary_scores_200[i][n]['precision'] = precision[0]
        dictionary_scores_200[i][n]['kappa'] = cohen_kappa_score(ytest, y_pred_pu)
        dictionary_scores_200[i][n]['accuracy'] = accuracy_score(ytest, y_pred_pu)

import pandas as pd
res = []

for i in range(1,3):
    res.append(pd.DataFrame(dictionary_scores_200[i]))

av = pd.concat(res).groupby(level=0).mean()
std = pd.concat(res).groupby(level=0).std()

def plot_score(score_name = 'f1_score'):
    index = np.where(np.array(av.index) == score_name)[0][0]
    df = pd.DataFrame({av.index[index]: list(av.iloc[index,:])})
    df.index = [20,40,60]
    df['std'] = std.iloc[index,:]
    df.reset_index(inplace=True)
    #df['index'] = df['index'].astype(str)

    x = np.array(['20','40','60'])
    y =  list(av.iloc[index,:])# Effectively y = x**2
    e = list(std.iloc[index,:])

    plt.errorbar(x, y, e, linestyle='None', marker='^')

    plt.show()

    fig, ax = plt.subplots()

    for key, group in df.groupby('index') :

        group.plot('index', score_name, yerr='std',
                   label=key, ax=ax)
    ax.set_xticklabels(df.iloc[: :freq]["datetime"].dt.strftime("%d-%b-%y"))
    # set the xticks at the same frequency as the xlabels
    xtix = ax.get_xticks()
    ax.set_xticks(xtix[: :freq])
    plt.show()

plot_score(score_name = 'kappa')


#GLM, GAM, MARS, MAXENT, BPNN, and SVM.
import statsmodels.api as sm
#gamma_model = sm.GLM(data.endog,
# .exog, family=sm.families.Gamma())
#gamma_results = gamma_model.fit()
#pip install pygam
#gam = LogisticGAM().fit(X, y)
#lambda_ = 0.6
#n_splines = [25, 6, 25, 25, 6, 4]
#constraints = None
#gam = LogisticGAM(constraints=constraints,
          #lam=lambda_,
         #n_splines=n_splines).fit(X, y)
#gam = LogisticGAM().gridsearch(X_train, y_train)

#mars https://machinelearningmastery.com/multivariate-adaptive-regression-splines-mars-in-python/
import pyearth