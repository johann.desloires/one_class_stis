import os
import numpy as np
import subprocess
import rasterio
import matplotlib.pyplot as plt

os.chdir('/home/johann/DATA/johann/PUL/TileHG')
#os.chdir('./data/')

path_root = './Sentinel2/GEOTIFFS/'
otb_path = '/home/johann/OTB-7.2.0-Linux64/bin'


tif_10_meters = []
tif_20_meters = []

for k in os.listdir(path_root):
    if np.any([x in k for x in ['GF']]):
        if np.any([x in k for x in ['B5', 'B6', 'B7', 'B8A', 'B11', 'B12']]) :
            tif_20_meters.append(k)
        elif np.any([x in k for x in ['B2', 'B3', 'B4', 'B8']]):
            tif_10_meters.append(k)


for band in ['B5', 'B6']:
    file = os.path.join(path_root, 'GFstack_' + band + '_crop.tif')
    reference = os.path.join(path_root,tif_10_meters[0])
    out = os.path.join(path_root, 'GFstack_SI_' + band + '_crop.tif')

    cmd = [os.path.join(otb_path,"otbcli_Superimpose"),
           "-inr", "%s" % reference,
           "-inm", "%s" % file,
           "-out", "%s" % out,
           "uint16",
           "-ram", "10000"]

    shell = False  # if run through terminal using python3 GapFilling.py
    # subprocess.call(cmd, shell=shell)
    # subprocess.check_call(cmd, shell=shell)
    subprocess.call(cmd, shell=shell)
    del_ = 'sudo rm ' + file
    subprocess.call(del_,shell = True)
    #os.remove(file)


#################CHECK RESULTS#######################

#Test
import rasterio
import matplotlib.pyplot as plt
raster = rasterio.open(os.path.join(path_root, 'GFstack_SI_' + 'B11' + '_crop.tif'))
raster = raster.read(20)
raster.shape
plt.imshow(raster,vmin = 100,vmax = 1000)
plt.show()
