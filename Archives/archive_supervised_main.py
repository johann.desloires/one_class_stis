import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import os
import numpy as np #do numpy 1.19.5

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import Supervised.models as models
import Archives.supervised_training as supervised_training
from Autoencoder import utils as utils
from importlib import reload
utils = reload(utils)
supervised_training = reload(supervised_training)
#################################################################################

origin_path = '/media/DATA/johann/PUL/TileHG/'
#os.chdir(origin_path)

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/Market gardenning'
#v3 : lr at 10e-5 + AE at 64 => 16
#v1 : lr at 10e-5 + AE at 32
#v4 : le at 10e-4 + AE at 32->16 + FCGRU 32 T + FCGRU 16 S & beta 0.5
#v5 : le at 10e-4 + AE at 32->16 + FCGRU 32 T + FCGRU 16 S & beta 0

def launch_experiments(reconstruction_df,
                       rsuffix='tanh',
                       nb_epoch=30,
                       alpha=0.5,
                       temperature = 1,
                       beta = 1,
                       name_model = 'GRU_V2_',
                       metric = 'Huber'):

    pu_process = supervised_training.PUL_NN(reconstruction_df,
                                            path_experiment=path_experiment,
                                            path_folder_model=output_NN)

    ##############################################################################################
    ##############################################################################################
    ##############################################################################################

    # work well with nunits = 16, ndim = 8, dropout_rate=0.15 + only 20 epochs
    name_model += rsuffix

    pu_process.initialize_output(n_sample=n_sample,
                                 i_exp=i_exp,
                                 name_model=name_model,
                                 metric = metric,
                                 beta = beta
                                 )

    #
    #=models.TempCNN(ndim = 64, n_filters=16)
    #models.TempCNN(ndim = 64, n_filters=16)

    pu_process.RunClassifier_TeacherStudent(model_AE=model,
                                            classifier_teacher = models.GRUClassifImproved(nunits=32, fcunits=32,dropout_rate=0.2, l2_reg=0), #models.MLP(ndim  = 64, dropout_rate=0.5),
                                            classifier_student= models.GRUClassifImproved(nunits=32, fcunits=32,dropout_rate=0.2, l2_reg=0),#models.MLP(ndim  = 64, dropout_rate=0.5),
                                            optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                                            loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                                            nb_epoch=nb_epoch,
                                            input_shape=(25,6),
                                            alpha=alpha,
                                            temperature = temperature)





alpha = 0.5

for alpha in [0.5, 1]:
    for n_sample in [20,40,60,80,100]: #20, 60, 80
        for i_exp in range(1,11, 2):
            path_file = 'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp)
            output_NN = os.path.join(path_file, 'PUL_NN')

            name_model = 'GRU_V2_tanh_64_64_Student_VAE_alpha_' + str(alpha)
            filepath = os.path.join(path_file, name_model)
            if not os.path.exists(os.path.join(filepath, name_model + '_teacher_CE.index')):
                ##############################################################################################
                X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment = path_experiment,
                                                                              n_sample=n_sample,
                                                                              i_exp = i_exp)

                ##############################################################################################
                #path_VAE = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')
                path_AE = os.path.join(path_experiment,
                                       os.path.join(path_file,'Reconstruction'))

                dico_tanh, filename, model = utils.read_AE_file(path_out = path_AE,
                                                         hidden_activation='tanh',
                                                         embedding_activation='None',
                                                         output_activation='None',
                                                         name_optimizer='Adam_10e-4',
                                                         name_loss='huber',
                                                         prefix = 'VAEGRU_V2_32')

                reconstruction_df_tanh = dico_tanh['Reconstruction']
                reconstruction_df_tanh['binary'] = y_u
                reconstruction_df_tanh['True_class'] = Y_U[:, 0]

                reconstruction_U = model.predict(X_U.reshape(X_U.shape[0], 25, 6), batch_size=1028)
                h = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
                res = h(X_U.reshape(X_U.shape[0], 25, 6), reconstruction_U)
                res = np.mean(res, axis=1)
                reconstruction_df_tanh['Huber'] = res

                #increase alpha : more weights on training
                launch_experiments(reconstruction_df_tanh,
                                   rsuffix = 'relu_64_alpha_' + str(alpha), #tanh_64_64_Student_AE_alpha_' + str(alpha) , #+ '_NOV', #,
                                   alpha = alpha, nb_epoch = 30, temperature = 1, name_model = 'GRU_V2',
                                   metric = 'Huber', beta = 0)
            else:
                pass




'''
for alpha in [0.5,1]:
    for n_sample in [20, 40, 60, 80, 100]:
        for i_exp in range(1,11):
            output_NN = 'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/PUL_NN'
            path_file = os.path.join(path_experiment, output_NN)
            name_model = 'GRU_V2_tanh_64_64_Student_VAE_alpha_' + str(alpha)
            filepath = os.path.join(path_file, name_model)
            if not os.path.exists(os.path.join(filepath, 'dictionary_results.pickle')):
                ##############################################################################################
                X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment = path_experiment,
                                                                              n_sample=n_sample,
                                                                              i_exp = i_exp)
                Y_P.shape
                ##############################################################################################
                path_AE = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')

                dico_tanh, filename, model = utils.read_AE_file(path_out = path_AE,
                                                         hidden_activation='tanh',
                                                         embedding_activation='None',
                                                         output_activation='None',
                                                         name_optimizer='Adam_10e-4',
                                                         name_loss='mae',
                                                         prefix = 'VAEGRU_V2_128')


                reconstruction_df_tanh = dico_tanh['Reconstruction']
                reconstruction_df_tanh['binary'] = y_u
                reconstruction_df_tanh['True_class'] = Y_U[:, 0]

                 ##################################################################################################
                output_NN = 'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/PUL_NN'

                #increase alpha : more weights on training
                launch_experiments(reconstruction_df_tanh, rsuffix = 'tanh_64_64_Student_VAE_alpha_' + str(alpha),
                                   alpha = alpha, nb_epoch = 30)
            else:
                pass


for alpha in [0.1, 0.3, 0.7, 0.9]: #
    for n_sample in [60]:
        for i_exp in range(1,11):
            output_NN = 'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/PUL_NN'
            path_file = os.path.join(path_experiment, output_NN)
            name_model = 'GRU_V2_tanh_64_64_Student_VAE_alpha_' + str(alpha)
            filepath = os.path.join(path_file, name_model)
            if not os.path.exists(os.path.join(filepath, 'dictionary_results.pickle')):
                ##############################################################################################
                X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment = path_experiment,
                                                                              n_sample=n_sample,
                                                                              i_exp = i_exp)

                ##############################################################################################
                path_AE = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')

                dico_tanh, filename, model = utils.read_AE_file(path_out = path_AE,
                                                         hidden_activation='tanh',
                                                         embedding_activation='None',
                                                         output_activation='None',
                                                         name_optimizer='Adam_10e-4',
                                                         name_loss='mae',
                                                         prefix = 'VAEGRU_V2_128')


                reconstruction_df_tanh = dico_tanh['Reconstruction']
                reconstruction_df_tanh['binary'] = y_u
                reconstruction_df_tanh['True_class'] = Y_U[:, 0]

                 ##################################################################################################
                output_NN = 'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/PUL_NN'

                #increase alpha : more weights on training
                launch_experiments(reconstruction_df_tanh, rsuffix = 'tanh_64_64_Student_VAE_alpha_' + str(alpha),
                                   alpha = alpha, nb_epoch = 30)
            else:
                pass

'''
##############################################################################################
##############################################################################################
##############################################################################################


##################################################################################
import os
import pickle

from Autoencoder import utils

path_experiments = ['/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds',
                    '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/Fodder',
                    '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/Forest',
                    '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/MeadowsUncultivated']

for path_experiment in path_experiments:
    print(path_experiment)
    for i_exp in range(1,11):
        print(i_exp)
        for n_sample in range(20,120,20):
            print(n_sample)

            path_file = 'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp)

            path_AE = os.path.join(path_experiment,
                                   os.path.join(path_file, 'Reconstruction'))

            X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                          n_sample=n_sample,
                                                                          i_exp=i_exp)

            dico_tanh, filename, model = utils.read_AE_file(path_out=path_AE,
                                                            hidden_activation='tanh',
                                                            embedding_activation='None',
                                                            output_activation='None',
                                                            name_optimizer='Adam_10e-4',
                                                            name_loss='huber',
                                                            prefix='AEGRU_V2_64')

            reconstruction_df_tanh = dico_tanh['Reconstruction']
            reconstruction_df_tanh['binary'] = y_u
            reconstruction_df_tanh['True_class'] = Y_U[:, 0]

            import tensorflow as tf
            import numpy as np

            reconstruction_U = model.predict(X_U.reshape(X_U.shape[0], 25, 6), batch_size=5012)
            h = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
            res = h(X_U.reshape(X_U.shape[0], 25, 6), reconstruction_U)
            res = np.mean(res, axis=1)
            reconstruction_df_tanh['Huber'] = res
            dico_tanh['Reconstruction'] = reconstruction_df_tanh

            with open(os.path.join(path_AE, 'AEGRU_V2_64_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_huber/reconstruction_df.pickle'), 'wb') as d:
                pickle.dump(dico_tanh, d, protocol=pickle.HIGHEST_PROTOCOL)



'''

for n_sample in [20, 40, 60, 80, 100]:
    for i_exp in range(1, 11, 1):
        for alpha in [0.1,0.5,0.7,0.9]:
            try:
                path_in = os.path.join(path_experiment,'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/PUL_NN/GRU_V2_tanh_CB_Student')
                subprocess.call(["rm", "-rf" , path_in])
            except:
                pass


path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds'

for n_sample in [20, 40, 60, 80, 100]:
    for i_exp in range(1, 11, 1):
        try:
            path_in = os.path.join(path_experiment,'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction/AEGRU_V2_64_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_mae/VAEGRU_V2_64_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_mae/reconstruction_df.pickle')
            path_NN = os.path.join(path_experiment,'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction/AEGRU_V2_64_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_mae/VAEGRU_V2_64_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_mae/NN')
            os.path.exists(path_in)
            os.path.exists(path_NN)
            path_out = os.path.join(path_experiment,'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction/AEGRU_V2_64_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_mae/')

            subprocess.call(["mv", path_in, path_out])
            subprocess.call(["mv", path_NN, path_out])

            old_path = os.path.join(path_experiment,'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction/AEGRU_V2_64_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_mae/VAEGRU_V2_64_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_mae')
            subprocess.call(["rm", "-rf", old_path])

        except:
            pass


'''

