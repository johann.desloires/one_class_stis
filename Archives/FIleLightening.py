import rasterio
import os
import numpy as np
import subprocess
import matplotlib.pyplot as plt
import gdal

path = '/media/DATA/johann/PUL/TileHG/Sentinel2'

##############################################################################################"

band_names = ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12']

band_file = [os.path.join(path,'GEOTIFFS_Archive/' + k) for k in os.listdir(os.path.join(path,'GEOTIFFS_Archive/'))
             if np.any([x in k for x in band_names])]


indices = ['NDVI','GNDVI','NDWI']

indices_file = [os.path.join(path,'GEOTIFFS_Archive/' + k) for k in os.listdir(os.path.join(path,'GEOTIFFS_Archive/'))
             if np.any([x in k for x in indices])]

target = ['Label_Code','Class_ID']

target_file = [os.path.join(path,'GEOTIFFS_Archive/' + k) for k in os.listdir(os.path.join(path,'GEOTIFFS_Archive/'))
             if np.any([x in k for x in target])]

for path_band in band_file:
    src_ds = gdal.Open(path_band)
    out_path = path_band.split('/')
    out_path[-2] = 'GEOTIFFS_New'
    out_path = '/'.join(out_path)
    if not os.path.exists('/'.join(out_path.split('/')[:-1])):
        os.makedirs('/'.join(out_path.split('/')[:-1]))
    topts = gdal.TranslateOptions(format='GTiff',
                                  outputType=gdal.GDT_UInt16,
                                  creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                                  bandList=list(range(1,src_ds.RasterCount ))) #gdal.GDT_Byte

    outds = gdal.Translate(out_path, src_ds, options=topts)


for path_file in indices_file:
    src_ds = gdal.Open(path_file)
    out_path = path_file.split('/')
    out_path[-2] = 'GEOTIFFS_New'
    out_path = '/'.join(out_path)
    if not os.path.exists('/'.join(out_path.split('/')[:-1])):
        os.makedirs('/'.join(out_path.split('/')[:-1]))
    topts = gdal.TranslateOptions(format='GTiff',
                                  outputType=gdal.GDT_Float32,
                                  BIGTIFF=YES,
                                  creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                                  bandList=list(range(1,src_ds.RasterCount ))) #gdal.GDT_Byte

    outds = gdal.Translate(out_path, src_ds, options=topts)


for path_file in target_file:
    src_ds = gdal.Open(path_file)
    out_path = path_file.split('/')
    out_path[-2] = 'GEOTIFFS_New'
    out_path = '/'.join(out_path)
    if not os.path.exists('/'.join(out_path.split('/')[:-1])):
        os.makedirs('/'.join(out_path.split('/')[:-1]))
    topts = gdal.TranslateOptions(format='GTiff',
                                  creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                                  outputType=gdal.GDT_UInt16) #gdal.GDT_Byte

    outds = gdal.Translate(out_path, src_ds, options=topts)

#################################################################################################

src_ds = gdal.Open("/media/DATA/johann/PUL/TileHG/Sentinel2/mask_R10_crop.tif")

topts = gdal.TranslateOptions(format='GTiff',
                              creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                              outputType=gdal.GDT_Byte,
                              bandList=list(range(1,src_ds.RasterCount+1))) #gdal.GDT_Byte

outds = gdal.Translate("/media/DATA/johann/PUL/TileHG/Sentinel2/mask_R10_crop_2019.tif", src_ds, options=topts)

#https://github.com/mapbox/rasterio/blob/master/rasterio/dtypes.py#L29-L45

src_ds = gdal.Open("/media/DATA/johann/PUL/TileHG/Sentinel2/mask_R10_crop_2019.tif")
myarray = np.array(src_ds.GetRasterBand(20).ReadAsArray())
plt.imshow(myarray)
plt.show()