
# Number of reliable positive or negative
n = 300
nb_iteration = 50
nb_epoch = 500
batchnorm = False
name_model = 'MLP_300_pixels_50it'
# Load experiment dataset
n_sample = 60
i_exp = 1
case = str(n_sample) + '_P/'

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds'
path_to_read = os.path.join(path_experiment, case + str(i_exp))

X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = training.load_data(
    path_experiment=path_experiment,
    n_sample=n_sample,
    i_exp=i_exp)

reconstruction_df['Class'] = Y_U[:, 0]
reconstruction_df['Object'] = Y_U[:, 1]

# Path to save the model
path_out = 'AE_PUL/MLP/RN_' + str(n_sample) + '_' + str(nb_iteration)
path_saving = os.path.join(path_to_read, path_out)

if not os.path.exists(path_saving):
    os.makedirs(path_saving)

# Positve Unlabeled Learning
n_largest = reconstruction_df.nlargest(X_P.shape[0], 'Reconstruction_error')

dictionary_RN = dict(X_RN=X_U[n_largest.index, :],
                     X_P=X_P,
                     X_U=np.delete(X_U, n_largest.index, axis=0),
                     ids_RN=np.array(n_largest.index),
                     ids_RP=np.array([]),
                     metrics=dict(f1_score=[],
                                  kappa=[],
                                  accuracy=[]))

for i in range(nb_iteration + 1):
    x_train = np.concatenate([dictionary_RN["X_P"], dictionary_RN['X_RN']], axis=0)

    classifier = models.MLP(ndim=150,
                            dropout_rate=0.15,
                            hidden_activation='tanh',
                            output_activation='sigmoid')

    supervised_training.fit_model(model=classifier,
                                  x_train=np.concatenate([dictionary_RN['X_P'], dictionary_RN['X_RN']], axis=0),
                                  y_train=np.concatenate([np.zeros(dictionary_RN['X_P'].shape[0]) + 1,
                                                          np.zeros(dictionary_RN['X_RN'].shape[0])],
                                                         axis=0),
                                  batchnorm=batchnorm,
                                  optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                                  loss=tf.keras.losses.BinaryCrossentropy(),  # from_logits=True
                                  nb_epoch=nb_epoch,
                                  filepath='/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds/Models/PUL_NN/' + name_model,
                                  filename='MLP_iteration_' + str(i) + '_n_epoch_' + str(nb_epoch))

    probas = classifier.predict(dictionary_RN['X_U'])
    probas = probas.flatten()
    index_sort = np.argsort(probas)
    dictionary_RN["ids_RN"] = np.concatenate([dictionary_RN["ids_RN"], index_sort[-n:]], axis=0)
    dictionary_RN["ids_RP"] = np.concatenate([dictionary_RN["ids_RP"], index_sort[:n]], axis=0)

    dictionary_RN["X_P"] = np.concatenate([dictionary_RN["X_P"],
                                           dictionary_RN["X_U"][index_sort[:n]]], axis=0)

    dictionary_RN["X_RN"] = np.concatenate([dictionary_RN["X_RN"],
                                            dictionary_RN["X_U"][index_sort[-n:]]], axis=0)

    index_delete = np.concatenate([index_sort[-n:], index_sort[:n]], axis=0)
    dictionary_RN["X_U"] = np.delete(dictionary_RN["X_U"], index_delete, axis=0)

    predictions = classifier.predict(X_T)
    predictions = predictions.flatten()
    predictions[predictions > 0.5] = 1
    predictions[predictions <= 0.5] = 0

    dictionary_RN['metrics']['f1_score'].append(f1_score(y_t, predictions, average='weighted'))
    dictionary_RN['metrics']['accuracy'].append(accuracy_score(y_t, predictions))
    dictionary_RN['metrics']['kappa'].append(cohen_kappa_score(y_t, predictions))

    with open(os.path.join(
            '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds/Models/PUL_NN/' + name_model,
            'dictionary_results.pickle'), 'wb') as d:
        pickle.dump(dictionary_RN, d, protocol=pickle.HIGHEST_PROTOCOL)
