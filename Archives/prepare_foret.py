import geopandas as gpd
import os
import numpy as np
###################FORET#############################
os.chdir('/home/s999379/tmp/pycharm_project_923/data/FinalDB')

bdd_foret_department = gpd.read_file("./FORET/BD_FORET_32361_INTERSECTION/BD_FORET_32631_INTERSECTION.shp")
bdd_foret_department.shape
values_class = ['Forêt fermée à mélange de feuillus',
                'Forêt fermée de chênes décidus purs',
                'Forêt fermée de feuillus purs en îlots',
                'Peupleraie',
                'Forêt fermée à mélange de conifères prépondérants et feuillus',
                'Forêt fermée d’un autre feuillu pur',
                'Forêt fermée à mélange de conifères',
                'Forêt fermée de sapin ou épicéa',
                'Forêt fermée de hêtre pur',
                'Forêt fermée à mélange de feuillus prépondérants et conifères',
                'Forêt fermée de châtaignier pur', 'Forêt fermée de robinier pur',
                'Forêt fermée de chênes sempervirents purs',
                'Forêt fermée de conifères purs en îlots',
                'Forêt fermée de pin maritime pur',
                'Forêt fermée de pin sylvestre pur', 'Forêt fermée de mélèze pur',
                'Forêt fermée de douglas pur',
                'Forêt fermée à mélange de pins purs',
                'Forêt fermée d’un autre pin pur',
                'Forêt fermée à mélange d’autres conifères',
                'Forêt fermée d’un autre conifère pur autre que pin\xa0',
                'Forêt fermée de pin laricio ou pin noir pur']

voi = list(set([k for k in bdd_foret_department.TFV if np.any([x in k for x in ['ouverte','sans couvert']])]))
bdd_foret_department = bdd_foret_department[~bdd_foret_department.TFV.isin(voi)]
bdd_foret_department['Class'] = 'Foret'

bdd_foret_department.to_file(driver = 'ESRI Shapefile', filename = './FORET/BD_FORET_READY')
