#####################################################################################################
#####################################################################################################
# must do first : download .run file on otb
# then ' chmod +x /home/s999379/OTB-7.2.0-Linux64.run'
# then /home/s999379/OTB-7.2.0-Linux64.run
# then go to the directory and do source otbenv.profile

import subprocess
import os


def shell_source(script):
    """Sometime you want to emulate the action of "source" in bash,
    settings some environment variables. Here is a way to do it."""
    pipe = subprocess.Popen(". %s; env" % script, stdout=subprocess.PIPE, shell=True)
    output = pipe.communicate()[0]
    env = dict((line.split("=", 1) for line in output.splitlines()))
    os.environ.update(env)

path = '/media/DATA/johann/PUL/TileHG/'
#path = './data/'
os.chdir(path)

script = '/home/johann/OTB-7.2.0-Linux64/otbenv.profile'
# shell_source(script)

###
path_root = './Sentinel2/GEOTIFFS/'
os.listdir(path_root)


#otb_path = '/home/johann/OTB-7.2.0-Linux64/bin'

otb_path = '/home/johann/OTB-7.2.0-Linux64/bin'


def GapFill(bands = ['B2', 'B3', 'B4', 'B8'], res = '10m'):
    for band in bands:
        file = os.path.join(path_root, 'stack_' + band + '_crop.tif')
        mask = os.path.join(path_root, 'stack_' + res + '_crop.tif')
        out = os.path.join(path_root, 'GFstack_' + band + '_crop.tif')

        cmd = [os.path.join(otb_path,"otbcli_ImageTimeSeriesGapFilling"),
               "-in", "%s" % file,
               "-mask", "%s" % mask,
               "-out", "%s" % out,
               "-comp", "1",
               "-it", "linear",
               "-ram", "20000",
               ]

        shell = False  # if run through terminal using python3 GapFilling.py
        # subprocess.call(cmd, shell=shell)
        # subprocess.check_call(cmd, shell=shell)
        subprocess.call(cmd, shell=shell)
        #os.remove(file)

GapFill(bands = ['B2','B3','B4','B8'], res = '10m') #,
GapFill(bands =['B5', 'B6', 'B7', 'B8A','B11','B12'], res='20m')

old_files = [k for k in os.listdir(path_root) if 'stack' in k.split('_')]

for file in old_files:
    del_ = 'sudo rm ' + os.path.join(path_root,file)
    subprocess.call(del_,shell = True)


#Test
import rasterio
import matplotlib.pyplot as plt
raster_B8 = rasterio.open(os.path.join(path_root, 'GFstack_' + 'B8' + '_crop.tif'))
raster_B4 = rasterio.open(os.path.join(path_root, 'GFstack_' + 'B4' + '_crop.tif'))
raster_ndvi = (raster_B8.read(20) - raster_B4.read(20)) / (raster_B8.read(20) + raster_B4.read(20))
raster_ndvi.shape
plt.imshow(raster_ndvi,vmin = 0,vmax = 1)
plt.show()