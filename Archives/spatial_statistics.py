# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 14:52:55 2020

@author: s999379
"""
import geopandas as gpd
import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns
#import split_AOI

#ctrl +w for window
gridded_hg = gpd.read_file('./data/FinalDBPreprocessed/GRIDS/grid_10km_intersection.shp')
gridded_hg.shape

#BATI
bati = gpd.read_file('./data/FinalDBPreprocessed/BATI/BATI_GRIDDED_CONCATENATE/BATI_GRIDDED_CONCATENATE.shp')
bati.crs = 'epsg:32631'
#Surface activite no bati
surf_act = gpd.read_file('./data/FinalDBPreprocessed/URBAIN/ZONE_ACTIVITE_NO_BATI/ZONE_ACTIVITE_NO_BATI.shp')

#foret
foret = gpd.read_file('./data/FinalDBPreprocessed/FORET/BD_FORET_READY/BD_FORET_SINGLE_PART.shp')

#RPG
RPG = gpd.read_file('./data/FinalDBPreprocessed/RPG_2019_HG/RPG_CATEGORY_TILE/rpg_single_part.shp')

#eau
eau = gpd.read_file('./data/FinalDBPreprocessed/SURFACE_EAU/Surface_eau_dissolved_single_part.shp')


#Shape field
gridded_hg = gpd.read_file('./FinalDB/HG_TILE_INTERSECTION/gridded_hg/gridded_hg.shp')

#Intersection
intersection = gpd.read_file('./data/FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT')
gridded_hg = gpd.overlay(gridded_hg,intersection,how = 'intersection')
gridded_hg = gridded_hg.drop_duplicates(['index','index_x','index_y'])
gridded_hg['box'] = gridded_hg['geometry'].copy()
gridded_hg = gridded_hg.rename(columns = {'index':'index_box'})
gridded_hg.index_box = gridded_hg.index_box.astype(str)
gridded_hg['surface_box'] = gridded_hg['geometry'].apply(lambda x : x.area*1e-6)

def get_statistics(data_,feat = 'Class'):
    data_['surface'] = data_['geometry'].apply(lambda x: x.area * 1e-6)
    np.sum(data_.surface)
    data_['geometry_field'] = data_['geometry']
    data =gpd.sjoin(data_,gridded_hg, how = 'left', op = 'within')
    #data = data.drop_duplicates(['ID_PARCEL','surface'])
    np.sum(data.surface)
    #data['index_box'] = data.index_box.astype(int).astype(str)

    cols_merge = [feat]
    cols_merge.extend(['surface','index_box'])
    counting = data[cols_merge].groupby('index_box').sum('surface')
    counting.reset_index(inplace = True)
    cols_output = ['index']
    cols_output.append(feat)
    counting.columns = cols_output
    counting = counting.rename(columns = {'index':'index_box'})

    counting = pd.merge(counting,gridded_hg[['index_box','geometry','surface_box']],
                        on = 'index_box',how = 'right')
    counting = counting.drop_duplicates('index_box')
    #counting[counting[feat] >= 0]
    counting = counting[counting[feat]>=0]
    counting = gpd.GeoDataFrame(counting,geometry = counting.geometry)
    return(counting)

#bati : 86.36 km2
#surface = 108
#rpg : 1900
#eau : 21.64
#foret 350
stats_spatial = get_statistics(eau,'Class')
np.sum(stats_spatial.Class)
np.sum(stats_spatial.surface_box)

sns.displot(stats_spatial,x='Class',binwidth=1)
plt.show()


lb, ub = np.quantile(stats_spatial.Class, 0.05), np.quantile(stats_spatial.Class, 0.95)

fig, ax = plt.subplots(figsize = (20,16))
stats_spatial.plot('Class',vmin = lb, vmax = ub,ax=ax,legend = True)
stats_spatial.geometry.boundary.plot(color=None,edgecolor='k',linewidth = 2,ax=ax)
plt.show()

