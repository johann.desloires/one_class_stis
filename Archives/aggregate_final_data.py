# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 10:49:08 2020

@author: s999379
"""

import geopandas as gpd
import pandas as pd
#import split_AOI
from shapely.geometry import *
#import seaborn as sns
import numpy as np
import shapely.wkt
import os

hg = gpd.read_file('C:/Users/s999379/OneDrive - Syngenta/Documents/Topograhpy/data/departements-20180101/departements-20180101.shp')
hg = hg[hg.code_insee == '31']
gridded_hg = split_AOI.get_gridded_boundary(hg,5000)
gridded_hg = gridded_hg[gridded_hg.check]
gridded_hg = gridded_hg[['index', 'index_x', 'index_y', 'geometry']]
gridded_hg['cell_geometry'] = gridded_hg['geometry']

bati = gpd.read_file('./FinalDB/BATI/BATI_GRIDDED_CONCATENATE/BATI_GRIDDED_CONCATENATE_Buffered_5_single_part.shp')
bati.crs = 'epsg:32631'
bati['Class'] = 'Built'
bati['surface'] = bati['geometry'].apply(lambda x: x.area * 1e-6)
bati['surface'] = bati['geometry'].apply(lambda x: np.log(x.area * 1e-6))
sns.displot(bati,x='surface',binwidth=1)
bati.shape

rpg = gpd.read_file('./FinalDB/RPG_2019_HG/RPG_CATEGORY_TILE/rpg_category_tile_buffered.shp')
rpg.crs = 'epsg:32631'
rpg = rpg[rpg.Class.isin(['Cereals & Oilseeds', 'Uncultivated', 'Vegetables', 'Feed',
                         'Orchards'])]
rpg.shape
rpg.loc[rpg.Class.isin(['Uncultivated']),'Class'] = 'Meadows/Uncultivated'
rpg.loc[rpg.Class.isin(['Cereals & Oilseeds']),'Class'] = 'Cereals/Oilseeds'
rpg = rpg[~rpg.geometry.isin([None,""])]
rpg['surface'] = rpg['geometry'].apply(lambda x: (x.area * 1e-6))
sns.displot(rpg,x='surface',binwidth=5)


#foret
foret = gpd.read_file('./FinalDBPreprocessed/FORET/BD_FORET_READY/BD_FORET_SINGLE_PART.shp')
foret['Class'] = 'Forest'
foret['surface'] = foret['geometry'].apply(lambda x: (x.area * 1e-6))
sns.displot(foret,x='surface',binwidth=1)



#eau
eau = gpd.read_file('./FinalDBPreprocessed/SURFACE_EAU/Surface_eau_dissolved_single_part.shp')
eau['Class'] = 'Water'
eau['surface'] = eau['geometry'].apply(lambda x: np.log(x.area * 1e-6))
sns.displot(eau,x='surface',binwidth=1)
plt.show()



data_all = pd.concat([bati[['Class','geometry']],
                      rpg[['Class','geometry']],
                      foret[['Class','geometry']],
                      eau[['Class','geometry']]],axis = 0)

data_all = gpd.GeoDataFrame(data_all,geometry = data_all.geometry)

data_all.Class.unique()
data_all.shape
data_all_grid = gpd.sjoin(data_all,gridded_hg,how = 'left',op = 'within')
data_all_grid.shape

def remove_third_dimension(geom):

    if geom is None:
        return(geom)
    if geom.is_empty:
        return geom


    if isinstance(geom, Polygon):
        exterior = geom.exterior
        new_exterior = remove_third_dimension(exterior)

        interiors = geom.interiors
        new_interiors = []
        for int in interiors:
            new_interiors.append(remove_third_dimension(int))

        return Polygon(new_exterior, new_interiors)

    elif isinstance(geom, LinearRing):
        return LinearRing([xy[0:2] for xy in list(geom.coords)])

    elif isinstance(geom, LineString):
        return LineString([xy[0:2] for xy in list(geom.coords)])

    elif isinstance(geom, Point):
        return Point([xy[0:2] for xy in list(geom.coords)])

    elif isinstance(geom, MultiPoint):
        points = list(geom.geoms)
        new_points = []
        for point in points:
            new_points.append(remove_third_dimension(point))

        return MultiPoint(new_points)

    elif isinstance(geom, MultiLineString):
        lines = list(geom.geoms)
        new_lines = []
        for line in lines:
            new_lines.append(remove_third_dimension(line))

        return MultiLineString(new_lines)

    elif isinstance(geom, MultiPolygon):
        pols = list(geom.geoms)

        new_pols = []
        for pol in pols:
            new_pols.append(remove_third_dimension(pol))

        return MultiPolygon(new_pols)

    elif isinstance(geom, GeometryCollection):
        geoms = list(geom.geoms)

        new_geoms = []
        for geom in geoms:
            new_geoms.append(remove_third_dimension(geom))

        return GeometryCollection(new_geoms)

    else:
        raise RuntimeError("Currently this type of geometry is not supported: {}".format(type(geom)))

data_all_grid['geometry'] = data_all_grid['geometry'].apply(lambda x : remove_third_dimension(x))
data_all_grid = gpd.GeoDataFrame(data_all_grid,geometry = data_all_grid.geometry)
data_all_grid = data_all_grid.drop(['cell_geometry','index_right'],axis =1)

data_all_grid.to_file(driver = 'ESRI Shapefile', filename = './FinalDBPreprocessed/DATABASE_READY.shp')

#Split data according to
data_all_grid = gpd.read_file(filename = './FinalDBPreprocessed/DATABASE_READY/DATABASE_READY.shp')
data_all_grid = data_all_grid[~data_all_grid.geometry.isin([None,""])]
data_all_grid['surface'] =  data_all_grid['geometry'].apply(lambda x: x.area * 1e-6)
count = data_all_grid[['Class','geometry']].groupby('Class').count()
count.reset_index(inplace = True)
surface = data_all_grid[['Class','surface']].groupby('Class').sum()
surface.reset_index(inplace = True)
all = pd.merge(count,surface, how = 'left',on = 'Class')
summary = pd.DataFrame(['Total',np.sum(all.geometry),np.sum(all.surface)]).T
summary.columns = all.columns
all = pd.concat([all,summary],axis = 0)
print(all.to_latex(index=False))


# entre 1000 et 2000 par Class < 50 m2
# echantilloner => chaque class entre 1000 et 2000
# 2 raster : un de la class, un de l'ID
#gdalward pour couper + burn pour rasteriser

###Eau : split les polygons selon une grille de 1000m
gridded_water = split_AOI.get_gridded_boundary(hg,1000)
gridded_water = gridded_water[gridded_water.check]
gridded_water['geometry_cell'] = gridded_water['geometry']
gridded_water = gridded_water.drop(['geometry_cell'],axis =1)
gridded_water = gpd.GeoDataFrame(gridded_water,geometry = gridded_water.results)
gridded_water = gridded_water.drop(['results'],axis =1)
gridded_water.crs = eau_split.crs
gridded_water.to_file(driver = 'ESRI Shapefile', filename = './FinalDBPreprocessed/grid_1km.shp')


#####################################################################################
###SAMPLING
#####################################################################################
##Split large polygons and keep only surface > 250 m2
import os
os.chdir('/home/johann/DATA/johann/PUL/TileHG')

data_all_grid = gpd.read_file(filename = './FinalDBPreprocessed/DATABASE_READY/DATABASE_READY.shp')
data_all_grid = data_all_grid[data_all_grid['geometry'].is_valid]

mask = gpd.read_file('./FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/intersection_hg_tile.shp')



#data_all_grid = data_all_grid[~data_all_grid.geometry.isin([None,""])] not need as we apply filter over the surface
data_all_grid.columns = ['Class', 'index_5', 'index_x_5', 'index_y_5', 'geometry']
data_all_grid.Class.unique()

gridd_1km = gpd.read_file('./FinalDBPreprocessed/GRIDS/grid_1km.shp')
gridd_1km['surface'] = gridd_1km['geometry'].apply(lambda x: x.area)
#6356820090.812935
gridd_1km = gpd.overlay(gridd_1km,mask)

##
eau = data_all_grid[data_all_grid.Class == 'Water']

eau_split = gpd.overlay(eau,gridd_1km, 'intersection')
eau_split['surface'] = eau_split['geometry'].apply(lambda x: x.area)
eau_split.shape
eau_split = eau_split[eau_split.surface > 250]

##
Forest = data_all_grid[data_all_grid.Class == 'Forest']
Forest_split = gpd.overlay(Forest,gridd_1km, 'intersection')

Forest_split['surface'] = Forest_split['geometry'].apply(lambda x: x.area)
Forest_split = Forest_split[Forest_split.surface > 250]
Forest_split.shape

##
bati = data_all_grid[data_all_grid.Class == 'Built']
bati['surface'] = bati['geometry'].apply(lambda x: x.area)
bati = bati[bati['surface']>250]
bati.shape

#REMAIN

remain = data_all_grid.loc[~data_all_grid.Class.isin(['Built','Water','Forest']),:]
remain = remain[remain['geometry'].is_valid]
remain['surface'] = remain['geometry'].apply(lambda x: x.area)
remain = remain[remain['surface']>250]

####
#Champ id
champ_id = {}
for index,value in zip(range(len(data_all_grid.Class.unique())),data_all_grid.Class.unique()):
    champ_id[value] = [str(index +1)]

champ_id = pd.DataFrame.from_dict(champ_id).T
champ_id.reset_index(inplace = True)
champ_id.columns = ['Class','Label_Code']

cols_to_keep = ['Class', 'surface','index_5', 'index_x_5', 'index_y_5', 'geometry']

data_all = pd.concat([bati[cols_to_keep],
                      Forest_split[cols_to_keep],
                      eau_split[cols_to_keep],
                      remain[cols_to_keep]], axis = 0)

data_all.columns = ['Class', 'surface','index', 'index_x', 'index_y', 'geometry']

data_all = pd.merge(data_all,champ_id,on = 'Class', how = 'left')

data_all.loc[data_all.Class == 'Feed',['Class']] = 'Fodder'
data_all.loc[data_all.Class == 'Vegetables',['Class']] = 'Market Gardening'

data_all.to_file(driver = 'ESRI Shapefile', filename = './FinalDBPreprocessed/DATABASE_GEOM_SPLIT')
data_all.shape

########################################################################################################
########################################################################################################
########################################################################################################


data_all = gpd.read_file('./FinalDBPreprocessed/DATABASE_GEOM_SPLIT/DATABASE_GEOM_SPLIT.shp')
data_all.shape
count_all = data_all[['Class', 'index']].groupby('Class','index').count()
count_all.reset_index(inplace = True)

data_all['Count'] = 1
pivot = pd.pivot_table(data_all[['Class','index','Count']],index = 'index',columns = ['Class'],aggfunc='count')
pivot = pd.DataFrame(pivot)
pivot.columns = [k[1] for k in pivot.columns]
pivot.reset_index(inplace = True)
pivot['index'] = pivot['index'].astype(int)
data_all.shape
data_all.Class.unique()

res = []

for index_grid in pivot['index']:
    for Class in list(data_all.Class.unique()):
        n = pivot.loc[pivot['index'] == index_grid,[Class]].iloc[0][Class]
        N = np.sum(pivot[Class])
        if Class == 'Built':
            alpha = 0.025
        elif Class in ['Feed','Vegetables','Water']:
            alpha = 0.15
        elif Class == 'Orchards':
            alpha = 0.2
        elif Class in ['Cereals/Oilseeds','Meadows/Uncultivated','Forest']:
            alpha = 0.05
        else:
            alpha = 0.1

        theoretical_number = N / len(pivot['index'].unique()) * alpha
        if data_all.loc[(data_all['index'] == index_grid) & (data_all.Class == Class)].shape[0] > theoretical_number:
            sample = data_all.loc[(data_all['index'] == index_grid) & (data_all.Class == Class)].sample(int(theoretical_number+1))
        else:
            sample = data_all.loc[(data_all['index'] == index_grid) & (data_all.Class == Class)]

        res.append(sample)

data_reduce = pd.concat(res,axis = 0)
data_reduce.shape

data_reduce.loc[data_reduce.Class == 'Feed',['Class']] = 'Fodder'
data_reduce.loc[data_reduce.Class == 'Vegetables',['Class']] = 'Market Gardening'
#data_reduce['Label_Code'] = data_reduce['Class_ID'].copy()

data_reduce['Class_ID'] = range(1,data_reduce.shape[0]+1)

data_reduce = data_reduce.drop(['Count'],axis = 1)

data_reduce.to_file(driver = 'ESRI Shapefile', filename = './FinalDBPreprocessed/DATABASE_SAMPLED')
data_reduce.shape

labelling = data_reduce[['Class','Class_ID','Label_Code','geometry']].drop_duplicates(['Class','Class_ID','Label_Code'])
data_reduce.to_csv('./FinalDBPreprocessed/DATABASE_SAMPLED/Labelling.csv',index = False)


np.sum(data_reduce[data_reduce.surface>0].surface)/100
np.sum(data_reduce.surface)/100
data_reduce['Count'] = 1
pd.pivot_table(data_reduce[['Class','index','Count']],index = 'index',columns = ['Class'],aggfunc='count')



count = [['Class', 'index']].groupby('Class','index').count()
count.reset_index(inplace = True)
print(count.to_latex(index=False))

surf = data_reduce[['Class', 'surface']].groupby('Class').sum('surface')
surf.reset_index(inplace = True)
surf['surface'] = surf['surface'] * 1e-6
print(count.to_latex(index=False))

total = pd.merge(count,surf,on = 'Class',how = 'left')
total['surface'] = round(total['surface'],2)

total = pd.merge(total,associations, on = 'Class',how = 'left')
total = total.sort_values(by=['Label_Code'])
total = total[['Class','index','surface']]

print(total.to_latex(index=False))

#Intersection
intersection = gpd.read_file('./data/FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT')
gridded_hg = gpd.overlay(gridded_hg,intersection,how = 'intersection')
gridded_hg = gridded_hg.drop_duplicates(['index','index_x','index_y'])
gridded_hg['box'] = gridded_hg['geometry'].copy()
gridded_hg = gridded_hg.rename(columns = {'index':'index_box'})
gridded_hg.index_box = gridded_hg.index_box.astype(str)
gridded_hg['surface_box'] = gridded_hg['geometry'].apply(lambda x : x.area*1e-6)

def get_statistics(data_,feat = 'Class'):
    data_['surface'] = data_['geometry'].apply(lambda x: x.area * 1e-6)

    data_['geometry_field'] = data_['geometry']
    data =gpd.sjoin(data_,gridded_hg, how = 'left', op = 'within')
    #data = data.drop_duplicates(['ID_PARCEL','surface'])
    np.sum(data.surface)
    #data['index_box'] = data.index_box.astype(int).astype(str)

    cols_merge = [feat]
    cols_merge.extend(['surface','index_box'])
    counting = data[cols_merge].groupby('index_box').sum('surface')
    counting.reset_index(inplace = True)
    cols_output = ['index']
    cols_output.append(feat)
    counting.columns = cols_output
    counting = counting.rename(columns = {'index':'index_box'})

    counting = pd.merge(counting,gridded_hg[['index_box','geometry','surface_box']],
                        on = 'index_box',how = 'right')
    counting = counting.drop_duplicates('index_box')
    #counting[counting[feat] >= 0]
    counting = counting[counting[feat]>=0]
    counting = gpd.GeoDataFrame(counting,geometry = counting.geometry)
    return(counting)

stats_spatial = get_statistics(eau,'Class')
np.sum(stats_spatial.Class)
np.sum(stats_spatial.surface_box)

sns.displot(stats_spatial,x='Class',binwidth=1)
plt.show()

lb, ub = np.quantile(stats_spatial.Class, 0.05), np.quantile(stats_spatial.Class, 0.95)

fig, ax = plt.subplots(figsize = (20,16))
stats_spatial.plot('Class',vmin = lb, vmax = ub,ax=ax,legend = True)
stats_spatial.geometry.boundary.plot(color=None,edgecolor='k',linewidth = 2,ax=ax)
plt.show()



########################################################################################################
########################################################################################################
########################################################################################################
