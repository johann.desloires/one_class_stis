##
import os
import numpy as np
import rasterio
import gdal
import matplotlib.pyplot as plt
import pickle
path = '/media/DATA/johann/PUL/TileHG/'
#path = './data/'
os.chdir(path)

path_images = './Sentinel2/GEOTIFFS/'
path_theia = './Sentinel2/theia_download/'


def Open_array_info(filename=''):
    """
    Opening a tiff info, for example size of array, projection and transform matrix.
    Keyword Arguments:
    filename -- 'C:/file/to/path/file.tif' or a gdal file (gdal.Open(filename))
        string that defines the input tiff file or gdal file
    """
    f = gdal.Open(r"%s" % filename)
    if f is None:
        print('%s does not exists' % filename)
    else:
        geo_out = f.GetGeoTransform()
        proj = f.GetProjection()
        size_X = f.RasterXSize
        size_Y = f.RasterYSize
        f = None
    return (geo_out, proj, size_X, size_Y)



file_bands = []
band_names = ['B2', 'B4','B8','B11']

for band in band_names:
    for k in os.listdir(path_images) :
        if band in k.split('_'):
            file_bands.append(os.path.join(path_images, k))

def get_dictionary(band_names):
    dictionary_bands = {}
    for index_band, band_name in enumerate(band_names) :
        print(band_name)

        band = rasterio.open(file_bands[index_band])
        print(band.count)
        array_time = []
        count = 0
        for array_index in range(1, band.count) :
            count += 1
            if count%5 == 0:
                print(count)
            band_read = band.read(array_index)
            # plt.imshow(band_read, vmin=100, vmax=1000)
            # plt.show()
            array_time.append(band_read)

        array_time = np.stack(array_time,axis = 0)

        dictionary_bands[band_name] = array_time
        del array_time

    return dictionary_bands
    #with open('./Sentinel2/vis.pickle','wb') as d:  pickle.dump(dictionary_bands,d,protocol=pickle.HIGHEST_PROTOCOL)

dictionary_bands = get_dictionary(band_names)

#dictionary_bands = pickle.load(open('./Sentinel2/vis.pickle','rb'))


geo, proj, size_X, size_Y = Open_array_info(os.path.join(path_images, 'GFstack_' + 'B2' + '_crop.tif'))
# filter array

##################################

# Read metadata of first file
with rasterio.open(os.path.join(path_images, 'GFstack_' + 'B2' + '_crop.tif')) as src0 :
    meta = src0.meta
    meta['nodata'] = np.nan
    meta['dtype'] = 'float32'

times = dictionary_bands['B2'].shape[0]
meta.update(count=times+1)
meta.update(nodata=np.nan)


def compute_VIs():

    def write_tif(variable):
        with rasterio.open(os.path.join(path_images, 'GFstack_' + variable + '_crop.tif'), 'w', **meta) as dst:
            for id in range(times):
                dst.write_band(id + 1, dictionary_bands[variable][id, :, :].astype(np.float32))
        del dictionary_bands[variable]

    # Vegetation indices

    dictionary_bands['NDVI'] = (dictionary_bands['B8']-dictionary_bands['B4'])/(
                dictionary_bands['B8']+dictionary_bands['B4'])
    del dictionary_bands['B4']

    write_tif('NDVI')

    dictionary_bands['GNDVI'] = (dictionary_bands['B8']-dictionary_bands['B2'])/(
                dictionary_bands['B8']+dictionary_bands['B2'])
    write_tif('GNDVI')
    del dictionary_bands['B2']


    dictionary_bands['NDWI'] = (dictionary_bands['B8']-dictionary_bands['B11'])/(
                dictionary_bands['B8']+dictionary_bands['B11'])
    del dictionary_bands['B8']
    del dictionary_bands['B11']
    write_tif('NDWI')


compute_VIs()


#Aggregate everything into a single array
array_bands = []
for key in band_names:
    array_bands.append(dictionary_bands[key])
    del dictionary_bands[key]

array_bands = [k.astype(np.int16) for k in array_bands]
array_bands = np.stack(array_bands,axis = -1)
array_bands.shape
#array_bands_EC = np.sqrt(np.sum((array_bands+1**2), axis=-1))



#with rasterio.open(os.path.join(path_images, 'stack_'+'ECNorm.tif'), 'w', **meta) as dst :    for id in range(times) :
        #dst.write_band(id+1, array_bands[id, :, :].astype(np.float32))

#del array_bands


