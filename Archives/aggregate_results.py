import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import tensorflow as tf
import pandas as pd
import pickle5 as pickle
import utils
import numpy as np
path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/MeadowsUncultivated'
name_model= 'GRU_V2_tanh_64_64_Student_VAE_alpha_1' #_VAE

metric_all = []

n_sample = 80
i_exp = 1

DF_pos_all = []
DF_neg_all = []

1836251

for n_sample in range(20,120,20):#:
    for i_exp in range(1,11):
        try:
            output_NN = 'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/PUL_NN'
            path_NN = os.path.join(path_experiment, output_NN)

            X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                          n_sample=n_sample,
                                                                          i_exp=i_exp)

            #name_model='MLP_1000pix_10it'

            #model = tf.keras.models.load_model('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds/Models/AE/N_60_Exp_1/PUL_NN/MLPtest')
            dictionary_RN = pickle.load(open(
                os.path.join(path_NN,name_model + '/dictionary_results.pickle'),
                'rb'))

            metric = pd.DataFrame(dictionary_RN['metrics'])
            metric['Sample_size'] = n_sample
            metric['i_exp'] = i_exp
            metric_all.append(metric)
        except:
            pass

'''
for n_sample in range(20, 100, 20):
    for i_exp in range(1, 11):

        X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                      n_sample=n_sample,
                                                                      i_exp=i_exp)

        path_AE = os.path.join(path_experiment,
                               'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')

        dico_tanh, filename, model = utils.read_AE_file(path_out=path_AE,
                                                        hidden_activation='tanh',
                                                        embedding_activation='None',
                                                        output_activation='None',
                                                        name_optimizer='Adam_10e-4',
                                                        name_loss='mae',
                                                        prefix='VAEGRU_V2_128')

        reconstruction_df_tanh = dico_tanh['Reconstruction']
        reconstruction_df_tanh['binary'] = y_u
        reconstruction_df_tanh['True_class'] = Y_U[:, 0]

        DF_neg, DF_pos = utils.plot_histogram(reconstruction_df_tanh,
                                          X_P.shape[0], 0.01,
                                          hue='binary')  # + int(0.01 * X_U.shape[0]

        DF_neg['Sample_size'] = n_sample
        DF_neg['i_exp'] = i_exp
        DF_neg_all.append(DF_neg)

        DF_pos['Sample_size'] = n_sample
        DF_pos['i_exp'] = i_exp
        DF_pos_all.append(DF_pos)
'''


metrics = pd.concat(metric_all, axis = 0)
#DFS_pos = pd.concat(DF_pos_all, axis = 0)
#DFS_neg = pd.concat(DF_neg_all, axis = 0)


metrics_agg = metrics.groupby('Sample_size').agg('mean')
metrics_std = metrics.groupby('Sample_size').agg(np.nanstd)

metrics_all = metrics_agg.copy()
metrics_all = metrics_all.drop('i_exp', axis = 1)

for metric in metrics_all.columns:
    mean = [str((round(k, 3))) for k in metrics_agg[metric]]
    se = ['(' + str(round(k, 3)) + ')' for k in metrics_std[metric]]
    res = [i + ' ' + j for i, j in zip(mean, se)]
    metrics_all[metric] = res

print(metrics_all.to_latex())

#####################################################################################
alphas = [0.1, 0.3, 0.5, 0.7, 0.9, 1]

metric_all = []
for alpha in alphas:
    for i_exp in range(1,11):
        name_model = 'GRU_V2_tanh_64_64_Student_VAE_alpha_' + str(alpha)
        output_NN = 'Models/VAE/' + 'N_' + str(60) + '_Exp_' + str(i_exp) + '/PUL_NN'
        path_NN = os.path.join(path_experiment, output_NN)

        X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                      n_sample=60,
                                                                      i_exp=i_exp)

        # name_model='MLP_1000pix_10it'

        # model = tf.keras.models.load_model('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds/Models/AE/N_60_Exp_1/PUL_NN/MLPtest')
        dictionary_RN = pickle.load(open(
            os.path.join(path_NN, name_model + '/dictionary_results.pickle'),
            'rb'))

        metric = pd.DataFrame(dictionary_RN['metrics'])
        metric['alpha'] = alpha
        metric['i_exp'] = i_exp
        metric_all.append(metric)

metrics_alpha = pd.concat(metric_all)
metrics_alpha_agg = metrics_alpha.groupby('alpha').agg('mean')
metrics_alpha_std = metrics_alpha.groupby('alpha').agg(np.nanstd)

metrics_all = metrics_alpha_agg.copy()

for metric in metrics_alpha_agg.columns:
    mean = [str((round(k, 3))) for k in metrics_alpha_agg[metric]]
    se = ['(' + str(round(k, 3)) + ')' for k in metrics_alpha_std[metric]]
    res = [i + ' ' + j for i, j in zip(mean, se)]
    metrics_all[metric] = res

print(metrics_all.to_latex())

#####################################################################################
DFS_pos = DFS_pos.groupby(["Description", "Sample_size"]).agg('mean')
DFS_pos = DFS_pos.drop(['i_exp', 'Share_U'],axis = 1)
DFS_pos = DFS_pos.rename(columns = {'True_class' : 'RP_pixels',
                                    'Share_RN' : 'Share_RP'})

DFS_pos = DFS_pos.round(3)

DFS_pos_se =  pd.concat(DF_pos_all, axis = 0).groupby(["Description", "Sample_size"]).agg('std')
DFS_pos_se = DFS_pos_se.drop(['i_exp', 'Share_U'],axis = 1)
DFS_pos_se.columns = DFS_pos.columns
DFS_neg = DFS_neg.round(3)

for indicator in ['Share_RP']:
    mean = [str((round(k, 3))) for k in DFS_pos[indicator]]
    se = ['(' + str(round(k, 3)) + ')' for k in DFS_pos_se[indicator]]
    res = [i + ' ' + j for i, j in zip(mean, se)]
    DFS_pos[indicator] = res



DFS_neg= pd.concat(DF_neg_all, axis = 0).groupby(["Description", "Sample_size"]).agg('mean')
DFS_neg = DFS_neg.drop('i_exp',axis = 1)
DFS_neg = DFS_neg.rename(columns = {'True_class' : 'RN_pixels'})
DFS_neg = DFS_neg.round(3)

DFS_neg_se =  pd.concat(DF_neg_all, axis = 0).groupby(["Description", "Sample_size"]).agg('std')
DFS_neg_se = DFS_neg_se.drop(['i_exp'],axis = 1)
DFS_neg_se.columns = DFS_neg.columns
DFS_neg_se = DFS_neg_se.round(3)

for indicator in ['Share_RN']:
    mean = [str((round(k, 3))) for k in DFS_neg[indicator]]
    se = ['(' + str(round(k, 3)) + ')' for k in DFS_neg_se[indicator]]
    res = [i + ' ' + j for i, j in zip(mean, se)]
    DFS_neg[indicator] = res
    
    
all_results = pd.merge(DFS_pos,DFS_neg,left_index = True,right_index=True)
print(all_results.to_latex())