import zipfile
import os
import numpy as np
import subprocess

path_images = '/media/DATA/johann/PUL/TileHG/Sentinel2/theia_download/'
#path_images = '/home/s999379/theia_download/'
os.getcwd()
os.chdir(path_images)
os.listdir(path_images)
path_to_del = [""] #[os.path.join('./',k) for k in os.listdir(path_images) if k.split('_')[0] in ['SENTINEL2A','SENTINEL2B']]

os.getcwd()
def download_data(path_images,tile,start,end):
    os.chdir(path_images)
    cmd = ['python3', './theia_download.py',
           "-t", "%s" % tile,
           "-c", "%s" % "SENTINEL2",
           "-a", "%s" % "config_theia.cfg",
           "-d", "%s" % start,
           "-f", "%s" % end]

    shell = False  #
    subprocess.call(cmd, shell=shell)

#SENTINEL2A_20190903-105858-769_L2A_T31TCJ_D.zip
#SENTINEL2A_20191013-105901-621_L2A_T31TCJ_D.zip
#SENTINEL2B_20191227-105853-805_L2A_T31TCJ_D.zip

date = ["2017-12-01","2017-12-31"]

paths = os.listdir(path_images)
paths.sort()

download_data(path_images,tile = "T31TCJ",start = date[0],end = date[1])


def unzip_data(path_images):
    files = os.listdir(path_images)
    zip_file = [k for k in files if np.any([x in k for x in ['.zip']])]

    for zipf in zip_file:
        try:
            print(zipf)
            path = os.path.join(path_images,zipf)
            directory_to_extract_to = path_images#os.path.join(path_images,zipf.split('.')[0])
            with zipfile.ZipFile(path, 'r') as zip_ref:
                zip_ref.extractall(directory_to_extract_to)
            os.remove(path)
        except:
            print('CANNOT UNZIP')
            pass

import os
import numpy as np
unzip_data(path_images)
os.chdir('/home/johann/DATA/johann/PUL/TileHG')
#os.chdir('./data')
#path_download = '/home/s999379/theia_download'
path_download = './Sentinel2/theia_download/'
import shutil

folders = [k for k in os.listdir(path_download) if
           np.any([x in k for x in ['SENTINEL']]) and ~np.any([x in k for x in ['.zip','tmp']])]


for folder in folders:
    path = os.path.join(path_download,folder)
    if len(os.listdir(path)) <5:
        shutil.rmtree(path)
        script = "sudo rm -rf " + path
        call(script, shell=True)
        folders.remove(folder)
    else:
        pass

