import time
import tensorflow as tf

from tensorflow import keras as keras
import numpy as np
import os
from sklearn.utils import shuffle
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score
import random
import utils as utils
import pickle
import keras
import tensorflow.python.keras.backend as K
import copy
tf.keras.backend.set_floatx('float32')
import matplotlib.pyplot as plt
from joblib import dump, load

def get_iteration(array, batch_size):
    '''
    Function to get the number of iterations over one epoch w.r.t batch size
    '''
    n_batch = int(array.shape[0] / batch_size)
    if array.shape[0] % batch_size != 0:
        n_batch += 1
    return n_batch


def get_batch(array, i, batch_size):
    '''
    Function to select batch of training/validation/test set
    '''
    start_id = i * batch_size
    end_id = min((i + 1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch


def app_grad():
    @tf.function
    def training_epoch(train_ds,
                       model,
                       optimizer=tf.keras.optimizers.Adam(learning_rate=10e-3),
                       loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False)):  # do f-measure

        tot_loss = 0.0
        iterations = 0.0

        #https://www.tensorflow.org/guide/effective_tf2
        for step, (x_batch_train, y_batch_train) in enumerate(train_ds):
            with tf.GradientTape() as tape:
                y_pred = model(x_batch_train,
                               training=True)
                cost = loss(y_batch_train, y_pred) #loss_reduced

            grads = tape.gradient(cost, model.trainable_variables)
            optimizer.apply_gradients(zip(grads, model.trainable_variables))

            tot_loss = tf.add(tot_loss,cost)
            iterations = tf.add(iterations,1.0)

        return tf.divide(tot_loss,iterations)

    return training_epoch


def fit_model(model, x_train, y_train, x_test, y_test,
              optimizer, loss,
              nb_epoch, filepath, filename):
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    y_train = tf.keras.utils.to_categorical(y_train, num_classes=2)
    train_loss_list = []
    count = 0

    for e in range(nb_epoch):

        x_train, y_train = shuffle(x_train, y_train)

        train_ds = tf.data.Dataset.from_tensor_slices((x_train, y_train))
        train_ds = train_ds.batch(128)
        apply_grads = app_grad()
        train_loss = apply_grads(train_ds, model, optimizer, loss) #model,

        #os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
        #y_pred_test = model.predict_on_batch(x_test)
        '''
        y_pred_test = model.predict(x_test, batch_size = 32)
        y_pred_test = np.argmax(y_pred_test, axis = 1)
        '''

        print(
            "Epoch {0}: Training loss {1}".format(
                str(e),
                str(round(train_loss.numpy(), 6)))) #     np.argmax(y_pred_test,axis=1)
        #print("\t F1-Score on the test set %f"% f1_score(y_test, y_test, average="weighted" ))

    #model.save_weights(os.path.join(filepath, filename + '.h5'), overwrite=True)
        if not filepath is None:
            if not os.path.exists(filepath):
                os.makedirs(filepath)
        if count >0:
            #model.save(os.path.join(filepath, filename), save_format='tf', overwrite=True)
            if train_loss < np.min(train_loss_list):
                model.save_weights(os.path.join(filepath, filename))

        train_loss_list.append(train_loss)

        count += 1
        #model.save_weights(os.path.join(os.path.join(filepath, filename), '/weights.h5'), overwrite=True)



def app_grad_student():
    @tf.function
    def training_epoch(train_ds,
                       classifier_teacher,
                       classifier_student,
                       alpha = 0.5,
                       optimizer=tf.keras.optimizers.Adam(learning_rate=10e-3),
                       loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                       temperature = 1):  # do f-measure

        tot_loss = 0.0
        iterations = 0.0

        #https://www.tensorflow.org/guide/effective_tf2
        for step, (x_batch_train_T, x_batch_train_S, y_batch_train) in enumerate(train_ds):
            print(step)
            teacher_predictions = classifier_teacher(x_batch_train_T,
                                                     training=False)
            with tf.GradientTape() as tape:

                student_predictions = classifier_student(x_batch_train_S,
                                                         training=True)
                #how to give the information to the teacher next?
                kl = tf.keras.losses.KLDivergence()

                cost =  alpha * loss(y_batch_train, student_predictions) \
                         + (1-alpha) * kl(tf.nn.softmax(teacher_predictions / temperature, axis=1),
                                          tf.nn.softmax(student_predictions / temperature, axis=1))

            grads = tape.gradient(cost, classifier_student.trainable_variables)
            optimizer.apply_gradients(zip(grads, classifier_student.trainable_variables))

            tot_loss = tf.add(tot_loss,cost)
            iterations = tf.add(iterations,1.0)

        return tf.divide(tot_loss,iterations)
    return training_epoch


def fit_model_student(classifier_teacher,
                      classifier_student,
                      x_train_T, x_train_S, y_train,
                      x_test, y_test,
                      alpha,
                      optimizer, loss,
                      nb_epoch, filepath, filename,
                      temperature = 1):

    y_train = tf.keras.utils.to_categorical(y_train, num_classes=2)

    count = 0
    train_loss_list = []
    train_loss_teacher = []
    for e in range(nb_epoch):

        x_train_T, x_train_S, y_train = shuffle(x_train_T, x_train_S, y_train)
        #Teacher
        train_ds = tf.data.Dataset.from_tensor_slices((x_train_T, y_train))
        train_ds = train_ds.batch(64)
        apply_grads = app_grad()
        train_loss = apply_grads(train_ds, classifier_teacher, optimizer, loss)
        #Student
        train_ds = tf.data.Dataset.from_tensor_slices((x_train_T, x_train_S, y_train))
        train_ds = train_ds.batch(64)
        apply_grad_student = app_grad_student()
        train_loss_student = apply_grad_student(train_ds,
                                                classifier_teacher, classifier_student,
                                                alpha,
                                                optimizer, loss,
                                                temperature)
        #then we should update loss from teacher ?
        y_pred_test = classifier_student.predict(x_test,batch_size = 64)
        y_pred_test = np.argmax(y_pred_test, axis = 1)

        print(
            "Epoch {0}: Training loss {1}".format(
                str(e),
                str(round(train_loss_student.numpy(), 6)))) #     np.argmax(y_pred_test,axis=1)
        print("\t F1-Score on the test set %f"% f1_score(y_test, y_pred_test, average="weighted" ))

        if not filepath is None:
            if not os.path.exists(filepath):
                os.makedirs(filepath)

        if count > 0:
            if train_loss_student < np.min(train_loss_list):
                classifier_student.save_weights(os.path.join(filepath, filename + '_student'))
                classifier_teacher.save_weights(os.path.join(filepath, filename + '_teacher'))
            if train_loss < np.min(train_loss_teacher):
                classifier_teacher.save_weights(os.path.join(filepath, filename + '_teacher_CE'))

        train_loss_list.append(train_loss_student)
        train_loss_teacher.append(train_loss)
        count += 1

############################################################################################################
class PUL_NN:
    def __init__(self, 
                 reconstruction_df,
                 path_experiment='/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds',
                 path_folder_model = 'Models/PUL_NN/'):

        self.reconstruction_df = reconstruction_df
        self.path_experiment = path_experiment
        self.path_folder_model = path_folder_model
        self.name_model = ''

    @staticmethod
    def reset_seeds():
        np.random.seed(2)
        random.seed(2)
        if tf.__version__[0] == '2':
            tf.random.set_seed(2)
        else:
            tf.set_random_seed(2)
        print("RANDOM SEEDS RESET")

    def initialize_output(self, n_sample, i_exp,
                          name_model='test', beta = 1,
                          metric = 'Reconstruction_error'):

        X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(
            path_experiment=self.path_experiment,
            n_sample=n_sample,
            i_exp=i_exp)

        self.reconstruction_df['Class'] = Y_U[:, 0]
        self.reconstruction_df['Object'] = Y_U[:, 1]

        #Case 2 : add Reliable Positive and Reliable Negative
        threshold = np.mean(
            self.reconstruction_df[metric]) + beta * np.std(
            self.reconstruction_df[metric])

        subset = self.reconstruction_df[metric][
            self.reconstruction_df[metric] > threshold]

        if subset.shape[0] > X_P.shape[0]:
            sample_size = self.reconstruction_df.nlargest(subset.shape[0], metric)
        else:
            sample_size = self.reconstruction_df.nlargest(X_P.shape[0], metric)

        rdm_class = random.sample(list(sample_size.index), int(X_P.shape[0]))
        index_delete = rdm_class.copy()
        X_RP = X_P.copy()

        self.dictionary_PUL = dict(X_RN=X_U[rdm_class, :],
                                   X_RP=X_RP,
                                   X_P=X_P.copy(),
                                   X_U=np.delete(X_U.copy(), index_delete, axis=0),
                                   X_T=X_T,
                                   y_t=y_t,
                                   ids_RN=np.array(rdm_class),
                                   ids_RP=np.array([]),
                                   predictions_U = [],
                                   metrics=dict(f1_score=[],
                                                kappa=[],
                                                accuracy=[]))

        self.name_model = name_model

        scores_U = self.reconstruction_df[metric]
        scores_U = np.delete(np.array(scores_U), index_delete)
        scores_U = np.round(scores_U, 5)

        scores_U = scores_U/np.sum(scores_U)
        print(scores_U)
        print(np.sum(scores_U))

        self.dictionary_PUL['scores_U'] = scores_U

    @staticmethod
    def get_iteration(array, batch_size):
        '''
        Function to get the number of iterations over one epoch w.r.t batch size
        '''
        n_batch = int(array.shape[0] / batch_size)
        if array.shape[0] % batch_size != 0:
            n_batch += 1
        return n_batch

    @staticmethod
    def get_batch(array, i, batch_size):
        '''
        Function to select batch of training/validation/test set
        '''
        start_id = i * batch_size
        end_id = min((i + 1) * batch_size, array.shape[0])
        batch = array[start_id:end_id]
        return batch

    def RunClassifier_TeacherStudent(self,
                                     model_AE,
                                     classifier_teacher,
                                     classifier_student,
                                     optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                                     loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                                     alpha = 0.5,
                                     nb_epoch=20,
                                     input_shape=(25, 6),
                                     temperature = 1):

        path_file = os.path.join(self.path_experiment, self.path_folder_model)
        ############################################################$##########
        encoder_input = tf.keras.layers.Input(shape=input_shape)
        classifier_teacher._set_inputs(encoder_input)
        classifier_student._set_inputs(encoder_input)
        ######################################################################
        X_T = self.dictionary_PUL['X_T'].copy()
        X_T = X_T.reshape(X_T.shape[0], encoder_input.shape[1], encoder_input.shape[2])

        classifier_teacher.build(input_shape=(64, encoder_input.shape[1], encoder_input.shape[2]))
        classifier_student.build(input_shape=(64, encoder_input.shape[1], encoder_input.shape[2]))

        weights_init_teacher = classifier_teacher.get_weights()
        weights_init_student = classifier_student.get_weights()
        ######################################################################
        self.dictionary_PUL["ids_RP"] = []
        self.dictionary_PUL["ids_RN"] = []

        classifier_teacher.set_weights(weights_init_teacher)
        classifier_student.set_weights(weights_init_student)

        filepath = os.path.join(path_file, self.name_model)

        x_train = np.concatenate(
            [self.dictionary_PUL['X_RP'], self.dictionary_PUL['X_RN']], axis=0)

        x_train = x_train.reshape(x_train.shape[0], encoder_input.shape[1], encoder_input.shape[2])
        x_train_AE = model_AE.predict(tf.convert_to_tensor(x_train.astype('float32')),batch_size=32)

        fit_model_student(classifier_teacher = classifier_teacher,
                          classifier_student = classifier_student,
                          x_train_T =x_train_AE,
                          x_train_S =x_train,
                          y_train=np.concatenate([np.zeros(self.dictionary_PUL['X_RP'].shape[0]) + 1,
                                                  np.zeros(self.dictionary_PUL['X_RN'].shape[0])],
                                                 axis=0),
                          x_test=X_T,
                          y_test=self.dictionary_PUL['y_t'],
                          alpha = alpha,
                          optimizer=copy.deepcopy(optimizer),
                          loss=copy.deepcopy(loss),
                          nb_epoch=nb_epoch ,
                          filepath=filepath,
                          filename=self.name_model,
                          temperature = temperature)

        # Get binary probabilistic classifier
        predictions = classifier_student.predict(X_T, batch_size = 64)
        predictions = np.argmax(predictions, axis = 1)

        self.dictionary_PUL['metrics']['f1_score'].append(f1_score(self.dictionary_PUL['y_t'],
                                                                   predictions, average='weighted'))
        print(self.dictionary_PUL['metrics']['f1_score'])

        self.dictionary_PUL['metrics']['accuracy'].append(accuracy_score(self.dictionary_PUL['y_t'],
                                                                    predictions))

        self.dictionary_PUL['metrics']['kappa'].append(cohen_kappa_score(self.dictionary_PUL['y_t'],
                                                                    predictions))

        key_to_del = ['X_T', 'X_U', 'X_P', 'X_RN', 'X_RP']
        for key in key_to_del:
            del self.dictionary_PUL[key]

        with open(os.path.join(os.path.join(filepath, 'dictionary_results.pickle')), 'wb') as d:
            pickle.dump(self.dictionary_PUL, d, protocol=pickle.HIGHEST_PROTOCOL)


    def RunClassifier_Competitors(self,
                                  model):

        path_file = os.path.join(self.path_experiment, self.path_folder_model)
        ######################################################################
        X_T = self.dictionary_PUL['X_T'].copy()

        ######################################################################
        self.dictionary_PUL["ids_RP"] = []
        self.dictionary_PUL["ids_RN"] = []

        filepath = os.path.join(path_file, self.name_model)

        x_train = np.concatenate(
            [self.dictionary_PUL['X_RP'], self.dictionary_PUL['X_RN']], axis=0)

        # Get binary probabilistic classifier
        model.fit(x_train, np.concatenate([np.zeros(self.dictionary_PUL['X_RP'].shape[0]) + 1,
                                           np.zeros(self.dictionary_PUL['X_RN'].shape[0])],
                                          axis=0))
        predictions = model.predict(X_T)
        predictions[predictions>0.5] = 1
        predictions[predictions <= 0.5] = 0

        self.dictionary_PUL['metrics']['f1_score'].append(f1_score(self.dictionary_PUL['y_t'],
                                                                   predictions, average='weighted'))
        print(self.dictionary_PUL['metrics']['f1_score'])

        self.dictionary_PUL['metrics']['accuracy'].append(accuracy_score(self.dictionary_PUL['y_t'],
                                                                    predictions))

        self.dictionary_PUL['metrics']['kappa'].append(cohen_kappa_score(self.dictionary_PUL['y_t'],
                                                                         predictions))

        key_to_del = ['X_T', 'X_U', 'X_P', 'X_RN', 'X_RP']
        for key in key_to_del:
            del self.dictionary_PUL[key]

        if not os.path.exists(filepath):
            os.makedirs(filepath)

        with open(os.path.join(filepath, 'dictionary_results.pickle'), 'wb') as d:
            pickle.dump(self.dictionary_PUL, d, protocol=pickle.HIGHEST_PROTOCOL)

        dump(model, os.path.join(os.path.join(filepath, 'model.joblib')))


    def RunClassifierIteratively(self,
                                 model_AE,
                                 classifier_teacher,
                                 classifier_student,
                                 optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                                 loss = tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                                 nb_epoch=500,
                                 input_shape = (25,6),
                                 n_iteration=5,
                                 n_pixels = 300,
                                 alpha = 0.5):

        path_file = os.path.join(self.path_experiment, self.path_folder_model)
        ######################################################################
        encoder_input = tf.keras.layers.Input(shape=input_shape)
        classifier_teacher._set_inputs(encoder_input)
        classifier_student._set_inputs(encoder_input)
        ######################################################################
        X_T = self.dictionary_PUL['X_T'].copy()

        classifier_teacher.build(input_shape=(64, encoder_input.shape[1], encoder_input.shape[2]))
        classifier_student.build(input_shape=(64, encoder_input.shape[1], encoder_input.shape[2]))

        weights_init_teacher = classifier_teacher.get_weights()
        weights_init_student = classifier_student.get_weights()
        ######################################################################
        self.dictionary_PUL["ids_RP"] = []
        self.dictionary_PUL["ids_RN"] = []

        ######################################################################

        for i in range(n_iteration + 1):
            self.reset_seeds()

            classifier_teacher.set_weights(weights_init_teacher)
            classifier_student.set_weights(weights_init_student)

            filepath = os.path.join(path_file,self.name_model)

            x_train = np.concatenate(
                [self.dictionary_PUL['X_RP'], self.dictionary_PUL['X_RN']], axis=0)

            X_U = self.dictionary_PUL['X_U'].copy()

            if len(encoder_input.shape)>2:
                x_train = x_train.reshape(x_train.shape[0],encoder_input.shape[1],encoder_input.shape[2])
                X_U = X_U.reshape(X_U.shape[0], encoder_input.shape[1], encoder_input.shape[2])
                X_T = X_T.reshape(X_T.shape[0], encoder_input.shape[1], encoder_input.shape[2])

            x_train_AE = model_AE.predict(tf.convert_to_tensor(x_train.astype('float32')), batch_size=32)

            fit_model_student(classifier_teacher=classifier_teacher,
                              classifier_student=classifier_student,
                              x_train_T=x_train_AE,
                              x_train_S=x_train,
                              y_train=np.concatenate([np.zeros(self.dictionary_PUL['X_RP'].shape[0]) + 1,
                                                      np.zeros(self.dictionary_PUL['X_RN'].shape[0])],
                                                     axis=0),
                              x_test=X_T,
                              y_test=self.dictionary_PUL['y_t'],
                              alpha=alpha,
                              optimizer=copy.deepcopy(optimizer),
                              loss=copy.deepcopy(loss),
                              nb_epoch=nb_epoch + int((i -1)*2),
                              filepath=filepath,
                              filename=self.name_model,
                              temperature=1)

            #os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
            # Predict over unlabeled data
            predictions = classifier_student.predict(X_U,batch_size = 128)
            predictions = predictions[:,1]
            predictions_P = np.random.choice(a = list(range(predictions.shape[0])),
                                             size = int(i * 0.1 * X_U.shape[0]))

            predictions_N = np.random.choice(a = list(range(predictions.shape[0])),
                                             size = int(i * 0.1 * X_U.shape[0]))

            ids = np.concatenate([predictions_P, predictions_N])
            predictions = predictions[ids]
            # Get the n biggest and loest probabilities for RN and RP
            index_sort = np.argsort(predictions)#probas
            index_sort = ids[index_sort]
            # Reliable negative
            self.dictionary_PUL["ids_RN"] = np.concatenate([self.dictionary_PUL["ids_RN"],
                                                            index_sort[:n_pixels]],
                                                           axis=0)
            # Reliable negative samples
            self.dictionary_PUL["X_RN"] = np.concatenate([self.dictionary_PUL["X_RN"],
                                                          self.dictionary_PUL["X_U"][index_sort[:n_pixels]]],
                                                         axis=0)

            # Reliable positive
            self.dictionary_PUL["ids_RP"] = np.concatenate([self.dictionary_PUL["ids_RP"],
                                                            index_sort[-n_pixels:]],
                                                           axis=0)
            # Reliable positive samples
            self.dictionary_PUL["X_RP"] = np.concatenate([self.dictionary_PUL["X_RP"],
                                                         self.dictionary_PUL["X_U"][index_sort[-n_pixels:]]],
                                                        axis=0)

            # Remove reliable positive and negatives from Unlabeled data
            #index_delete = np.concatenate([index_sort[-n_pixels:], index_sort[:n_pixels]], axis=0)
            self.dictionary_PUL["X_U"] = np.delete(self.dictionary_PUL["X_U"], ids, axis=0)
            self.dictionary_PUL["scores_U"] = np.delete(self.dictionary_PUL["scores_U"], ids)
            self.dictionary_PUL["scores_U"] = np.round(self.dictionary_PUL["scores_U"], 5)
            self.dictionary_PUL["scores_U"] = self.dictionary_PUL["scores_U"] / np.sum(self.dictionary_PUL["scores_U"])


            # Get binary probabilistic classifier
            predictions = classifier_student.predict(X_T, batch_size = 64)
            predictions = np.argmax(predictions, axis = 1)

            self.dictionary_PUL['metrics']['f1_score'].append(f1_score(self.dictionary_PUL['y_t'],
                                                                       predictions, average='weighted'))
            print(self.dictionary_PUL['metrics']['f1_score'])
            self.dictionary_PUL['metrics']['accuracy'].append(accuracy_score(self.dictionary_PUL['y_t'],
                                                                             predictions))

            self.dictionary_PUL['metrics']['kappa'].append(cohen_kappa_score(self.dictionary_PUL['y_t'],
                                                                             predictions))

            tf.keras.backend.clear_session()
            tf.compat.v1.reset_default_graph()

            with open(os.path.join(os.path.join(filepath, 'dictionary_results_' + str(i) + '.pickle')), 'wb') as d:
                pickle.dump(self.dictionary_PUL, d, protocol=pickle.HIGHEST_PROTOCOL)

        key_to_del = ['X_T','X_U','X_P','X_RN']
        for key in key_to_del:
            del self.dictionary_PUL[key]

        with open(os.path.join(os.path.join(filepath,'dictionary_results.pickle')),'wb') as d:
            pickle.dump(self.dictionary_PUL, d, protocol=pickle.HIGHEST_PROTOCOL)

#####################################################################################################################
#####################################################################################################################
#####################################################################################################################

'''



'''
'''
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import tensorflow as tf
from importlib import reload
import Supervised.models as models
utils = reload(utils)

name_model = 'test'
path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds'
path_folder_model = output_NN = 'Models/AE/' + 'N_' + str(60) + '_Exp_' + str(1) + '/PUL_NN'

X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(
    path_experiment=path_experiment,
    n_sample=60,
    i_exp=1)


path_AE = os.path.join(path_experiment,'Models/AE/' + 'N_' + str(60) + '_Exp_' + str(1) + '/Reconstruction')

dico_tanh, filename, model = utils.read_AE_file(path_out = path_AE,
                                         hidden_activation='tanh',
                                         embedding_activation='None',
                                         output_activation='None',
                                         name_optimizer='Adam_10e-4',
                                         name_loss='mae',
                                         prefix = 'AEGRU_V2_128')

#https://arxiv.org/pdf/1703.01780.pdf
reconstruction_df = dico_tanh['Reconstruction']
n_largest = reconstruction_df.nlargest(X_P.shape[0], 'Reconstruction_error')

######################################################################
encoder_input = tf.keras.layers.Input(shape=[25,6])

X_P = X_P.reshape(X_P.shape[0], encoder_input.shape[1], encoder_input.shape[2])
X_U = X_U.reshape(X_U.shape[0], encoder_input.shape[1], encoder_input.shape[2])
X_T = X_T.reshape(X_T.shape[0], encoder_input.shape[1], encoder_input.shape[2])

X_P_AE = model.predict(X_P, batch_size=32)
X_U_AE = model.call(X_U,False)
X_RN=X_U[n_largest.index, :, :]
X_RN_AE = X_U_AE.numpy()[n_largest.index, ]
X_T_AE = model.call(X_T,False)

######################
classifier_teacher = models.GRUClassifImproved(nunits = 6, fcunits = 6,
                                                 dropout_rate=0.5)
classifier_teacher._set_inputs(encoder_input)
classifier_teacher.build(input_shape=(32, encoder_input.shape[1], encoder_input.shape[2]))

classifier_student = models.GRUClassifImproved(nunits = 6, fcunits = 6,
                                               dropout_rate=0.5)
classifier_student._set_inputs(encoder_input)
classifier_student.build(input_shape=(32, encoder_input.shape[1], encoder_input.shape[2]))


#################

x_train_T = np.concatenate(
    [X_P, X_RN], axis=0)
x_train_S = model.call(tf.convert_to_tensor(x_train_T.astype('float32')),False)


x_train_S = np.concatenate(
    [X_P_AE, X_RN_AE], axis=0)
#https://github.com/chengshengchan/model_compression/blob/master/teacher-student.py
y_train = np.concatenate([np.zeros(X_P.shape[0]) + 1,
                          np.zeros(X_RN.shape[0])],
                         axis=0)

x_train_T, x_train_S, y_train = shuffle(x_train_T, x_train_S, y_train)
train_ds = tf.data.Dataset.from_tensor_slices((x_train_S, x_train_T, y_train))


'''


'''

reconstruction_df = dico_tanh['Reconstruction']
reconstruction_df['Class'] = Y_U[:, 0]
reconstruction_df['Object'] = Y_U[:, 1]

n_largest = reconstruction_df.nlargest(X_P.shape[0], 'Reconstruction_error')

dictionary_PUL = dict(X_RN=X_U[n_largest.index, :],
                           X_RP=X_P.copy(),
                           X_P=X_P.copy(),
                           X_U=np.delete(X_U.copy(), n_largest.index, axis=0),
                           X_T=X_T,
                           y_t=y_t,
                           ids_RN=np.array(n_largest.index),
                           ids_RP=np.array([]),
                           predictions = [],
                           metrics=dict(f1_score=[],
                                        kappa=[],
                                        accuracy=[]))


import Supervised.models as models
classifier = models.GRUClassifImproved(nunits=4, fcunits=4,  # 64, 32
                                           dropout_rate=0.5)
n_iteration =5
nb_epoch=0
scores_U = (reconstruction_df['Reconstruction_error'] - np.min(reconstruction_df['Reconstruction_error'] )) / (np.max(reconstruction_df['Reconstruction_error']) - np.min(reconstruction_df['Reconstruction_error'] ))
scores_U = 1- scores_U


def RunClassifier_ConfirmationBias(
                                   classifier,
                                   optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                                   loss = tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                                   nb_epoch=500,
                                   input_shape = (25,6)):

    path_file = os.path.join(path_experiment, path_folder_model)
    ############################################################$##########
    encoder_input = tf.keras.layers.Input(shape=input_shape)
    classifier._set_inputs(encoder_input)
    ######################################################################
    X_T = dictionary_PUL['X_T'].copy()

    if len(encoder_input.shape) > 2:
        X_T = X_T.reshape(X_T.shape[0], encoder_input.shape[1], encoder_input.shape[2])
        classifier.build(input_shape=(32, encoder_input.shape[1], encoder_input.shape[2]))
    else:
        classifier.build(input_shape=(32, encoder_input.shape[1]))

    weights_init = classifier.get_weights()
    ######################################################################
    dictionary_PUL["ids_RP"] = []
    dictionary_PUL["ids_RN"] = []

    for i in range(n_iteration + 1):
        classifier.set_weights(weights_init)

        filepath = os.path.join(path_file,name_model)

        x_train = np.concatenate(
            [dictionary_PUL['X_RP'], dictionary_PUL['X_RN']], axis=0)

        print('x_train shape ' + str(x_train.shape[0]))
        X_U = np.concatenate([dictionary_PUL['X_U'].copy(),
                              dictionary_PUL['X_RN'].copy()], #Add reliable negative from autoencoder
                             axis = 0)

        #si on remet tout à plat : on repart du meme point initial? quel est l'interet d'ajouter plus d'obs?
        #on reprédit juste ce qu'on a pris dans le unlabeled. Si des nouvelles probas sont plus élevées, on substitut
        print('X_U shape ' + str(X_U.shape[0]))
        if len(encoder_input.shape)>2:
            x_train = x_train.reshape(x_train.shape[0],encoder_input.shape[1],encoder_input.shape[2])
            X_U = X_U.reshape(X_U.shape[0], encoder_input.shape[1], encoder_input.shape[2])

        fit_model(model=classifier,
                  x_train=x_train,
                  y_train=np.concatenate([np.zeros(dictionary_PUL['X_RP'].shape[0]) + 1,
                                          np.zeros(dictionary_PUL['X_RN'].shape[0])],
                                         axis=0),
                  x_test = X_T,
                  y_test = dictionary_PUL['y_t'],
                  optimizer=copy.deepcopy(optimizer),
                  loss=copy.deepcopy(loss),
                  nb_epoch=1,
                  filepath=filepath,
                  filename=name_model + '_iteration_' + str(i))

        # Predict over unlabeled data
        predictions = []
        n_iterations = get_iteration(X_U, 512)
        for batch in range(n_iterations):
            if batch %200==0:
                print(batch*512)
            batch_X = get_batch(X_U, batch, 512)
            batch_X = tf.convert_to_tensor(batch_X, dtype=tf.float32)
            shape = [batch_X.shape[0]]
            shape.extend(list(input_shape))
            batch_X = tf.reshape(batch_X, shape)
            probas = classifier.call(batch_X.numpy(), is_training=False)
            probas = probas[:, 1]
            predictions.append(probas)

        predictions = tf.concat(predictions, axis=0).numpy()
        dictionary_PUL['predictions'].append(predictions)


        #blending = scores_U * predictions
        # Get the n biggest and lowest probabilities for RN and RP
        #probas = probas.flatten()
        index_sort = np.argsort(predictions)#blending

        # Reliable positive samples : take the same initial positives and add new unlabeled
        # some observations from X_U should be in X_RP from previous step : model identify them as well?
        index_RP = index_sort[-((n_pixels) * (i + 1)) : ]


        dictionary_PUL["ids_RP"].append([index_RP])

        new_RP = X_U[index_RP]
        new_RP = new_RP.reshape(new_RP.shape[0], -1)
        dictionary_PUL["X_RP"] = np.concatenate([dictionary_PUL["X_P"],
                                                 new_RP],
                                                axis=0)
        print(dictionary_PUL["X_RP"].shape)

        # Reliable negative samples
        index_delete = index_sort[:(dictionary_PUL["X_P"].shape[0] + n_pixels * (i+1))]

        # New reliable negatives
        new_RN = X_U[index_delete]
        new_RN = new_RN.reshape(new_RN.shape[0], -1)
        dictionary_PUL["X_RN"] = new_RN
        print('reliable negative ' + str(dictionary_PUL["X_RN"].shape))

        dictionary_PUL["ids_RN"].append([index_delete])

        #Delete reliable negative from X_U (added at the beginning of the loop)
        # Delete reliable negative from X_U (added at the beginning of the loop)
        new_XU = np.delete(X_U, index_delete, axis=0)
        new_XU = new_XU.reshape(new_XU.shape[0], -1)
        dictionary_PUL['X_U'] = new_XU
        print('unlabeled without RN ' + str(dictionary_PUL["X_U"].shape))

        # Get binary probabilistic classifier
        predictions = []
        n_iterations = get_iteration(X_T, 512)
        for batch in range(n_iterations):
            if batch % 200 == 0:
                print(batch * 512)
            batch_X = get_batch(X_T, batch, 512)
            batch_X = tf.convert_to_tensor(batch_X, dtype=tf.float32)
            shape = [batch_X.shape[0]]
            shape.extend(list(input_shape))
            batch_X = tf.reshape(batch_X, shape)
            probas = classifier.call(batch_X.numpy(), is_training=False)
            probas = np.argmax(probas, axis=1)
            predictions.append(probas)

        predictions = tf.concat(predictions, axis=0).numpy()

        dictionary_PUL['metrics']['f1_score'].append(f1_score(dictionary_PUL['y_t'],
                                                              predictions, average='weighted'))
        print(dictionary_PUL['metrics']['f1_score'])
        dictionary_PUL['metrics']['accuracy'].append(accuracy_score(dictionary_PUL['y_t'],
                                                                         predictions))

        dictionary_PUL['metrics']['kappa'].append(cohen_kappa_score(dictionary_PUL['y_t'],
                                                                         predictions))

        tf.keras.backend.clear_session()
        tf.compat.v1.reset_default_graph()

    key_to_del = ['X_T','X_U','X_P','X_RN','X_RP']
    for key in key_to_del:
        del dictionary_PUL[key]

    with open(os.path.join(os.path.join(filepath,'dictionary_results.pickle')),'wb') as d:
        pickle.dump(dictionary_PUL, d, protocol=pickle.HIGHEST_PROTOCOL)
        
'''