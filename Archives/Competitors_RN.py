import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import os
import numpy as np #do numpy 1.19.5

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import Archives.supervised_training as supervised_training
from Autoencoder import utils as utils
from sklearn.ensemble import RandomForestClassifier
from importlib import reload
utils = reload(utils)
supervised_training = reload(supervised_training)
#################################################################################

origin_path = '/media/DATA/johann/PUL/TileHG/'
#os.chdir(origin_path)

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/Market gardenning'


name_model = 'RF_AE_V1_16_BETA_0'

for n_sample in range(20,120,20):
    for i_exp in range(1,11,2):
        path_file = 'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp)

        X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                      n_sample=n_sample,
                                                                      i_exp=i_exp)
        X_P.shape[0]
        ##############################################################################################

        path_AE = os.path.join(path_experiment,
                               os.path.join(path_file, 'Reconstruction'))

        dico_tanh, filename, model = utils.read_AE_file(path_out=path_AE,
                                                        hidden_activation='tanh',
                                                        embedding_activation='None',
                                                        output_activation='None',
                                                        name_optimizer='Adam_10e-4',
                                                        name_loss='huber',
                                                        prefix='AEGRU_V1_16')

        reconstruction_df = dico_tanh['Reconstruction']
        reconstruction_df['binary'] = y_u
        reconstruction_df['True_class'] = Y_U[:, 0]
        output_RF = os.path.join(path_file, 'Competitors')
        pu_process = supervised_training.PUL_NN(reconstruction_df,
                                                path_experiment=path_experiment,
                                                path_folder_model=output_RF)

        ##############################################################################################
        # work well with nunits = 16, ndim = 8, dropout_rate=0.15 + only 20 epochs

        reconstruction_U = model.predict(X_U.reshape(X_U.shape[0], 25, 6), batch_size=1028)
        h = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
        res = h(X_U.reshape(X_U.shape[0], 25, 6), reconstruction_U)
        res = np.mean(res, axis=1)
        reconstruction_df['Huber'] = res

        pu_process.initialize_output(n_sample=n_sample,
                                     i_exp=i_exp,
                                     name_model=name_model,
                                     metric='Huber',
                                     beta = 0
                                     )

        pu_process.RunClassifier_Competitors(RandomForestClassifier())


#####################################################################################################
import pickle
import os
import pandas as pd
path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/MeadowsUncultivated'


name_model = 'RF_AE_V1_32_BETA_0'
#name_model = 'RF_VAE_32_BETA_0'

df_list = []
for n_sample in range(20,120,20):
    for i_exp in range(1,11,2):
        try:
            #path_file = os.path.join(path_experiment, 'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp))
            path_file = os.path.join(path_experiment, 'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp))
            path_RF = os.path.join(path_file, 'Competitors/' + name_model)
            dico = pickle.load(open(os.path.join(path_RF, 'dictionary_results_GRU.pickle'),'rb'))
            df_res = pd.DataFrame(dico['metrics'])
            df_res['n_sample'] = n_sample
            df_res['i_exp'] = i_exp
            df_list.append(df_res)
        except:
            pass

df_list = pd.concat(df_list)
df_list = df_list.groupby(['n_sample']).agg('mean')
df_list = df_list.drop(['i_exp'],axis = 1)
print(df_list.to_latex())