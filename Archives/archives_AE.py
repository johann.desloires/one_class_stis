
def run(x_train, x_val, filepath, name = 'VAEFeedForward'):
    for loss, name_loss in zip(dictionary_parameters['loss'],
                               dictionary_parameters['name_loss']):
        for hid_act in dictionary_parameters['hidden_activation']:
            for embedding_activation in dictionary_parameters['embedding_activation']:
                for output_activation in dictionary_parameters['output_activation']:
                    for optimizer, name_opt in zip(dictionary_parameters['optimizer'],
                                                   dictionary_parameters['name_optimizer']):

                        model = models.VariationalAutoEncoder(ndim=150,
                                                              hidden_activation=hid_act,
                                                              embedding_activation=embedding_activation,
                                                              output_activation=output_activation,
                                                              name=name)
                        if embedding_activation is None:
                            embedding_activation = 'None'

                        filename = name + \
                                   '_hid_' + hid_act + \
                                   '_embed_' + embedding_activation + \
                                   '_out_' + output_activation + \
                                   '_opt_' + name_opt + \
                                   '_loss_' + name_loss

                        ae = AE_Reconstruction(optimizer=optimizer,
                                               loss=loss,
                                               filepath=filepath)

                        ae.epoch_training(x_train,
                                          x_val,
                                          model,
                                          filename, nb_epoch=200)
                        error_df = ae.reconstruction(X_U, Y_U, y_u,input_shape=[150])
                        tf.keras.backend.clear_session()
                        del error_df


def run_V1(x_train, x_val, dictionary_parameters, filepath, ndim = 64, name = 'AEGRU_V1'):
    for loss, name_loss in zip(dictionary_parameters['loss'], dictionary_parameters['name_loss']):
        for hid_act in dictionary_parameters['hidden_activation']:
            for optimizer, name_opt in zip(dictionary_parameters['optimizer'],
                                           dictionary_parameters['name_optimizer']):

                        model = models.GRUAutoencoder_V1(hidden_activation=hid_act,
                                                         ndim = ndim)

                        filename = name + \
                                   '_hid_' + hid_act + \
                                   '_embed_None' +
                                   '_out_None' +
                                   '_opt_' + name_opt + \
                                   '_loss_' + name_loss

                        ae = AE_Reconstruction(optimizer=optimizer,
                                               loss=loss,
                                               filepath=filepath)

                        ae.epoch_training(x_train.reshape(x_train.shape[0], 25, 6),
                                          x_val.reshape(x_val.shape[0], 25, 6),
                                          model,
                                          filename, nb_epoch=150)
                        error_df = ae.reconstruction(X_U, Y_U, y_u,input_shape=[25,6])
                        tf.keras.backend.clear_session()
                        del error_df