import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt
import geopandas as gpd
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import tensorflow as tf
path = '/media/DATA/johann/PUL/TileHG/'
os.chdir(path)
bands_concatenated = pd.read_csv('./FinalDBPreprocessed/training_set_V2.csv')


def reshape_table(array, n = 6, t = 25):
    array = array.reshape(array.shape[0], n, t)
    array = np.moveaxis(array, 1, 2)
    return array

cols = np.array(list(bands_concatenated.columns))
cols = cols[:150].reshape(1,150)
cols = reshape_table(cols)
cols = cols.reshape(1,1,25,6)
cols = np.moveaxis(cols,2,3)
cols.reshape(1,25*6)

bands_concatenated[bands_concatenated.Object_ID.isin([7278])]
Dates = pd.read_csv('./Sentinel2/dates.csv')
Dates = Dates[['2019' in k for k in Dates.dates]]
out_2019 = pd.DataFrame(Dates)
out_2019.to_csv('./Sentinel2/dates_2019.csv',index = False)
cols = [x for x in bands_concatenated.columns
        if ~np.any([k in x for k in ['Class','Label']])]



bands = []
for i in cols:
    value_band = i.split('_')[0]
    if value_band not in bands:
        bands.append(value_band)

ndvi = [k for k in cols if 'NDVI' in k.split('_')]
NDVI = bands_concatenated[ndvi]
NDVI.describe()

NDVI.columns = Dates['dates']
dates = list(NDVI.columns)
NDVI['Class'] = bands_concatenated['Class']

NDVI_av = NDVI.groupby('Class').agg(np.mean)
NDVI_av = NDVI_av.T

NDVI_av.plot()
plt.xticks(rotation=20)
plt.legend(title='NDVI', bbox_to_anchor=(1.05, 1), loc='upper center')
plt.show()

import pickle
meta_info = pickle.load(open('./Sentinel2/dictionary_meta_info_V2.pickle', 'rb'))

NDVI_av_sc = (NDVI_av - meta_info['./Sentinel2/GEOTIFFS/GFstack_NDVI_crop_2019.tif']['q02'])/(meta_info['./Sentinel2/GEOTIFFS/GFstack_NDVI_crop_2019.tif']['q98'] - meta_info['./Sentinel2/GEOTIFFS/GFstack_NDVI_crop_2019.tif']['q02'])

NDVI_av_sc.plot()
plt.xticks(rotation=20)
plt.legend(title='NDVI', bbox_to_anchor=(1.05, 1), loc='upper center')
plt.show()

samples = pickle.load(open('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/Forest/dictionary_sample_selection.pickle', 'rb'))
l = samples[1][60]
[k for k in l['P']
[x for x, y in zip(bands_concatenated.Class, bands_concatenated.Object_ID) if y in l['P']]

database_sampled = gpd.read_file('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/DATABASE_SAMPLED/DATABASE_SAMPLED.shp')
bands_concatenated = pd.merge(bands_concatenated, database_sampled, on = ['Object_ID','Class'], how = 'left')
bands_concatenated = gpd.GeoDataFrame(bands_concatenated, geometry = bands_concatenated.geometry)
bands_concatenated[['geometry','Object_ID','Class']].drop_duplicates().to_file('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/DATABASE_SAMPLED/bands_concatenated.shp')

def VisualResults(bands_concatenated, X_T, Y_T):
    x_var = pd.DataFrame(X_T)
    y_var = pd.DataFrame(Y_T, columns=['Label_Code', 'Class_ID'])
    l = pd.DataFrame([[5, 'Built'],
                      [1, 'Cereals/Oilseeds '],
                      [4, 'Fodder'],
                      [6, 'Forest'],
                      [3, 'Market gardenning'],
                      [2, 'Meadows/Uncultivated'],
                      [8, 'Orchards'],
                      [7, 'Water']], columns=['Label_Code', 'Description'])

    y_var = pd.merge(y_var, l, on='Label_Code', how='left')
    cols = [k for k in bands_concatenated.columns
            if np.any([x in k for x in ['B2', 'B3', 'B4', 'B8', 'NDVI', 'GNDVI']])]

    cols = [k for k in cols
            if 'B8A' not in k]

    x_var.columns = cols
    ndvi = [k for k in cols if 'NDVI' in k.split('_')]
    NDVI = x_var[ndvi]
    NDVI = pd.concat([NDVI, y_var['Description']], axis=1)
    NDVI_av = NDVI.groupby('Description').agg(np.mean)
    NDVI_av = NDVI_av.T

    NDVI_av.plot()
    plt.xticks(rotation=20)
    plt.legend(title='NDVI', bbox_to_anchor=(1.05, 1), loc='upper center')
    plt.show()


#VisualResults(bands_concatenated, X_T, Y_T)
#VisualResults(bands_concatenated, X_U, Y_U)
#VisualResults(bands_concatenated, X_P, Y_P)
##############################################################
import utils
i_exp = 1
n_sample = 60
path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds'

path_AE = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')


#RNN
dico, filename, model = utils.read_AE_file(path_out = path_AE,
                                    hidden_activation='tanh',
                                    embedding_activation='None',
                                    output_activation='None',
                                    name_optimizer='Adam_10e-4',
                                    name_loss='mae',
                                    prefix = 'VAEGRU_V2_128')

print(filename)

X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                              n_sample=n_sample,
                                                              i_exp=i_exp)

predictions_U = model.predict(X_U.reshape(X_U.shape[0], 25, 6), batch_size = 16)
predictions_U = predictions_U.reshape(X_U.shape[0],150)
predictions_U = pd.DataFrame(predictions_U)
X_U = pd.DataFrame(X_U)
Y_U = pd.DataFrame(Y_U)
Y_U.columns = ['Class_ID', 'Object_ID']
bands_concatenated = pd.read_csv('./FinalDBPreprocessed/training_set.csv')

columns = [k for k in list(bands_concatenated.columns)
           if k.split('_')[0] in ['B2', 'B3', 'B4','B8', 'NDVI', 'NDWI']]
X_U.columns = columns
predictions_U.columns = columns
error = abs(X_U - predictions_U)
X_U = pd.concat([X_U, Y_U], axis = 1)
X_U = pd.merge(X_U, bands_concatenated[['Object_ID', 'Class']].drop_duplicates(),
               on = ['Object_ID'],
               how = 'left')

predictions_U = pd.concat([predictions_U, Y_U], axis = 1)
predictions_U = pd.merge(predictions_U, bands_concatenated[['Class_ID', 'Class']].drop_duplicates(),
                         on = ['Class_ID'],
                         how = 'left')

error = pd.concat([error, Y_U], axis = 1)
error = pd.merge(error, bands_concatenated[['Class_ID', 'Class']].drop_duplicates(),
                         on = ['Class_ID'],
                         how = 'left')



ndvi = [k for k in columns if 'NDVI' in k.split('_')]
ndvi.append('Class')
NDVI_U = X_U[ndvi]
NDVI_av = NDVI_U.groupby('Class').agg(np.mean)

from datetime import datetime
dates = ['_'.join(k.split('_')[1:]) for k in NDVI_av.columns]
dates = [datetime.strptime(k, '%Y-%m-%d') for k in dates]
dates = [k.strftime('%b') for k in dates]
NDVI_av.columns  = dates

NDVI_av = NDVI_av.T
plt.figure(figsize=(5,2))
ax = NDVI_av.plot(color = ['black','peru', 'chartreuse','darkgreen','salmon','brown',
                           'olive','cadetblue'])

plt.xticks(rotation=15)
# Shrink current axis by 20%
#box = ax.get_position()
#ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
# Put a legend to the right of the current axis
#ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
ax.get_legend().remove()

#plt.title('Time series profile')
plt.show()


import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 25})
plt.ylim([0,1])

plt.bar(['Negative', 'Positive'], [0.6, 0.4], width = 0.5, color = ['red','green'])
plt.show()

import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)
NDVI_U_P = predictions_U[ndvi]
NDVI_av_P = NDVI_U_P.groupby('Class').agg(np.mean)
NDVI_av_P.columns = dates
NDVI_av_P = NDVI_av_P.T
NDVI_av_P.index = dates
ax = NDVI_av_P.plot(color = ['black','peru', 'chartreuse','darkgreen','salmon','brown',
                             'olive','cadetblue'])
plt.xticks(rotation=15)
ax.get_legend().remove()
#plt.title('Reconstructed time series profile')

plt.show()


error = error[ndvi]
error_av = error.groupby('Class').agg(np.mean)
error_av = error_av.T
error_av.columns = NDVI_av.columns
ax = error_av.plot(color = ['black','peru', 'chartreuse','darkgreen','salmon','brown',
                             'olive','cadetblue'])

plt.xticks(rotation=15)
# Shrink current axis by 20%
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
# Put a legend to the right of the current axis
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.title('Absolute error NDVI time series profile')
plt.show()


##############################################################
#
import rasterio
import os
import numpy as np

path_images = '/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS/'

def rescale_images(array):
    mask = (array>10000)
    arr0 = np.ma.array(array,
                       dtype=np.int16,
                       mask=(mask).astype(bool),
                       fill_value=10000)
    array = arr0.filled()
    return array


b2 = rasterio.open(os.path.join(path_images,'GFstack_B2_crop_2019.tif'))
meta = b2.meta
meta['nodata'] = 0
meta['dtype'] = 'int16'

meta.update(nodata=0)
meta.update(count=4)
b2 = b2.read(14)
b2 = rescale_images(b2)

b3 = rasterio.open(os.path.join(path_images,'GFstack_B3_crop_2019.tif'))
b3 = b3.read(14)
b3 = rescale_images(b3)

b4 = rasterio.open(os.path.join(path_images,'GFstack_B4_crop_2019.tif'))
b4 = b4.read(14)
b4 = rescale_images(b4)

plt.imshow(b4)
plt.colorbar()
plt.show()
list_bands = [b2, b3, b4]
array_rgb = np.stack(list_bands,axis = 2)
#array_rgb =(array_rgb/10000) * 255

with rasterio.open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2','RGB.tif'), 'w', **meta) as dst :
    for id in range(3) :
        print(id)
        dst.write_band(id+1, array_rgb[:, :,id].astype(np.int16))

np.max(array_rgb)

plt.figure(figsize=(10,10))
plt.imshow(np.clip(array_rgb[..., [2,1,0]]/10000 * 2.5 , 0, 1), vmin=0, vmax=1);
plt.axis(False);
plt.show()

plt.imshow(array_rgb[:,:,2])
plt.colorbar()
plt.show()



