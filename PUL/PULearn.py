import pulearn
import os
import numpy as np


from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.metrics import precision_recall_fscore_support, cohen_kappa_score, accuracy_score, f1_score
from sklearn import svm
from joblib import dump, load
from varname import nameof
from sklearn.ensemble import  RandomForestClassifier
from sklearn.utils import shuffle
from pulearn import (
    ElkanotoPuClassifier,
    WeightedElkanotoPuClassifier,
    BaggingPuClassifier
)
import copy
import pickle
from varname import nameof


'''
import shutil
for f in os.listdir('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds/Models/'):
    file = os.path.join('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds/Models/',f)
    if len(os.listdir(file))<2:
        shutil.rmtree(file, ignore_errors=True)
'''

class PULPredictions:
    def __init__(self,
                 path_experiment,
                 saving_path,
                 number_experiments=10,
                 sample_geometry=[20, 40, 60, 80, 100]):

        self.number_experiments = number_experiments
        self.sample_geometry = sample_geometry
        self.path_experiment = path_experiment
        if not os.path.exists(saving_path):
            os.makedirs(saving_path)
        self.saving_path = saving_path

    @staticmethod
    def output_predictions(number_experiments, sample_geometry):

        dictionary_predictions = {}

        for i in range(1, number_experiments + 1):
            dictionary_predictions[i] = {}
            for n in sample_geometry:
                dictionary_predictions[i][n] = {}
                dictionary_predictions[i][n]['ypred'] = np.array([])
                dictionary_predictions[i][n]['ytest'] = np.array([])
                dictionary_predictions[i][n]['IndexP'] = np.array([])

        return dictionary_predictions

    @staticmethod
    def load_data(path_experiment, n_sample, i_exp):
        case = str(n_sample) + '_P/'
        path_to_read = os.path.join(path_experiment, case + str(i_exp))

        X_P = np.load(os.path.join(path_to_read, 'X_P.npy'), mmap_mode='r')
        Y_P = np.load(os.path.join(path_to_read, 'Y_P.npy'), mmap_mode='r')

        X_U = np.load(os.path.join(path_to_read, 'X_U.npy'), mmap_mode='r')
        Y_U = np.load(os.path.join(path_to_read, 'Y_U.npy'), mmap_mode='r')

        X_T = np.load(os.path.join(path_to_read, 'X_T.npy'), mmap_mode='r')
        Y_T = np.load(os.path.join(path_to_read, 'Y_T.npy'), mmap_mode='r')


        return X_P, Y_P, X_U, Y_U, X_T, Y_T


    def GeneratePrediction(self, PuClassifier, estimator, rsuffix, kwargs={} , subsample = False):

        dictionary_predictions = self.output_predictions(self.number_experiments,
                                                         self.sample_geometry)

        output_path = os.path.join(self.saving_path, rsuffix)

        if not os.path.exists(output_path):
            os.makedirs(output_path)

        for n in self.sample_geometry:
            print(n)
            case = str(n) + '_P/'
            for i_exp in range(1, self.number_experiments + 1):

                #path_to_read = os.path.join(path_experiment, case + str(i_exp))
                X_P, Y_P, X_U, Y_U, X_T, Y_T = self.load_data(self.path_experiment, n, i_exp)

                value_class = Y_P[0, 0]

                ypos = (np.zeros(Y_P.shape[0]) + 1).astype(int)
                yneg = np.zeros(Y_U.shape[0]).astype(int)  # - 1
                ytest = np.zeros(Y_T.shape[0]).astype(int)
                ytest[np.where(Y_T[:, 0] == value_class)] += 1

                x_train = np.concatenate([X_P, X_U], axis=0)
                labels_train = np.concatenate([Y_P, Y_U], axis=0)
                y_train = np.concatenate([ypos, yneg], axis=0)

                x_train, labels_train, y_train = shuffle(x_train, labels_train, y_train, random_state=2)

                if PuClassifier is not None:
                    estimator_ = PuClassifier(copy.deepcopy(estimator), **kwargs)
                    estimator_.fit(x_train, y_train)
                    y_pred = estimator_.predict_proba(X_T)
                else:
                    estimator_ = copy.deepcopy(estimator)
                    estimator_.fit(X_P)
                    y_pred = estimator_.predict(X_T)


                dictionary_predictions[i_exp][n]['ytest'] = ytest
                dictionary_predictions[i_exp][n]['ypred'] = y_pred
                dictionary_predictions[i_exp][n]['IndexP'] = np.where(Y_T[:, 0] == value_class)
                del estimator_

        with open(os.path.join(output_path, 'dictionary_prediction.pickle'), 'wb') as d:
            pickle.dump(dictionary_predictions, d, protocol=pickle.HIGHEST_PROTOCOL)

        dump(estimator_, os.path.join(output_path, 'model.joblib'))

        return dictionary_predictions



# https://stackoverflow.com/questions/52093388/how-to-choose-optimal-threshold-for-class-probabilities

def scoreFunc(model, X, y_true, threshold_list):
    y_pred = model.predict(X, threshold_list=threshold_list)
    y_pred_proba = model.predict_proba(X, threshold_list=threshold_list)

    ###### metrics ######
    from sklearn.metrics import accuracy_score
    from sklearn.metrics import roc_auc_score
    from sklearn.metrics import average_precision_score
    from sklearn.metrics import f1_score

    accuracy = accuracy_score(y_true, y_pred)
    roc_auc = roc_auc_score(y_true, y_pred_proba, average='weighted')
    pr_auc = average_precision_score(y_true, y_pred_proba, average='weighted')
    f1_value = f1_score(y_true, y_pred, average='weighted')

    return accuracy, roc_auc, pr_auc, f1_value

################################################################################################
################################################################################################
origin_path = '/media/DATA/johann/PUL/TileHG/'

paths_experiments = [
    'FinalDBPreprocessed/Experiments_WWB_IS/Positive'
]
'''
path_experiment = paths_experiments[0]
X_P, Y_P, X_U, Y_U, X_T, Y_T  = load_data(path_experiment, 20, 1)
'''

for path_experiment in paths_experiments:

    path_experiment = os.path.join(origin_path, path_experiment)
    saving_path = os.path.join(path_experiment, 'Models')

    PUL = PULPredictions(path_experiment,
                         saving_path=saving_path,
                         number_experiments=10,
                         sample_geometry=[20, 40, 60, 80, 100])

    ##############################

    ############################################################################################################################

    def generate_rf(PuClassifier, kwargs, n_trees=[500]):
        for n_estimator in n_trees:
            estimator = RandomForestClassifier(
                n_estimators=n_estimator,
                criterion='gini',
                bootstrap=True,
                n_jobs=3)

            PUL.GeneratePrediction(PuClassifier=PuClassifier,
                                   estimator=estimator,
                                   rsuffix='rf_' + str(kwargs) + '-' + str(n_estimator))

    def rf():

        generate_rf(PuClassifier=ElkanotoPuClassifier,
                    n_trees=[500], kwargs={'hold_out_ratio': 0.5})

    #rf()

    ############################################################################################################################

    def generate_logistic(PuClassifier, kwargs,penalty = ['l2','l1','elasticnet']):
        #pen = 'elasticnet'
        for pen in penalty:
            if pen == 'elasticnet':
                l1_ratio = 0.5
            else:
                l1_ratio = None
            estimator = LogisticRegression(penalty = pen, solver = 'saga',l1_ratio = l1_ratio)
            PUL.GeneratePrediction(PuClassifier=PuClassifier,
                                   estimator=estimator,
                                   rsuffix='Logit_' + str(kwargs) + '-' + str(pen))


    def logisticC():
        generate_logistic(ElkanotoPuClassifier,
                          kwargs={'hold_out_ratio': 0.5})

    logisticC()

    ############################################################################################################################

    def generate_LDA(PuClassifier, kwargs):

        estimator = LinearDiscriminantAnalysis()
        PUL.GeneratePrediction(PuClassifier=PuClassifier,
                               estimator=estimator,
                               rsuffix='LDA_' + str(kwargs))


    def LDA():

        generate_LDA(ElkanotoPuClassifier,
                     kwargs={'hold_out_ratio': 0.5})
    LDA()
    ############################################################################################################################

    def generate_MLP(PuClassifier, kwargs):

        estimator = MLPClassifier()
        PUL.GeneratePrediction(PuClassifier=PuClassifier,
                               estimator=estimator,
                               rsuffix='MLP_' + str(kwargs))

    def MLP():

        generate_MLP(ElkanotoPuClassifier,
                     kwargs={'hold_out_ratio': 0.5})

    MLP()


    #################################################################################################
    from sklearn.svm import SVC
    def generate_svm(PuClassifier, dictionary_parameters, kwargs):
        for C in dictionary_parameters['C']:
            for gamma in dictionary_parameters['gamma']:
                for kernel in dictionary_parameters['kernel']:
                    estimator = svm.SVC(C=C,
                                        gamma=gamma,
                                        kernel=kernel,
                                        probability=True)

                    PUL.GeneratePrediction(PuClassifier=PuClassifier,
                                           estimator=estimator,
                                           subsample= True,
                                           rsuffix='svm_' +
                                                   str(kwargs) + '-'.join([str(C),
                                                                           str(gamma),
                                                                           kernel]))

    ###########################################################

    def generate_OCsvm(dictionary_parameters):
        for nu in dictionary_parameters['nu']:
            for gamma in dictionary_parameters['gamma']:
                for kernel in dictionary_parameters['kernel']:
                    estimator = svm.OneClassSVM(
                        nu = nu,
                        gamma=gamma,
                        kernel=kernel)

                    PUL.GeneratePrediction(PuClassifier=None,
                                           estimator=estimator,
                                           rsuffix='OCsvm_' + '-'.join([gamma, kernel,'nu_' + str(nu)]))

    def ocsvm():
        dictionary_parameters = dict(gamma=['scale',],
                                     nu = [ 0.5],
                                     kernel=['rbf'])

        generate_OCsvm(dictionary_parameters)

    ocsvm()

#######################################################################################################
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sn
from sklearn.metrics import precision_recall_fscore_support, cohen_kappa_score, accuracy_score, f1_score
import copy
from Supervised import models

def plot_conf_matrix(y_t,preds):
    data = {'y_Actual': y_t,
            'y_Predicted': preds
            }

    df = pd.DataFrame(data, columns=['y_Actual', 'y_Predicted'])
    confusion_matrix = pd.crosstab(df['y_Actual'], df['y_Predicted'], rownames=['Label'],
                                   colnames=['Predicted'])

    confusion_matrix = confusion_matrix / confusion_matrix.sum(axis=1)[:, np.newaxis]
    plt.figure(figsize=(10, 10))
    sn.set(font_scale=3)
    sn.heatmap(confusion_matrix, annot=True, cmap = plt.cm.Greens)
    plt.show()

def load_data(path_experiment, n_sample, i_exp):
    case = str(n_sample) + '_P/'
    path_to_read = os.path.join(path_experiment, case + str(i_exp))

    X_P = np.load(os.path.join(path_to_read, 'X_P.npy'), mmap_mode='r')
    Y_P = np.load(os.path.join(path_to_read, 'Y_P.npy'), mmap_mode='r')

    X_U = np.load(os.path.join(path_to_read, 'X_U.npy'), mmap_mode='r')
    Y_U = np.load(os.path.join(path_to_read, 'Y_U.npy'), mmap_mode='r')

    X_T = np.load(os.path.join(path_to_read, 'X_T.npy'), mmap_mode='r')
    Y_T = np.load(os.path.join(path_to_read, 'Y_T.npy'), mmap_mode='r')

    return X_P, Y_P, X_U, Y_U, X_T, Y_T


def to_labels(pos_probs, threshold):
	return (pos_probs >= threshold).astype('int')

def reshape_table(array, n = 6, t = 25):
    array = array.reshape(array.shape[0], n, t)
    array = np.moveaxis(array, 1, 2)
    return array




dico = pickle.load(open("/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_CORN/Positive/Models/rf_{'hold_out_ratio': 0.5}-500/dictionary_prediction.pickle", "rb"))
#dico = pickle.load(open("/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds/Models/rf_{'hold_out_ratio': 0.5}-500/dictionary_prediction.pickle", "rb"))
preds_rf = dico[1][100]['ypred']
preds_rf[preds_rf>0.23] = 1
preds_rf[preds_rf<0.23] = 0
yt = dico[1][100]['ytest']


# define thresholds
thresholds = np.arange(0, 1, 0.01)
# evaluate each threshold
scores = [f1_score(yt, to_labels(preds_rf, t), average='weighted')
          for t in thresholds]
# get best threshold
ix = np.argmax(scores)
thresholds[ix]

plot_conf_matrix(yt,preds_rf)

f1_score(yt,preds_rf, average='weighted')
cohen_kappa_score(yt,preds_rf)

our_approach = pickle.load(open("/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_CORN/Positive/Models/VAE/Consistency_TS_EMA_GRU_V2_64_Noisy_TS_tanh_32_32_metrics.pickle", "rb"))

classifier_GRU = models.FCGRU_Model(nunits=32, fcunits=32,
                                    dropout_rate=0.2, l2_reg=0.0)

path_student = os.path.join("/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_WWB_IS/Positive/Models/VAE/N_100_Exp_1/Consistency_TS_EMA",
                            "GRU_V2_64_Noisy_TS_student")

classifier_GRU.load_weights(path_student)

X_P, Y_P, X_U, Y_U, X_T, Y_T = load_data("/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_WWB_IS/Positive", 100, 1)
yt = np.zeros(Y_T.shape[0]).astype(int)
yt[np.where(Y_T[:, 0] == 1)] += 1

preds,_ = classifier_GRU.predict(reshape_table(X_T, n = 6, t = 10))

preds_GRU = copy.deepcopy(preds[:,1])


thresholds = np.arange(0, 1, 0.001)
scores = [f1_score(yt, to_labels(preds_GRU, t), average='weighted')
          for t in thresholds]
# get best threshold
ix = np.argmax(scores)
thresholds[ix]

preds_GRU = copy.deepcopy(preds[:,1])
preds_GRU[preds_GRU>0.98] = 1
preds_GRU[preds_GRU<=0.98] = 0
plot_conf_matrix(yt,preds_GRU)
cohen_kappa_score(yt,preds_GRU)
f1_score(yt,preds_GRU)

###################################################################################################################################

reconstruction_df = pickle.load(open(
    "/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_CORN/Positive/Models/VAE/N_20_Exp_1/Reconstruction/VAEGRU_V2_64_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_huber/reconstruction_df.pickle","rb"))

reconstruction_df = reconstruction_df['Reconstruction']
qt = np.quantile(reconstruction_df['Huber'], 0.975)
reconstruction_df = reconstruction_df[reconstruction_df["Huber"] < qt]

fig, ax = plt.subplots(1, 1, figsize=(8, 15))
plt.rcParams.update({'font.size': 5})

threshold = np.mean(
    reconstruction_df['Huber']) + 0 * np.std(
    reconstruction_df['Huber'])


g1 = sn.displot(reconstruction_df,
                 x="Huber",
                 hue='binary',
                 stat="probability", common_norm=False,
                 palette=["#e74c3c", "#2ecc71"])

g1.set(xticklabels=['Reconstruction error'])
g1.set(yticklabels=[])
plt.axvline(threshold, linewidth=4, color='r')
plt.ylabel(" ")
plt.show()
# eclf = VotingClassifier(estimators=[('dt', clf1), ('knn', clf2), ('svc', clf3)],
# voting='soft', we
# ights=[2, 1, 2])
