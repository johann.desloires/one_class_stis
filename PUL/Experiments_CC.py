import rasterio
import numpy as np
import os
import pandas as pd
from sklearn.model_selection import train_test_split, GroupShuffleSplit
from sklearn.preprocessing import MinMaxScaler
import random
from varname import nameof
import matplotlib.pyplot as plt
import pickle
import gdal

origin_path = '/media/DATA/johann/PUL/TileHG/'
# origin_path = './data/'
os.chdir(origin_path)
path_images = './Sentinel2/GEOTIFFS_WWB/'


def make_directory(path):
    if os.path.exists(path) is False:
        os.makedirs(path)


def save_numpy(path, array, name_array):
    with open(os.path.join(path, name_array + '.npy'), 'wb') as f:
        np.save(f, array)


file_bands = []
band_names = ['B2', 'B3', 'B4','B8', 'NDVI','NDWI']

for band in band_names:
    for k in os.listdir(path_images):
        if band in k.split('_'):
            file_bands.append(os.path.join(path_images, k))


def reformat_labels(target, file_reference):
    target_to_mask = rasterio.open(target)
    meta = target_to_mask.meta

    target_to_mask = target_to_mask.read(1)

    raster_ref = rasterio.open(file_reference)
    raster_ref = raster_ref.read(1)
    mask = (raster_ref == 0)

    arr0 = np.ma.array(target_to_mask,
                       dtype=np.int16,
                       mask=mask,
                       fill_value=0)

    target_to_mask = arr0.filled()
    name_output = target.split('.')
    name_output[-2] += '_' + '2019'
    target_to_mask = target_to_mask.astype('uint16')
    with rasterio.open('.'.join(name_output), 'w', **meta) as dst:
        dst.write_band(1, target_to_mask)

    os.remove(target)

'''
targets = ['./Sentinel2/GEOTIFFS_WWB/Object_ID_crop.tif',
           './Sentinel2/GEOTIFFS_WWB/Class_ID_crop.tif']

reformat_labels(targets[0],file_bands[0])
reformat_labels(targets[1],file_bands[0])
'''

targets = ['./Sentinel2/GEOTIFFS_WWB/Object_ID_crop_2019.tif',
           './Sentinel2/GEOTIFFS_WWB/Class_ID_crop_2019.tif']

###############################################################################################################
labelling = pd.read_csv('./FinalDBPreprocessed/DATABASE_SAMPLED/Labelling_WWB.csv')
labelling = labelling[['Class', 'Object_ID', 'Class_ID', 'geometry']]
labelling.columns = ['Class', 'Object_ID', 'Class_ID', 'geometry']
dates = pd.read_csv('./Sentinel2/dates.csv')
dates = dates[['2019' in k for k in dates.dates]]

targets = ['./Sentinel2/GEOTIFFS_WWB/Object_ID_crop_2019.tif',
           './Sentinel2/GEOTIFFS_WWB/Class_ID_crop_2019.tif']


def prepare_training_set():
    array_target1 = rasterio.open(targets[0])
    array_target1 = array_target1.read(1)

    h, w = array_target1.shape
    array_target1 = array_target1.flatten()

    array_target2 = rasterio.open(targets[1])
    array_target2 = array_target2.read(1)
    array_target2 = array_target2.flatten()

    filter_observation = np.where(array_target1 > 0)

    x = np.linspace(0, h, h).astype(np.int16)
    y = np.linspace(0, w, w).astype(np.int16)
    xv, yv = np.meshgrid(x, y)

    dictionary_bands = {}
    dictionary_meta_info = {}
    dictionary_meta_info['xcoordinates_objects'] = xv.flatten()[filter_observation]
    dictionary_meta_info['ycoordinates_objects'] = yv.flatten()[filter_observation]
    dictionary_meta_info['Object_ID'] = array_target1[filter_observation]
    dictionary_meta_info['Class_ID'] = array_target2[filter_observation]
    dictionary_meta_info['bands'] = file_bands

    # scaling
    for index_band in file_bands:
        dictionary_meta_info[index_band] = {}
        band_name = index_band.split('/')[-1].split('_')[-3]
        print(band_name)

        band = gdal.Open(index_band).ReadAsArray()
        mask = (band > 10000)

        if True in mask:
            arr0 = np.ma.array(band,
                               dtype=np.float32,
                               mask=mask,
                               fill_value=10000)

            band = arr0.filled()

        band = band.reshape(band.shape[0],band.shape[1]*band.shape[2])
        array_time = band[:,filter_observation[0]]

        # Get statistics from the image
        if band_name not in ['B2', 'B3', 'B4', 'B8']:
            lb = -1
        else:
            lb = 0

        array_time_subset = band.flatten()
        array_time_subset = array_time_subset[array_time_subset>lb]

        dictionary_meta_info[index_band]['max'] = np.max(array_time_subset)
        dictionary_meta_info[index_band]['q98'] = np.quantile(array_time_subset,0.98)
        dictionary_meta_info[index_band]['min'] = np.min(array_time_subset)
        dictionary_meta_info[index_band]['q02'] = np.quantile(array_time_subset, 0.02)
        dictionary_meta_info[index_band]['mean'] = np.mean(array_time_subset)
        dictionary_meta_info[index_band]['median'] = np.median(array_time_subset)
        dictionary_meta_info[index_band]['std'] = np.std(array_time_subset)

        dictionary_bands[band_name] = array_time

    dictionary_bands['Object_ID'] = array_target1[filter_observation]
    dictionary_bands['Class_ID'] = array_target2[filter_observation]
    dictionary_meta_info['dates'] = dates.dates

    bands_concatenated = []

    for key in dictionary_bands.keys():
        print(key)
        if key not in ['Object_ID', 'Class_ID']:
            cols = [key + '_' + np.array(dates.dates)[id_] for id_ in range(dates.shape[0])]
            df_array = pd.DataFrame(dictionary_bands[key].T, columns=cols)
            print(df_array.shape)
        else:
            cols = [key]
            df_array = pd.DataFrame(dictionary_bands[key].T, columns=cols)
            print(df_array.shape)

        bands_concatenated.append(df_array)

    bands_concatenated = pd.concat(bands_concatenated, axis=1)

    bands_concatenated = pd.merge(bands_concatenated,
                                  labelling[['Class', 'Class_ID']].drop_duplicates(),
                                  on=['Class_ID'], how='left')

    x_cols = [k for k in bands_concatenated.columns if np.any([x in k for x in band_names])]
    #array_bands = np.array(bands_concatenated[x_cols])
    #array_bands = array_bands.reshape(dates.shape[0], bands_concatenated.shape[0], len(band_names))

    bands_concatenated.to_csv('./FinalDBPreprocessed/training_set_WWB.csv', index=False)
    #save_numpy('./FinalDBPreprocessed/', array_bands, nameof(array_bands))

    with open('./Sentinel2/dictionary_meta_info_WWB.pickle', 'wb') as d:
        pickle.dump(dictionary_meta_info, d, protocol=pickle.HIGHEST_PROTOCOL)


prepare_training_set()


#############################################################################################
bands_concatenated = pd.read_csv('./FinalDBPreprocessed/training_set_WWB.csv')
bands_concatenated = bands_concatenated.dropna()
bands_concatenated.loc[bands_concatenated.Class == 'Positive',:]
bands_concatenated[bands_concatenated['B2_2019-01-11']>0].shape
bands_concatenated[['Object_ID','Class_ID','Class']].groupby('Class_ID').count()
bands_concatenated.Class_ID.unique()
# bands_concatenated = bands_concatenated[bands_concatenated.Class.isin(['Cereals/Oilseeds','Forest'])]
# https://scikit-learn.org/dev/auto_examples/semi_supervised/plot_self_training_varying_threshold.html
dates_in_season = ['_'.join(k.split('_')[1:]) for k in bands_concatenated.columns[:10]]

cols = [k for k in bands_concatenated.columns
        if np.any([x in '_'.join(k.split('_')[1:]) for x in dates_in_season])]
cols.extend(['Object_ID', 'Class_ID', 'Class'])
bands_concatenated = bands_concatenated[cols]


def experiments(band_names, class_interest='Positive', rsuffix=None, nb_P = [20, 40, 60, 80, 100], window = 20):

    meta_info = pickle.load(open('./Sentinel2/dictionary_meta_info_WWB.pickle', 'rb'))

    bands_concatenated_scaled = bands_concatenated.copy()

    for band in band_names:
        print(band)
        cols_subset = [x for x in list(bands_concatenated.columns)
                       if band in x.split('_')]
        key_mata = [x for x in meta_info.keys() if band in x.split('_')][0]

        bands_concatenated_scaled[cols_subset] = (bands_concatenated[cols_subset] - meta_info[key_mata]['q02']) / (
                meta_info[key_mata]['q98'] - meta_info[key_mata]['q02'])

        for col in cols_subset:
            bands_concatenated_scaled.loc[bands_concatenated_scaled[col]<0,col] = 0
            bands_concatenated_scaled.loc[bands_concatenated_scaled[col]>1,col] = 1

    path_experiments = './FinalDBPreprocessed/Experiments_WWB_IS'

    if os.path.exists(path_experiments) is False:
        os.makedirs(path_experiments)

    if rsuffix is None:
        path_class = os.path.join(path_experiments,
                                  ''.join(class_interest.split('/')))
    else:
        path_class = os.path.join(path_experiments,
                                  rsuffix)

    make_directory(path_class)
    number_experiments = 10

    # Take n_first polygons
    print(nb_P)

    object_ids_selected = {}
    for i in range(1, number_experiments + 1):
        object_ids_selected[i] = {}
        for n in nb_P:
            object_ids_selected[i][n] = {}
            object_ids_selected[i][n]['P'] = []
            object_ids_selected[i][n]['U'] = []
            object_ids_selected[i][n]['T'] = []

    previous_nb_p = 0
    n = 100
    i = 8
    for n in nb_P:
        print(n)
        output_path_experiment_n = os.path.join(path_class, str(n) + '_P')
        make_directory(output_path_experiment_n)

        for i in range(1, 11):
            print(i)
            train_inds, test_inds = next(
                GroupShuffleSplit(test_size=.5, n_splits=2, random_state=i). \
                    split(bands_concatenated, groups=bands_concatenated['Object_ID']))

            bands_concatenated_train = bands_concatenated_scaled.iloc[train_inds, :]
            bands_concatenated_test = bands_concatenated_scaled.iloc[test_inds, :]
            #obj = set(bands_concatenated_train.Object_ID.unique())
            #obj_t = set(bands_concatenated_test.Object_ID.unique().flatten())
            #[k for k in obj if k in obj_t]
            path_experiments_i = os.path.join(output_path_experiment_n, str(i))
            make_directory(path_experiments_i)

            labels = bands_concatenated_train.loc[bands_concatenated_train.Class.isin([class_interest]), :]
            labels.Object_ID.unique().shape
            if previous_nb_p == 0:
                object_ids = labels.Object_ID.unique()
                rdm_class = random.sample(list(object_ids), window)
                object_ids_selected[i][n]['P'].extend(rdm_class)
            else:
                previous_polygons = object_ids_selected[i][previous_nb_p]['P'].copy()
                object_ids_selected[i][n]['P'] = previous_polygons
                object_ids = labels.loc[~labels.Object_ID.isin(previous_polygons), :].Object_ID.unique()

                rdm_class = random.sample(list(object_ids), window)
                object_ids_selected[i][n]['P'].extend(rdm_class)

            object_ids_U = list(
                bands_concatenated_train.loc[~bands_concatenated_train.Object_ID.isin(object_ids_selected[i][n]['P']),
                :].Object_ID.unique())

            object_ids_selected[i][n]['U'].extend(object_ids_U)
            object_ids_T = list(bands_concatenated_test.Object_ID.unique())
            object_ids_selected[i][n]['T'].extend(object_ids_T)

            print('There are ' + str(len(object_ids_selected[i][n]['P'])) + ' polygons selected')

            x_var = [k for k in bands_concatenated.columns
                     if k.split('_')[0] in ['B2', 'B3', 'B4', 'B8', 'NDVI', 'NDWI']]

            # Positive
            P = bands_concatenated_train[bands_concatenated_train.Object_ID.isin(object_ids_selected[i][n]['P'])]
            # P.to_csv(os.path.join(path_experiments_i,'Positive.csv'))
            X_P = np.array(P[x_var])
            print(X_P.shape)
            save_numpy(path_experiments_i, X_P, nameof(X_P))
            Y_P = np.array(P[['Class_ID', 'Object_ID']])
            save_numpy(path_experiments_i, Y_P, nameof(Y_P))

            # Unlabeled
            U = bands_concatenated_train[~bands_concatenated_train.Object_ID.isin(object_ids_selected[i][n]['P'])]
            # U.to_csv(os.path.join(path_experiments_i,'Unlabeled.csv'))
            X_U = np.array(U[x_var])
            print(X_U.shape)
            save_numpy(path_experiments_i, X_U, nameof(X_U))
            Y_U = np.array(U[['Class_ID', 'Object_ID']])
            save_numpy(path_experiments_i, Y_U, nameof(Y_U))

            # Test
            # bands_concatenated_test.to_csv(os.path.join(path_experiments_i,'Test.csv'))
            X_T = np.array(bands_concatenated_test[x_var])
            print(X_T.shape)
            save_numpy(path_experiments_i, X_T, nameof(X_T))
            Y_T = np.array(bands_concatenated_test[['Class_ID', 'Object_ID']])
            save_numpy(path_experiments_i, Y_T, nameof(Y_T))

        previous_nb_p = n

    with open(os.path.join(path_class, 'dictionary_sample_selection_WWB_IS.pickle'), 'wb') as d:
        pickle.dump(object_ids_selected, d, protocol=pickle.HIGHEST_PROTOCOL)


experiments(band_names, class_interest='Positive')
