import pickle
import os
import pandas as pd
from sklearn.metrics import precision_recall_fscore_support, cohen_kappa_score, \
    accuracy_score, f1_score, confusion_matrix, roc_curve, auc, \
    recall_score, precision_score, mean_squared_error
import matplotlib.pyplot as plt
import numpy as np
from joblib import load
import scipy
from sklearn.metrics import accuracy_score

origin_path = '/media/DATA/johann/PUL/TileHG/'
os.chdir(origin_path)

path_experiment = './FinalDBPreprocessed/Experiments_CORN_IS/Positive'

nb_experiments = 10

########################################################################################################################

def accuracy(y_true, y_pred):
    cnf_mat = confusion_matrix(y_true, y_pred)

    fpr, tpr, thresholds = roc_curve(y_true, y_pred)
    Auc = auc(fpr, tpr)
    # calculate the g-mean for each threshold
    gmeans = np.sqrt(tpr * (1 - fpr))
    ix = np.argmax(gmeans)
    print('Best Threshold=%f, G-Mean=%.3f' % (thresholds[ix], gmeans[ix]))

    Sens = recall_score(y_true, y_pred, average='weighted')
    Prec = precision_score(y_true, y_pred, average='weighted')
    F1 = f1_score(y_true, y_pred, average='weighted')
    kappa = cohen_kappa_score(y_true, y_pred)
    Support = precision_recall_fscore_support(y_true, y_pred, beta=0.5, average=None)

    mse = mean_squared_error(y_true, y_pred)
    return Sens, Prec, F1, cnf_mat, kappa, gmeans


# Sens, Prec, F1, cnf_mat, kappa, gmeans = accuracy(y_true, y_pred)


def init_output_ensemble(number_experiments, sample_geometry, paths_ensemble):
    dictionary_predictions = {}

    for i in range(1, number_experiments + 1):
        dictionary_predictions[i] = {}
        for n in sample_geometry:
            dictionary_predictions[i][n] = {}
            dictionary_predictions[i][n]['Ensemble'] = np.array([])
            dictionary_predictions[i][n]['TotalMSE'] = 0
            dictionary_predictions[i][n]['IndexP'] = np.array([])
            dictionary_predictions[i][n]['ytest'] = np.array([])
            for path in paths_ensemble:
                name = path.split('/')[-1]
                dictionary_predictions[i][n][name] = {}
                dictionary_predictions[i][n][name]['ypred'] = np.array([])
                dictionary_predictions[i][n][name]['mse'] = 0

    return dictionary_predictions

def read_training_data(n, i_exp, path_experiment):
    case = str(n) + '_P/'
    path_to_read = os.path.join(path_experiment, case + str(i_exp))

    X_P = np.load(os.path.join(path_to_read, 'X_P.npy'), mmap_mode='r')

    Y_P = np.load(os.path.join(path_to_read, 'Y_P.npy'), mmap_mode='r')
    value_class = Y_P[0, 0]
    ypos = (np.zeros(Y_P.shape[0]) + 1).astype(int)

    X_U = np.load(os.path.join(path_to_read, 'X_U.npy'), mmap_mode='r')
    Y_U = np.load(os.path.join(path_to_read, 'Y_U.npy'), mmap_mode='r')
    yneg = np.zeros(Y_U.shape[0]).astype(int)  # - 1

    x_train = np.concatenate([X_P, X_U], axis=0)
    y_train = np.concatenate([ypos, yneg], axis=0)

    return x_train, y_train


def aggregate_ensemble_results(paths_ensemble, path_experiment):
    dictionary_predictions = init_output_ensemble(10, range(20, 120, 20), paths_ensemble)

    for n in range(20, 120, 20):
        for i_exp in range(1, 10 + 1):
            it = 0
            x_train, y_train = read_training_data(n, i_exp, path_experiment)
            for path in paths_ensemble:

                results = pickle.load(open(os.path.join(path, 'dictionary_prediction.pickle'), 'rb'))
                model = load(os.path.join(path, 'model.joblib'))

                name = path.split('/')[-1]

                dictionary_predictions[i_exp][n][name]['ypred'] = results[i_exp][n]['ypred']

                weight = mean_squared_error(model.predict_proba(x_train),
                                            y_train)

                dictionary_predictions[i_exp][n][name]['mse'] = weight

                dictionary_predictions[i_exp][n]['TotalMSE'] += (1 / weight)

                if it == 0:
                    dictionary_predictions[i_exp][n]['IndexP'] = results[i_exp][n]['IndexP']
                    dictionary_predictions[i_exp][n]['Ensemble'] = np.append(
                        dictionary_predictions[i_exp][n]['Ensemble'],
                        (1 / weight) * results[i_exp][n]['ypred'])
                    dictionary_predictions[i_exp][n]['ytest'] = results[i_exp][n]['ytest']

                else:
                    dictionary_predictions[i_exp][n]['Ensemble'] = np.add(dictionary_predictions[i_exp][n]['Ensemble'],
                                                                          (1 / weight) * results[i_exp][n]['ypred'])

                it += 1

            dictionary_predictions[i_exp][n]['Ensemble'] /= dictionary_predictions[i_exp][n]['TotalMSE']

    with open(os.path.join(path_experiment, 'Models/dictionary_ensemble.pickle'), 'wb') as d:
        pickle.dump(dictionary_predictions, d, protocol=pickle.HIGHEST_PROTOCOL)

    return dictionary_predictions




def agg_results(path_experiment):
    paths = [os.path.join(os.path.join(path_experiment,'Models'), k) for k in os.listdir(os.path.join(path_experiment,'Models'))]
    os.getcwd()
    for range_ratio in [0.5]:
        print(range_ratio)
        ratio = {'hold_out_ratio': range_ratio}

        paths_ensemble = [k for k in paths
                          if str(ratio) in k.split('/')[-1]
                          and np.any([x in k for x in ['LDA', 'Logit', 'rf',"MLP"]])
                          and ~np.any([x in k for x in ['l1','nu']])]

        dictionary_predictions= aggregate_ensemble_results(paths_ensemble, path_experiment)

        with open(os.path.join(os.path.join(path_experiment,'Models'),
                               'dictionary_ensemble_' + str(ratio) + '.pickle'), 'wb') as d:
            pickle.dump(dictionary_predictions, d, protocol=pickle.HIGHEST_PROTOCOL)

#agg_results(path_experiment)

########################################################################################################################

def get_score_dict(results, key="rf_{'hold_out_ratio': 0.1}-500",
                   nb_P=[20, 40, 60, 80, 100], threshold=0.5):
    dictionary_scores = {}

    for i_exp in range(1, nb_experiments + 1):
        dictionary_scores[i_exp] = {}
        for n in nb_P:
            ytest = results[i_exp][n]['ytest']
            #"rf_{'hold_out_ratio': 0.1}-500"
            if key is not None:
                if key is not 'Ensemble':
                    y_pred = results[i_exp][n][key]['ypred']
                else:
                    y_pred = results[i_exp][n][key]
            else:
                y_pred = results[i_exp][n]['ypred']

            #y_pred_ens = dictionary_predictions[i_exp][n]['Ensemble']
            y_pred[y_pred <= threshold] = 0
            y_pred[y_pred > threshold] = 1

            precision, recall, f1_score_, _ = precision_recall_fscore_support(
                ytest, y_pred, labels=[1, 0])

            dictionary_scores[i_exp][n] = {}
            dictionary_scores[i_exp][n]['f1_score'] = f1_score(ytest, y_pred, average="weighted")
            dictionary_scores[i_exp][n]['precision'] = precision[0]
            dictionary_scores[i_exp][n]['kappa'] = cohen_kappa_score(ytest, y_pred)
            dictionary_scores[i_exp][n]['accuracy'] = accuracy_score(ytest, y_pred)

            del y_pred
    res = []
    for i in range(1, nb_experiments + 1):
        res.append(pd.DataFrame(dictionary_scores[i]))

    return res


def aggregate_score(res, name, measure='f1_score'):

    av = pd.concat(res).groupby(level=0).mean()
    std = pd.concat(res).groupby(level=0).std()

    mean = [str((round(k, 3))) for k in av.loc[av.index == measure, :].values[0]]
    se = ['(' + str(round(k, 3)) + ')' for k in std.loc[std.index == measure, :].values[0]]
    res = [i + ' ' + j for i, j in zip(mean, se)]

    df = pd.DataFrame(res)
    df.index = av.columns
    df.columns = [name.upper()]

    df_mean = pd.DataFrame(mean)
    df_mean.index = av.columns
    df_mean.columns = [name.upper()]

    return df, df_mean


def final_results_table(path):
    for measure in ['f1_score','kappa','accuracy']: #,
        for R in [0.5]: #0.1,0.25,0.7
            print(R)
            ratio = {'hold_out_ratio': R}

            dictionary_predictions = pickle.load(open(os.path.join(path,
                                                                   'dictionary_ensemble_' + str(ratio) + '.pickle'), 'rb'))

            names = [k for k in dictionary_predictions[1][20].keys() if str(ratio) in k]

            dictionary_ocsvm = pickle.load(open(
                os.path.join(path + '/OCsvm_scale-rbf-nu_' + str(R) + '/dictionary_prediction' + '.pickle'), 'rb'))

            ####################################################################################################

            res_rf = get_score_dict(dictionary_predictions,
                                          key="rf_" + str(ratio) + '-500',
                                          nb_P=[20, 40, 60, 80, 100],
                                          threshold=0.5)

            res_ensemble = get_score_dict(dictionary_predictions,
                                          key="Ensemble",
                                          nb_P=[20, 40, 60, 80, 100],
                                          threshold=0.5)

            res_ocsvm= get_score_dict(dictionary_ocsvm,
                                      key=None,
                                      nb_P=[20, 40, 60, 80, 100],
                                      threshold=0.5)

            #######################################################################################################################

            df_list_rf, df_mean_rf = aggregate_score(res_rf, 'RF', measure=measure)
            df_list_ensemble, df_mean_ensemble = aggregate_score(res_ensemble, 'Ensemble', measure=measure)
            df_list_ocsvm, df_mean_ocsvm = aggregate_score(res_ocsvm, 'OCSVM', measure=measure)

            df_table = pd.concat([df_list_rf,df_list_ensemble,df_list_ocsvm],axis = 1)
            df_MEAN= pd.concat([df_mean_rf,df_mean_ensemble,df_mean_ocsvm],axis = 1)

            path_out = os.path.join(path, str(R))

            if not os.path.exists(path_out):
                os.makedirs(path_out)

            dictionary_out = {}
            dictionary_out['table'] = df_table
            dictionary_out['plot'] = df_MEAN

            with open(os.path.join(path_out,
                                   'dictionary_output_' + measure + '.pickle'), 'wb') as d:
                pickle.dump(dictionary_out, d, protocol=pickle.HIGHEST_PROTOCOL)

final_results_table(path_experiment)


#########################################################################################################
#########################################################################################################
#########################################################################################################
#########################################################################################################
########################################################################################################################
##Confusion matrix

import seaborn as sn
import os
import pickle
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, f1_score, cohen_kappa_score
from Autoencoder import utils
import joblib
from joblib import dump, load
from Supervised import models

origin_path = '/media/DATA/johann/PUL/TileHG/'
os.chdir(origin_path)
i = 1
n = 100

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2/CerealsOilseeds/'

#dictionary_out = pickle.load(open(os.path.join(path_out, 'dictionary_r_' + metric + '.pickle'), 'rb'))

'''
#########################################################################################################
print('fit models')
import joblib
import numpy as np
i = 1
n = 100
class_ = 'CerealsOilseeds'
path_experiment = os.path.join('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2',
                               class_)
X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(
    path_experiment=path_experiment,
    n_sample=n,
    i_exp=i)

def discretize_preds(results):
    results[results > 0.5] = 1
    results[results <= 0.5] = 0

    return results

def plot_conf_matrix(y_t,preds):
    data = {'y_Actual': y_t,
            'y_Predicted': preds
            }

    df = pd.DataFrame(data, columns=['y_Actual', 'y_Predicted'])
    confusion_matrix = pd.crosstab(df['y_Actual'], df['y_Predicted'], rownames=['Label'],
                                   colnames=['Predicted'])

    confusion_matrix = confusion_matrix / confusion_matrix.sum(axis=1)[:, np.newaxis]
    plt.figure(figsize=(10, 10))
    sn.set(font_scale=3)
    sn.heatmap(confusion_matrix, annot=True, cmap = plt.cm.Greens)
    plt.show()


print('rf')
rf = joblib.load(os.path.join(path_experiment,"rf_{'hold_out_ratio': 0.5}-500" + "_i_1_n_100.sav"))
results = rf.predict_proba(X_T)
results = discretize_preds(results)
plot_conf_matrix(y_t,results)
mse_rf = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])],axis=0),
                            rf.predict_proba(np.concatenate([X_P,X_U],axis = 0)))
print('lda')
LDA = joblib.load(os.path.join(path_experiment, "LDA_{'hold_out_ratio': 0.5}" + "_i_1_n_60.sav"))
LDA.fit(np.concatenate([X_P,X_U],axis = 0))
res_LDA = LDA.predict_proba(X_T)
res_LDA = discretize_preds(res_LDA)
plot_conf_matrix(np.concatenate([y_p,y_u],axis = 0),
                 np.concatenate([np.zeros(X_P.shape[0])+1,np.zeros(X_U.shape[0])],axis = 0))

mse_lda = mean_squared_error(np.concatenate([y_p,y_u],axis = 0),
                             LDA.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

print('logits_l2')
logit_l2 = joblib.load(os.path.join(path_experiment,"Logit_{'hold_out_ratio': 0.5}-l2" + "_i_1_n_60.sav"))
res_logit_l2 = logit_l2.predict_proba(X_T)
res_logit_l2 = discretize_preds(res_logit_l2)

mse_logit_l2 = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])],axis=0),
                                  logit_l2.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

print('logits_elas')
logit_elas = joblib.load(os.path.join(path_experiment,"Logit_{'hold_out_ratio': 0.5}-elasticnet" + "_i_1_n_60.sav"))
res_logit_elas = logit_elas.predict_proba(X_T)
res_logit_elas = discretize_preds(res_logit_elas)
mse_logit_elas = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])],axis=0),
                                    logit_elas.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

print('mlp')
mlp = joblib.load(os.path.join(path_experiment,"MLP_{'hold_out_ratio': 0.5}" + "_i_1_n_60.sav"))
res_mlp = mlp.predict_proba(X_T)
res_mlp = discretize_preds(res_mlp)
mse_mlp = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])],axis=0),
                             mlp.predict_proba(np.concatenate([X_P,X_U],axis = 0)))
W_T = 1/mse_rf + 1/mse_lda + 1/mse_logit_l2 + 1/mse_logit_elas + 1/mse_mlp


ocsvm = joblib.load(os.path.join(path_experiment,"OCsvm_scale-rbf-nu_0.5" + "_i_1_n_100.sav"))
ocsvm.fit(X_P)
preds = ocsvm.predict(X_T)
preds[preds<0] = 0
cohen_kappa_score(y_t,preds)


cohen_kappa_score(dictionary_RF[i][n]['ytest'], preds_RF)
cohen_kappa_score(dictionary_ens[i][n]['ytest'], preds)
cohen_kappa_score(dictionary_ens[i][n]['ytest'],preds_OCSVM)



plot_conf_matrix(dictionary_ens[i][n]['ytest'],preds_RF)
plot_conf_matrix(dictionary_ens[i][n]['ytest'],preds)
plot_conf_matrix(dictionary_ens[i][n]['ytest'],preds_OCSVM)
'''

def reshape_table(array, n = 6, t = 25):
    array = array.reshape(array.shape[0], n, t)
    array = np.moveaxis(array, 1, 2)
    return array



X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(
    path_experiment=path_experiment,
    n_sample=n,
    i_exp=i)


classifier_GRU = models.FCGRU_Model(nunits=32, fcunits=32,
                                    dropout_rate=0.2, l2_reg=0.0)

path_student = os.path.join(path_experiment, 'Models/' + 'VAE' + '/N_' + \
                            str(n) + '_Exp_' + str(i) + \
                            '/Consistency_TS_EMA_50/GRU_V2_64_Noisy_TS_student_final')
classifier_GRU.load_weights(path_student)


X_T = reshape_table(X_T)

preds_gru, _ = classifier_GRU.predict(X_T, batch_size=256)
preds_gru = np.argmax(preds_gru,axis = 1)

plot_conf_matrix(dictionary_ens[i][n]['ytest'],preds_gru)

cohen_kappa_score(y_t,preds_gru)













from sklearn.ensemble import  RandomForestClassifier
from sklearn.utils import shuffle
from pulearn import (
    ElkanotoPuClassifier
)

value_class = Y_P[0, 0]

ypos = (np.zeros(Y_P.shape[0]) + 1).astype(int)
yneg = np.zeros(Y_U.shape[0]).astype(int)  # - 1
ytest = np.zeros(Y_T.shape[0]).astype(int)
ytest[np.where(Y_T[:, 0] == value_class)] += 1

x_train = np.concatenate([X_P, X_U], axis=0)
labels_train = np.concatenate([Y_P, Y_U], axis=0)
y_train = np.concatenate([ypos, yneg], axis=0)

x_train, labels_train, y_train = shuffle(x_train, labels_train, y_train, random_state=2)

estimator = RandomForestClassifier(
    n_estimators=500,
    criterion='gini',
    bootstrap=True,
    n_jobs=7)

estimator_ = ElkanotoPuClassifier(estimator)
estimator_.fit(x_train, y_train)
y_pred = estimator_.predict(X_T)
y_pred[y_pred>0.5] = 1
y_pred[y_pred<=0.5] = 0
plot_conf_matrix(y_t,y_pred)
f1_score(y_t,y_pred, average='weighted')
cohen_kappa_score(y_t,y_pred)

f1_score(y_t,preds, average='weighted')
cohen_kappa_score(y_t,preds)
plot_conf_matrix(y_t,preds)


rf.fit(np.concatenate([X_P,X_U],axis = 0),
       np.concatenate([y_p,y_u],axis = 0))
pd.DataFrame(y_t).agg('count')
dump(rf, os.path.join(path_experiment, 'PUL_RF.joblib'))


mse_rf = mean_squared_error(np.concatenate([y_p,y_u],axis = 0),
                            rf.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

print('lda')
LDA = joblib.load(os.path.join(path_experiment, "LDA_{'hold_out_ratio': 0.5}" + "_i_1_n_60.sav"))
mse_lda = mean_squared_error(np.concatenate([y_p,y_u],axis = 0),
                             LDA.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

print('logits')
logit_l2 = joblib.load(os.path.join(path_experiment,"Logit_{'hold_out_ratio': 0.5}-l2" + "_i_1_n_60.sav"))
mse_logit_l2 = mean_squared_error(np.concatenate([y_p,y_u],axis = 0),
                                  logit_l2.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

logit_elas = joblib.load(os.path.join(path_experiment,"Logit_{'hold_out_ratio': 0.5}-elasticnet" + "_i_1_n_60.sav"))
mse_logit_elas = mean_squared_error(np.concatenate([y_p,y_u],axis = 0),
                                    logit_elas.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

print('mlp')
mlp = joblib.load(os.path.join(path_experiment,"MLP_{'hold_out_ratio': 0.5}" + "_i_1_n_60.sav"))
mse_mlp = mean_squared_error(np.concatenate([y_p,y_u],axis = 0),
                             mlp.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

ocsvm = joblib.load(os.path.join(path_experiment,"OCsvm_scale-rbf-nu_0.5" + "_i_1_n_60.sav"))

W_T = 1/mse_rf + 1/mse_lda + 1/mse_logit_l2 + 1/mse_logit_elas + 1/mse_mlp


preds = rf.predict_proba(X_T)
preds[preds>0.5] = 1
preds[preds<0.5] = 0
plot_conf_matrix(preds,y_t)
f1_score(y_t,preds)

dictionary_ocsvm = pickle.load(open(os.path.join(path + '/Models/OCsvm_scale-rbf-nu_' + str(0.5) + '/dictionary_prediction' + '.pickle'), 'rb'))
preds = dictionary_ocsvm[i][n]['ypred']
preds[preds<=0.5] = 0

plot_conf_matrix(preds,y_t)

preds = dictionary_predictions[i][n]['Ensemble']
preds[preds>0.5] = 1
preds[preds<=0.5] = 0
plot_conf_matrix(preds,y_t)