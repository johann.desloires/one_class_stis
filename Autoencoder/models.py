import tensorflow as tf

from tensorflow.keras.layers import Input, \
    Layer, Activation, Dense, Dropout, BatchNormalization, Flatten, Reshape, \
    Conv1D, LeakyReLU, ReLU, MaxPool1D, GlobalAveragePooling1D, \
    LSTM, TimeDistributed, RNN, GRU, UpSampling1D

tf.keras.backend.set_floatx('float32')

#https://colab.research.google.com/github/SachsLab/IntracranialNeurophysDL/blob/master/notebooks/05_04_betaVAE_TFP.ipynb
##########################################################################################################
# https://github.com/FlorentF9/DeepTemporalClustering/blob/master/TAE.py
# https://github.com/PINTO0309/Keras-OneClassAnomalyDetection#4-preparing-the-model

#
##########################################################################################################
###DEFINE AE####
class AutoEncoder(tf.keras.Model):
    def __init__(self, ndim,
                 dropout_rate=0.0,
                 hidden_activation='relu',
                 embedding_activation=None,
                 output_activation=None,
                 name='AE_MLP', **kwargs):
        super(AutoEncoder, self).__init__(name=name, **kwargs)
        # ndim = input dimension

        self.enc1 = tf.keras.layers.Dense(units=int(ndim / 2),activation=hidden_activation)
        self.bottleneck = tf.keras.layers.Dense(units=int(ndim / 4))
        self.dec1 = tf.keras.layers.Dense(units=int(ndim / 2),activation=hidden_activation)
        self.dec2 = tf.keras.layers.Dense(units=ndim,activation=output_activation)

    def call(self, inputs):
        encode = self.enc1(inputs)
        embedding = self.bottleneck(encode)
        decoder = self.dec1(embedding)  # L2 SUR EMBEDDING AVANT DECODER
        output = self.dec2(decoder)
        return output , embedding


###########################################################################
###DEFINE VAE#### https://github.com/alecGraves/BVAE-tf/blob/master/bvae/models.py
class Sampling(Layer):
    """Uses (z_mean, z_log_var) to sample z, the vector encoding a digit."""

    def call(self, inputs):
        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch, dim))
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon


class Encoder(Layer):
    def __init__(self,
                 ndim=150,
                 hidden_activation='relu',
                 embedding_activation=None,
                 name='encoder',
                 **kwargs):
        super(Encoder, self).__init__(name=name, **kwargs)
        self.dense_proj = tf.keras.layers.Dense(units=int(ndim / 2),activation=hidden_activation)
        self.dense_mean = tf.keras.layers.Dense(units=int(ndim / 4)) #,activation=embedding_activation
        self.dense_log_var = tf.keras.layers.Dense(units=int(ndim / 4)) #,activation=embedding_activation
        self.sampling = Sampling()

    def call(self, inputs, is_training=False):
        x = self.dense_proj(inputs)
        z_mean = self.dense_mean(x)
        z_log_var = self.dense_log_var(x)
        z = self.sampling((z_mean, z_log_var))
        return z_mean, z_log_var, z


class Decoder(Layer):
    """Converts z, the encoded digit vector, back into a readable digit."""

    def __init__(self,
                 ndim,
                 hidden_activation,
                 output_activation,
                 name='decoder',
                 **kwargs):
        super(Decoder, self).__init__(name=name, **kwargs)
        self.dense_proj = tf.keras.layers.Dense(units=int(ndim / 2),activation=hidden_activation)
        self.dense_output = tf.keras.layers.Dense(units=ndim,activation=output_activation)

    def call(self, inputs, is_training=False):
        x = self.dense_proj(inputs)
        return self.dense_output(x)


class VariationalAutoEncoder(tf.keras.Model):
    """Combines the encoder and decoder into an end-to-end model for training."""

    def __init__(self,
                 ndim,
                 hidden_activation,
                 embedding_activation,
                 output_activation,
                 name='autoencoder',
                 **kwargs):
        super(VariationalAutoEncoder, self).__init__(name=name, **kwargs)
        self.encoder = Encoder(ndim=ndim,
                               hidden_activation=hidden_activation,
                               embedding_activation=embedding_activation)

        self.decoder = Decoder(ndim=ndim,
                               hidden_activation=hidden_activation,
                               output_activation=output_activation)

    def call(self, inputs, is_training=False):
        # self._set_inputs(inputs)
        z_mean, z_log_var, z = self.encoder(inputs, is_training)
        reconstructed = self.decoder(z, is_training)
        # Add KL divergence regularization loss.
        kl_loss = - 0.5 * tf.reduce_mean(
            z_log_var - tf.square(z_mean) - tf.exp(z_log_var) + 1)
        self.add_loss(kl_loss)
        return reconstructed , [z_mean, z_log_var, z]


#########################################################################################################################


class GRUAutoencoder_V1(tf.keras.Model):
    def __init__(self,
                 ndim = 64,
                 input_shape = (25,6),
                 hidden_activation='tanh'):

        super(GRUAutoencoder_V1, self).__init__(name='GRUAutoencoder_V1')

        self.bottleneck = GRU(ndim, activation=hidden_activation, return_sequences=False)
        self.embedding = tf.keras.layers.RepeatVector(input_shape[0])
        self.dec = GRU(ndim, activation=hidden_activation, return_sequences=True)
        self.output_layer = tf.keras.layers.TimeDistributed(Dense(input_shape[1]))

    def call(self, inputs, is_training=False):
        bottleneck = self.bottleneck(inputs)
        embedding = self.embedding(bottleneck)
        decoder = self.dec(embedding)
        output_layer = self.output_layer(decoder)
        return output_layer


class GRUAutoencoder_V2(tf.keras.Model):
    def __init__(self,
                 ndim = 64,
                 input_shape = (25,6),
                 hidden_activation='tanh'):

        super(GRUAutoencoder_V2, self).__init__(name='GRUAutoencoder_V2')

        self.enc1 = GRU(ndim, activation=hidden_activation, return_sequences=True)
        self.bottleneck = GRU(ndim//4, activation=hidden_activation, return_sequences=False)
        self.embedding = tf.keras.layers.RepeatVector(input_shape[0])
        self.dec1 = GRU(ndim//4, activation=hidden_activation, return_sequences=True)
        self.dec2 = GRU(ndim, activation=hidden_activation, return_sequences=True)
        self.output_layer = tf.keras.layers.TimeDistributed(Dense(input_shape[1]))

    def call(self, inputs, is_training=False):
        encode = self.enc1(inputs)
        bottleneck = self.bottleneck(encode)
        embedding = self.embedding(bottleneck)
        decoder = self.dec1(embedding)
        decoder2 = self.dec2(decoder)
        output_layer = self.output_layer(decoder2)
        return output_layer , bottleneck



class GRUAutoencoder_V3(tf.keras.Model):
    def __init__(self,
                 ndim = 64,
                 input_shape = (25,6),
                 hidden_activation='relu'):

        super(GRUAutoencoder_V3, self).__init__(name='GRUAutoencoder_V3')

        self.enc1 = GRU(ndim, activation=hidden_activation, return_sequences=True)
        self.bn1 = BatchNormalization()
        self.bottleneck = GRU(ndim//4, activation=hidden_activation, return_sequences=False)
        self.bn2 = BatchNormalization()
        self.embedding = tf.keras.layers.RepeatVector(input_shape[0])
        self.dec1 = GRU(ndim//4, activation=hidden_activation, return_sequences=True)
        self.bn3 = BatchNormalization()
        self.dec2 = GRU(ndim, activation=hidden_activation, return_sequences=True)
        self.bn4 = BatchNormalization()
        self.output_layer = tf.keras.layers.TimeDistributed(Dense(input_shape[1]))

    def call(self, inputs, is_training=False):
        encode = self.enc1(inputs)
        bn = self.bn1(encode, is_training)
        bottleneck = self.bottleneck(bn)
        bn = self.bn2(bottleneck, is_training)
        embedding = self.embedding(bn)
        decoder = self.dec1(embedding)
        bn = self.bn3(decoder, is_training)
        decoder2 = self.dec2(bn)
        bn = self.bn4(decoder2, is_training)
        output_layer = self.output_layer(bn)
        return output_layer




class GRUVariationalAutoencoder_V1(tf.keras.Model):
    def __init__(self,
                 ndim = 64,
                 input_shape = (25,6),
                 hidden_activation='tanh',
                 kernel_initializer = 'glorot_uniform'):

        super(GRUVariationalAutoencoder_V1, self).__init__(name='GRUVariationalAutoencoder_V1')

        self.bottleneck = GRU(ndim, activation=hidden_activation, return_sequences=False,
                              kernel_initializer=kernel_initializer)
        self.dense_mean = tf.keras.layers.Dense(units=ndim) #,activation=embedding_activation
        self.dense_log_var = tf.keras.layers.Dense(units=ndim) #,activation=embedding_activation
        self.sampling = Sampling()
        self.embedding = tf.keras.layers.RepeatVector(input_shape[0])
        self.dec1 = GRU(ndim, activation=hidden_activation, return_sequences=True,
                        kernel_initializer=kernel_initializer)
        self.output_layer = tf.keras.layers.TimeDistributed(Dense(input_shape[1]))

    def call(self, inputs, is_training=False):
        bottleneck = self.bottleneck(inputs)
        z_mean = self.dense_mean(bottleneck)
        z_log_var = self.dense_log_var(bottleneck)
        z = self.sampling((z_mean, z_log_var))
        embedding = self.embedding(z)
        decoder = self.dec1(embedding)
        output_layer = self.output_layer(decoder)
        return output_layer



class GRUVariationalAutoencoder(tf.keras.Model):
    def __init__(self,
                 ndim = 64,
                 input_shape = (25,6),
                 hidden_activation='tanh',
                 kernel_initializer = 'glorot_uniform'):

        super(GRUVariationalAutoencoder, self).__init__(name='GRUVariationalAutoencoder')

        self.enc1 = GRU(ndim, activation=hidden_activation, return_sequences=True,
                        kernel_initializer=kernel_initializer)
        self.bottleneck = GRU(ndim//4, activation=hidden_activation, return_sequences=False,
                              kernel_initializer=kernel_initializer)
        self.dense_mean = tf.keras.layers.Dense(units=ndim//4) #,activation=embedding_activation
        self.dense_log_var = tf.keras.layers.Dense(units=ndim//4) #,activation=embedding_activation
        self.sampling = Sampling()
        self.embedding = tf.keras.layers.RepeatVector(input_shape[0])
        self.dec1 = GRU(ndim//4, activation=hidden_activation, return_sequences=True,
                        kernel_initializer=kernel_initializer)
        self.dec2 = GRU(ndim, activation=hidden_activation, return_sequences=True,
                        kernel_initializer=kernel_initializer)
        self.output_layer = tf.keras.layers.TimeDistributed(Dense(input_shape[1]))

    def call(self, inputs, is_training=False):
        encode = self.enc1(inputs)
        bottleneck = self.bottleneck(encode)
        z_mean = self.dense_mean(bottleneck)
        z_log_var = self.dense_log_var(bottleneck)
        z = self.sampling((z_mean, z_log_var))
        embedding = self.embedding(z)
        decoder = self.dec1(embedding)
        decoder2 = self.dec2(decoder)
        output_layer = self.output_layer(decoder2)
        return output_layer



#########################################################################################################################

class EncoderCNN(Layer):

    def __init__(self,
                 hidden_activation='relu',
                 embedding_activation = None,
                 latent_dim = 32,
                 n_filters=32, kernel_size=5):

        super(EncoderCNN, self).__init__(name='CNNENCODER')
        self.block1 = Conv1D(filters = n_filters, kernel_size = kernel_size, padding='same', activation = hidden_activation)
        self.maxpool1 = MaxPool1D(2)
        self.block2 = Conv1D(filters = n_filters*2, kernel_size = kernel_size, padding='same', activation = hidden_activation)
        self.maxpool2 = MaxPool1D(2)

        self.Flatten = GlobalAveragePooling1D()

        self.dense_mean = tf.keras.layers.Dense(latent_dim) #no_activation #,embedding_activation = embedding_activation
        self.dense_log_var = tf.keras.layers.Dense(units=latent_dim) #no_activation , embedding_activation = embedding_activation

        self.sampling = Sampling()

    def call(self, inputs, is_training=False):
        x = self.block1(inputs)
        x = self.maxpool1(x)
        x = self.block2(x)
        x = self.maxpool2(x)
        x = self.Flatten(x)
        z_mean = self.dense_mean(x)
        z_log_var = self.dense_log_var(x)
        z = self.sampling((z_mean, z_log_var))
        return z_mean, z_log_var, z


class Conv1DTranspose(tf.keras.layers.Layer):
    def __init__(self, filters, kernel_size, activation = 'relu', strides=1, padding='valid'):
        super().__init__()
        self.conv2dtranspose = tf.keras.layers.Conv2DTranspose(
          filters, (kernel_size, 1), (strides, 1), padding, activation = activation
        )

    def call(self, x, is_training=False):
        x = tf.expand_dims(x, axis=2)
        x = self.conv2dtranspose(x)
        x = tf.squeeze(x, axis=2)
        return x

class DecoderCNN(Layer):
    """Converts z, the encoded digit vector, back into a readable digit."""

    def __init__(self,
                 ndim,
                 hidden_activation,
                 output_activation,
                 name='decoder',
                 **kwargs):
        super(DecoderCNN, self).__init__(name=name, **kwargs)
        self.dense_proj = tf.keras.layers.Dense(units=5*32, activation=hidden_activation)
        self.reshape = tf.keras.layers.Reshape(target_shape=(5, 32))

        #self.conv = Conv1DTranspose(filters=32, kernel_size=5, padding='same', activation='relu')
        self.conv = Conv1D(filters=32, kernel_size=5, padding='same',
                                   activation='relu')

        self.upsampling1 = UpSampling1D(2)

        self.conv2 = Conv1D(filters=64, kernel_size=4, padding='valid',
                                   activation='relu')

        self.upsampling2 = UpSampling1D(2)

        self.conv3 = Conv1D(filters=6, kernel_size=4, padding='valid',
                            activation='relu')

        self.upsampling3 = UpSampling1D(2)

        self.flat = Flatten()

        self.out = Dense(ndim, activation = output_activation)


    def call(self, inputs, is_training=False):
        x = self.dense_proj(inputs)
        x = self.reshape(x)
        x = self.conv(x)
        print('Convolution 1 has shape ' + str(x.get_shape()))
        x = self.upsampling1(x)
        print('Convolution 2 has shape ' + str(x.get_shape()))
        x = self.conv2(x)
        print('Convolution 3 has shape ' + str(x.get_shape()))
        x = self.upsampling2(x)
        print('Convolution 4 has shape ' + str(x.get_shape()))
        #x = self.conv3(x)
        #x  = self.flatten(x)
        x = self.conv3(x)
        x = self.upsampling3(x)
        print('Convolution 5 has shape ' + str(x.get_shape()))
        x= self.flat(x)
        out = self.out(x)
        return out



class VariationalAutoEncoderCNN(tf.keras.Model):
    """Combines the encoder and decoder into an end-to-end model for training."""

    def __init__(self,
                 ndim,
                 hidden_activation,
                 embedding_activation,
                 output_activation,
                 name='autoencoder'):
        super(VariationalAutoEncoderCNN, self).__init__(name=name)
        self.encoder = EncoderCNN(hidden_activation = hidden_activation, embedding_activation = embedding_activation)

        self.decoder = DecoderCNN(ndim=ndim,
                                  hidden_activation=hidden_activation,
                                  output_activation=output_activation)

    def call(self, inputs, is_training=False):
        # self._set_inputs(inputs)
        z_mean, z_log_var, z = self.encoder(inputs, is_training)
        reconstructed = self.decoder(z, is_training)
        # Add KL divergence regularization loss.
        kl_loss = - 0.5 * tf.reduce_mean(
            z_log_var - tf.square(z_mean) - tf.exp(z_log_var) + 1)
        self.add_loss(kl_loss)
        return reconstructed, [z_mean, z_log_var, z]


#https://towardsdatascience.com/time-series-of-price-anomaly-detection-with-lstm-11a12ba4f6d9

##########################################################################################################
#from tensorflow import tfa
#from tensorflow_addons.rnn import LayerNormLSTMCell

