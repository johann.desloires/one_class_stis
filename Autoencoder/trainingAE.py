import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

######################################################################################################################

import pandas as pd
import pickle
import Supervised.models as models
import time
import tensorflow as tf
import numpy as np
from sklearn.utils import shuffle
import os
from tqdm import tqdm
tf.keras.backend.set_floatx('float32')

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
# print(tf.test.is_gpu_available())

tf.keras.backend.set_floatx('float32')

#######################################################################################################################################
def train_info(model, checkpoint_path, epoch, train_loss, valid_loss, best_loss, elapsed):
    '''
    Output of training step
    Save model if accuracy improves
    '''
    if valid_loss is None:
        valid_loss = train_loss
    print(
        f'Epoch {epoch + 1}, Loss: {round(train_loss,6)},  Valid Loss: {round(valid_loss,6)}, Time: {elapsed}')
    if valid_loss < best_loss:
        model.save(checkpoint_path, save_format='tf', overwrite=True)
        model.save_weights(os.path.join(checkpoint_path, 'model'))
        print(f' Loss improved from {round(best_loss,6)} to {round(valid_loss,6)}, saving to {checkpoint_path}')
        best_loss = valid_loss

    return best_loss


def app_grad():
    @tf.function
    def training_epoch(train_ds,
                       model,
                       optimizer=tf.keras.optimizers.Adam(learning_rate=10e-3),
                       loss=tf.keras.losses.MAE,
                       is_training = True):  # do f-measure

        tot_loss = 0.0
        iterations = 0.0

        # https://www.tensorflow.org/guide/effective_tf2
        for step, x_batch_train in enumerate(train_ds):
            with tf.GradientTape() as tape:

                output = model(x_batch_train,
                               training=is_training)

                loss_reduced = loss(x_batch_train,
                                    output)

            tot_loss = tf.add(tot_loss, loss_reduced)
            iterations = tf.add(iterations, 1.0)

            if is_training:
                grads = tape.gradient(loss_reduced, model.trainable_variables)
                optimizer.apply_gradients(zip(grads, model.trainable_variables))

        return tf.divide(tot_loss, iterations)

    return training_epoch



def fit_model(model,
              x_train,
              x_val = None,
              optimizer=tf.keras.optimizers.Adam(learning_rate=10e-1),
              loss=tf.keras.losses.MAE,
              nb_epoch=10,
              filepath='',
              filename=''):

    if not os.path.exists(filepath):
        os.makedirs(filepath)

    train_loss_list = []
    val_loss_list = []
    val_loss = None
    count = 0
    best_loss = float("+inf")

    for e in range(nb_epoch):
        start = time.time()
        count += 1
        x_train = shuffle(x_train)
        train_ds = tf.data.Dataset.from_tensor_slices(x_train)
        train_ds = train_ds.batch(32)
        training_epoch = app_grad()
        train_loss = training_epoch(train_ds, model, optimizer, loss, is_training = True)
        train_loss_list.append(train_loss)
        train_loss = train_loss.numpy()

        if x_val is not None:
            x_val = shuffle(x_val)
            val_ds = tf.data.Dataset.from_tensor_slices(x_val)
            val_ds = val_ds.batch(32)
            val_loss = training_epoch(val_ds, model, optimizer, loss, is_training=False)
            val_loss_list.append(val_loss)
            val_loss = val_loss.numpy()

        stop = time.time()
        elapsed = stop - start
        best_loss = train_info(model, os.path.join(filepath, filename), e, train_loss, val_loss, best_loss, elapsed)


##############################################################################################################
class AE_Reconstruction:
    def __init__(self,
                 optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                 loss=tf.keras.losses.MeanAbsoluteError(),
                 filepath=None,
                 name='MLP'):

        self.filepath = filepath
        self.optimizer = optimizer
        self.loss = loss
        self.folder_out = ''

    def epoch_training(self,
                       X_P,
                       x_val,
                       model, filename, nb_epoch=500):

        self.folder_out = os.path.join(self.filepath, filename)

        fit_model(model,
                  X_P,
                  x_val,
                  optimizer=self.optimizer,
                  loss=self.loss,
                  nb_epoch=nb_epoch,
                  filepath=self.folder_out,
                  filename='NN')


    @staticmethod
    def reshape_table(array, t = 25, n = 6):
        array = array.reshape(array.shape[0], n, t)
        array = np.moveaxis(array, 1, 2)
        return array

    def reconstruction(self,
                       X_U, Y_U, y_u,
                       input_shape = [25,6]):

        model = tf.keras.models.load_model(os.path.join(self.folder_out, 'NN'), compile=False)

        reconstruction_U = model.predict(self.reshape_table(X_U, input_shape[0],input_shape[1]), batch_size=1028)
        h = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
        res = h(self.reshape_table(X_U, input_shape[0],input_shape[1]), reconstruction_U)
        res = np.mean(res, axis=1)

        error_df = pd.DataFrame({'Reconstruction_error': res,
                                 'Huber' : res,
                                 'True_class': Y_U[:,0].tolist(),
                                 'binary' : y_u.tolist()})

        dictionary_predictions = {'Predictions': res,
                                  'Reconstruction': error_df}

        with open(os.path.join(self.folder_out, 'reconstruction_df.pickle'), 'wb') as d:
            pickle.dump(dictionary_predictions, d, protocol=pickle.HIGHEST_PROTOCOL)

        return error_df



##################################################################################################################
