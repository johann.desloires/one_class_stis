import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

########################################################################################################################
import os

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import utils as utils
from trainingAE import AE_Reconstruction
import models as models
import os
import numpy as np
from sklearn.model_selection import GroupShuffleSplit
import matplotlib.pyplot as plt
##############################################################################################################
def reshape_table(array, t = 25, n = 6):
    array = array.reshape(array.shape[0], n, t)
    array = np.moveaxis(array, 1, 2)
    return array

##############################################################################################################

dictionary_parameters_GRU = {
                         'hidden_activation': ['tanh'], #'relu',
                         'optimizer': [tf.keras.optimizers.Adam(learning_rate=10e-4)],#
                         'name_optimizer': ['Adam_10e-4'], # 'Adadelta_10e-4', 'Rmsprop10e-4', 'Rmsprop-10e-3'],
                         'name_loss': ['huber'],
                         'loss': [tf.keras.losses.Huber()]} #
optimizer= tf.keras.optimizers.Adam(learning_rate=10e-4)

def run(x_train, x_val, dictionary_parameters, filepath, ndim = 64, name = 'VAEGRU_V1_16'):
    for loss, name_loss in zip(dictionary_parameters['loss'], dictionary_parameters['name_loss']):
        for hid_act in dictionary_parameters['hidden_activation']:
            for optimizer, name_opt in zip(dictionary_parameters['optimizer'], dictionary_parameters['name_optimizer']):

                        model = models.GRUVariationalAutoencoder(hidden_activation='tanh',
                                                                 ndim = 64, input_shape=(14,6))

                        filename = name + \
                                   '_hid_' + hid_act + \
                                   '_embed_None' + \
                                   '_out_None' + \
                                   '_opt_' + name_opt + \
                                   '_loss_' + name_loss

                        ae = AE_Reconstruction(optimizer=optimizer,
                                               loss=loss,
                                               filepath=filepath)

                        ae.epoch_training(x_train,
                                          x_val,
                                          model,
                                          filename, nb_epoch=50)

                        error_df = ae.reconstruction(X_U, Y_U, y_u,input_shape=[14,6])
                        tf.keras.backend.clear_session()
                        del error_df


######################################################################################################

path_experiments = ['/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_SF_IS/Positive']
path_experiment = path_experiments[0]
n_sample = 20
i_exp = 1

for path_experiment in path_experiments:
    for n_sample in range(20,120,20):
        for i_exp in range(1,11,1):
            print(i_exp)

            X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                          n_sample=n_sample,
                                                                          i_exp=i_exp)

            output_VAE = 'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction'
            path_out = os.path.join(path_experiment, output_VAE)

            if not os.path.exists(path_out):
                os.makedirs(path_out)

            train_inds, test_inds = next(
                GroupShuffleSplit(test_size=.2, n_splits=2, random_state=0). \
                    split(X_P, groups=Y_P[:,1]))

            x_train = X_P[train_inds, :]
            x_val = X_P[test_inds, :]

            #name_model = 'VAEGRU_V2_32_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_huber'
            x_train = reshape_table(x_train, t = 14, n = 6)
            x_val = reshape_table(x_val, t = 14, n = 6)
            X_P = reshape_table(X_P, t = 14, n = 6)

            run(X_P,
                None,
                dictionary_parameters_GRU,
                path_out, ndim = 32,
                name = 'VAEGRU_V2_32')



#######################################################################################################

import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import tensorflow as tf
import utils

i_exp = 1
n_sample = 20
path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2/Forest'

X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                              n_sample=n_sample,
                                                              i_exp=i_exp)

output_VAE = 'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction'
path_out = os.path.join(path_experiment, output_VAE)
#AE
dico, filename, model = utils.read_AE_file(path_out = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2/Forest/Models/VAE/N_20_Exp_1/Reconstruction/',
                                    hidden_activation='tanh',
                                    embedding_activation='None',
                                    output_activation='None',
                                    name_optimizer='Adam_10e-4',
                                    name_loss='huber',
                                    prefix = 'VAEGRU_V2_64')

reconstruction_df = dico['Reconstruction']

plot_histogram(reconstruction_df,1000,0,column_error = 'Huber')


reconstruction_U = model.predict(reshape_table(X_U), batch_size = 1028)
h = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
res = h(reshape_table(X_U), reconstruction_U)

res = np.mean(res, axis = 1)
reconstruction_df['Huber'] = res



#AE V2
path_VAE = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')
dico2, filename, model2 = utils.read_AE_file(path_out = path_VAE,
                                    hidden_activation='tanh',
                                    embedding_activation='None',
                                    output_activation='None',
                                    name_optimizer='Adam_10e-4',
                                    name_loss='huber',
                                    prefix = 'VAEGRU_V2_64')

reconstruction_df2 = dico2['Reconstruction']

reconstruction_U = model2.predict(X_U.reshape(X_U.shape[0], 25, 6), batch_size = 1028)
h = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
res = h(X_U.reshape(X_U.shape[0], 25, 6), reconstruction_U)
res = np.mean(res, axis = 1)
reconstruction_df2['Huber'] = res


#VAE
path_VAE = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')

dico_vae, filename, model_vae = utils.read_AE_file(path_out = path_VAE,
                                    hidden_activation='tanh',
                                    embedding_activation='None',
                                    output_activation='None',
                                    name_optimizer='Adam_10e-4',
                                    name_loss='huber',
                                    prefix = 'VAEGRU_V2A_64')


reconstruction_df_vae = dico_vae['Reconstruction']
reconstruction_U = model_vae.predict(X_U.reshape(X_U.shape[0], 25, 6), batch_size = 1028)
h = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
res = h(X_U.reshape(X_U.shape[0], 25, 6), reconstruction_U)
res = np.mean(res, axis = 1)
reconstruction_df_vae['Huber'] = res

from importlib import reload
utils = reload(utils)

DF = utils.plot_histogram(reconstruction_df,
                          X_P.shape[0],1,
                          hue = 'Description',
                          column_error = 'Huber')

DF2 = utils.plot_histogram(reconstruction_df2,
                          X_P.shape[0],1,
                          hue = 'Description',
                          column_error = 'Huber')

DF2_ = utils.plot_histogram(reconstruction_df2,
                          X_P.shape[0],1,
                          hue = 'Description')


DF_vae = utils.plot_histogram(reconstruction_df_vae,X_P.shape[0],1,
                              hue = 'Description',
                              column_error = 'Huber')



#########################################################################################################################################
#########################################################################################################################################
#########################################################################################################################################
#########################################################################################################################################
import subprocess
import os

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/MeadowsUncultivated'
'''
for n_sample in [20, 40, 60, 80, 100]:
    for i_exp in range(1, 11, 1):
        try:
            path_in = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction/VAEGRU_V2A_64_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_huber/')

            path_out = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction/VAEGRU_V2_64_hid_tanh_embed_None_out_None_opt_Adam_10e-4_loss_huber/')
            os.makedirs(path_out)
            subprocess.call(["mv", path_in, path_out])
        except:
            pass
'''

#include huber in each dictionary

