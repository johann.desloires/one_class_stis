import sys
sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import streamlit as st
import os
import numpy as np
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
import utils as utils
import numpy as np
import pandas as pd
from utils import plot_histogram, read_AE_file
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import tensorflow as tf

def main():
    #################################################################################
    def reshape_table(array):
        array = np.array(array).reshape(array.shape[0], 6, 25)
        array = np.moveaxis(array, 1, 2)
        return array

    @st.cache(suppress_st_warning=True, allow_output_mutation=True)
    def read_file(model,
                  name_optimizer, name_loss,
                  path_experiment = 'CerealsOilseeds',
                  n_sample = 20, i_exp = 1, AE_type = 'VAE'):

        act_function = 'tanh'

        filename =  model + \
                   '_hid_' + act_function + \
                   '_embed_' + 'None' + \
                   '_out_' + 'None' + \
                   '_opt_' + name_optimizer + \
                   '_loss_' + name_loss

        output_path = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2/'
        path_experiment = os.path.join(output_path, path_experiment)

        dictionary, filename, model = utils.read_AE_file(path_out=os.path.join(path_experiment,'Models/' + AE_type + '/N_' + str(n_sample) + '_Exp_'+ str(i_exp) + '/Reconstruction'),
                                                          hidden_activation=act_function,
                                                          embedding_activation='None',
                                                          output_activation='None',
                                                          name_optimizer=name_optimizer,
                                                          name_loss=name_loss,
                                                          prefix=model)

        X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                      n_sample=n_sample,
                                                                      i_exp=i_exp)

        reconstruction_df = dictionary['Reconstruction']

        if 'Huber' not in list(reconstruction_df.columns):
            reconstruction_U = model.predict(reshape_table(X_U), batch_size=1028)
            h = tf.keras.losses.MeanSquaredError(reduction=tf.keras.losses.Reduction.NONE)
            res = h(reshape_table(X_U), reconstruction_U)
            res = np.mean(res, axis=1)
            reconstruction_df['Huber'] = res

        n = int(X_P.shape[0])
        del X_P, X_T, Y_T, y_t, X_U

        return reconstruction_df, filename, n , Y_U, y_u



    st.sidebar.title("Navigation")

    AE_type = st.sidebar.selectbox(label="AE_type",
                                   options=(
                                       'VAE', 'AE'))

    path_experiment = st.sidebar.selectbox(label="Class",
                                           options= ('CerealsOilseeds', 'Forest','Built',
                                                     'MeadowsUncultivated', 'Fodder',
                                                     'Market gardenning'))

    n_sample = st.sidebar.selectbox(label="n_sample",
                                    options=('20','40','60','80','100'))

    i_exp = st.sidebar.selectbox(label="i_exp",
                                 options=('1','3','5','7','9'))

    if AE_type == 'VAE':
        model = st.sidebar.selectbox(label="Model",
                                     options=('VAEGRU_V2_64', 'VAEGRU_V2_64', 'VAEGRU_V2_32', 'VAEGRU_V1_16','VAEGRU_V2_128'))  # VAEGRU_V2_64 = AE V2
    else:
        model = st.sidebar.selectbox(label="Model",
                                     options=('AEGRU_V1_32','AEGRU_V1_16', 'AEGRU_V2_64', 'VAEGRU_V3_32'))  # VAEGRU_V2_64 = AE V2

    name_optimizer = st.sidebar.selectbox(label="Optimizer",
                                          options=('Adam_10e-4', 'Adam_10e-3'))


    name_loss = st.sidebar.selectbox(label="Loss",
                                     options=('huber', 'mae'))

    #dropout_rate = st.sidebar.selectbox(label="Drop out", options=(0, 0.15))

    reconstruction_df, filename, n, Y_U, y_u = read_file(model,
                                                         name_optimizer, name_loss,
                                                         path_experiment,
                                                         n_sample, i_exp, AE_type)

    data_load_state = st.sidebar.text("Loading " + filename)

    run_the_app(reconstruction_df, n, name_loss)


def run_the_app(reconstruction_df,n, loss):
    '''Define time series of patch'''

    class_ = st.selectbox(label="Which details?",
                        options=('binary', 'Description'))

    subclass_selected = st.multiselect('Select class',
                                       options=('Built', 'Cereals/Oilseeds ',
                                                'Forest', 'Fodder', 'Market gardenning',
                                                'Meadows/Uncultivated', 'Orchards', 'Water'),
                                       default=['Built', 'Cereals/Oilseeds ',
                                                'Forest', 'Fodder', 'Market gardenning',
                                                'Meadows/Uncultivated', 'Orchards', 'Water'])

    AE_type = 'Reconstruction_error'
    if loss == 'huber':
        AE_type = 'Huber'

    st.set_option('deprecation.showPyplotGlobalUse', False)
    beta = st.slider('Beta for Chebyshev', 0.0, 2.0, 1.0, 0.1)
    count = plot_histogram(reconstruction_df, n, beta, subclass_selected,class_,AE_type)
    st.pyplot()

    st.write('Reliable Negative')
    st.table(count)


main()
