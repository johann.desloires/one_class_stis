import os
import numpy as np
import pandas as pd
import pickle
import tensorflow as tf
import matplotlib.pyplot as plt
import seaborn as sns

def load_data(path_experiment, n_sample, i_exp):
    case = str(n_sample) + '_P/'
    path_to_read = os.path.join(path_experiment, case + str(i_exp))

    X_P = np.load(os.path.join(path_to_read, 'X_P.npy'), mmap_mode='r')
    Y_P = np.load(os.path.join(path_to_read, 'Y_P.npy'), mmap_mode='r')
    value_class = Y_P[0, 0]
    y_p = (np.zeros(Y_P.shape[0]) + 1).astype(int)

    X_U = np.load(os.path.join(path_to_read, 'X_U.npy'), mmap_mode='r')
    Y_U = np.load(os.path.join(path_to_read, 'Y_U.npy'), mmap_mode='r')
    y_u = np.zeros(Y_U.shape[0]).astype(int)
    y_u[np.where(Y_U[:, 0] == value_class)] += 1

    X_T = np.load(os.path.join(path_to_read, 'X_T.npy'), mmap_mode='r')
    Y_T = np.load(os.path.join(path_to_read, 'Y_T.npy'), mmap_mode='r')
    y_t = np.zeros(Y_T.shape[0]).astype(int)
    y_t[np.where(Y_T[:, 0] == value_class)] += 1

    return X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t




def read_AE_file(path_out,
                 hidden_activation, embedding_activation,
                 output_activation, name_optimizer, name_loss,
                 prefix = 'AEFeedForward'):

    filename = prefix + \
               '_hid_' + hidden_activation + \
               '_embed_' + embedding_activation + \
               '_out_' + output_activation + \
               '_opt_' + name_optimizer + \
               '_loss_' + name_loss

    dictionary = pickle.load(open(os.path.join(os.path.join(path_out,filename), 'reconstruction_df.pickle'), 'rb'))

    model = tf.keras.models.load_model(os.path.join(os.path.join(path_out, filename), 'NN'))

    return dictionary, filename, model



def plot_histogram(reconstruction_df, n, beta, Description=['Built', 'Cereals/Oilseeds ',
                                                            'Fodder', 'Forest', 'Market gardenning',
                                                            'Meadows/Uncultivated', 'Orchards', 'Water'],
                   hue='Description', column_error = 'Reconstruction_error'):

    l = pd.DataFrame([[5, 'Built'],
                      [1, 'Cereals/Oilseeds '],
                      [4, 'Fodder'],
                      [6, 'Forest'],
                      [3, 'Market gardenning'],
                      [2, 'Meadows/Uncultivated'],
                      [8, 'Orchards'],
                      [7, 'Water']], columns=['True_class', 'Description'])

    reconstruction_df = pd.merge(reconstruction_df, l, on='True_class')
    shape_U = reconstruction_df.shape[0]

    qt = np.quantile(reconstruction_df[column_error], 0.975)
    reconstruction_df = reconstruction_df[reconstruction_df[column_error] < qt]

    reconstruction_df = reconstruction_df[reconstruction_df.Description.isin(Description)]
    reconstruction_df['Class'] =  'Positive'
    reconstruction_df.loc[reconstruction_df['binary'] == 0, 'Class'] = 'Negative'
    threshold = np.mean(
        reconstruction_df[column_error]) + beta * np.std(
        reconstruction_df[column_error])

    fig, ax = plt.subplots(1, 1, figsize=(8, 15))
    plt.rcParams.update({'font.size': 15})
    g1 = sns.displot(reconstruction_df[reconstruction_df[column_error] < qt],
                     x=column_error,
                     hue='Class',
                     stat="probability", common_norm=False,
                     palette=["#e74c3c", "#2ecc71"])

    g1.set(xticklabels=['Reconstruction error'])
    g1.set(yticklabels=[])
    plt.axvline(threshold, linewidth=4, color='r')
    plt.ylabel(" ")
    plt.show()

    count_origin = reconstruction_df[['True_class', 'Description']].groupby('Description').count().reset_index()
    reconstruction_df_neg = reconstruction_df[reconstruction_df[column_error] > threshold]
    n_threshold = reconstruction_df_neg.shape[0]
    reconstruction_df_neg = reconstruction_df_neg[['True_class', 'Description']].groupby(
        'Description').count().reset_index()
    reconstruction_df_neg['Share_RN'] = reconstruction_df_neg['True_class'] / n_threshold
    reconstruction_df_neg['Share_U'] = count_origin['True_class'] / shape_U

    return reconstruction_df_neg