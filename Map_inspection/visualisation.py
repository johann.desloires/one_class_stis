import sys
sys.path.insert(0, '/home/johann/Topography/Map_inspection/')
sys.path.append('../')  #

import geopandas as gpd
import os
import numpy as np
from Map_inspection.mask_prediction import prepare_file
from prepare_raster import Vector_to_Raster
import gdal
import matplotlib.pyplot as plt
import sys


#########################################################################################


check = gdal.Open('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping/MASK_FOREST_PREDICTED_1024/rgb_3072_5120.tif').ReadAsArray()
check = np.swapaxes(check,0,2)
plt.imshow(np.clip(check[:, :, [0,1,2]], 0, 1), vmin=0, vmax=1);
plt.axis(False);
plt.show()

mask_fit_forest = gdal.Open('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping/MASK_FOREST_PREDICTED_1024/mask_3072_6144.tif').ReadAsArray()

plt.imshow(mask_fit_forest, vmin=0, vmax=1);
plt.axis(False);
plt.show()

mask_fit_cereals = gdal.Open('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping/MASK_CEREALS_PREDICTED_1024/mask_3072_6144.tif').ReadAsArray()

plt.imshow(mask_fit_cereals);
plt.axis(False);
plt.show()

plt.imshow(mask_fit_cereals-mask_fit_forest, vmin=-1, vmax=2);
plt.axis(False);
plt.colorbar()
plt.show()


###############################################################################################
##check########################################################################################
###############################################################################################

vectors = gpd.read_file('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/DATABASE_SAMPLED/bands_concatenated.shp')

list_files = [os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping/' + k + '_size_' + str(tilesize),
                           'GFstack_' + k + '_crop_2019_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif') \
              for k in index]
imgs, rescales = prepare_file(index, tilesize, width_index, height_index, i, j)


objIDS = Vector_to_Raster('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/DATABASE_SAMPLED/',
                          '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/DATABASE_SAMPLED/bands_concatenated.shp',
                          list_files[0],
                          'Object_ID','Object_ID')

mask_object = np.dstack([objIDS == np.max(objIDS)]*25)
res = []
for i in range(rescales.shape[-1]):
    x = np.ma.array(rescales[:,:,:,i],
                    mask=~mask_object)

    res.append(x.filled(fill_value=np.nan))

res = np.stack(res, axis = 3)
plt.imshow(res[:,:,10,4])
plt.show()
y = np.apply_over_axes(np.nanmean, res, (0, 1))
y = y.reshape(1,25,6)
plt.plot(y[0,:,4])
plt.show()

import pandas as pd
bands_concatenated = pd.read_csv('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/training_set_V2.csv')
def reshape_table(array, n = 6, t = 25):
    array = array.reshape(array.shape[0], n, t)
    array = np.moveaxis(array, 1, 2)
    return array


reshape_table(np.array(list(bands_concatenated.columns)[:150]).reshape(1,150))[0,:,0]

bands_concatenated = bands_concatenated[bands_concatenated.Object_ID.isin([int(np.max(objIDS))])]
meta_info = pickle.load(open('/media/DATA/johann/PUL/TileHG/Sentinel2/dictionary_meta_info_V2.pickle', 'rb'))

bands_concatenated_scaled = bands_concatenated.copy()

for band in ['B2','B3','B4','B8','NDVI','NDWI']:
    print(band)
    cols_subset = [x for x in list(bands_concatenated.columns)
                   if band in x.split('_')]
    key_mata = [x for x in meta_info.keys() if band in x.split('_')][0]

    bands_concatenated_scaled[cols_subset] = (bands_concatenated[cols_subset] - meta_info[key_mata]['q02']) / (
            meta_info[key_mata]['q98'] - meta_info[key_mata]['q02'])

    for col in cols_subset:
        bands_concatenated_scaled.loc[bands_concatenated_scaled[col]<0,col] = 0
        bands_concatenated_scaled.loc[bands_concatenated_scaled[col]>1,col] = 1

ndvi = [k for k in list(bands_concatenated_scaled.columns)
        if 'NDVI' in k.split('_')]

ndvi.append('Object_ID')
NDVI = bands_concatenated_scaled[ndvi]
reshape_table
NDVI_av = NDVI.groupby('Object_ID').agg(np.mean)
NDVI_av = NDVI_av.T

NDVI_av.plot()
plt.xticks(rotation=20)
plt.legend(title='NDVI', bbox_to_anchor=(1.05, 1), loc='upper center')
plt.show()


features = list(bands_concatenated_scaled.columns[:150])
features.append('Object_ID')
moy_df = bands_concatenated_scaled.groupby('Object_ID').agg('mean')
ndvi.remove('Object_ID')
moy_df_ndvi = moy_df[ndvi]
plt.plot(moy_df_ndvi.values[0,:])
plt.show()
t = np.array(list(bands_concatenated_scaled.columns[:150]))
t = reshape_table(t)

x_df = np.array(moy_df[list(bands_concatenated_scaled.columns[:150])]).reshape(25,6)

t, _ = classifier_GRU.predict(x_df)
plt.plot(x_df[:,8])
plt.show()


def reshape_table(array):
    array = np.array(array).reshape(array.shape[0], 6, 25)
    array = np.swapaxes(array, 1, 2)
    return array

X = np.array(bands_concatenated_scaled[bands_concatenated_scaled.columns[:150]])
X = reshape_table(X)
plt.plot(X[10,:,4])
plt.show()
t, _ = classifier_GRU.predict(X)


y[0,0,:,4]  - NDVI_av.values.flatten()
t, _ = classifier_GRU.predict(y[0,0,:,:].reshape(1,25,6))
