import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #


import geopandas as gpd
import os
import numpy as np
import gdal
import matplotlib.pyplot as plt
import earthpy.spatial as es
import joblib
from Autoencoder import utils
import pickle
import rasterio as rio


#########################################################



def grid_zone():
    extent = gpd.read_file('/media/DATA/johann/PUL/TileHG/Sentinel2/Zone_A/Zone_A.shp')
    extent['ID'] = '1'
    extent = extent.to_crs({'init': 'epsg:32631'})
    tilesize = 256
    for index in ['B2','B3','B4','B8','NDVI','NDWI']:
        es.crop_all(['/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS/GFstack_' + index + '_crop_2019.tif'],
                    '/media/DATA/johann/PUL/TileHG/Sentinel2/Zone_A', extent,
                    overwrite=True, all_touched=True, verbose=True)

        file = 'GFstack_' + index + '_crop_2019_crop.tif'
        values = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Zone_A', file))
        width = values.RasterXSize
        height = values.RasterYSize

        path_output = os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Zone_A', index + '_size_' + str(tilesize))

        if not os.path.exists(path_output):
            os.makedirs(path_output)

        os.chdir(path_output)

        for i in range(0, width, tilesize):
            for j in range(0, height, tilesize):
                gdaltranString = "gdal_translate -of GTIFF -srcwin " + str(i)+ ", "+str(j)+", "+str(tilesize)+ ", " \
                    + str(tilesize)+ " " + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Zone_A', file) + \
                                 ' ' + 'GFstack_' + index + '_crop_2019_' + str(i) + "_" + str(j) + ".tif"
                os.system(gdaltranString)

#####################################################################################################
#########################################################################################################
print('fit models')
from sklearn import svm

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2/Forest/'
X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(
    path_experiment=path_experiment,
    n_sample=100,
    i_exp=1)

'''
estimator = svm.OneClassSVM(
    nu=0.5,
    gamma='scale',
    kernel='rbf')

estimator.fit(X_P)
joblib.dump(estimator, os.path.join(path_experiment, "OCsvm_scale-rbf-nu_0.5" + "_i_1_n_100.sav"))
'''

from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score, mean_squared_error
import joblib
ocsvm = joblib.load(os.path.join(path_experiment, "OCsvm_scale-rbf-nu_0.5" + "_i_1_n_100.sav"))

########################################################
tilesize = 256
ref = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Zone_A', 'GFstack_' + 'B2' + '_crop_2019_crop.tif'))
width = ref.RasterXSize
height = ref.RasterYSize
width_index = list(range(0, width, tilesize))
height_index = list(range(0, height, tilesize))


index=['B2','B3','B4','B8','NDVI','NDWI']
#Load descriptive statistics
stats = pickle.load(open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                      '/media/DATA/johann/PUL/TileHG/Sentinel2/dictionary_meta_info_V2.pickle'),'rb'))

key_bands = [k for k in list(stats.keys())
             if np.any([x in k for x in index])]

key_bands = [k for k in key_bands if 'B8A' not in k.split('_')]

def filter_outliers(results):
    mask = results > 0
    x = np.ma.array(results,
                    mask=~mask)
    results = x.filled(fill_value=0)

    mask = results < 1
    x = np.ma.array(results,
                    mask=~mask)
    results = x.filled(fill_value=1)
    return results


def prepare_file(index, tilesize, width_index, height_index, i,j):
    list_files = [os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Zone_A/' + k + '_size_' + str(tilesize),
                               'GFstack_' + k + '_crop_2019_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif') \
                  for k in index]

    #Read arrays
    img = [np.moveaxis(gdal.Open(file).ReadAsArray(),0,-1)  for file in list_files]

    #Rescale it to apply the model
    rescale = []
    for i, feature in enumerate(index):

        key = [k for k in key_bands if feature in k][0]
        numerator = np.nan_to_num(img[i]) - stats[key]['q02']
        denominator = stats[key]['q98'] - stats[key]['q02']
        results = numerator/denominator
        results = filter_outliers(results)

        rescale.append(results)

    imgs = np.stack(img, axis = 3)
    rescales = np.stack(rescale, axis = 3)
    return imgs, rescales



def discretize_preds(results, imgs):
    results[results > 0.5] = 1
    results[results <= 0.5] = 0
    results = results.reshape(imgs.shape[0], imgs.shape[1])
    results = results.astype('int16')
    mask = imgs[:, :, 0, [0]] > 0

    x = np.ma.array(results,
                    mask=~mask)

    results = x.filled(fill_value=-1).astype('int16')
    return results


def get_batch(array, i, batch_size):
    start_id = i * batch_size
    end_id = min((i + 1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch



def apply_competitors(width_index, height_index, index,
                      output_name='MASK_FOREST_COMPETITORS', tilesize=1024):
    for i in range(len(width_index)):
        for j in range(len(height_index)):
            print(j)
            path_output = os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Zone_A/Mask_Mapping_Competitors',
                                       output_name + '_' + str(tilesize))

            imgs, rescales = prepare_file(index, tilesize, width_index, height_index, i, j)

            if not os.path.exists(path_output):
                os.makedirs(path_output)

            ref_file = os.path.join(
                '/media/DATA/johann/PUL/TileHG/Sentinel2/Zone_A/' + 'NDVI' + '_size_' + str(tilesize),
                'GFstack_' + 'NDVI' + '_crop_2019_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif')

            with rio.open(ref_file) as src:
                ras_meta = src.profile

            ras_meta['dtype'] = "int16"
            ras_meta['nodata'] = -1
            ras_meta['count'] = 1

            x_map = rescales.reshape(rescales.shape[0]*rescales.shape[1],rescales.shape[2],rescales.shape[3])
            x_map = np.moveaxis(x_map, 1, 2)
            x_map = x_map.reshape(x_map.shape[0], 25 * 6)

            # OCSVM###################################################################
            results = np.array([])
            n_batch = int(x_map.shape[0] / 1024)
            if x_map.shape[0] % 1024 != 0:
                n_batch += 1
            for i_batch in range(n_batch):
                print(i_batch)
                batch_X = get_batch(x_map, i_batch, 1024)
                results = np.append(results, ocsvm.predict(batch_X))  #

            results = discretize_preds(results, imgs)

            with rio.open(os.path.join(path_output,
                                       'mask_ocsvm_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif'),
                          'w', **ras_meta) as dst:
                dst.write(results, 1)

apply_competitors(width_index, height_index, index,
                  output_name='MASK_FOREST_COMPETITORS_100', tilesize=256)


##############################################################################################"
#MOSAIC PREDICTIONS

import os
import gdal
path = '/media/DATA/johann/PUL/TileHG/Sentinel2/Zone_B/Mask_Mapping_Competitors'
gdaltranString = 'gdalwarp ' + os.path.join(path, 'MASK_FOREST_COMPETITORS_100_256/mask_ocsvm_*.tif') + \
                 ' ' + os.path.join(path,
                                    'mask_forest_ocsvm_A.tif') + ' -srcnodata -1' + ' -dstnodata -1' + ' -ot int16' + ' -overwrite'
os.system(gdaltranString)

src_ds = gdal.Open(os.path.join(path,
                                'mask_forest_ocsvm_B.tif'))


img = src_ds.ReadAsArray()
import matplotlib.pyplot as plt
set(list(img.flatten()))
plt.imshow(img, vmin = -1, vmax = 1)
plt.colorbar()
plt.show()

topts = gdal.TranslateOptions(format='GTiff',
                              outputType=gdal.GDT_UInt16,
                              creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                              bandList=[1])  # gdal.GDT_Cyte

gdal.Translate(os.path.join(path,
                            'mask_cereals_ocsvm_A_cw.tif'), src_ds, options=topts)
                            
