import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import gdal as gdal
import os, sys
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import models
from osgeo import ogr, gdal

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession

    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.8
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)

else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import osr
from Supervised import models
import rasterio as rio
from Autoencoder import utils
import joblib
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score, mean_squared_error

path = '/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS'

#########################################################################################################
print('fit models')
from joblib import load
import os
class_ = 'Forest'

path_experiment = os.path.join('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2',
                               class_)


dictionary_predictions = {}
n = 100
i = 1
for i in range(1, 10 + 1):
    dictionary_predictions[i] = {}
    for n in range(20,120,20):
        dictionary_predictions[i][n] = {}
        dictionary_predictions[i][n]['ypred'] = {}
        dictionary_predictions[i][n]['ytest'] = np.array([])

for i in range(1, 10 + 1):
    for n in range(20,120,20):
        X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(
            path_experiment=path_experiment,
            n_sample=n,
            i_exp=i)

        dictionary_predictions[i][n]['ytest'] = y_t
        ###
        print('rf')
        rf = joblib.load(os.path.join(path_experiment, "rf_{'hold_out_ratio': 0.5}-500" + "_i_1_n_60.sav"))

        rf.fit(np.concatenate([X_P, X_U], axis=0),
               np.concatenate([np.zeros(X_P.shape[0]) + 1, np.zeros(X_U.shape[0])], axis=0))

        mse_rf = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0]) + 1, np.zeros(X_U.shape[0])], axis=0),
                                    rf.predict_proba(np.concatenate([X_P, X_U], axis=0)))
        preds_rf = rf.predict_proba(X_T)

        dictionary_predictions[i][n]['ypred']['rf'] = preds_rf

        ###
        print('lda')
        LDA = joblib.load(os.path.join(path_experiment, "LDA_{'hold_out_ratio': 0.5}" + "_i_1_n_60.sav"))
        LDA.fit(np.concatenate([X_P, X_U], axis=0),
                np.concatenate([np.zeros(X_P.shape[0]) + 1, np.zeros(X_U.shape[0])], axis=0))
        preds_lda = LDA.predict_proba(X_T)

        dictionary_predictions[i][n]['ypred']['lda'] = preds_lda
        mse_lda = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0]) + 1, np.zeros(X_U.shape[0])], axis=0),
                                     LDA.predict_proba(np.concatenate([X_P, X_U], axis=0)))
        ###
        print('logits_l2')
        logit_l2 = joblib.load(os.path.join(path_experiment, "Logit_{'hold_out_ratio': 0.5}-l2" + "_i_1_n_60.sav"))
        logit_l2.fit(np.concatenate([X_P, X_U], axis=0),
                     np.concatenate([np.zeros(X_P.shape[0]) + 1, np.zeros(X_U.shape[0])], axis=0))
        preds_l2 = logit_l2.predict_proba(X_T)

        dictionary_predictions[i][n]['ypred']['logit_l2'] = preds_l2

        mse_logit_l2 = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0]) + 1, np.zeros(X_U.shape[0])], axis=0),
                                          logit_l2.predict_proba(np.concatenate([X_P, X_U], axis=0)))
        ###
        print('logits_elas')
        logit_elas = joblib.load(os.path.join(path_experiment, "Logit_{'hold_out_ratio': 0.5}-elasticnet" + "_i_1_n_60.sav"))
        logit_elas.fit(np.concatenate([X_P, X_U], axis=0),
                       np.concatenate([np.zeros(X_P.shape[0]) + 1, np.zeros(X_U.shape[0])], axis=0))
        preds_elas = logit_elas.predict_proba(X_T)

        dictionary_predictions[i][n]['ypred']['logit_elas'] = preds_elas

        mse_logit_elas = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0]) + 1, np.zeros(X_U.shape[0])], axis=0),
                                            logit_elas.predict_proba(np.concatenate([X_P, X_U], axis=0)))

        ###
        print('MLP')
        mlp = joblib.load(os.path.join(path_experiment, "MLP_{'hold_out_ratio': 0.5}" + "_i_1_n_60.sav"))
        mlp.fit(np.concatenate([X_P, X_U], axis=0),
                np.concatenate([np.zeros(X_P.shape[0]) + 1, np.zeros(X_U.shape[0])], axis=0))
        preds_mlp = mlp.predict_proba(X_T)

        dictionary_predictions[i][n]['ypred']['mlp'] = preds_mlp

        mse_mlp = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0]) + 1, np.zeros(X_U.shape[0])], axis=0),
                                     mlp.predict_proba(np.concatenate([X_P, X_U], axis=0)))


        W_T = 1 / mse_rf + 1 / mse_lda + 1 / mse_logit_l2 + 1 / mse_logit_elas + 1 / mse_mlp

        dictionary_predictions[i][n]['ypred']['ens'] = (preds_rf / mse_rf + \
                                                        preds_lda/mse_lda + \
                                                        preds_l2/mse_logit_l2 + \
                                                        preds_elas / mse_logit_elas +\
                                                        preds_mlp / mse_mlp) / W_T

with open(os.path.join(path_experiment, 'dictionary_prediction_check.pickle'), 'wb') as d:
    pickle.dump(dictionary_predictions, d, protocol=pickle.HIGHEST_PROTOCOL)


preds = dictionary_predictions[10][100]['ypred']['ens']
preds[preds>0.5] = 1
preds[preds<0.5] = 0
cohen_kappa_score(dictionary_predictions[10][100]['ytest'],preds)
f1_score(dictionary_predictions[1][60]['ytest'],preds, average='weighted')



dico = pickle.load(open("/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2/CerealsOilseeds/dictionary_prediction_check.pickle",'rb'))

preds = dico[1][60]['ypred']['rf']
preds[preds>0.5] = 1
preds[preds<0.5] = 0
cohen_kappa_score(dico[1][60]['ytest'],preds)
f1_score(dico[1][60]['ytest'],preds, average='weighted')