import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import gdal as gdal
import os, sys
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import models
from osgeo import ogr, gdal
import subprocess
import geopandas as gpd

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession

    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.8
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)

else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import osr
from Supervised import  models
import rasterio as rio
from Autoencoder import utils
import joblib
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score, mean_squared_error
path = '/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS'



###########################
'''
df = gpd.read_file('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/DATABASE_READY/DATABASE_READY.shp')
df.columns
df['Class_ID'] = 0
df.loc[df.Class.isin(['Forest']),'Class_ID'] = 1
df = df.loc[df.Class_ID>0,]
df = df[['Class_ID','geometry']]
df = gpd.GeoDataFrame(df, geometry = 'geometry')
df.to_file('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/DATABASE_READY/SUBSET_FOREST_DATABASE_READY.shp')
df.plot('Class_ID')
plt.show()
'''

def Open_array_info(filename=''):
    """
    Opening a tiff info, for example size of array, projection and transform matrix.
    Keyword Arguments:
    filename -- 'C:/file/to/path/file.tif' or a gdal file (gdal.Open(filename))
        string that defines the input tiff file or gdal file
    """
    f = gdal.Open(r"%s" % filename)
    if f is None:
        print('%s does not exists' % filename)
    else:
        geo_out = f.GetGeoTransform()
        proj = f.GetProjection()
        size_X = f.RasterXSize
        size_Y = f.RasterYSize
        f = None
    return geo_out, proj, size_X, size_Y

def Vector_to_Raster(Dir, vector_path, reference_file, attribute, mask_name):
    """
    This function creates a raster of a vector_path file

    Dir (str): path to save the rasterized labels
    vector_path (str) : Name of the shapefile
    reference_file (str): Path to an example tiff file (all arrays will be reprojected to this example)
    attrbute (str) : column name of the attribute to rasterize
    """

    geo, proj, size_X, size_Y = Open_array_info(reference_file)

    x_min = geo[0]
    x_max = geo[0] + size_X * geo[1]
    y_min = geo[3] + size_Y * geo[5]
    y_max = geo[3]
    pixel_size = geo[1]


    Basename = os.path.basename(vector_path)
    Dir_Raster_end = os.path.join(Dir, 'MASK_' + mask_name + '.tif')

    # Open the data source and read in the extent
    source_ds = ogr.Open(vector_path)
    source_layer = source_ds.GetLayer()

    # Create the destination data source
    x_res = int(round((x_max - x_min) / pixel_size))
    y_res = int(round((y_max - y_min) / pixel_size))

    # Create tiff file
    target_ds = gdal.GetDriverByName('GTiff').Create(Dir_Raster_end,
                                                     x_res, y_res,
                                                     1, gdal.GDT_UInt16,
                                                     ['COMPRESS=LZW'])

    target_ds.SetGeoTransform(geo)
    srse = osr.SpatialReference()
    srse.SetWellKnownGeogCS(proj)
    target_ds.SetProjection(srse.ExportToWkt())
    band = target_ds.GetRasterBand(1)
    target_ds.GetRasterBand(1).SetNoDataValue(0)
    band.Fill(0)

    # Rasterize the shape and save it as band in tiff file
    gdal.RasterizeLayer(target_ds, [1], source_layer,
                        None, None, [1], ['ATTRIBUTE=' + attribute])
    target_ds = None

    # Open array
    Raster_out = Open_tiff_array(Dir_Raster_end)

    return Raster_out

def Open_tiff_array(filename='', band=''):
    """
    Opening a tiff array.
    Keyword Arguments:
    filename -- 'C:/file/to/path/file.tif' or a gdal file (gdal.Open(filename))
        string that defines the input tiff file or gdal file
    band -- integer
        Defines the band of the tiff that must be opened.
    """
    f = gdal.Open(filename)
    if f is None:
        print('%s does not exists' % filename)
    else:
        if band == '':
            band = 1
        Data = f.GetRasterBand(band).ReadAsArray()
        Data = Data.astype(np.uint16)

    return Data

##########################################################
os.path.exists( os.path.join(path, 'GFstack_' + 'B2' + '_crop_2019.tif'))

out = Vector_to_Raster('/media/DATA/johann/PUL/TileHG/Sentinel2',
                       '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/DATABASE_READY/SUBSET_CEREALS_DATABASE_READY.shp',
                       os.path.join(path, 'GFstack_' + 'B2' + '_crop_2019.tif'),
                       'Class_ID','CEREALS')

out = Vector_to_Raster('/media/DATA/johann/PUL/TileHG/Sentinel2',
                       '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/DATABASE_READY/SUBSET_FOREST_DATABASE_READY.shp',
                       os.path.join(path, 'GFstack_' + 'B2' + '_crop_2019.tif'),
                       'Class_ID','FOREST')

plt.imshow(out)
plt.show()

#########################################################
from gdalconst import *
#Split on a given grid
index = 'NDVI'
tilesize = 1024
i = 6144
j = 7168

def prepare_grid(tilesize,
                 path_source = '/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping',
                 index_list = ['B2','B3','B4','B8','NDVI','NDWI']):

    for index in index_list:
        file = 'GFstack_' + index + '_crop_2019.tif'
        values = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS', file))
        width = values.RasterXSize
        height = values.RasterYSize

        path_output = os.path.join(path_source, index + '_size_' + str(tilesize))

        if not os.path.exists(path_output):
            os.makedirs(path_output)

        os.chdir(path_output)

        for i in range(0, width, tilesize):
            for j in range(0, height, tilesize):
                gdaltranString = "gdal_translate -of GTIFF -srcwin " + str(i)+ ", "+str(j)+", "+str(tilesize)+ ", " \
                    + str(tilesize)+ " " + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS', file) + \
                                 ' ' + 'GFstack_' + index + '_crop_2019_' + str(i) + "_" + str(j) + ".tif"
                os.system(gdaltranString)

    for mask in ['CEREALS','FOREST']:
        file = 'MASK_' + mask + '.tif'
        values = gdal.Open(os.path.join(path_source , file))
        width = values.RasterXSize
        height = values.RasterYSize

        path_output = '/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping/' + mask + '_size_' + str(tilesize)

        if not os.path.exists(path_output):
            os.makedirs(path_output)

        os.chdir(path_output)

        for i in range(0, width, tilesize):
            for j in range(0, height, tilesize):
                gdaltranString = "gdal_translate -of GTIFF -srcwin " + str(i)+ ", "+str(j)+", "+str(tilesize)+ ", " \
                    + str(tilesize)+ " " + os.path.join(path_source, file) + ' ' + 'MASK_' + mask + '_' + str(i) + "_" + str(j) + ".tif"
                os.system(gdaltranString)
