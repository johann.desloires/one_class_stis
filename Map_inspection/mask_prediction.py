import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import gdal as gdal
import os, sys
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import models
from osgeo import ogr, gdal

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession

    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.8
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)

else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import osr
from Supervised import  models
import rasterio as rio
from Autoencoder import utils
import joblib
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score, mean_squared_error
path = '/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS'


##################################################################################################
#Load data


ref = gdal.Open(os.path.join(path, 'GFstack_' + 'B2' + '_crop_2019.tif'))
width = ref.RasterXSize
height = ref.RasterYSize
tilesize = 1024
width_index = list(range(0, width, tilesize))
height_index = list(range(0, height, tilesize))

#Load descriptive statistics
stats = pickle.load(open(os.path.join(path,'/media/DATA/johann/PUL/TileHG/Sentinel2/dictionary_meta_info_V2.pickle'),'rb'))

key_bands = [k for k in list(stats.keys())
             if np.any([x in k for x in ['B2','B3','B4','B8','NDVI','NDWI']])]

key_bands = [k for k in key_bands if 'B8A' not in k.split('_')]

index = ['B2','B3','B4','B8','NDVI','NDWI']
i = 7
j = 5

def filter_outliers(results):
    mask = results > 0
    x = np.ma.array(results,
                    mask=~mask)
    results = x.filled(fill_value=0)

    mask = results < 1
    x = np.ma.array(results,
                    mask=~mask)
    results = x.filled(fill_value=1)
    return results


def prepare_file(index, tilesize, width_index, height_index, i,j):
    list_files = [os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping/' + k + '_size_' + str(tilesize),
                               'GFstack_' + k + '_crop_2019_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif') \
                  for k in index]

    #Read arrays
    img = [np.moveaxis(gdal.Open(file).ReadAsArray(),0,-1)  for file in list_files]

    #Rescale it to apply the model
    rescale = []
    for i, feature in enumerate(index):

        key = [k for k in key_bands if feature in k][0]
        numerator = np.nan_to_num(img[i]) - stats[key]['q02']
        denominator = stats[key]['q98'] - stats[key]['q02']
        results = numerator/denominator
        results = filter_outliers(results)

        rescale.append(results)

    imgs = np.stack(img, axis = 3)
    rescales = np.stack(rescale, axis = 3)
    return imgs, rescales


imgs, rescales = prepare_file(index, tilesize, width_index, height_index, 7,5)
imgs.shape
#Visual inspection
plt.figure(figsize=(10, 10))
#plt.imshow(np.clip(imgs[:, :, 10, [3, 2, 1]]/10000, 0, 1), vmin = 0, vmax = 1);
plt.imshow(np.clip(rescales[:, :, 10, [3, 2, 1]], 0, 1), vmin = 0, vmax = 1);
plt.show()


#######################################################################
path = '/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS'
ref = gdal.Open(os.path.join(path, 'GFstack_' + 'B2' + '_crop_2019.tif'))

width = ref.RasterXSize
height = ref.RasterYSize
tilesize = 1024
width_index = list(range(0, width, tilesize))
height_index = list(range(0, height, tilesize))

gru_results = pickle.load(
    open(
        "/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2/Forest/Models/VAE/Consistency_TS_EMA_50_GRU_V2_64_Noisy_TS_tanh_32_32_metrics.pickle",
        'rb')
)

gru_results_model = gru_results['student']
gru_results_model = pd.concat(gru_results_model)
#gru_results_model = gru_results_model[gru_results_model.i_exp.isin([2,4,6,8,10])]
gru_results_model = gru_results_model.drop('i_exp', axis=1).groupby('Sample_size').agg('mean')
gru_results_model.reset_index(inplace=True)

#################################################################################################
# pd.concat(gru_results['student'], axis = 0)
tilesize = 1024
ref = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS', 'GFstack_' + 'B2' + '_crop_2019.tif'))
width = ref.RasterXSize
height = ref.RasterYSize
width_index = list(range(0, width, tilesize))
height_index = list(range(0, height, tilesize))
index = ['B2','B3','B4','B8','NDVI','NDWI']
path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_WWB_IS/Positive'
time_window = 10

def apply_gru_predictions(path_experiment, width_index,
                          height_index, index,
                          output_name = 'MASK_FOREST_PREDICTED', tilesize = 1024):
    for i in range(len(width_index)):
        print(i)
        for j in range(len(height_index)):
            print(j)
            path_output = os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping_WWB_IS',
                                       output_name + '_' + str(tilesize))
            if not os.path.exists(os.path.join(path_output,'mask_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif')):
                imgs, rescales = prepare_file(index, tilesize, width_index, height_index, i, j)

                imgs = imgs[:,:,:time_window,:]
                rescales = rescales[:, :, :time_window, :]

                if not os.path.exists(path_output):
                    os.makedirs(path_output)

                ref_file = os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping/' + 'NDVI' + '_size_' + str(tilesize),
                                   'GFstack_' + 'NDVI' + '_crop_2019_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif')

                with rio.open(ref_file) as src:
                    ras_meta = src.profile

                ras_meta['dtype'] = "int16"
                ras_meta['nodata'] = -1
                ras_meta['count'] = 1

                # make any necessary changes to raster properties, e.g.:
                classifier_GRU = models.FCGRU_Model(nunits=32, fcunits=32,
                                                    dropout_rate=0.2, l2_reg=0.0)

                path_student = os.path.join(path_experiment, 'Models/' + 'VAE'+ '/N_' + \
                                            str(60) + '_Exp_' + str(1) + \
                                            '/Consistency_TS_EMA_QT_32/GRU_V2_64_Noisy_TS_student_final')

                classifier_GRU.load_weights(path_student)

                x_map = rescales.reshape(rescales.shape[0] * rescales.shape[1],time_window,6)

                train_ds = tf.data.Dataset.from_tensor_slices(x_map)
                train_ds = train_ds.batch(128)

                results = np.array([])
                for step, x_batch_test in enumerate(train_ds):
                    preds, _ = classifier_GRU.predict(x_batch_test, batch_size=1000)
                    preds = np.argmax(preds, axis = 1)
                    results = np.append(results,preds)

                results = results.reshape(imgs.shape[0], imgs.shape[1])
                #plt.imshow(results)
                #plt.show()
                results = results.astype('int16')

                mask = imgs[:, :, 0, [0]] > 0

                x = np.ma.array(results,
                                mask=~mask)

                results = x.filled(fill_value=-1).astype('int16')

                with rio.open(os.path.join(path_output,'mask_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif'), 'w', **ras_meta) as dst:
                    dst.write(results, 1)
                '''
                rgb_false = np.clip(imgs[:, :, 15, [3,2,1,0]], 0, 1).astype('int16')

                ras_meta['dtype'] = "int16"
                ras_meta['count'] = 4
                ras_meta['nodata'] = 0

                with rio.open(os.path.join(path_output,'rgb_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif'), 'w', **ras_meta) as dst:
                    for id in range(rgb_false.shape[2]):
                        dst.write_band(id + 1, rgb_false[:, :, id])
                '''
apply_gru_predictions('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_WWB_IS/Positive',
                      width_index, height_index, index, output_name = 'MASK_WWB_IS_PREDICTED',tilesize = 1024)


"""

############################################################################################################"
#########################################################################################
from joblib import load
import os
class_ = 'Forest'

path_experiment = os.path.join('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2',
                               class_)

paths = [os.path.join(os.path.join(path_experiment,'Models'), k) \
         for k in os.listdir(os.path.join(path_experiment,'Models'))]

paths_ensemble = [k for k in paths
                  if str(0.5) in k.split('/')[-1]
                  and np.any([x in k for x in ['LDA', 'Logit', 'rf', "MLP"]])
                  and ~np.any([x in k for x in ['l1', 'nu']])]



def fit_competitors(paths_ensemble, i = 1, n = 100):

    for path in paths_ensemble:
        print(path)
        name_model = path.split('/')[-1]
        model = load(os.path.join(path,'model.joblib'))

        X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=os.path.join('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2',class_),
                                                                      n_sample=n,
                                                                      i_exp=i)

        model.fit(np.concatenate([X_P,X_U],axis = 0),
                  np.concatenate([y_p,y_u],axis = 0))
        filename = name_model + '_i_' + str(i) + '_n_' + str(n) + '.sav'

        joblib.dump(model, os.path.join(os.path.join('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2',class_),
                                        filename))

#fit_competitors(paths_ensemble, i = 1, n = 60)

def fit_ocsvm(path, i = 1, n = 100):
    from sklearn import svm
    import joblib
    print(path)
    name_model = "OCsvm_scale-rbf-nu_0.5"
    estimator = svm.OneClassSVM(
        nu=0.5,
        gamma='scale',
        kernel='rbf')

    X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=os.path.join('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2',class_),
                                                                  n_sample=n,
                                                                  i_exp=i)

    estimator.fit(X_P)
    filename = name_model + '_i_' + str(i) + '_n_' + str(n) + '.sav'

    joblib.dump(estimator, os.path.join(path_experiment,
                                        filename))



#fit_ocsvm(os.path.join(path_experiment, "Models/OCsvm_scale-rbf-nu_0.5"), i = 1, n = 60)

'''
#########################################################################################################
print('fit models')
import joblib
import numpy as np
i = 1
n = 80
class_ = 'CerealsOilseeds'
path_experiment = os.path.join('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2',
                               class_)
X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(
    path_experiment=path_experiment,
    n_sample=n,
    i_exp=i)


print('rf')
rf = joblib.load(os.path.join(path_experiment,"rf_{'hold_out_ratio': 0.5}-500" + "_i_1_n_60.sav"))
rf.fit(np.concatenate([X_P, X_U], axis=0),
          np.concatenate([np.zeros(X_P.shape[0]) + 1, np.zeros(X_U.shape[0])], axis=0))
joblib.dump(rf,os.path.join(path_experiment, "rf_{'hold_out_ratio': 0.5}-500" + "_i_1_n_80.sav"))

mse_rf = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])],axis=0),
                            rf.predict_proba(np.concatenate([X_P,X_U],axis = 0)))
print('lda')
LDA = joblib.load(os.path.join(path_experiment, "LDA_{'hold_out_ratio': 0.5}" + "_i_1_n_60.sav"))
LDA.fit(np.concatenate([X_P, X_U], axis=0),
        np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])],
                       axis=0))
joblib.dump(LDA,os.path.join(path_experiment, "LDA_{'hold_out_ratio': 0.5}-500" + "_i_1_n_80.sav"))

mse_lda = mean_squared_error(np.concatenate([y_p,y_u],axis = 0),
                             LDA.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

print('logits_l2')
logit_l2 = joblib.load(os.path.join(path_experiment,"Logit_{'hold_out_ratio': 0.5}-l2" + "_i_1_n_60.sav"))
logit_l2.fit(np.concatenate([X_P, X_U], axis=0),
             np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])], axis=0))
joblib.dump(logit_l2,os.path.join(path_experiment,
                                  "Logit_{'hold_out_ratio': 0.5}-l2" + "_i_1_n_80.sav"))

mse_logit_l2 = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])],axis=0),
                                  logit_l2.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

print('logits_elas')
logit_elas = joblib.load(os.path.join(path_experiment,"Logit_{'hold_out_ratio': 0.5}-elasticnet" + "_i_1_n_60.sav"))
logit_elas.fit(np.concatenate([X_P, X_U], axis=0),
               np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])],axis=0))
joblib.dump(logit_elas,os.path.join(path_experiment,
                                  "Logit_{'hold_out_ratio': 0.5}-elasticnet" + "_i_1_n_80.sav"))

mse_logit_elas = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])],axis=0),
                                    logit_elas.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

print('mlp')
mlp = joblib.load(os.path.join(path_experiment,"MLP_{'hold_out_ratio': 0.5}" + "_i_1_n_60.sav"))
mlp.fit(np.concatenate([X_P, X_U], axis=0),
          np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])],axis=0))
joblib.dump(mlp,os.path.join(path_experiment,
                                  "MLP_{'hold_out_ratio': 0.5}" + "_i_1_n_80.sav"))

mse_mlp = mean_squared_error(np.concatenate([np.zeros(X_P.shape[0])+1, np.zeros(X_U.shape[0])],axis=0),
                             mlp.predict_proba(np.concatenate([X_P,X_U],axis = 0)))

W_T = 1/mse_rf + 1/mse_lda + 1/mse_logit_l2 + 1/mse_logit_elas + 1/mse_mlp


ocsvm = joblib.load(os.path.join(path_experiment,"OCsvm_scale-rbf-nu_0.5" + "_i_1_n_60.sav"))
ocsvm.fit(X_P)
joblib.dump(mlp,os.path.join(path_experiment,
                                  "OCsvm_scale-rbf-nu_0.5" + "_i_1_n_80.sav"))

preds = ocsvm.predict(X_T)
preds[preds<0] = 0
cohen_kappa_score(y_t,preds)
'''

ref = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS', 'GFstack_' + 'B2' + '_crop_2019.tif'))
width = ref.RasterXSize
height = ref.RasterYSize
width_index = list(range(0, width, tilesize))
height_index = list(range(0, height, tilesize))


def discretize_preds(results, imgs):
    results[results > 0.5] = 1
    results[results <= 0.5] = 0
    results = results.reshape(imgs.shape[0], imgs.shape[1])
    results = results.astype('int16')
    mask = imgs[:, :, 0, [0]] > 0

    x = np.ma.array(results,
                    mask=~mask)

    results = x.filled(fill_value=-1).astype('int16')
    return results


def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch



def apply_competitors(width_index, height_index, index,
                      output_name = 'MASK_FOREST_COMPETITORS', tilesize = 1024):

    for i in range(len(width_index)-1,0,-1): #
        print(i)
        for j in range(len(height_index)-1,0,-1): #
            print(j)
            path_output = os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping_Competitors',
                                       output_name + '_' + str(tilesize))
            if not os.path.exists(path_output):
                os.makedirs(path_output)

            if not os.path.exists(os.path.join(path_output, 'mask_ocsvm_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif')):

                #if not os.path.exists(os.path.join(path_output, 'mask_ocsvm_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif')):
                imgs, rescales = prepare_file(index, tilesize, width_index, height_index, i, j)
                if set(list(imgs[:,:,:,0].flatten())) == {0.0}:
                    pass
                else:
                    ref_file = os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping/' + 'NDVI' + '_size_' + str(tilesize),
                                       'GFstack_' + 'NDVI' + '_crop_2019_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif')

                    with rio.open(ref_file) as src:
                        ras_meta = src.profile

                    ras_meta['dtype'] = "int16"
                    ras_meta['nodata'] = -1
                    ras_meta['count'] = 1
                    x_map = np.moveaxis(rescales, 2, 3)
                    x_map = x_map.reshape(x_map.shape[0] * x_map.shape[1], 25 * 6)
                    '''
                    ######################################################################
                    #RF###################################################################
    
                    results = rf.predict_proba(x_map)
                    results = discretize_preds(results, imgs)

                    with rio.open(os.path.join(path_output,'mask_rf_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif'), 'w', **ras_meta) as dst:
                        dst.write(results, 1)
                    plt.imshow(results)
                    plt.show()
                    ######################################################################
                    #ENSEMBLE###################################################################
                    results = 1/mse_rf * rf.predict_proba(x_map) + \
                              1/mse_lda * LDA.predict_proba(x_map) + \
                              1/mse_logit_l2 * logit_l2.predict_proba(x_map) + \
                              1/mse_logit_elas * logit_elas.predict_proba(x_map) + \
                              1/mse_mlp * mlp.predict_proba(x_map)
        
                    results /= W_T
                    results = discretize_preds(results, imgs)
        
                    with rio.open(os.path.join(path_output, 'mask_ens_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif'),
                                  'w', **ras_meta) as dst:
                        dst.write(results, 1)
                    '''
                    ######################################################################
                    #OCSVM###################################################################
                    results = np.array([])
                    n_batch = int(x_map.shape[0] / 1024)
                    if x_map.shape[0] % 1024 != 0:
                        n_batch += 1
                    for i_batch in range(n_batch):
                        print(i_batch)
                        batch_X = get_batch(x_map,i_batch,1024)
                        results = np.append(results, ocsvm.predict(batch_X)) #

                    results = discretize_preds(results, imgs)

                    with rio.open(os.path.join(path_output, 'mask_ocsvm_' + str(width_index[i]) + '_' + str(height_index[j]) + '.tif'),
                                  'w', **ras_meta) as dst:
                        dst.write(results, 1)

            else:
                pass


#apply_competitors(width_index, height_index, index,
#                  output_name = 'MASK_CEREALS_COMPETITORS_80', tilesize = 1024)

#########################################################################################################
#########################################################################################################
#MOSAIC PREDICTIONS
import os
import gdal
gdaltranString = 'gdalwarp ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping_Competitors/MASK_CEREALS_COMPETITORS_80_1024', 'mask_ocsvm_*.tif') + \
                 ' ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                    'mask_cereals_ocsvm_80.tif') + ' -srcnodata -1' + ' -dstnodata -1' + ' -ot int16' + ' -overwrite'
os.system(gdaltranString)

src_ds = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                'mask_cereals_ocsvm_80.tif'))
data = src_ds.ReadAsArray()
import matplotlib.pyplot as plt
plt.imshow(data, vmin = 0)
plt.show()

topts = gdal.TranslateOptions(format='GTiff',
                              outputType=gdal.GDT_UInt16,
                              creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                              bandList=[1])  # gdal.GDT_Byte

gdal.Translate(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                            'mask_cereals_ocsvm_cw_80.tif'), src_ds, options=topts)


gdaltranString = 'gdalwarp ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping_Competitors/MASK_FOREST_COMPETITORS_80_1024', 'mask_ocsvm_*.tif') + \
                 ' ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                    'mask_forest_ocsvm_80.tif') + ' -srcnodata -1' + ' -dstnodata -1' + ' -ot int16' + ' -overwrite'
os.system(gdaltranString)

src_ds = gdal.Open(
    os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                 'mask_forest_ocsvm_80.tif'))
data = src_ds.ReadAsArray()
import matplotlib.pyplot as plt
plt.imshow(data,vmin = 0)
plt.show()

src_ds = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                'mask_forest_ocsvm_80.tif'))

topts = gdal.TranslateOptions(format='GTiff',
                              outputType=gdal.GDT_UInt16,
                              creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                              bandList=[1])  # gdal.GDT_Byte

gdal.Translate(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                            'mask_forest_ocsvm_cw_80.tif'), src_ds, options=topts)




####################################################################################################################################
import os
import gdal

gdaltranString = 'gdalwarp ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping_Competitors/MASK_CEREALS_COMPETITORS_80_1024', 'mask_rf_*.tif') + \
                 ' ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                    'mask_cereals_rf_80.tif') + ' -srcnodata -1' + ' -dstnodata -1' + ' -ot int16' + ' -overwrite'
os.system(gdaltranString)

src_ds = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                'mask_cereals_rf_80.tif'))
data = src_ds.ReadAsArray()
import matplotlib.pyplot as plt
plt.imshow(data, vmin = 0)
plt.show()

topts = gdal.TranslateOptions(format='GTiff',
                              outputType=gdal.GDT_UInt16,
                              creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                              bandList=[1])  # gdal.GDT_Byte

gdal.Translate(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                            'mask_cereals_rf_cw_80.tif'), src_ds, options=topts)


gdaltranString = 'gdalwarp ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping_Competitors/MASK_FOREST_COMPETITORS_80_1024', 'mask_rf_*.tif') + \
                 ' ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                    'mask_forest_rf_80.tif') + ' -srcnodata -1' + ' -dstnodata -1' + ' -ot int16' + ' -overwrite'
os.system(gdaltranString)

src_ds = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                'mask_forest_rf_80.tif'))
data = src_ds.ReadAsArray()
import matplotlib.pyplot as plt
plt.imshow(data, vmin = 0)
plt.show()

topts = gdal.TranslateOptions(format='GTiff',
                              outputType=gdal.GDT_UInt16,
                              creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                              bandList=[1])  # gdal.GDT_Byte

gdal.Translate(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                            'mask_forest_rf_cw_80.tif'), src_ds, options=topts)




####################################################################################################################################


#########################################################################################################
#MOSAIC PREDICTIONS
import os
import gdal
gdaltranString = 'gdalwarp ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping_Competitors/MASK_CEREALS_COMPETITORS_80_1024', 'mask_ens_*.tif') + \
                 ' ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                    'mask_cereals_ens_80.tif') + ' -srcnodata -1' + ' -dstnodata -1' + ' -ot int16' + ' -overwrite'
os.system(gdaltranString)

src_ds = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                'mask_cereals_ens_80.tif'))
data = src_ds.ReadAsArray()
import matplotlib.pyplot as plt
plt.imshow(data, vmin = 0)
plt.show()

topts = gdal.TranslateOptions(format='GTiff',
                              outputType=gdal.GDT_UInt16,
                              creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                              bandList=[1])  # gdal.GDT_Byte

gdal.Translate(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                            'mask_cereals_ens_cw_80.tif'), src_ds, options=topts)


gdaltranString = 'gdalwarp ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping_Competitors/MASK_FOREST_COMPETITORS_80_1024', 'mask_ens_*.tif') + \
                 ' ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                    'mask_forest_ens_80.tif') + ' -srcnodata -1' + ' -dstnodata -1' + ' -ot int16' + ' -overwrite'
os.system(gdaltranString)

src_ds = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                'mask_forest_ens_80.tif'))
data = src_ds.ReadAsArray()
import matplotlib.pyplot as plt
plt.imshow(data, vmin = 0)
plt.show()

topts = gdal.TranslateOptions(format='GTiff',
                              outputType=gdal.GDT_UInt16,
                              creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                              bandList=[1])  # gdal.GDT_Byte

gdal.Translate(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                            'mask_forest_ens_cw_80.tif'), src_ds, options=topts)




####################################################################################################################################

import os
import gdal
import matplotlib.pyplot as plt

#############
#forest
gdaltranString = 'gdalwarp ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping/MASK_FOREST_PREDICTED_100_1024',
                                            'mask_*.tif') + \
                 ' ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                    'mask_forest_100.tif') + ' -srcnodata -1' + ' -dstnodata -1' + ' -ot int16' + ' -overwrite'
os.system(gdaltranString)

src_ds = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                'mask_forest_100.tif'))

topts = gdal.TranslateOptions(format='GTiff',
                              outputType=gdal.GDT_UInt16,
                              creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                              bandList=[1])  # gdal.GDT_Byte

gdal.Translate(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                            'mask_forest_cw_100.tif'), src_ds, options=topts)

#############
#cereals
gdaltranString = 'gdalwarp ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/Mask_Mapping/MASK_CEREALS_PREDICTED_100_1024',
                                            'mask_*.tif') + \
                 ' ' + os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                    'mask_cereals_100.tif') + ' -srcnodata -1' + ' -dstnodata -1' + ' -ot int16' + ' -overwrite'
os.system(gdaltranString)

src_ds = gdal.Open(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                                'mask_cereals_100.tif'))

topts = gdal.TranslateOptions(format='GTiff',
                              outputType=gdal.GDT_UInt16,
                              creationOptions=['COMPRESS=LZW', 'GDAL_PAM_ENABLED=NO'],
                              bandList=[1])  # gdal.GDT_Byte

gdal.Translate(os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS',
                            'mask_cereals_cw_100.tif'), src_ds, options=topts)


out = '/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS'

Image = gdal.Open(os.path.join(out,'mask_forest.tif'),
                  gdal.GA_ReadOnly).ReadAsArray()

plt.imshow(Image, vmin = 0, vmax = 1)
plt.show()


#########################################################################################################
#########################################################################################################
"""