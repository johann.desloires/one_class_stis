import geopandas as gpd
import os
import matplotlib.pyplot as plt
import pandas as pd
from shapely.geometry import Polygon, MultiPolygon
import numpy as np

#Departments
origin_path = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/'
os.chdir(origin_path)

labels = pd.read_csv('./RPG/LPIS_FR_2019.csv')
labels = labels[~labels.CUSTOM_CODE_GROUP.isin([np.nan])]
labels.columns = ['CODE_CULTU','LABEL_CULTURE','CODE_GROUP','CULTURE_GROUP_LABEL','CATEGORY','SUBCATEGORY']

rpg = gpd.read_file('./RPG/RPG_2-0_SHP_LAMB93_R76-2019/RPG_2-0_SHP_LAMB93_R76-2019/PARCELLES_GRAPHIQUES.shp')



AOI = gpd.read_file('./HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/intersection_hg_tile.shp')
AOI = AOI.dissolve(('code_insee'))

rpg = rpg.to_crs(AOI.crs)
rpg_hg = gpd.overlay(rpg,AOI)


def explode(indf):
    outdf = gpd.GeoDataFrame(columns=indf.columns)
    for idx, row in indf.iterrows():
        if type(row.geometry) == Polygon:
            outdf = outdf.append(row,ignore_index=True)
        if type(row.geometry) == MultiPolygon:
            multdf = gpd.GeoDataFrame(columns=indf.columns)
            recs = len(row.geometry)
            multdf = multdf.append([row]*recs,ignore_index=True)
            for geom in range(recs):
                multdf.loc[geom,'geometry'] = row.geometry[geom]
            outdf = outdf.append(multdf,ignore_index=True)
    return outdf

rpg_hg = explode(rpg_hg)

rpg_hg['CODE_GROUP'] = rpg_hg['CODE_GROUP'].astype(str)
labels['CODE_GROUP'] = labels['CODE_GROUP'].astype(str)

rpg_hg = pd.merge(rpg_hg,labels, on = ['CODE_CULTU','CODE_GROUP'],how = 'left')

rpg_hg.to_file(driver = 'ESRI Shapefile', filename = './RPG/RPG_HG')



##########################################################################
##########################################################################
##########################################################################
##########################################################################
##########################################################################
#rpg_ = prepare_RPG_tile()


surface_tot = pd.DataFrame(total.groupby(['CUSTOM_CODE_GROUP'])['surfaces'].agg('sum'))
surface_tot.reset_index(inplace = True)
counting_tot = pd.DataFrame(total.groupby(['Class'])['CODE_GROUP'].agg('sum'))
counting_tot.reset_index(inplace = True)
total_tot = pd.merge(surface_tot,counting_tot, on = 'Class',how = 'left')
total_tot.surfaces = total_tot.surfaces.apply(lambda x : round(x,2))
print(total_tot.to_latex(index=False))

rpg_ = pd.merge(rpg_,total[['Class','CATEGORY']], on = 'CATEGORY', how = 'left')

rpg_.to_file(driver = 'ESRI Shapefile', filename = './rpg_category_tile_hg')
##################################################################################################



associations = rpg_[['CODE_CULTU','CODE_GROUP','CATEGORY','SUB-CATEGO','surfaces']].drop_duplicates()
associations['CROP'] = ""

association_cereales = associations[associations.CATEGORY.isin(['CEREALES','OLEAGINEUX','PROTEAGINEUX','SEMENCES'])][['CODE_CULTU','CODE_GROUP','CATEGORY','CROP']].drop_duplicates()
association_cereales[association_cereales.CROP =='']

associations.loc[associations.CODE_CULTU.isin(['BTH','ORH','BDH','TTH','EPE','CZH','SGH','AVH','LIH','LIP','CHT']),['CROP']] = "Winter crops"
associations.loc[associations.CODE_CULTU.isin(['MID','MIE','MIS','TRN','SOJ','MLT',"SOG",'SRS','CGO','CGF']),['CROP']] = "Summer crops"
associations.loc[associations.CODE_CULTU.isin(['BDP','BTP','ORP','AVP','CZP']),['CROP']] = "Spring crops"
associations.loc[associations.CODE_CULTU.isin(['CAG','MCR','ORP']),['CROP']] = "Undefined cereals"

associations.loc[associations.CATEGORY.isin(['PROTEAGINEUX',' LEGUMES-GRAINS','LEGUMES-FLEURS']),['CROP']] = "Market gardening"
associations.loc[associations.CATEGORY.isin(['Herbe','GEL']),['CROP']] = "Meadows"
associations.loc[associations.CATEGORY.isin(['ARBORICULTURE','FRUITS-COQUES']),['CROP']] = "Orchards"
associations.loc[associations.CATEGORY.isin(['FOURRAGE']) & associations.CODE_CULTU.isin(['SM']) ,['CROP']] = "FOURRAGE"

associations[['CROP']].groupby('CROP').count()
associations.groupby(['CATEGORY'])['surfaces'].agg('sum')


#oleagineux + cereales
#just foret dense
#classe bati TOPO => dillatation (morphological) et erosion
#bati plus petit => 10m gommé => faire dilatation : reste connecté puis erosion : tache urbanisée
#classe eau BDD TOPO souvent 4/5 classes. OSO Jordi classification large echelle
#lecture papiers + shapefile + formation

