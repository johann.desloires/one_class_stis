import geopandas as gpd
import pandas as pd
import VectorsDPP.split_AOI as split_AOI
from shapely.geometry import *
#import seaborn as sns
import numpy as np
import shapely.wkt
import os


origin_path = '/media/DATA/johann/PUL/TileHG/'
os.chdir(origin_path)

rpg = gpd.read_file('./FinalDBPreprocessed/RPG/RPG_HG/RPG_HG.shp')
rpg.crs = {'init' : 'epsg:32631'}

rpg = rpg[['geometry','LABEL_CULT','CATEGORY', 'SUBCATEGOR']]
rpg = rpg.rename(columns = {'CATEGORY': 'Class','SUBCATEGOR' : 'Subclass'})
rpg = rpg[rpg['geometry'].is_valid]
rpg = rpg[~rpg['LABEL_CULT'].isin([np.nan, None])]

rpg['Corn'] = [1 if np.any([x in k for x in ['Corn','CORN']]) else 0 for k in rpg['LABEL_CULT'] ]
rpg[rpg['Corn'] == 1].LABEL_CULT.unique()
rpg['Sunflower'] = [1 if np.any([x in k for x in ['Sunflower','SUNFLOWER']]) else 0 for k in rpg['LABEL_CULT'] ]
rpg['Winter wheat/barley'] = [1 if np.any([x in k for x in ['Winter wheat','Winter barley']]) else 0 for k in rpg['LABEL_CULT'] ]
#rpg['Winter barley'] = [1 if np.any([x in k for x in ['Winter barley']]) else 0 for k in rpg['LABEL_CULT'] ]

###########################################################################################
hg = gpd.read_file('./FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/intersection_hg_tile.shp')
hg = hg.dissolve('code_insee')
hg = hg.to_crs('epsg:4326')
gridded_hg = split_AOI.get_gridded_boundary(hg,5000)
gridded_hg = gridded_hg[gridded_hg.check]
gridded_hg = gridded_hg[['index', 'index_x', 'index_y', 'geometry']]
gridded_hg['cell_geometry'] = gridded_hg['geometry']

rpg = gpd.sjoin(rpg,gridded_hg,how = 'left',op = 'within')

##########################################################################
rpg['Class'] = 'Unlabelled'
rpg.loc[rpg['Winter wheat/barley'] == 1,'Class'] = 'Positive'

###########################################################################################################
rpg['type'] = rpg['geometry'].apply(lambda x : type(x) == MultiPolygon)
rpg['type'].unique()


###########################################################################################

count_all = rpg[['Class', 'index']].groupby('Class','index').count()
count_all.reset_index(inplace = True)
print(count_all.to_latex())

rpg['surface'] = rpg['geometry'].apply(lambda x: x.area)
rpg = rpg[rpg.surface > 500]


def stats(data_reduce):
    count_all = data_reduce[['Class', 'index']].groupby('Class', 'index').count()
    count_all.reset_index(inplace=True)

    surface = data_reduce[['Class', 'surface']].groupby('Class').sum()
    surface.reset_index(inplace=True)
    surface['surface'] = round(surface['surface'] * 1e-6,2)

    df_stat = pd.merge(count_all, surface, on='Class', how='left')

    return df_stat

df_stat = stats(rpg)
print(df_stat.to_latex())


rpg['Count'] = 1
pivot = pd.pivot_table(rpg[['Class','index','Count']],index = 'index',columns = ['Class'],aggfunc='count')
pivot = pd.DataFrame(pivot)
pivot.columns = [k[1] for k in pivot.columns]
pivot.reset_index(inplace = True)
pivot['index'] = pivot['index'].astype(int)

res = []

for index_grid in pivot['index']:
    for Class in list(rpg.Class.unique()):
        n = pivot.loc[pivot['index'] == index_grid,[Class]].iloc[0][Class]
        N = np.sum(pivot[Class])
        if Class == 'Positive':
            alpha = 0.1
        else:
            alpha = 0.05
        theoretical_number = N / len(pivot['index'].unique()) * alpha
        if rpg.loc[(rpg['index'] == index_grid) & (rpg.Class == Class)].shape[0] > theoretical_number:
            sample = rpg.loc[(rpg['index'] == index_grid) & (rpg.Class == Class)].sample(int(theoretical_number+1))
        else:
            sample = rpg.loc[(rpg['index'] == index_grid) & (rpg.Class == Class)]

        res.append(sample)

data_reduce = pd.concat(res,axis = 0)
df_stat = stats(data_reduce)
print(df_stat.to_latex())
np.sum(df_stat['surface'])



def GetLabelID(data_reduce):
    champ_id = {}
    for index,value in zip(range(len(data_reduce.Class.unique())),data_reduce.Class.unique()):
        champ_id[value] = [str(index +1)]

    champ_id = pd.DataFrame.from_dict(champ_id).T
    champ_id.reset_index(inplace = True)
    champ_id.columns = ['Class','Class_ID']

    return champ_id

champ_id = GetLabelID(data_reduce)
data_reduce = pd.merge(data_reduce,champ_id,on = 'Class',how = 'left')
data_reduce['Object_ID'] = range(1,data_reduce.shape[0]+1)


data_reduce = data_reduce.drop(['Count'],axis = 1)
data_reduce = data_reduce.drop(['cell_geometry'],axis = 1)

data_reduce.to_file(driver = 'ESRI Shapefile', filename = './FinalDBPreprocessed/DATABASE_SAMPLED_WWB')


labelling = data_reduce[['Class','Object_ID','Class_ID','geometry']].drop_duplicates(['Class','Object_ID','Class_ID'])
data_reduce.to_csv('./FinalDBPreprocessed/DATABASE_SAMPLED/Labelling_WWB.csv',index = False)

#####################################################################################################
import os
from Sentinel2Theia import stack_data, training_set


origin_path = '/media/DATA/johann/PUL/TileHG/'
os.chdir(origin_path)
reference_file = stack_data.GetRandomTheiaFile('/media/DATA/johann/PUL/TileHG/Sentinel2/theia_download', band_name='B2')
mask_data = './FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/intersection_hg_tile.shp'
path_output = './Sentinel2/GEOTIFFS_WWB'
os.path.exists(path_output)

rasterize_labels = stack_data.RasterLabels(vector_path='./FinalDBPreprocessed/DATABASE_SAMPLED_WWB/DATABASE_SAMPLED_WWB.shp',
                                           reference_file=reference_file,
                                           extent_vector=mask_data,
                                           saving_path=path_output,
                                           ObjectID='Object_ID',
                                           LabelID='Class_ID')

rasterize_labels.rasterize_labels()

source_image_files = [os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS', k)
               for k in os.listdir('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS')
               if k.split('_')[1] in  ['B2', 'B3', 'B4', 'B8', 'NDVI', 'NDWI']]

dest_image_files = [os.path.join('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS_WWB', k)
                    for k in os.listdir('/media/DATA/johann/PUL/TileHG/Sentinel2/GEOTIFFS')
                    if k.split('_')[1] in  ['B2', 'B3', 'B4', 'B8', 'NDVI', 'NDWI']]

for src, dest in zip(source_image_files, dest_image_files):
    os.system('cp ' + src + ' ' +  dest)