# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 10:49:08 2020

@author: s999379
"""

import geopandas as gpd
import pandas as pd
import VectorsDPP.split_AOI as split_AOI
from shapely.geometry import *
#import seaborn as sns
import numpy as np
import shapely.wkt
import os
import matplotlib.pyplot as plt
import seaborn as sns


hg = gpd.read_file('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/intersection_hg_tile.shp')
diss = hg.dissolve('code_insee')
hg['geometry'].area/ 10**6
hg.plot('code_insee')
plt.show()
gridded_hg = split_AOI.get_gridded_boundary(hg,5000)
gridded_hg = gridded_hg[gridded_hg.check]
gridded_hg = gridded_hg[['index', 'index_x', 'index_y', 'geometry']]
gridded_hg['cell_geometry'] = gridded_hg['geometry']

bati = gpd.read_file('./FinalDB/BATI/BATI_GRIDDED_CONCATENATE/BATI_GRIDDED_CONCATENATE_Buffered_5_single_part.shp')
bati.crs = 'epsg:32631'
bati['Class'] = 'Built'
bati['surface'] = bati['geometry'].apply(lambda x: x.area * 1e-6)
bati['surface'] = bati['geometry'].apply(lambda x: np.log(x.area * 1e-6))
sns.displot(bati,x='surface',binwidth=1)
bati.shape


#foret
foret = gpd.read_file('./FinalDBPreprocessed/FORET/BD_FORET_READY/BD_FORET_SINGLE_PART.shp')
foret['Class'] = 'Forest'
foret['surface'] = foret['geometry'].apply(lambda x: (x.area * 1e-6))
sns.displot(foret,x='surface',binwidth=1)


#eau
eau = gpd.read_file('./FinalDBPreprocessed/SURFACE_EAU/Surface_eau_dissolved_single_part.shp')
eau['Class'] = 'Water'
eau['surface'] = eau['geometry'].apply(lambda x: np.log(x.area * 1e-6))
sns.displot(eau,x='surface',binwidth=1)
plt.show()



data_all = pd.concat([bati[['Class','geometry']],
                      foret[['Class','geometry']],
                      eau[['Class','geometry']]],axis = 0)

data_all = gpd.GeoDataFrame(data_all,geometry = data_all.geometry)


data_all_grid = gpd.sjoin(data_all,gridded_hg,how = 'left',op = 'within')


def remove_third_dimension(geom):

    if geom is None:
        return(geom)
    if geom.is_empty:
        return geom


    if isinstance(geom, Polygon):
        exterior = geom.exterior
        new_exterior = remove_third_dimension(exterior)

        interiors = geom.interiors
        new_interiors = []
        for int in interiors:
            new_interiors.append(remove_third_dimension(int))

        return Polygon(new_exterior, new_interiors)

    elif isinstance(geom, LinearRing):
        return LinearRing([xy[0:2] for xy in list(geom.coords)])

    elif isinstance(geom, LineString):
        return LineString([xy[0:2] for xy in list(geom.coords)])

    elif isinstance(geom, Point):
        return Point([xy[0:2] for xy in list(geom.coords)])

    elif isinstance(geom, MultiPoint):
        points = list(geom.geoms)
        new_points = []
        for point in points:
            new_points.append(remove_third_dimension(point))

        return MultiPoint(new_points)

    elif isinstance(geom, MultiLineString):
        lines = list(geom.geoms)
        new_lines = []
        for line in lines:
            new_lines.append(remove_third_dimension(line))

        return MultiLineString(new_lines)

    elif isinstance(geom, MultiPolygon):
        pols = list(geom.geoms)

        new_pols = []
        for pol in pols:
            new_pols.append(remove_third_dimension(pol))

        return MultiPolygon(new_pols)

    elif isinstance(geom, GeometryCollection):
        geoms = list(geom.geoms)

        new_geoms = []
        for geom in geoms:
            new_geoms.append(remove_third_dimension(geom))

        return GeometryCollection(new_geoms)

    else:
        raise RuntimeError("Currently this type of geometry is not supported: {}".format(type(geom)))

data_all_grid['geometry'] = data_all_grid['geometry'].apply(lambda x : remove_third_dimension(x))
data_all_grid = gpd.GeoDataFrame(data_all_grid,geometry = data_all_grid.geometry)
data_all_grid = data_all_grid.drop(['cell_geometry','index_right'],axis =1)

data_all_grid.to_file(driver = 'ESRI Shapefile', filename = './FinalDBPreprocessed/DATABASE_READY.shp')

######################################################################################################
######################################################################################################
######################################################################################################

#Split data according to
data_all_grid = gpd.read_file(filename = './FinalDBPreprocessed/DATABASE_READY/DATABASE_READY.shp')
data_all_grid = data_all_grid[~data_all_grid.geometry.isin([None,""])]
data_all_grid['surface'] =  data_all_grid['geometry'].apply(lambda x: x.area * 1e-6)
count = data_all_grid[['Class','geometry']].groupby('Class').count()
count.reset_index(inplace = True)
surface = data_all_grid[['Class','surface']].groupby('Class').sum()
surface.reset_index(inplace = True)
all = pd.merge(count,surface, how = 'left',on = 'Class')
summary = pd.DataFrame(['Total',np.sum(all.geometry),np.sum(all.surface)]).T
summary.columns = all.columns
all = pd.concat([all,summary],axis = 0)
print(all.to_latex(index=False))


# entre 1000 et 2000 par Class < 50 m2
# echantilloner => chaque class entre 1000 et 2000
# 2 raster : un de la class, un de l'ID
#gdalward pour couper + burn pour rasteriser

###Eau : split les polygons selon une grille de 1000m
grid_1km = split_AOI.get_gridded_boundary(hg,1000)
grid_1km = grid_1km[grid_1km.check]
grid_1km['geometry_cell'] = grid_1km['geometry']
grid_1km = grid_1km.drop(['geometry_cell'],axis =1)
grid_1km = gpd.GeoDataFrame(grid_1km,geometry = grid_1km.results)
grid_1km = grid_1km.drop(['results'],axis =1)
grid_1km.crs = eau_split.crs
grid_1km.to_file(driver = 'ESRI Shapefile', filename = './FinalDBPreprocessed/grid_1km.shp')


#####################################################################################
###SAMPLING
#####################################################################################
##Split large polygons and keep only surface > 250 m2
import os
os.chdir('/home/johann/DATA/johann/PUL/TileHG')

data_all_grid = gpd.read_file(filename = './FinalDBPreprocessed/DATABASE_READY/DATABASE_READY.shp')
data_all_grid = data_all_grid[data_all_grid['geometry'].is_valid]

mask = gpd.read_file('./FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/intersection_hg_tile.shp')



#data_all_grid = data_all_grid[~data_all_grid.geometry.isin([None,""])] not need as we apply filter over the surface
data_all_grid.columns = ['Class', 'index_5', 'index_x_5', 'index_y_5', 'geometry']
data_all_grid.Class.unique()

gridd_1km = gpd.read_file('./FinalDBPreprocessed/GRIDS/grid_1km.shp')
gridd_1km['surface'] = gridd_1km['geometry'].apply(lambda x: x.area)
#6356820090.812935
gridd_1km = gpd.overlay(gridd_1km,mask)

##
eau = data_all_grid[data_all_grid.Class == 'Water']

eau_split = gpd.overlay(eau,gridd_1km, 'intersection')
eau_split['surface'] = eau_split['geometry'].apply(lambda x: x.area)
eau_split.shape
eau_split = eau_split[eau_split.surface > 250]

##
Forest = data_all_grid[data_all_grid.Class == 'Forest']
Forest_split = gpd.overlay(Forest,gridd_1km, 'intersection')

Forest_split['surface'] = Forest_split['geometry'].apply(lambda x: x.area)
Forest_split = Forest_split[Forest_split.surface > 250]
Forest_split.shape

##
bati = data_all_grid[data_all_grid.Class == 'Built']
bati['surface'] = bati['geometry'].apply(lambda x: x.area)
bati = bati[bati['surface']>250]
bati.shape

#REMAIN

remain = data_all_grid.loc[~data_all_grid.Class.isin(['Built','Water','Forest']),:]
remain['surface'] = remain['geometry'].apply(lambda x: x.area)
remain = remain[remain['surface']>250]



####
#Champ id
def GetLabelID(data_all_grid):
    champ_id = {}
    for index,value in zip(range(len(data_all_grid.Class.unique())),data_all_grid.Class.unique()):
        champ_id[value] = [str(index +1)]

    champ_id = pd.DataFrame.from_dict(champ_id).T
    champ_id.reset_index(inplace = True)
    champ_id.columns = ['Class','Class_ID']

    return(champ_id)

cols_to_keep = ['Class', 'surface','index_5', 'index_x_5', 'index_y_5', 'geometry']

data_all = pd.concat([bati[cols_to_keep],
                      Forest_split[cols_to_keep],
                      eau_split[cols_to_keep],
                      remain[cols_to_keep]], axis = 0)

data_all.columns = ['Class', 'surface','index', 'index_x', 'index_y', 'geometry']

data_all = pd.merge(data_all,champ_id,on = 'Class', how = 'left')

data_all.loc[data_all.Class == 'Feed',['Class']] = 'Fodder'
data_all.loc[data_all.Class == 'Vegetables',['Class']] = 'Market Gardening'

data_all.to_file(driver = 'ESRI Shapefile', filename = './FinalDBPreprocessed/DATABASE_GEOM_SPLIT')
data_all.shape



########################################################################################################
########################################################################################################
########################################################################################################
