import geopandas as gpd
import pandas as pd
import VectorsDPP.split_AOI as split_AOI
from shapely.geometry import *
#import seaborn as sns
import numpy as np
import shapely.wkt
import os


origin_path = '/media/DATA/johann/PUL/TileHG/'
os.chdir(origin_path)


data_all = gpd.read_file('./FinalDBPreprocessed/DATABASE_GEOM_SPLIT/DATABASE_GEOM_SPLIT.shp')

data_all.Class.unique()
rpg = gpd.read_file('./FinalDBPreprocessed/RPG/RPG_HG/RPG_HG.shp')
rpg.crs = data_all.crs

rpg = rpg[['geometry','LABEL_CULT','CATEGORY', 'SUBCATEGOR']]
rpg = rpg.rename(columns = {'CATEGORY': 'Class','SUBCATEGOR' : 'Subclass'})
rpg = rpg[rpg['geometry'].is_valid]
rpg = rpg[~rpg['LABEL_CULT'].isin([np.nan, None])]

rpg['Corn'] = [1 if np.any([x in k for x in ['Corn','CORN']]) else 0 for k in rpg['LABEL_CULT'] ]
rpg['Sunflower'] = [1 if np.any([x in k for x in ['Sunflower','SUNFLOWER']]) else 0 for k in rpg['LABEL_CULT'] ]
rpg['Winter wheat'] = [1 if np.any([x in k for x in ['Winter wheat']]) else 0 for k in rpg['LABEL_CULT'] ]
rpg['Winter barley'] = [1 if np.any([x in k for x in ['Winter barley']]) else 0 for k in rpg['LABEL_CULT'] ]


data_all = data_all[~data_all.Class.isin(['Fodder','Feed,','Cereals/Oilseeds',
                                          'Market Gardening','Vegetables',
                                          'Meadows/Uncultivated',
                                          'Fodder', 'Orchards'])]


data_all = data_all[data_all['geometry'].is_valid]
data_all = data_all[~data_all['geometry'].is_empty]

data_all['type'] = data_all['geometry'].apply(lambda x : type(x) == MultiPolygon)

to_explode = data_all[data_all['type']]
data_all = data_all[~data_all['type']]

def explode(indf):
    outdf = gpd.GeoDataFrame(columns=indf.columns)
    for idx, row in indf.iterrows():
        if type(row.geometry) == Polygon:
            outdf = outdf.append(row,ignore_index=True)
        if type(row.geometry) == MultiPolygon:
            multdf = gpd.GeoDataFrame(columns=indf.columns)
            recs = len(row.geometry)
            multdf = multdf.append([row]*recs,ignore_index=True)
            for geom in range(recs):
                multdf.loc[geom,'geometry'] = row.geometry[geom]
            outdf = outdf.append(multdf,ignore_index=True)
    return outdf

to_explode = explode(to_explode)

data_all = pd.concat([data_all,to_explode],axis = 0)

data_all = data_all[['Class','geometry']]


data_all['Subclass'] = ''
data_all['LABEL_CULT'] = ''

data_all = data_all[rpg.columns]

data_all = pd.concat([rpg,data_all],axis = 0)
data_all = data_all[~data_all.Class.isin([None])]

data_all.Class.unique()
data_all.shape

###########################################################################################
hg = gpd.read_file('./FinalDBPreprocessed/HG_TILE_INTERSECTION/INTERSECTION_TILE_DEPARTMENT/intersection_hg_tile.shp')
hg = hg.dissolve('code_insee')
hg = hg.to_crs('epsg:4326')
gridded_hg = split_AOI.get_gridded_boundary(hg,5000)
gridded_hg = gridded_hg[gridded_hg.check]
gridded_hg = gridded_hg[['index', 'index_x', 'index_y', 'geometry']]
gridded_hg['cell_geometry'] = gridded_hg['geometry']

data_all = gpd.sjoin(data_all,gridded_hg,how = 'left',op = 'within')
data_all.index_x
###########################################################################################

count_all = data_all[['Class', 'index']].groupby('Class','index').count()
count_all.reset_index(inplace = True)
print(count_all.to_latex())

data_all['surface'] = data_all['geometry'].apply(lambda x: x.area)
data_all = data_all[data_all.surface > 500]


def stats(data_reduce):
    count_all = data_reduce[['Class', 'index']].groupby('Class', 'index').count()
    count_all.reset_index(inplace=True)

    surface = data_reduce[['Class', 'surface']].groupby('Class').sum()
    surface.reset_index(inplace=True)
    surface['surface'] = round(surface['surface'] * 1e-6,2)

    df_stat = pd.merge(count_all, surface, on='Class', how='left')

    return df_stat

df_stat = stats(data_all)
print(df_stat.to_latex())
np.sum(df_stat['surface'])


data_all['Count'] = 1
pivot = pd.pivot_table(data_all[['Class','index','Count']],index = 'index',columns = ['Class'],aggfunc='count')
pivot = pd.DataFrame(pivot)
pivot.columns = [k[1] for k in pivot.columns]
pivot.reset_index(inplace = True)
pivot['index'] = pivot['index'].astype(int)

res = []

for index_grid in pivot['index']:
    for Class in list(data_all.Class.unique()):
        n = pivot.loc[pivot['index'] == index_grid,[Class]].iloc[0][Class]
        N = np.sum(pivot[Class])
        if Class == 'Built':
            alpha = 0.05
        elif Class in ['Fodder','Market Gardening','Water']:
            alpha = 0.1#0.15
        elif Class == 'Orchards':
            alpha = 0.2#0.2
        elif Class in ['Cereals/Oilseeds','Meadows/Uncultivated','Forest']:
            alpha = 0.025
        else:
            alpha = 0.1

        theoretical_number = N / len(pivot['index'].unique()) * alpha
        if data_all.loc[(data_all['index'] == index_grid) & (data_all.Class == Class)].shape[0] > theoretical_number:
            sample = data_all.loc[(data_all['index'] == index_grid) & (data_all.Class == Class)].sample(int(theoretical_number+1))
        else:
            sample = data_all.loc[(data_all['index'] == index_grid) & (data_all.Class == Class)]

        res.append(sample)

data_reduce = pd.concat(res,axis = 0)
df_stat = stats(data_reduce)
print(df_stat.to_latex())
np.sum(df_stat['surface'])




def GetLabelID(data_reduce):
    champ_id = {}
    for index,value in zip(range(len(data_reduce.Class.unique())),data_reduce.Class.unique()):
        champ_id[value] = [str(index +1)]

    champ_id = pd.DataFrame.from_dict(champ_id).T
    champ_id.reset_index(inplace = True)
    champ_id.columns = ['Class','Class_ID']

    return(champ_id)

champ_id = GetLabelID(data_reduce)
data_reduce = pd.merge(data_reduce,champ_id,on = 'Class',how = 'left')
data_reduce['Object_ID'] = range(1,data_reduce.shape[0]+1)


data_reduce = data_reduce.drop(['Count'],axis = 1)
data_reduce = data_reduce.drop(['cell_geometry'],axis = 1)

data_reduce.to_file(driver = 'ESRI Shapefile', filename = './FinalDBPreprocessed/DATABASE_SAMPLED')


labelling = data_reduce[['Class','Object_ID','Class_ID','geometry']].drop_duplicates(['Class','Object_ID','Class_ID'])
data_reduce.to_csv('./FinalDBPreprocessed/DATABASE_SAMPLED/Labelling.csv',index = False)

#####################################################################################################

