import os
import geopandas as gpd
import pandas as pd
from shapely.geometry.polygon import Polygon
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry import *

path = "C:/Users/s999379/OneDrive - Syngenta/Documents/Topograhpy/data/FinalDB/"

os.chdir(path)

def get_intersection(grid,gpdf,ids,i):
    gpdf_grid = gpd.sjoin(grid[grid.id == ids[i]],
                          gpdf,
                          how = 'inner', op = 'intersects')
    gpdf_grid['geometry_cell'] = gpdf.geometry
    gpdf_grid = gpd.GeoDataFrame(gpdf_grid, geometry = gpdf_grid.geometry_bati)
    return(gpdf_grid)

def remove_third_dimension(geom):
    if geom.is_empty:
        return geom

    if isinstance(geom, Polygon):
        exterior = geom.exterior
        new_exterior = remove_third_dimension(exterior)

        interiors = geom.interiors
        new_interiors = []
        for int in interiors:
            new_interiors.append(remove_third_dimension(int))

        return Polygon(new_exterior, new_interiors)

    elif isinstance(geom, LinearRing):
        return LinearRing([xy[0:2] for xy in list(geom.coords)])

    elif isinstance(geom, LineString):
        return LineString([xy[0:2] for xy in list(geom.coords)])

    elif isinstance(geom, Point):
        return Point([xy[0:2] for xy in list(geom.coords)])

    elif isinstance(geom, MultiPoint):
        points = list(geom.geoms)
        new_points = []
        for point in points:
            new_points.append(remove_third_dimension(point))

        return MultiPoint(new_points)

    elif isinstance(geom, MultiLineString):
        lines = list(geom.geoms)
        new_lines = []
        for line in lines:
            new_lines.append(remove_third_dimension(line))

        return MultiLineString(new_lines)

    elif isinstance(geom, MultiPolygon):
        pols = list(geom.geoms)

        new_pols = []
        for pol in pols:
            new_pols.append(remove_third_dimension(pol))

        return MultiPolygon(new_pols)

    elif isinstance(geom, GeometryCollection):
        geoms = list(geom.geoms)

        new_geoms = []
        for geom in geoms:
            new_geoms.append(remove_third_dimension(geom))

        return GeometryCollection(new_geoms)

    else:
        raise RuntimeError("Currently this type of geometry is not supported: {}".format(type(geom)))

def explode(indf):
    outdf = gpd.GeoDataFrame(columns=indf.columns)
    for idx, row in indf.iterrows():
        if type(row.geometry) == Polygon:
            outdf = outdf.append(row,ignore_index=True)
        if type(row.geometry) == MultiPolygon:
            multdf = gpd.GeoDataFrame(columns=indf.columns)
            recs = len(row.geometry)
            multdf = multdf.append([row]*recs,ignore_index=True)
            for geom in range(recs):
                multdf.loc[geom,'geometry'] = row.geometry[geom]
            outdf = outdf.append(multdf,ignore_index=True)
    return outdf

grid = gpd.read_file('./grid_10km_intersection.shp')

grid = grid[['id','geometry']]
grid = grid.drop_duplicates('id')
#grid.plot('id')
grid.shape

bati_indifferencie = gpd.read_file("./BATI/BATI_INDIFFERENCIE/BATI_INDIFFERENCIE_32631_intersection/BATI_INDIFFERENTIEL_32631_INTERSECTION.dbf.shp")
bati_indifferencie['Class'] = 'BATI'
bati_indifferencie['geometry_bati'] = bati_indifferencie['geometry']
bati_industriel = gpd.read_file("./BATI/BATI_INDUSTRIEL/Bati_INDUSTRIEL_32363_Intersection/Bati_INDUSTRIEL_32363_Intersection.shp")
bati_industriel['Class'] = 'BATI'
bati_industriel['geometry_bati'] = bati_industriel['geometry']
cs_light = gpd.read_file("./BATI/CONSTR_LEGERE/CONSTRUCTION_LEGERE_32631_INTERSECTION/CONSTRUCTION_LEGERE_32631_INTERSECTION.shp")
cs_light['Class'] = 'BATI'
cs_light['geometry_bati'] = cs_light['geometry']

ids = list(grid['id'].unique())
res =  []

for i in range(len(ids)):
    print(i)

    grid_indifferencie =  get_intersection(grid,bati_indifferencie,ids,i)
    grid_industriel =  get_intersection(grid,bati_industriel,ids,i)
    grid_cs_light =  get_intersection(grid,cs_light,ids,i)

    grid_agg = pd.concat([grid_indifferencie[['geometry','Class']],
                          grid_industriel[['geometry','Class']],
                          grid_cs_light[['geometry','Class']]],axis = 0)

    grid_agg.dropna(inplace = True)


    if grid_agg.shape[0] == 0:
        print('No building in this zone')
        pass
    else:
        grid_agg['geometry'] = grid_agg['geometry'].apply(lambda x: remove_third_dimension(x))
        dissolving = grid_agg.dissolve('Class')
        dissolving.reset_index(inplace = True)

        output_grid = explode(dissolving)
        output_grid['id'] = ids[i]
        grid['geometry_cell'] =grid['geometry']
        grid_ = grid.drop('geometry',axis = 1)
        output = pd.merge(output_grid,grid_, on = 'id',how = 'left')

        print(output.shape)
        res.append(output)
        output = gpd.GeoDataFrame(output,geometry = output.geometry)

        output['geometry_cell'] = output.geometry_cell.astype(str)
        output.to_file(driver = 'ESRI Shapefile', filename = './BATI/BATI_GRIDDED/BATI_' + str(int(ids[i])))

res = []

for file in os.listdir('./BATI/BATI_GRIDDED/'):
    bati_file = gpd.read_file('./BATI/BATI_GRIDDED/' +file)
    res.append(bati_file)

data_final = pd.concat(res,axis = 0)
data_final.shape
data_final = gpd.GeoDataFrame(data_final,geometry = data_final.geometry)

data_final.to_file(driver = 'ESRI Shapefile', filename = './BATI/BATI_GRIDDED_CONCATENATE')


#Urban zone
import geopandas as gpd
uz = gpd.read_file('./URBAIN/URBAIN_AGGREGATED/URBAN_ZONE_32631.shp')

uz['Class'] = 'Urban Zone'

dissolving = uz.dissolve('Class')
dissolving.reset_index(inplace=True)
output_uz = explode(dissolving)

output_uz.to_file(driver = 'ESRI Shapefile', filename = './URBAIN/URBAIN_AGGREGATED_POLYGONS')

data_final = gpd.read_file( './BATI/BATI_GRIDDED_CONCATENATE/BATI_GRIDDED_CONCATENATE.shp')
data_all = pd.concat([output_uz[['Class','geometry']],data_final[['Class','geometry']]],
                      axis = 0)

data_all['Class'] = "Urban Zone"

dissolving = data_all.dissolve('Class')
dissolving.reset_index(inplace=True)
output_all = explode(dissolving)
output_all.shape

output_all.to_file(driver = 'ESRI Shapefile', filename = './URBAIN/URBAIN_AGGREGATED_BATI')



