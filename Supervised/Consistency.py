import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import time
import os
import numpy as np #do numpy 1.19.5

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

from sklearn.model_selection import GroupShuffleSplit
from tensorflow import keras as keras
import numpy as np
import os
from sklearn.utils import shuffle
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score
import random

tf.keras.backend.set_floatx('float32')

from tensorflow.python.ops import array_ops
from Supervised.pretraining import PreTraining
import tensorflow_addons as tfa

def focal_loss(prediction_tensor, target_tensor, weights=None, alpha=0.25, gamma=2):
    r"""Compute focal loss for predictions.
        Multi-labels Focal loss formula:
            FL = -alpha * (z-p)^gamma * log(p) -(1-alpha) * p^gamma * log(1-p)
                 ,which alpha = 0.25, gamma = 2, p = sigmoid(x), z = target_tensor.
    Args:
     prediction_tensor: A float tensor of shape [batch_size, num_anchors,
        num_classes] representing the predicted logits for each class
     target_tensor: A float tensor of shape [batch_size, num_anchors,
        num_classes] representing one-hot encoded classification targets
     weights: A float tensor of shape [batch_size, num_anchors]
     alpha: A scalar tensor for focal loss alpha hyper-parameter
     gamma: A scalar tensor for focal loss gamma hyper-parameter
    Returns:
        loss: A (scalar) tensor representing the value of the loss function
    """
    sigmoid_p = tf.nn.sigmoid(prediction_tensor)
    zeros = array_ops.zeros_like(sigmoid_p, dtype=sigmoid_p.dtype)

    # For poitive prediction, only need consider front part loss, back part is 0;
    # target_tensor > zeros <=> z=1, so poitive coefficient = z - p.
    pos_p_sub = array_ops.where(target_tensor > zeros, target_tensor - sigmoid_p, zeros)

    # For negative prediction, only need consider back part loss, front part is 0;
    # target_tensor > zeros <=> z=1, so negative coefficient = 0.
    neg_p_sub = array_ops.where(target_tensor > zeros, zeros, sigmoid_p)
    per_entry_cross_ent = - alpha * (pos_p_sub ** gamma) * tf.math.log(tf.clip_by_value(sigmoid_p, 1e-8, 1.0)) \
                          - (1 - alpha) * (neg_p_sub ** gamma) * tf.math.log(tf.clip_by_value(1.0 - sigmoid_p, 1e-8, 1.0))
    return tf.reduce_sum(per_entry_cross_ent)



class Consistency:
    def __init__(self,
                 pretrained_model,
                 classifier,
                 optimizer,
                 loss,
                 input_shape,
                 filepath,
                 filename):

        self.pretrained_model = pretrained_model
        self.classifier = classifier
        self.optimizer = optimizer
        self.loss = loss
        self.input_shape = input_shape
        self.filepath = filepath
        self.filename = filename

    @staticmethod
    def get_iteration(array, batch_size):
        '''
        Function to get the number of iterations over one epoch w.r.t batch size
        '''
        n_batch = int(array.shape[0] / batch_size)
        if array.shape[0] % batch_size != 0:
            n_batch += 1
        return n_batch

    @staticmethod
    def get_batch(array, i, batch_size):
        '''
        Function to select batch of training/validation/test set
        '''
        start_id = i * batch_size
        end_id = min((i + 1) * batch_size, array.shape[0])
        batch = array[start_id:end_id]
        return batch

    def app_grad(self):
        @tf.function
        def training_epoch(train_ds,
                           alpha,
                           loss = tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                           optimizer = tf.keras.optimizers.Adam(learning_rate=10e-4),
                           EMA = False):  # do f-measure

            tot_loss = 0.0
            tot_loss_1 = 0.0
            tot_loss_2 = 0.0
            iterations = 0.0

            #https://www.tensorflow.org/guide/effective_tf2
            for step, (x_batch_train, y_batch_train, train_U_noised_A, train_U_noised_B) in enumerate(train_ds):
                print(step)
                #train_U_noised_A = tf.reshape(train_U_noised_A,[64, 25, 6])
                #train_U_noised_B = tf.reshape(train_U_noised_B,[64, 25, 6])
                with tf.GradientTape() as tape:
                    student_predictions, _ = self.classifier(x_batch_train,
                                                             training=True)

                    student_predictions_A, encoding_A = self.classifier(train_U_noised_A,
                                                                        training=True)

                    student_predictions_B, encoding_B = self.classifier(train_U_noised_B,
                                                                        training=True)

                    cost_1 = loss(y_batch_train, student_predictions)
                    cost_2 = tf.reduce_mean(tf.reduce_sum(tf.square(encoding_A-encoding_B), axis = 1))

                    cost = cost_1 + alpha * cost_2

                grads = tape.gradient(cost, self.classifier.trainable_variables)
                optimizer.apply_gradients(zip(grads, self.classifier.trainable_variables))

                tot_loss = tf.add(tot_loss,cost)
                tot_loss_1 = tf.add(tot_loss_1, cost_1)
                tot_loss_2 = tf.add(tot_loss_2, cost_2)
                iterations = tf.add(iterations,1.0)

                # Create an ExponentialMovingAverage object
                if EMA:
                    ema = tf.train.ExponentialMovingAverage(decay=0.99)
                    self.classifier.add_update(ema.apply())

            return tf.divide(tot_loss,iterations), tf.divide(tot_loss_1,iterations), tf.divide(tot_loss_2,iterations)
        return training_epoch


    def init_weights_pretrained(self, classifier, size_batch):
        self.pretrained_model.build((size_batch, self.input_shape[0], self.input_shape[1]))
        self.pretrained_model.load_weights(os.path.join(self.filepath, 'init_model/model'))

        encoder_input = tf.keras.layers.Input(shape=(self.input_shape[0], self.input_shape[1]))
        classifier._set_inputs(encoder_input)
        classifier.build((size_batch, self.input_shape[0], self.input_shape[1]))

        for i in range(len(classifier.layers) - 1):
            classifier.layers[i].set_weights(self.pretrained_model.layers[i].get_weights())

        return classifier

    @staticmethod
    def random_unlabelled_noise(X_U, shape):
        train_U = X_U[0:shape]
        train_U_noised_A = train_U * np.random.normal(1, 0.05, train_U.shape)
        train_U_noised_B = train_U * np.random.normal(1, 0.15, train_U.shape)
        return train_U_noised_A, train_U_noised_B


    def fit_model(self,
                  model_transformer,
                  x_train, y_train,
                  X_U,
                  x_test, y_test,
                  alpha,
                  nb_epoch,
                  MA = False,
                  EMA = False,
                  size_batch = 64):


        y_train = tf.keras.utils.to_categorical(y_train, num_classes=2)

        count = 0
        train_loss_list= []
        best_f1 = 0.0
        best_epoch = 0
        '''
        if X_U.shape[0] > x_train.shape[0]:
            random.seed(30)
            rdm_U = random.sample(list(range(X_U.shape[0])), int(x_train.shape[0]))
            X_U = X_U[rdm_U, :, :]
        '''
        if not model_transformer is None:
            X_U = model_transformer.predict(tf.convert_to_tensor(X_U.astype('float32')), batch_size=2048)
            x_train = model_transformer.predict(tf.convert_to_tensor(x_train.astype('float32')), batch_size=2048)
            x_test = model_transformer.predict(tf.convert_to_tensor(x_test.astype('float32')), batch_size=2048)

        opt = tf.keras.optimizers.Adam(learning_rate=10e-4)
        if MA:
            EMA = False
            opt = tfa.optimizers.MovingAverage(tf.keras.optimizers.Adam(learning_rate=10e-4))

        if not self.pretrained_model is None:
            pretraining = PreTraining(model=self.pretrained_model,
                                      input_shape=self.input_shape,
                                      optimizer=self.optimizer,
                                      loss=tf.keras.losses.MeanSquaredError(),
                                      filepath=self.filepath,
                                      filename='init_model')

            pretraining.fit_model(x_train=X_U, x_val=x_train, size_batch=size_batch, nb_epoch=30)
            self.classifier, _ = self.init_weights_pretrained(self.classifier, size_batch)
            self.classifier, _ = self.init_weights_pretrained(self.classifier, size_batch)

        for e in range(nb_epoch):
            x_train, y_train = shuffle(x_train,y_train)
            X_U  = shuffle(X_U)
            train_U_noised_A, train_U_noised_B = self.random_unlabelled_noise(X_U, x_train.shape[0])

            train_ds = tf.data.Dataset.from_tensor_slices((x_train, y_train, train_U_noised_A, train_U_noised_B))
            train_ds = train_ds.batch(size_batch)

            apply_grads = self.app_grad()

            train_loss, train_loss_1, train_loss_2 = apply_grads(train_ds = train_ds,
                                                                 alpha = alpha,
                                                                 optimizer = opt,
                                                                 EMA = EMA)

            y_pred_test, _ = self.classifier.predict(x_test,batch_size = 2048)
            y_pred_test = np.argmax(y_pred_test, axis = 1)

            print(
                "Epoch {0}: MSE loss {1} , Total loss {2}, CE loss {3}".format(
                    str(e),
                    str(train_loss_2.numpy()),#str(round(train_loss.numpy(), 6)),
                    str(round(train_loss.numpy(), 6)),
                    str(round(train_loss_1.numpy(), 6))
                ))



            train_loss_list.append(train_loss)
            count += 1
            print("\t Best f1 on the test set %f" % best_f1)
            print("\t F1 on the test set %f" % f1_score(y_test, y_pred_test, average='weighted'))
            print("\t Kappa on the test set %f" % cohen_kappa_score(y_test, y_pred_test))

            losses = dict(loss=train_loss_list,
                          epoch_stop=best_epoch
                          )

        if not self.filepath is None:
            if not os.path.exists(self.filepath):
                os.makedirs(self.filepath)

        if MA:
            opt.assign_average_vars(self.classifier.variables)

        self.classifier.save_weights(os.path.join(self.filepath, self.filename))

        import pickle
        with open(os.path.join(self.filepath, 'history.pickle'), 'wb') as d:
            pickle.dump(losses, d, protocol=pickle.HIGHEST_PROTOCOL)