import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

gpu = False
import os
if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    '''
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.8
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
    '''
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import Supervised
from Supervised import models
from Autoencoder import utils
import os
import pickle
import numpy as np
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score,matthews_corrcoef
import pandas as pd
###########################
# Teacher
metrics = {}
# metrics['teacher'] = []
metrics['model'] = []
metrics['history'] = []

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/CerealsOilseeds'
type_ = 'VAE'
folder = '/Consistency_EMA/'
name_model = 'GRU_V1_16_Noisy_TS'
n_sample = 20
i_exp = 1


#path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/Forest'

classifier_GRU = models.FCGRU_Model(nunits=32, fcunits=32, dropout_rate=0.2,  l2_reg=0.0)
n_sample = 20
i_exp = 3

for n_sample in range(20, 120, 20):  #:
    print(str(n_sample))
    for i_exp in range(1, 11, 2):
        try:
            print(i_exp)
            path_save = os.path.join(path_experiment, 'Models/' + type_ + '/N_' + \
                                     str(n_sample) + '_Exp_' + str(i_exp) + \
                                     folder)

            if os.path.exists(os.path.join(path_save, name_model + '.index')):
                X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                              n_sample=n_sample,
                                                                              i_exp=i_exp)

                path_AE = os.path.join(path_experiment,
                                       'Models/' + type_ + '/N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')
                import time
                start = time.time()
                dico_tanh, filename, model = utils.read_AE_file(path_out=path_AE,
                                                                hidden_activation='tanh',
                                                                embedding_activation='None',
                                                                output_activation='None',
                                                                name_optimizer='Adam_10e-4',
                                                                name_loss='huber',
                                                                prefix='VAEGRU_V2_64')

                predictions_T = model.predict(X_T.reshape(X_T.shape[0], 25, 6), batch_size=256)
                end = time.time()
                time_transfo = end - start
                print('time transfo ' + str(time_transfo))
                #####################################################################################
                path = os.path.join(path_experiment, 'Models/' + type_ + '/N_' + \
                                            str(n_sample) + '_Exp_' + str(i_exp) + \
                                            folder + name_model )

                # TRANSFORMED DATA
                start = time.time()

                classifier_GRU = models.FCGRU_Model(nunits=32, fcunits=32,  # 64, 32
                                                           dropout_rate=0.2, l2_reg=0.0)

                classifier_GRU.load_weights(path)

                preds, _ = classifier_GRU.predict(predictions_T,
                                                  batch_size=256)
                preds = np.argmax(preds, axis=1)
                end = time.time()
                time_model = end - start
                print('time model ' + str(time_model))

                metric = dict(f1_score=[f1_score(y_t,
                                                 preds, average='weighted')],
                              accuracy=[accuracy_score(y_t, preds)],
                              matthieu = [matthews_corrcoef(y_t, preds)],
                              kappa=[cohen_kappa_score(y_t, preds)])

                metric = pd.DataFrame(metric)
                metric['Sample_size'] = n_sample
                metric['i_exp'] = i_exp
                metric['time_transf'] = time_transfo
                metric['time_model'] = time_model
                metrics['model'].append(metric)
                print('student')
                print(metric)

        except:
            pass


import pickle
with open(os.path.join(os.path.join(path_experiment, 'Models/' + type_ + '/Consistency_MA_' + name_model + '_tanh_32_32_metrics.pickle')), 'wb') as d:
    pickle.dump(metrics, d, protocol=pickle.HIGHEST_PROTOCOL)


###########################################################################################################
path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/Market gardenning'
type_ = 'AE'
name_model = 'GRU_V1_16_Noisy_TS'
df = pickle.load(open(os.path.join(os.path.join(path_experiment, 'Models/' + type_ + '/Consistency_MA_' + name_model + '_tanh_32_32_metrics.pickle')), 'rb'))
#history = pickle.load(open(os.path.join(os.path.join(path_experiment, 'Models/AE/N_60_Exp_1/Consistency/history.pickle')), 'rb'))
df = pd.concat(df['model'], axis = 0)
res = df.groupby(['Sample_size']).agg('mean')
res.columns
res = res.drop(['i_exp','time_transf','time_model','matthieu'], axis = 1)
print(res.reset_index().to_latex())