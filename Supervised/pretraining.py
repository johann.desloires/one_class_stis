from importlib import reload
from Autoencoder import utils as utils
import os
import matplotlib.pyplot as plt

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import numpy as np
import random
from trainingAE import train_info
from sklearn.utils import shuffle


class PreTraining:
    def __init__(self, model,
                 input_shape = (25,6),
                 optimizer = tf.keras.optimizers.Adam(learning_rate=10e-3),
                 loss = tf.keras.losses.MeanSquaredError(),
                 filepath = '',
                 filename = ''):
        self.model = model
        self.input_shape = input_shape
        self.optimizer = optimizer
        self.loss = loss
        self.filepath = filepath
        self.filename = filename

    def apply_random_noise(self, x_batch):
        ts_masking = x_batch.copy()
        mask = np.zeros((ts_masking.shape[0], self.input_shape[0],), dtype=int)
        for i in range(x_batch.shape[0]):
            for j in range(x_batch.shape[1]):
                prob = random.random()
                if prob < 0.15:
                    prob /= 0.15
                    mask[i,j] = 1
                    if prob < 0.5:
                        ts_masking[i, j, :] += np.random.uniform(low=-0.5, high=0, size=(self.input_shape[1],))
                    else:
                        ts_masking[i, j, :] += np.random.uniform(low=0, high=0.5, size=(self.input_shape[1],))

        return ts_masking, mask

    def app_grad(self):
        @tf.function
        def training_epoch(train_ds,
                           is_training = True):  # do f-measure

            tot_loss = 0.0
            iterations = 0.0

            # https://www.tensorflow.org/guide/effective_tf2
            for step, (x_batch_train, mask_batch) in enumerate(train_ds):
                with tf.GradientTape() as tape:
                    output = self.model(x_batch_train,
                                        training=is_training)

                    output = tf.reshape(output,
                                        shape = tf.shape(x_batch_train))

                    loss_reduced = self.loss(x_batch_train,output) * mask_batch

                    loss_reduced = tf.reduce_mean(loss_reduced)

                tot_loss = tf.add(tot_loss, loss_reduced)
                iterations = tf.add(iterations, 1.0)

                if is_training:
                    grads = tape.gradient(loss_reduced, self.model.trainable_variables)
                    self.optimizer.apply_gradients(zip(grads, self.model.trainable_variables))

            return tf.divide(tot_loss, iterations)

        return training_epoch


    def fit_model(self,
                  x_train,
                  x_val = None,
                  size_batch = 64,
                  nb_epoch=10):

        import time
        if not os.path.exists(self.filepath):
            os.makedirs(self.filepath)

        train_loss_list = []
        val_loss_list = []
        val_loss = None
        count = 0
        best_loss = float("+inf")
        x_train = x_train.reshape(x_train.shape[0], self.input_shape[0], self.input_shape[1])
        if x_val is not None:
            x_val = shuffle(x_val)
            x_val = x_val.reshape(x_val.shape[0], self.input_shape[0], self.input_shape[1])
            ts_val_masking, mask_valid = self.apply_random_noise(x_val)

        for e in range(nb_epoch):

            start = time.time()
            count += 1
            x_train = shuffle(x_train)
            ts_masking, mask = self.apply_random_noise(x_train)
            mask = mask.astype(np.float32)
            train_ds = tf.data.Dataset.from_tensor_slices((ts_masking, mask))
            train_ds = train_ds.batch(size_batch)
            training_epoch = self.app_grad()
            train_loss = training_epoch(train_ds, is_training = True)
            train_loss_list.append(train_loss)
            train_loss = train_loss.numpy()

            if x_val is not None:
                mask_valid = mask.astype(np.float32)
                val_ds = tf.data.Dataset.from_tensor_slices((ts_val_masking, mask_valid))
                val_ds = val_ds.batch(size_batch)
                val_loss = training_epoch(val_ds, is_training=False)
                val_loss_list.append(val_loss)
                val_loss = val_loss.numpy()

            stop = time.time()
            elapsed = stop - start
            best_loss = train_info(self.model,
                                   os.path.join(self.filepath, self.filename),
                                   e,
                                   train_loss, val_loss, best_loss, elapsed)

