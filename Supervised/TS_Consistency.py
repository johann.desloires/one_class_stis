import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import time
import os
import numpy as np #do numpy 1.19.5

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

from sklearn.model_selection import GroupShuffleSplit
from tensorflow import keras as keras
import numpy as np
import os
from sklearn.utils import shuffle
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score
import random



tf.keras.backend.set_floatx('float32')

from tensorflow.python.ops import array_ops
from Supervised.pretraining import PreTraining


def focal_loss(prediction_tensor, target_tensor, weights=None, alpha=0.25, gamma=2):
    r"""Compute focal loss for predictions.
        Multi-labels Focal loss formula:
            FL = -alpha * (z-p)^gamma * log(p) -(1-alpha) * p^gamma * log(1-p)
                 ,which alpha = 0.25, gamma = 2, p = sigmoid(x), z = target_tensor.
    Args:
     prediction_tensor: A float tensor of shape [batch_size, num_anchors,
        num_classes] representing the predicted logits for each class
     target_tensor: A float tensor of shape [batch_size, num_anchors,
        num_classes] representing one-hot encoded classification targets
     weights: A float tensor of shape [batch_size, num_anchors]
     alpha: A scalar tensor for focal loss alpha hyper-parameter
     gamma: A scalar tensor for focal loss gamma hyper-parameter
    Returns:
        loss: A (scalar) tensor representing the value of the loss function
    """
    sigmoid_p = tf.nn.sigmoid(prediction_tensor)
    zeros = array_ops.zeros_like(sigmoid_p, dtype=sigmoid_p.dtype)

    # For poitive prediction, only need consider front part loss, back part is 0;
    # target_tensor > zeros <=> z=1, so poitive coefficient = z - p.
    pos_p_sub = array_ops.where(target_tensor > zeros, target_tensor - sigmoid_p, zeros)

    # For negative prediction, only need consider back part loss, front part is 0;
    # target_tensor > zeros <=> z=1, so negative coefficient = 0.
    neg_p_sub = array_ops.where(target_tensor > zeros, zeros, sigmoid_p)
    per_entry_cross_ent = - alpha * (pos_p_sub ** gamma) * tf.math.log(tf.clip_by_value(sigmoid_p, 1e-8, 1.0)) \
                          - (1 - alpha) * (neg_p_sub ** gamma) * tf.math.log(tf.clip_by_value(1.0 - sigmoid_p, 1e-8, 1.0))
    return tf.reduce_sum(per_entry_cross_ent)



class TS_Consistency:
    def __init__(self,
                 pretrained_model,
                 classifier_teacher,
                 classifier_student,
                 optimizer,
                 loss,
                 input_shape,
                 filepath,
                 filename):

        self.pretrained_model = pretrained_model
        self.classifier_teacher = classifier_teacher
        self.classifier_student = classifier_student
        self.optimizer = optimizer
        self.loss = loss
        self.input_shape = input_shape
        self.filepath = filepath
        self.filename = filename

    @staticmethod
    def get_iteration(array, batch_size):
        '''
        Function to get the number of iterations over one epoch w.r.t batch size
        '''
        n_batch = int(array.shape[0] / batch_size)
        if array.shape[0] % batch_size != 0:
            n_batch += 1
        return n_batch

    @staticmethod
    def get_batch(array, i, batch_size):
        '''
        Function to select batch of training/validation/test set
        '''
        start_id = i * batch_size
        end_id = min((i + 1) * batch_size, array.shape[0])
        batch = array[start_id:end_id]
        return batch

    @staticmethod
    def reshape_table(array):
        array = array.reshape(array.shape[0], 6, 25)
        array = np.moveaxis(array, 1, 2)
        return array

    def app_grad_teacher(self):
        @tf.function
        def training_epoch(train_ds,
                           loss = tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                           optimizer = tf.keras.optimizers.Adam(learning_rate=10e-4)):  # do f-measure

            tot_loss = 0.0
            iterations = 0.0

            #https://www.tensorflow.org/guide/effective_tf2
            for step, (x_batch_train, x_batch_U1, x_batch_U2, y_batch_train) in enumerate(train_ds): #, train_U_noised_A, train_U_noised_B
                print(step)
                with tf.GradientTape() as tape:

                    teacher_predictions, _ = self.classifier_teacher(x_batch_train,
                                                                     training=True)

                    teacher_predictions_U1, rnn1 = self.classifier_teacher(x_batch_U1,
                                                                           training=False)

                    teacher_predictions_U2, rnn2 = self.classifier_teacher(x_batch_U2,
                                                                           training=False)

                    KL = tf.keras.losses.KLDivergence()
                    cost = loss(y_batch_train, teacher_predictions) + KL(teacher_predictions_U1,
                                                                         teacher_predictions_U2)

                grads = tape.gradient(cost, self.classifier_teacher.trainable_variables)
                optimizer.apply_gradients(zip(grads, self.classifier_teacher.trainable_variables))

                tot_loss = tf.add(tot_loss,cost)
                iterations = tf.add(iterations,1.0)
                #del tape
                ema = tf.train.ExponentialMovingAverage(decay=0.99)
                self.classifier_teacher.add_update(ema.apply())
            return tf.divide(tot_loss, iterations)
        return training_epoch


    def app_grad_student(self):
        @tf.function
        def training_epoch(train_ds,
                           lambda_,
                           loss = tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                           optimizer = tf.keras.optimizers.Adam(learning_rate=10e-4)):  # do f-measure

            tot_loss = 0.0
            tot_loss_1 = 0.0
            tot_loss_2 = 0.0

            iterations = 0.0

            #https://www.tensorflow.org/guide/effective_tf2
            for step, (x_batch_train, y_batch_train,train_U, train_U_T) in enumerate(train_ds):
                teacher_predictions, _ = self.classifier_teacher(train_U_T,
                                                                 training=False)
                with tf.GradientTape() as tape:

                    student_predictions, _ = self.classifier_student(x_batch_train,
                                                                     training=True)


                    student_predictions_noisy, _ = self.classifier_student(train_U,
                                                                           training=True)

                    loss_2 = tf.keras.losses.KLDivergence()
                    cost_2 = loss_2(teacher_predictions, student_predictions_noisy)

                    cost_1 = loss(y_batch_train, student_predictions)

                    cost = cost_1 + lambda_ * cost_2

                grads = tape.gradient(cost, self.classifier_student.trainable_variables)
                optimizer.apply_gradients(zip(grads, self.classifier_student.trainable_variables))

                tot_loss = tf.add(tot_loss,cost)
                tot_loss_1 = tf.add(tot_loss_1, cost_1)
                tot_loss_2 = tf.add(tot_loss_2, cost_2)

                iterations = tf.add(iterations,1.0)
                #del tape
                ema = tf.train.ExponentialMovingAverage(decay=0.99)
                self.classifier_student.add_update(ema.apply())

            return tf.divide(tot_loss,iterations), tf.divide(tot_loss_1,iterations), tf.divide(tot_loss_2,iterations)
        return training_epoch


    def init_weights_pretrained(self, classifier, size_batch):
        self.pretrained_model.build((size_batch, self.input_shape[0], self.input_shape[1]))
        self.pretrained_model.load_weights(os.path.join(self.filepath, 'init_model/model'))

        encoder_input = tf.keras.layers.Input(shape=(self.input_shape[0], self.input_shape[1]))
        classifier._set_inputs(encoder_input)
        classifier.build((size_batch, self.input_shape[0], self.input_shape[1]))

        for i in range(len(classifier.layers) - 1):
            classifier.layers[i].set_weights(self.pretrained_model.layers[i].get_weights())

        return classifier


    def fit_model_student(self,
                          model_transformer,
                          x_train, y_train,
                          X_U,
                          x_test, y_test,
                          lambda_,
                          nb_epoch,
                          r = 0,
                          size_batch = 64):


        count = 0
        train_loss_list_student = []
        train_loss_list_teacher = []
        train_score_list_student = []
        train_score_list_teacher = []
        best_f1 = 0.0
        best_f1_teacher= 0.0
        best_epoch = 0.0


        X_U_T = model_transformer.predict(tf.convert_to_tensor(X_U.astype('float32')), batch_size=512)
        x_train_T = model_transformer.predict(tf.convert_to_tensor(x_train.astype('float32')), batch_size=512)
        x_test_T = model_transformer.predict(tf.convert_to_tensor(x_test.astype('float32')), batch_size=512)

        if not self.pretrained_model is None:
            pretraining = PreTraining(model=self.pretrained_model,
                                      input_shape=self.input_shape,
                                      optimizer=self.optimizer,
                                      loss=tf.keras.losses.MeanSquaredError(),
                                      filepath=self.filepath,
                                      filename='init_model')

            pretraining.fit_model(x_train=X_U, x_val=x_train, size_batch=size_batch, nb_epoch=30)
            self.classifier_teacher, _ = self.init_weights_pretrained(self.classifier_teacher, size_batch)
            self.classifier_student, _ = self.init_weights_pretrained(self.classifier_student, size_batch)

        n_iter = self.get_iteration(x_train, 32)

        for e in range(1,nb_epoch):

            x_train, x_train_T, y_train = shuffle(x_train, x_train_T, y_train)

            X_U, X_U_T = shuffle(X_U, X_U_T)
            train_U, train_U_T = X_U[:x_train.shape[0],],  X_U_T[:x_train.shape[0],]
            n,times,card = train_U_T.shape

            train_ds = tf.data.Dataset.from_tensor_slices((x_train_T,
                                                           train_U_T * np.random.normal(1, 0.15,(n,times,card)),
                                                           train_U_T * np.random.normal(1, 0.05, (n, times, card)),
                                                           tf.keras.utils.to_categorical(y_train, num_classes=2)))
            train_ds = train_ds.batch(size_batch)

            apply_grads = self.app_grad_teacher()
            train_loss_teacher = apply_grads(train_ds)

            # random batch of same size
            train_ds = tf.data.Dataset.from_tensor_slices((x_train,
                                                           tf.keras.utils.to_categorical(y_train, num_classes=2),
                                                           train_U * np.random.normal(1, 0.05, train_U.shape),
                                                           train_U_T))

            train_ds = train_ds.batch(size_batch)

            apply_grads = self.app_grad_student()
            train_loss_student, train_loss_1, train_loss_2 = apply_grads(train_ds, lambda_)
            y_pred_test, _ = self.classifier_student.predict(x_test,batch_size = 512)
            y_pred_test = np.argmax(y_pred_test, axis = 1)

            y_pred_test_teacher, _ = self.classifier_student.predict(x_test_T,batch_size = 512)
            y_pred_test_teacher = np.argmax(y_pred_test_teacher, axis = 1)

            print(
                "Epoch {0}: CE loss {1}, KL loss {2}, Total loss {3},".format(
                    str(e),
                    str(train_loss_1.numpy()),#str(round(train_loss_student.numpy(), 6)),
                    str(round(train_loss_2.numpy(), 6)),
                    str(round(train_loss_student.numpy(), 6))
                ))

            if not self.filepath is None:
                if not os.path.exists(self.filepath):
                    os.makedirs(self.filepath)

            if count > 0:
                if train_loss_student < np.min(train_loss_list_student):
                    print('save noisy')
                    self.classifier_student.save_weights(os.path.join(self.filepath, self.filename + '_student'))
                    best_f1 = f1_score(y_test,y_pred_test, average='weighted')
                    best_epoch = count

                if train_loss_teacher < np.min(train_loss_list_teacher):
                    self.classifier_teacher.save_weights(os.path.join(self.filepath, self.filename + '_teacher'))
                    self.classifier_teacher.save_weights(os.path.join(self.filepath, self.filename + '_teacher_CE'))
                    best_f1_teacher = f1_score(y_test, y_pred_test_teacher, average='weighted')

            train_loss_list_teacher.append(train_loss_teacher)
            train_loss_list_student.append(train_loss_student)
            train_score_list_teacher.append(f1_score(y_test, y_pred_test_teacher, average='weighted'))
            train_score_list_student.append(f1_score(y_test,y_pred_test, average='weighted'))

            count += 1
            #Online PSL
            if r > 0:
                if (nb_epoch - (count - 1)) % (nb_epoch // 10) == 0:
                    preds_U, _ = self.classifier_student.predict(X_U)
                    preds_U = preds_U[:, 1]
                    n = int((r * (count / nb_epoch) * x_train.shape[0]))
                    index_sort = np.argsort(preds_U)
                    if count > 10:
                        n_previous = (r * (count - (nb_epoch // 10)) / nb_epoch) * x_train.shape[0]
                        x_train = x_train[:-int(n_previous * 2)]
                        x_train_T = x_train_T[:-int(n_previous * 2)]
                        y_train = y_train[:-int(n_previous * 2)]

                    x_train = np.concatenate([x_train, X_U[index_sort[-n:]], X_U[index_sort[:n]]], axis=0)
                    x_train_T = np.concatenate([x_train_T, X_U_T[index_sort[-n:]], X_U_T[index_sort[:n]]], axis=0)
                    y_train = np.concatenate([y_train, preds_U[index_sort[-n:]], preds_U[index_sort[-n:]]], axis=0)

            print(x_train.shape)

            print("\t f1 on the test set %f" % f1_score(y_test,y_pred_test, average='weighted'))
            print("\t kappa on the test set %f" % cohen_kappa_score(y_test, y_pred_test))
            print("\t Best f1 on the test set %f" % best_f1)
            print("\t Best f1 teacher on the test set %f" % best_f1_teacher)

            self.classifier_student.save_weights(os.path.join(self.filepath, self.filename + '_student_final'))
            self.classifier_teacher.save_weights(os.path.join(self.filepath, self.filename + '_teacher_final'))

        losses = dict(teacher_loss = train_loss_list_teacher,
                      teacher_score = train_loss_list_student,
                      student_loss = train_loss_list_student,
                      epoch_stop = best_epoch
                      )
        import pickle
        with open(os.path.join(self.filepath, 'history.pickle'), 'wb') as d:
            pickle.dump(losses, d, protocol=pickle.HIGHEST_PROTOCOL)
