import copy
import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import os
import numpy as np #do numpy 1.19.5

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession

    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.8
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)

else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import random
from Autoencoder import utils as utils
import pandas as pd
from importlib import reload
utils = reload(utils)
import time
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score
from Supervised import TS_Consistency as TS_Noisy

from importlib import reload
import random

#########################################################################################################################

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_WWB_IS/Positive'
model_name = 'GRU_V2_64_Noisy_TS'
n_sample = 20
i_exp = 1
timestamp = 10

def reshape_table(array, n = 6, t = 25):
    array = array.reshape(array.shape[0], n, t)
    array = np.moveaxis(array, 1, 2)
    return array


def launch_experiment():
    for n_sample in range(20,120,20):
        for i_exp in range(1, 11, 2):
            print(n_sample)# range(20,120,40)
            path_file = 'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp)
            path_AE = os.path.join(path_experiment,
                                   os.path.join(path_file, 'Reconstruction'))
            path_NN = os.path.join(path_experiment,
                                   os.path.join(path_file, 'Consistency_TS_EMA_QT_32_Student'))

            X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                          n_sample=n_sample,
                                                                          i_exp=i_exp)

            dico_tanh, filename, model = utils.read_AE_file(path_out=path_AE,
                                                            hidden_activation='tanh',
                                                            embedding_activation='None',
                                                            output_activation='None',
                                                            name_optimizer='Adam_10e-4',
                                                            name_loss='huber',
                                                            prefix='VAEGRU_V2_32')

            reconstruction_df_tanh = dico_tanh['Reconstruction']
            reconstruction_df_tanh['binary'] = y_u
            reconstruction_df_tanh['True_class'] = Y_U[:, 0]

            if 'Huber' not in list(reconstruction_df_tanh.columns):
                reconstruction_U = model.predict(reshape_table(X_U, 6, timestamp), batch_size=512)
                h = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
                res = h(reshape_table(X_U, 6, timestamp), reconstruction_U)
                res = np.mean(res, axis=1)
                reconstruction_df_tanh['Huber'] = res

            index_RP = np.where(reconstruction_df_tanh['Huber'] <= np.quantile(reconstruction_df_tanh['Huber'], 0.25)) #np.mean(reconstruction_df_tanh['Huber'])- 0.5 * np.std(reconstruction_df_tanh['Huber']))
            index_RN = np.where(reconstruction_df_tanh['Huber'] > np.quantile(reconstruction_df_tanh['Huber'], 0.25)) #np.mean(reconstruction_df_tanh['Huber']) - 0.5 * np.std(reconstruction_df_tanh['Huber']))

            rdm_neg = random.sample(list(index_RN[0]), int(X_P.shape[0]))
            #rdm_neg = random.sample(set(list(Y_U[list(index_RN[0]), 1])), n_sample)

            X_RN = X_U[rdm_neg,]

            x_train = np.concatenate([X_P, X_RN], axis = 0)
            x_train = reshape_table(x_train, 6, timestamp)

            if len(list(index_RP[0])) <  int(x_train.shape[0]):
                rdm_pos = random.sample(list(index_RP[0]), int(x_train.shape[0]))
                X_U_update = X_U[rdm_pos,]
            else:
                X_U_update = copy.deepcopy(X_U)

            X_U_update = reshape_table(X_U_update, 6, timestamp)

            y_train = np.concatenate([np.zeros(X_P.shape[0]) + 1,
                                      np.zeros(X_RN.shape[0])],
                                     axis=0)

            x_test = reshape_table(X_T, 6, timestamp)
            ############################################################################################
            from Supervised import models as models
            classifier_teacher = models.FCGRU_Model(nunits=32, fcunits=32,
                                                    dropout_rate=0.2, l2_reg=0.0)

            classifier_student = models.FCGRU_Model(nunits=32, fcunits=32,
                                                    dropout_rate=0.2, l2_reg=0.0)

            encoder_input = tf.keras.layers.Input(shape=(timestamp,6))
            classifier_teacher._set_inputs(encoder_input)
            classifier_student._set_inputs(encoder_input)
            ######################################################################
            classifier_teacher.build(input_shape=(32, encoder_input.shape[1], encoder_input.shape[2]))
            classifier_student.build(input_shape=(32, encoder_input.shape[1], encoder_input.shape[2]))

            ############################################################################################
            # Teacher + annotated data
            FCGRU_PRETRAIN = models.FCGRUPretrain(nunits=32, fcunits=32,
                                                  dropout_rate=0.2, l2_reg=0.0)

            SSL = TS_Noisy.TS_Consistency(None,
                                          classifier_teacher,
                                          classifier_student,
                                          input_shape = (timestamp,6),
                                          optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                                          loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                                          filepath=path_NN,
                                          filename=model_name)

            SSL.fit_model_student(model_transformer=model,
                                  x_train = x_train,
                                  y_train=y_train,
                                  X_U=X_U_update,
                                  x_test=x_test,
                                  y_test=y_t,
                                  lambda_= 0,
                                  r = 0,
                                  nb_epoch=50,
                                  size_batch=32)

launch_experiment()
