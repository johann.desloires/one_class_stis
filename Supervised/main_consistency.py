import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import os
import numpy as np #do numpy 1.19.5

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    '''
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.8
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
    '''
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import Supervised.models as models
import Archives.supervised_training as supervised_training
from Autoencoder import utils as utils
import pandas as pd
from importlib import reload
utils = reload(utils)
supervised_training = reload(supervised_training)
import time
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score
import Supervised
import random

from Supervised import Consistency as supervised_training
from importlib import reload

Supervised = reload(Supervised)
supervised_training = reload(supervised_training)


#########################################################################################################################

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/Forest'
n_sample = 60
i_exp = 3

def launch_experiment():
    for n_sample in range(20, 120, 20):
        for i_exp in range(1,11,2):
            if n_sample == 20 and i_exp == 1:
                i_exp = 3
            ##############################################################################################
            # path_VAE = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')
            path_file = 'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp)
            path_NN = os.path.join(path_experiment,
                                   os.path.join(path_file, 'Consistency_EMA'))

            if not os.path.exists(os.path.join(path_NN, 'history.pickle')):
                X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                              n_sample=n_sample,
                                                                              i_exp=i_exp)
                path_AE = os.path.join(path_experiment,
                                       os.path.join(path_file, 'Reconstruction'))

                dico_tanh, filename, model = utils.read_AE_file(path_out=path_AE,
                                                                hidden_activation='tanh',
                                                                embedding_activation='None',
                                                                output_activation='None',
                                                                name_optimizer='Adam_10e-4',
                                                                name_loss='huber',
                                                                prefix='AEGRU_V1_16')

                reconstruction_df_tanh = dico_tanh['Reconstruction']
                reconstruction_df_tanh['binary'] = y_u
                reconstruction_df_tanh['True_class'] = Y_U[:, 0]

                if 'Huber' not in list(reconstruction_df_tanh.columns):
                    reconstruction_U = model.predict(X_U.reshape(X_U.shape[0], 25, 6), batch_size=512)
                    h = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
                    res = h(X_U.reshape(X_U.shape[0], 25, 6), reconstruction_U)
                    res = np.mean(res, axis=1)
                    reconstruction_df_tanh['Huber'] = res

                index_RP = np.where(reconstruction_df_tanh['Huber'] <= np.mean(reconstruction_df_tanh['Huber']) )
                index_RN = np.where(reconstruction_df_tanh['Huber'] > np.mean(reconstruction_df_tanh['Huber']))

                rdm_neg = random.sample(list(index_RN[0]), int(X_P.shape[0]))
                X_RN = X_U[rdm_neg,]

                x_train = np.concatenate([X_P, X_RN], axis = 0)
                x_train = x_train.reshape(x_train.shape[0],25,6)

                X_U_update = X_U[list(index_RP[0]), ]
                X_U_update = X_U_update.reshape(X_U_update.shape[0], 25, 6)

                y_train = np.concatenate([np.zeros(X_P.shape[0]) + 1,
                                          np.zeros(X_RN.shape[0])],
                                         axis=0)

                x_test = X_T.reshape(X_T.shape[0], 25, 6)
                ############################################################################################
                from Supervised import models as models
                classifier = models.FCGRU_Model(nunits=32, fcunits=32,
                                                dropout_rate=0.2, l2_reg=0.0)

                encoder_input = tf.keras.layers.Input(shape=(25,6))
                classifier._set_inputs(encoder_input)
                classifier.build(input_shape=(64, encoder_input.shape[1], encoder_input.shape[2]))
                ############################################################################################
                FCGRU_PRETRAIN = models.FCGRUPretrain(nunits=32, fcunits=32,dropout_rate=0.2, l2_reg=0.0)

                input_consistency = supervised_training.Consistency(None,
                                                          classifier,
                                                          input_shape = (25,6),
                                                          optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                                                          loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                                                          filepath=path_NN,
                                                          filename='GRU_V1_16_Noisy_TS')

                input_consistency.fit_model(model_transformer=model,
                                            x_train=x_train,
                                            y_train=y_train,
                                            X_U=X_U_update,
                                            x_test=x_test,
                                            y_test=y_t,
                                            alpha=1,
                                            nb_epoch=100,
                                            EMA = True,
                                            MA = False,
                                            size_batch=32)




launch_experiment()
