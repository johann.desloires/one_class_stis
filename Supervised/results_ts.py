import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

gpu = False
import os

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    '''
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.8
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
    '''
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import Supervised
from Supervised import models
from Autoencoder import utils
import os
import numpy as np
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score,matthews_corrcoef
import pandas as pd
from sklearn.metrics import roc_curve, roc_auc_score
import pickle
import seaborn as sn
import matplotlib.pyplot as plt
###########################
# Teacher
metrics = {}
metrics['teacher'] = []
metrics['teacher_X_T'] = []

metrics['student'] = []
metrics['student_final'] = []

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_CORN_IS/Positive'
type_ = 'VAE'
folder = '/Consistency_TS_EMA_MED_32_CR_V2/'
name_model = 'GRU_V2_64_Noisy_TS'
n_sample = 60
i_exp = 1


classifier_GRU = models.FCGRU_Model(nunits=32, fcunits=32, dropout_rate=0.2,  l2_reg=0.0)

def plot_roc(false_positive_rate,true_positive_rate):
    import matplotlib.pyplot as plt
    plt.subplots(1, figsize=(10, 10))
    plt.title('Receiver Operating Characteristic - DecisionTree')
    plt.plot(false_positive_rate, true_positive_rate)
    plt.plot([0, 1], ls="--")
    plt.plot([0, 0], [1, 0], c=".7"), plt.plot([1, 1], c=".7")
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.show()

def plot_conf_matrix(preds,y_t):
    data = {'y_Actual': y_t,
            'y_Predicted': preds
            }

    df = pd.DataFrame(data, columns=['y_Actual', 'y_Predicted'])
    confusion_matrix = pd.crosstab(df['y_Actual'], df['y_Predicted'], rownames=['Label'],
                                   colnames=['Predicted'])

    confusion_matrix = confusion_matrix / confusion_matrix.sum(axis=1)[:, np.newaxis]
    plt.figure(figsize=(10, 10))
    sn.set(font_scale=3)
    sn.heatmap(confusion_matrix, annot=True, cmap = plt.cm.Greens)
    plt.show()

def reshape_table(array, n = 6, t = 25):
    array = array.reshape(array.shape[0], n, t)
    array = np.moveaxis(array, 1, 2)
    return array


for n_sample in range(20, 120, 20):  #:
    print(str(n_sample))
    for i_exp in range(1, 11, 1):
        try:
            print(i_exp)
            path_save = os.path.join(path_experiment, 'Models/' + type_ + '/N_' + \
                                     str(n_sample) + '_Exp_' + str(i_exp) + \
                                     folder)

            if os.path.exists(os.path.join(path_save, name_model +'_student.index')):

                X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                              n_sample=n_sample,
                                                                              i_exp=i_exp)

                path_AE = os.path.join(path_experiment,
                                       'Models/' + type_ + '/N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')
                import time
                start = time.time()
                dico_tanh, filename, model = utils.read_AE_file(path_out=path_AE,
                                                                hidden_activation='tanh',
                                                                embedding_activation='None',
                                                                output_activation='None',
                                                                name_optimizer='Adam_10e-4',
                                                                name_loss='huber',
                                                                prefix='VAEGRU_V2_64')

                predictions_T = model.predict(reshape_table(X_T, n = 6, t = 25), batch_size=256)
                end = time.time()
                time_transfo = end - start
                print('time transfo ' + str(time_transfo))
                #####################################################################################
                path_student = os.path.join(path_experiment, 'Models/' + type_ + '/N_' + \
                                            str(n_sample) + '_Exp_' + str(i_exp) + \
                                            folder + name_model + '_student_final')

                # TRANSFORMED DATA
                start = time.time()

                classifier_GRU.load_weights(path_student)

                preds_, _ = classifier_GRU.predict(reshape_table(X_T, n = 6, t = 25),
                                               batch_size=256)#
                preds = np.argmax(preds_, axis=1)
                end = time.time()
                time_model = end - start
                print('time model ' + str(time_model))

                plot_conf_matrix(preds, y_t)

                metric = dict(f1_score=[f1_score(y_t,
                                                 preds, average='weighted')],
                              accuracy=[accuracy_score(y_t, preds)],
                              matthieu = [matthews_corrcoef(y_t, preds)],
                              kappa=[cohen_kappa_score(y_t, preds)])

                metric = pd.DataFrame(metric)
                metric['Sample_size'] = n_sample
                metric['i_exp'] = i_exp
                metric['time_transf'] = time_transfo
                metric['time_model'] = time_model
                metrics['student_final'].append(metric)
                print('student')
                print(metric)

                #####################################################################################
                path_student = os.path.join(path_experiment, 'Models/' + type_ + '/N_' + \
                                            str(n_sample) + '_Exp_' + str(i_exp) + \
                                            folder + name_model + '_student')

                start = time.time()

                classifier_GRU.load_weights(path_student)

                preds, _ = classifier_GRU.predict(reshape_table(X_T, n = 6, t = 25),
                                               batch_size=256)#
                preds = np.argmax(preds, axis=1)
                end = time.time()
                time_model = end - start
                print('time model ' + str(time_model))

                metric = dict(f1_score=[f1_score(y_t,
                                                 preds, average='weighted')],
                              accuracy=[accuracy_score(y_t, preds)],
                              matthieu = [matthews_corrcoef(y_t, preds)],
                              kappa=[cohen_kappa_score(y_t, preds)])

                metric = pd.DataFrame(metric)
                metric['Sample_size'] = n_sample
                metric['i_exp'] = i_exp
                metric['time_transf'] = time_transfo
                metric['time_model'] = time_model
                metrics['student'].append(metric)
                print('student best')
                print(metric)
                ################################################################################################################
                # TEACHER ONLY
                path_teacher = os.path.join(path_experiment, 'Models/' + type_ + '/N_' + \
                                            str(n_sample) + '_Exp_' + str(i_exp) + \
                                            folder + name_model + '_teacher_CE')


                # TRANSFORMED DATA
                classifier_GRU.load_weights(path_teacher)

                preds, _ = classifier_GRU.predict(predictions_T, batch_size=256)
                preds = np.argmax(preds, axis=1)

                metrics_teacher = dict(f1_score=[f1_score(y_t,
                                                          preds, average='weighted')],
                                       accuracy=[accuracy_score(y_t, preds)],
                                       kappa=[cohen_kappa_score(y_t, preds)])

                metric_teacher = pd.DataFrame(metrics_teacher)
                metric_teacher['Sample_size'] = n_sample
                metric_teacher['i_exp'] = i_exp
                print('teacher_T')
                print(metric_teacher)
                metrics['teacher_X_T'].append(metric_teacher)

        except:
            print('no path')
            pass


with open(os.path.join(os.path.join(path_experiment, 'Models/' + type_ + '/Consistency_TS_EMA_Student_' + name_model + '_tanh_32_32_metrics.pickle')), 'wb') as d:
    pickle.dump(metrics, d, protocol=pickle.HIGHEST_PROTOCOL)

#################################################
import os
import pickle5 as pickle
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_WWB_IS/Positive'
type_ = 'VAE'
folder = '/Consistency_TS_EMA_QT_32/' #QT 32 works wells for Corn => must change VAE to 32
name_model = 'GRU_V2_64_Noisy_TS'
predfix = 'QT_32_' #QT_16_

path_dico = os.path.join(os.path.join(path_experiment, 'Models/' + type_ + '/Consistency_TS_EMA_' + predfix + name_model + '_tanh_32_32_metrics.pickle'))


experiment = ['Teacher','Consistency_TS_EMA_']

def aggregate_results(path_experiment, name_folder = 'Consistency_TS_EMA_',
                      name_model = 'GRU_V2_64_Noisy_TS_tanh_32_32_metrics.pickle',
                      key = 'student',
                      agg = 'mean'):
    type_ = 'VAE/'
    gru_results = pickle.load(
        open(path_dico,
             'rb'))


    gru_results_model = gru_results[key]
    gru_results_model = pd.concat(gru_results_model)
    gru_results_model = gru_results_model.drop('i_exp', axis=1).groupby('Sample_size').agg(agg)
    gru_results_model.reset_index(inplace=True)
    gru_results_model['type'] = key
    return gru_results_model

gru = aggregate_results(path_experiment, 'Consistency_TS_EMA_',
                      name_model = 'GRU_V2_64_Noisy_TS_tanh_32_32_metrics.pickle',
                      key = 'student_final',
                      agg = 'mean')

gru['kappa']

dictionary_results = dict(f1_score = {},
                          accuracy = {},
                          kappa = {})
exp = experiment[-1]
for exp in experiment:
    if exp == 'Teacher':
        gru_results_student = aggregate_results(path_experiment, 'Consistency_TS_EMA_50_',
                                                name_model = 'GRU_V2_64_Noisy_TS_tanh_32_32_metrics.pickle',
                                                key = 'teacher_X_T')
    else:
        gru_results_student = aggregate_results(path_experiment, exp,
                                                name_model = 'GRU_V2_64_Noisy_TS_tanh_32_32_metrics.pickle',
                                                key = 'student')

    dictionary_results['f1_score'][exp] = gru_results_student['f1_score']
    dictionary_results['accuracy'][exp] = gru_results_student['accuracy']
    dictionary_results['kappa'][exp] = gru_results_student['kappa']

metric = 'accuracy'
dictionary_out = pickle.load(
    open(os.path.join(os.path.join('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2',class_), 'Models/' + str(0.5) + '/dictionary_output_' + metric + '.pickle'), 'rb'))
plot_competitors = dictionary_out['plot']
plot_competitors.reset_index(inplace=True)

df = pd.concat([plot_competitors, pd.DataFrame(dictionary_results[metric])], axis = 1)
df.columns = ['Samples','RF', 'ENSEMBLE','OCSVM','Teacher','OUR']
df = df.set_index('Samples')
df= df.apply(pd.to_numeric)

ax = df.plot(y = list(df.columns), style=['x--','p--','o--','s-','^-','v-','.-',',-'])
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=2, fancybox=True, shadow=True)
ax.set_xlabel("# Obj Samples")
ax.set_ylabel(metric)
ax.set_xticks(range(20,120,20))
plt.show()


##########################################################################################
#Standard error
class_ = 'Built'
path_experiment = os.path.join('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments_V2',
                               class_)

dictionary_results = dict(f1_score = {},
                          accuracy = {},
                          kappa = {})

for exp in experiment:
    if exp=='Teacher':
        key = 'teacher_X_T'
    else:
        key = 'student'

    gru_results_student_std = aggregate_results(path_experiment, 'Consistency_TS_EMA_50_',
                                                name_model = 'GRU_V2_64_Noisy_TS_tanh_32_32_metrics.pickle',
                                                key = key,
                                                agg = 'std')
    gru_results_student_std = gru_results_student_std[['f1_score','accuracy','kappa']]

    gru_results_student_mean = aggregate_results(path_experiment, 'Consistency_TS_EMA_50_',
                                                 name_model='GRU_V2_64_Noisy_TS_tanh_32_32_metrics.pickle',
                                                 key=key,
                                                 agg = 'mean')
    gru_results_student_mean = gru_results_student_mean[['f1_score','accuracy','kappa']]

    gru_results_student = gru_results_student_mean.copy()
    for metric in gru_results_student.columns:
        if metric != 'kappa':
            alpha = 100
            round_ = 1
        else:
            alpha = 1
            round_ = 2
        mean = [str((round(alpha*k, round_))) for k in gru_results_student_mean[metric]]
        se = ['(' + str(round(alpha * k, round_)) + ')' for k in gru_results_student_std[metric]]
        res = [i + ' ' + j for i, j in zip(mean, se)]
        gru_results_student[metric] = res

    dictionary_results['f1_score'][key] = gru_results_student['f1_score']
    dictionary_results['accuracy'][key] = gru_results_student['accuracy']
    dictionary_results['kappa'][key] = gru_results_student['kappa']


metric = 'kappa'
dictionary_out = pickle.load(
    open(os.path.join(os.path.join('/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments',
                               class_),
                      'Models/' + str(0.5) + '/dictionary_output_' + metric + '.pickle'), 'rb'))
plot_competitors = dictionary_out['table']
plot_competitors.reset_index(inplace=True)


df = pd.concat([plot_competitors, pd.DataFrame(dictionary_results[metric])], axis = 1)
df.columns = ['Samples','RF', 'ENSEMBLE','OCSVM', 'Teacher', 'KD']
df = df.set_index('Samples')


if metric != 'kappa':
    for cols in ['RF', 'ENSEMBLE' , 'OCSVM']:
        l = [k.split(' ') for k in df[cols]]
        l = [str(round(float(x) * 100,1)) + ' (' + str(round(float(y.split('(')[-1].split(')')[0]) * 100,1)) + ')' for x,y in l]
        df[cols] = l

print(df.to_latex())


'''
import shutil
type = 'VAE'
path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/Forest'
folder_in = '/Consistency_TS_Noise/'
folder_out = '/Consistency_TS/'
name_model = 'GRU_V1_16_Noisy_TS'

for n_sample in range(20, 120, 20):  #:
    print(str(n_sample))
    for i_exp in range(1, 11, 2):
        try:
            path_in= os.path.join(path_experiment, 'Models/' + type_ + '/N_' + \
                                        str(n_sample) + '_Exp_' + str(i_exp) + \
                                        folder_in )


            shutil.rmtree(path_in)
        except:
            pass
'''

