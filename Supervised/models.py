##########################################################################################################
import tensorflow as tf
import collections
import math
from tensorflow.python.ops import variable_scope as vs
from tensorflow.python.ops.math_ops import sigmoid, tanh, array_ops

from tensorflow.python.framework import dtypes, ops
from tensorflow.python.ops import array_ops, control_flow_ops, embedding_ops, math_ops, nn_ops, rnn_cell, rnn_cell_impl, variable_scope, rnn

from tensorflow.keras.layers import Input, \
    Layer, Activation, Dense, Dropout, BatchNormalization, Flatten, Reshape, \
    Conv1D, LeakyReLU, ReLU, MaxPool1D, GlobalAveragePooling1D, \
    LSTM, TimeDistributed, RNN, GRU

from keras.regularizers import l2

tf.keras.backend.set_floatx('float32')

import numpy as np
from tensorflow.keras.layers import GRU
from collections import OrderedDict
from tensorflow.keras import backend as K
##########################################################################################################
# https://github.com/FlorentF9/DeepTemporalClustering/blob/master/TAE.py
# https://github.com/PINTO0309/Keras-OneClassAnomalyDetection#4-preparing-the-model

###DEFINE LAYERS####
class DenseLayer(Layer):
    def __init__(self, units, dropout_rate=0.0, activation=None,#'relu',
                 kernel_initializer='he_normal'):  # ,l1=1e-5, l2=1e-4):

        super(DenseLayer, self).__init__()
        self.dense1 = tf.keras.layers.Dense(units=units,
                                            kernel_initializer=kernel_initializer,
                                            kernel_regularizer=tf.keras.regularizers.l2(l=1E-6))
        self.bn = BatchNormalization()
        self.drop_layer = Dropout(rate=dropout_rate)
        self.act = Activation(activation)

    def call(self, inputs, is_training=False):
        b1 = self.dense1(inputs)
        b1 = self.bn(b1, is_training)
        dropout = self.drop_layer(b1, is_training)
        act = self.act(dropout)

        return act


class FC(Layer):
    '''
    Dense layer with batch normalization
    '''
    def __init__(self,num_units,dropout_rate,act='relu', l2_reg = 0):
        super(FC,self).__init__()
        self.dense = Dense(num_units,
                           kernel_regularizer=l2(l2_reg),
                           bias_regularizer=l2(l2_reg))
        self.bn = BatchNormalization()
        self.act = Activation(act)
        self.drop_layer = Dropout(rate=dropout_rate)

    def call(self,inputs,is_training):
        x = self.dense(inputs)
        x = self.bn(x, is_training)
        x = self.act(x)
        x = self.drop_layer(x, training=is_training)
        return x

#CONV->BN->RELU->DROP lower rate (0.1)
class ConvBlock(Layer):
    '''
    1D Convolution block with batch normalization and dropout layer
    '''
    # filters parameters is just how many different windows you will have
    def __init__(self, dropout_rate=0.5, n_filters=64, kernel_size=5, hidden_activation = 'relu'):
        super(ConvBlock, self).__init__()
        self.conv1 = Conv1D(filters = n_filters, kernel_size = kernel_size, padding='valid',
                            kernel_regularizer=tf.keras.regularizers.l2(l=1E-6))
        self.bn = BatchNormalization()
        self.drop_layer = Dropout(rate=dropout_rate)
        self.act = Activation(hidden_activation)

    def call(self, inputs, is_training):
        conv1 = self.conv1(inputs)
        #conv1 = self.bn(conv1, is_training)
        drop_out = self.drop_layer(conv1, is_training)
        activation = self.act(drop_out)

        return activation

# vae = VariationalAutoEncoder(original_dim, 64, 32)
###########################################################################
###DEFINE MLP####
class MLP(tf.keras.Model):
    def __init__(self, ndim,
                 dropout_rate=0.0,
                 hidden_activation='relu',
                 output_activation='softmax',
                 name='MLP',
                 n_classes=2, **kwargs):
        super(MLP, self).__init__(name=name, **kwargs)
        # ndim = input dimension

        self.dense1 = DenseLayer(units=int(ndim / 2),
                                 activation=hidden_activation,
                                 dropout_rate=dropout_rate)

        self.dense2 = DenseLayer(units=int(ndim / 2),
                                 activation=hidden_activation,
                                 dropout_rate=dropout_rate)

        self.prob = DenseLayer(units=n_classes, activation=output_activation)

    def call(self, inputs, is_training=False):
        dense1 = self.dense1(inputs, is_training)
        dense2 = self.dense2(dense1, is_training)
        probabilities = self.prob(dense2, is_training)
        return probabilities


##################################
class GRUClassif(tf.keras.Model):
    def __init__(self, nunits = 64,
                 fcunits = 256,
                 dropout_rate=0.0, recurrent_dropout=0.0,
                 hidden_activation='tanh', n_classes=2,
                 input_shape = (25,6),
                 name='GRUClassif',
                 **kwargs):

        super(GRUClassif, self).__init__(name=name, **kwargs)

        self.RNN = tf.keras.layers.GRU(units=nunits,
                                       activation=hidden_activation,
                                       return_sequences=False,
                                       recurrent_dropout=recurrent_dropout)

        self.drop_layer = Dropout(rate=dropout_rate)
        self.FC1 = FC(fcunits, dropout_rate, act=hidden_activation)
        self.FC2 = FC(fcunits, dropout_rate, act=hidden_activation)
        self.output_layer = Dense(n_classes, activation='softmax')

    def call(self, inputs, is_training=False):
        rnn = self.RNN(inputs)
        rnn = self.drop_layer(rnn, training=is_training)
        dense1 = self.FC1(rnn, is_training)
        dense2 = self.FC2(dense1, is_training)
        proba = self.output_layer(dense2)
        return proba


########################################################################################################################
###DEFINE GRU LAYERs####

#https://github.com/pbhatia243/tf-layer-norm
class FCGRU(tf.keras.layers.SimpleRNNCell):
    '''
	Gated Recurrent Unit cell (http://arxiv.org/abs/1406.1078)
    enriched with Fully Connected layers
	'''
    def __init__(self,units,fc_units,drop=0, l2_reg = 0):
        super(FCGRU, self).__init__(units)

        self.fc_units = fc_units
        self.dense1 = Dense(fc_units,activation='tanh',
                            kernel_regularizer=l2(l2_reg),
                            bias_regularizer=l2(l2_reg))

        self.drop1 = Dropout(rate=drop)
        self.dense2 = Dense(fc_units*2,activation='tanh',
                            kernel_regularizer=l2(l2_reg),
                            bias_regularizer=l2(l2_reg))
        self.drop2 = Dropout(rate=drop)
        self.drop3 = Dropout(rate=drop)


    def build(self,input_shapes):
        self.b_g1 = self.add_weight(name='b_g1', shape=(self.units,))
        self.b_g2 = self.add_weight(name='b_g2', shape=(self.units,))
        self.b_g3 = self.add_weight(name='b_g3', shape=(self.units,))

        self.weights_g1 = self.add_weight(name='weights_g1', shape=(self.fc_units*2, self.units))
        self.weights_g2 = self.add_weight(name='weights_g2', shape=(self.fc_units*2, self.units))
        self.weights_g3 = self.add_weight(name='weights_g3', shape=(self.fc_units*2, self.units))

        self.weights_g1h = self.add_weight(name='weights_g1_h', shape=(self.units, self.units))
        self.weights_g2h = self.add_weight(name='weights_g2_h', shape=(self.units, self.units))
        self.weights_g3h = self.add_weight(name='weights_g3_h', shape=(self.units, self.units))

    def call(self, inputs, state, training):
        # FC Layers
        fc1 = self.dense1(inputs)
        fc1 = self.drop1(fc1, training)
        fc2 = self.dense2(fc1)
        fc2 = self.drop2(fc2, training)

        # Update Gate
        zt = tf.math.sigmoid( tf.matmul(fc2, self.weights_g1) + tf.matmul(state[0], self.weights_g1h) + self.b_g1)
        # Reset Gate
        rt = tf.math.sigmoid( tf.matmul(fc2, self.weights_g2) + tf.matmul(state[0], self.weights_g2h) + self.b_g2)
        # Memory content
        ht_c = self.activation( tf.matmul(fc2, self.weights_g3) + tf.matmul(rt * state[0], self.weights_g3h) + self.b_g3)
        # New hidden state
        ht = (1-zt) * state[0] + zt * ht_c
        ht = self.drop3(ht, training)

        return ht, [ht]


class FCGRU_Model(tf.keras.Model):
    def __init__(self,
                 nunits = 64,
                 fcunits = 32,
                 dropout_rate=0.0,
                 hidden_activation = 'relu',
                 n_classes=2,
                 l2_reg = 0.0,
                 **kwargs):

        super(FCGRU_Model, self).__init__(**kwargs)

        self.fcgru = FCGRU(nunits,fcunits,dropout_rate, l2_reg=l2_reg)
        self.gru_cell = RNN(self.fcgru,return_sequences=False)
        self.FC1 = FC(fcunits, dropout_rate, act = hidden_activation, l2_reg=l2_reg)
        self.FC2 = FC(fcunits, dropout_rate, act = hidden_activation, l2_reg=l2_reg)
        self.output_layer = Dense(n_classes, activation='softmax')

    def call(self, inputs, is_training=False):
        rnn = self.gru_cell(inputs)
        fc1 = self.FC1(rnn, is_training)
        fc2 = self.FC2(fc1, is_training)
        proba = self.output_layer(fc2)
        return proba, rnn


class FCGRUPretrain(tf.keras.Model):
    def __init__(self,
                 nunits = 64,
                 fcunits = 32,
                 dropout_rate=0.0,
                 hidden_activation = 'relu',
                 n_classes=150,
                 l2_reg = 0.0,
                 **kwargs):

        super(FCGRUPretrain, self).__init__(**kwargs)

        self.fcgru = FCGRU(nunits,fcunits,dropout_rate, l2_reg=l2_reg)
        self.gru_cell = RNN(self.fcgru,return_sequences=False)
        self.FC1 = FC(fcunits, dropout_rate, act = hidden_activation, l2_reg=l2_reg)
        self.FC2 = FC(fcunits, dropout_rate, act = hidden_activation, l2_reg=l2_reg)
        self.output_layer = Dense(n_classes, activation='linear')

    def call(self, inputs, training=False):
        rnn = self.gru_cell(inputs)
        fc1 = self.FC1(rnn, training)
        fc2 = self.FC2(fc1, training)
        output = self.output_layer(fc2)
        return output


#######################################################################################################################
#######################################################################################################################
###DEFINE LSTM####
class LSTMClassif(tf.keras.Model):
    def __init__(self, nunits, ndim = 128,
                 dropout_rate=0.0, recurrent_dropout=0.0,
                 hidden_activation='tanh', n_classes=2,
                 name='LSTM',
                 **kwargs):

        super(LSTMClassif, self).__init__(name=name, **kwargs)

        self.RNN = tf.keras.layers.LSTM(units=nunits,
                                        activation=hidden_activation,
                                        return_sequences=False,
                                        recurrent_dropout=recurrent_dropout)

        #self.MLP = MLP(ndim, dropout_rate=dropout_rate, hidden_activation=hidden_activation, n_classes=n_classes)
        self.FC1 = FC(ndim, dropout_rate, act = hidden_activation)
        self.FC2 = FC(ndim, dropout_rate, act = hidden_activation)
        self.output_layer = Dense(n_classes, activation='softmax',
                                  kernel_regularizer=tf.keras.regularizers.l2(l=1E-6))

    def call(self, inputs, is_training=False):
        rnn = self.RNN(inputs)
        fc = self.FC1(rnn, is_training)
        fc = self.FC2(fc, is_training)
        proba = self.output_layer(fc)
        return proba

########################################################################################################################
###DEFINE CNN####
class TempCNN(tf.keras.Model):
    def __init__(self, ndim = 256,
                 hidden_activation = 'relu', dropout_rate=0.5,
                 n_filters=64, kernel_size=5, n_classes=2):
        #
        super(TempCNN, self).__init__(name='TempCNN')
        self.conv1 = ConvBlock(hidden_activation = hidden_activation,
                                dropout_rate=dropout_rate,
                                n_filters=n_filters,
                                kernel_size=kernel_size)

        self.conv2 = ConvBlock(hidden_activation = hidden_activation,
                                dropout_rate=dropout_rate,
                                n_filters=n_filters,
                                kernel_size=kernel_size)

        self.conv3 = ConvBlock(hidden_activation = hidden_activation,
                                dropout_rate=dropout_rate,
                                n_filters=n_filters,
                                kernel_size=kernel_size)

        self.encode = GlobalAveragePooling1D() #Flatten()#

        # self.MLP = MLP(ndim = ndim, dropout_rate=drop,  hidden_activation=hidden_activation, n_classes=n_classes)
        self.FC = FC(ndim, dropout_rate, act = hidden_activation)
        self.output_layer = Dense(n_classes, activation='softmax',
                                  kernel_regularizer=tf.keras.regularizers.l2(l=1E-6))#, kernel_regularizer=tf.keras.regularizers.l2(l=1E-6))

    def call(self, inputs, is_training = False):
        conv1 = self.conv1(inputs, is_training)
        conv2 = self.conv2(conv1, is_training)
        conv3 = self.conv3(conv2, is_training)
        encode = self.encode(conv3)
        FC = self.FC(encode, is_training)

        return self.output_layer(FC)


##########################################################################################################################
##########################################################################################################################



class GRULN(GRU):
    '''Gated Recurrent Unit with Layer Normalization
    Current impelemtation only works with consume_less = 'gpu' which is already
    set.
    # Arguments
        output_dim: dimension of the internal projections and the final output.
        ...: see GRU documentation for all other arguments.
        gamma_init: name of initialization function for scale parameter.
            The default is 1, but in some cases this is too high resulting
            in NaN loss while training. If this happens try reducing to 0.2
    # References
        -[Layer Normalization](https://arxiv.org/abs/1607.06450)
    '''
    def __init__(self, output_dim, gamma_init=1., **kwargs):
        super(GRULN, self).__init__(output_dim, **kwargs)

        def gamma_init_func(shape, c=gamma_init, **kwargs):
            if c == 1.:
                return tf.compat.v1.keras.initializers.get('zero')('one')(shape, **kwargs) #initializations
            return K.variable(np.ones(shape) * c, **kwargs)

        self.gamma_init = gamma_init_func
        self.beta_init = tf.compat.v1.keras.initializers.get('zero')
        self.epsilon = 1e-5

    def build(self, input_shape):
        super(GRULN, self).build(input_shape)
        shape = (self.output_dim,)
        shape1 = (2*self.output_dim,)
        # LN is applied in 4 inputs/outputs (fields) of the cell
        gammas = OrderedDict()
        betas = OrderedDict()
        # each location has its own BN
        for slc, shp in zip(['state_below', 'state_belowx', 'preact', 'preactx'], [shape1, shape, shape1, shape]):
            gammas[slc] = self.gamma_init(shp,
                                          name='{}_gamma_{}'.format(
                                              self.name, slc))
            betas[slc] = self.beta_init(shp,
                                        name='{}_beta_{}'.format(
                                            self.name, slc))

        self.gammas = gammas
        self.betas = betas

        self.trainable_weights += self.gammas.values() + self.betas.values()

    def ln(self, x, slc):
        # sample-wise normalization
        m = K.mean(x, axis=-1, keepdims=True)
        std = K.sqrt(K.var(x, axis=-1, keepdims=True) + self.epsilon)
        x_normed = (x - m) / (std + self.epsilon)
        x_normed = self.gammas[slc] * x_normed + self.betas[slc]
        return x_normed

    def step(self, x, states):
        h_tm1 = states[0]  # previous memory
        B_U = states[1]  # dropout matrices for recurrent units
        B_W = states[2]

        matrix_x = K.dot(x * B_W[0], self.W) + self.b
        x_ = self.ln(matrix_x[:, : 2 * self.output_dim], 'state_below')
        xx_ = self.ln(matrix_x[:, 2 * self.output_dim:], 'state_belowx')
        matrix_inner = self.ln(K.dot(h_tm1 * B_U[0], self.U[:, :2 * self.output_dim]), 'preact')

        x_z = x_[:, :self.output_dim]
        x_r = x_[:, self.output_dim: 2 * self.output_dim]
        inner_z = matrix_inner[:, :self.output_dim]
        inner_r = matrix_inner[:, self.output_dim: 2 * self.output_dim]

        z = self.inner_activation(x_z + inner_z)
        r = self.inner_activation(x_r + inner_r)

        x_h = xx_
        inner_h = r * self.ln(K.dot(h_tm1 * B_U[0], self.U[:, 2 * self.output_dim:]), 'preactx')
        hh = self.activation(x_h + inner_h)

        h = z * h_tm1 + (1 - z) * hh
        return h, [h]




class LSTMCNN(tf.keras.Model):
    def __init__(self, ndim = 256,
                 hidden_activation = 'relu', dropout_rate=0.0,
                 recurrent_dropout = 0.0,
                 n_filters=32, kernel_size=3, n_classes=2):
        #
        super(LSTMCNN, self).__init__(name='LSTMCNN')

        self.RNN = tf.keras.layers.LSTM(units=128,
                                       activation=hidden_activation,
                                       return_sequences=True,
                                       recurrent_dropout=recurrent_dropout)

        self.conv1 = ConvBlock(
                                dropout_rate=dropout_rate,
                                n_filters=n_filters,
                                kernel_size=kernel_size)

        self.conv2 = ConvBlock(
                                dropout_rate=dropout_rate,
                                n_filters=n_filters,
                                kernel_size=kernel_size)

        self.conv3 = ConvBlock(
                                dropout_rate=dropout_rate,
                                n_filters=n_filters,
                                kernel_size=kernel_size)

        self.encode = Flatten()#GlobalAveragePooling1D()

        # self.MLP = MLP(ndim = ndim, dropout_rate=drop,  hidden_activation=hidden_activation, n_classes=n_classes)
        self.FC = FC(ndim, dropout_rate, act = hidden_activation)
        self.output_layer = Dense(n_classes, activation='softmax')#, kernel_regularizer=tf.keras.regularizers.l2(l=1E-6))

    def call(self, inputs, is_training = False):
        lstm = self.RNN(inputs)
        conv1 = self.conv1(lstm, is_training)
        conv2 = self.conv2(conv1, is_training)
        conv3 = self.conv3(conv2, is_training)
        encode = self.encode(conv3)
        FC = self.FC(encode, is_training)

        return self.output_layer(FC)
