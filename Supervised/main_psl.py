import sys

sys.path.insert(0, '/home/johann/Topography/Autoencoder/')
sys.path.append('../')  #

import os
import numpy as np #do numpy 1.19.5

gpu = False

if gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    import tensorflow as tf
    from tensorflow.compat.v1 import InteractiveSession
    config = tf.compat.v1.ConfigProto()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    session = InteractiveSession(config=config)
    tf.compat.v1.keras.backend.set_session(session)
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf

import Supervised.models as models
from Autoencoder import utils as utils
import pandas as pd
from importlib import reload
import time
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score

from Supervised import TS_Consistency as supervised_training
from Supervised import pretraining
from importlib import reload

pretraining = reload(pretraining)
supervised_training = reload(supervised_training)


#########################################################################################################################

path_experiment = '/media/DATA/johann/PUL/TileHG/FinalDBPreprocessed/Experiments/Forest'
n_sample = 100
i_exp = 1
def launch_experiment():
    for i_exp in range(1,11,2):
        for n_sample in range(20,120,20):
            print(n_sample)# range(20,120,40)
            X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment=path_experiment,
                                                                          n_sample=n_sample,
                                                                          i_exp=i_exp)

            ##############################################################################################
            # path_VAE = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')
            path_file = 'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp)
            path_AE = os.path.join(path_experiment,
                                   os.path.join(path_file, 'Reconstruction'))

            dico_tanh, filename, model = utils.read_AE_file(path_out=path_AE,
                                                            hidden_activation='tanh',
                                                            embedding_activation='None',
                                                            output_activation='None',
                                                            name_optimizer='Adam_10e-4',
                                                            name_loss='huber',
                                                            prefix='AEGRU_V1_16')

            reconstruction_df_tanh = dico_tanh['Reconstruction']
            reconstruction_df_tanh['binary'] = y_u
            reconstruction_df_tanh['True_class'] = Y_U[:, 0]

            if 'Huber' not in list(reconstruction_df_tanh.columns):
                reconstruction_U = model.predict(X_U.reshape(X_U.shape[0], 25, 6), batch_size=512)
                h = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
                res = h(X_U.reshape(X_U.shape[0], 25, 6), reconstruction_U)
                res = np.mean(res, axis=1)
                reconstruction_df_tanh['Huber'] = res

            #mean_ = np.mean(reconstruction_df_tanh['Huber']) + 0.5 * np.std(reconstruction_df_tanh['Huber'])
            #avnt, le + et - étaient inversées .. wtf mais marchait bien sans transformer (model = NOne et x_test en comm)
            index_RP = np.where(reconstruction_df_tanh['Huber'] <= np.mean(reconstruction_df_tanh['Huber']) )#- 0.5 * np.std(reconstruction_df_tanh['Huber']))
            index_RN = np.where(reconstruction_df_tanh['Huber'] > np.mean(reconstruction_df_tanh['Huber']))
            #+ 0.5 * np.std(reconstruction_df_tanh['Huber']))
            import random
            rdm_neg = random.sample(list(index_RN[0]), int(X_P.shape[0]))
            X_RN = X_U[rdm_neg,]

            x_train = np.concatenate([X_P, X_RN], axis = 0)
            x_train = x_train.reshape(x_train.shape[0],25,6)
            '''
            if len(list(index_RP[0]))>int(x_train.shape[0]) *2:
                rdm_U = random.sample(list(index_RP[0]), int(x_train.shape[0]) *2)
            else:
                rdm_U = index_RP
            '''

            X_U_update = X_U[list(index_RP[0]), ]
            X_U_update = X_U_update.reshape(X_U_update.shape[0], 25, 6)

            y_train = np.concatenate([np.zeros(X_P.shape[0]) + 1,
                                      np.zeros(X_RN.shape[0])],
                                     axis=0)

            x_test = X_T.reshape(X_T.shape[0], 25, 6)
            ############################################################################################
            from Supervised import models as models
            classifier_teacher = models.FCGRU_Model(nunits=32, fcunits=32,
                                                           dropout_rate=0.2, l2_reg=0.0)

            classifier_student = models.FCGRU_Model(nunits=32, fcunits=32,
                                                           dropout_rate=0.2, l2_reg=0.0)

            encoder_input = tf.keras.layers.Input(shape=(25,6))
            classifier_teacher._set_inputs(encoder_input)
            classifier_student._set_inputs(encoder_input)
            ######################################################################
            classifier_teacher.build(input_shape=(64, encoder_input.shape[1], encoder_input.shape[2]))
            classifier_student.build(input_shape=(64, encoder_input.shape[1], encoder_input.shape[2]))

            ############################################################################################
            # Teacher + annotated data
            #optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4)
            #loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False)

            path_NN = os.path.join(path_experiment,
                                   os.path.join(path_file, 'PSL'))

            FCGRU_PRETRAIN = models.FCGRUPretrain(nunits=32, fcunits=32,dropout_rate=0.2, l2_reg=0.0)

            MetaPSL = supervised_training.MetaPseudoLabels(None,
                                                           classifier_teacher,
                                                           classifier_student,
                                                           input_shape = (25,6),
                                                           optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                                                           loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                                                           filepath=path_NN,
                                                           filename='GRU_V1_16_T_MEAN_RP_PSL_SL') #sl = no soft labels (error from my side)

            #RP : define a subset of RP with median - beta*std
            MetaPSL.fit_model_student(model,
                                      x_train,
                                      y_train,
                                      X_U_update,
                                      x_test,
                                      y_t,
                                      1,50,
                                      hard_labels=False,
                                      size_batch=64)

#launch_experiment()

#################################################################################



#####################################################################################
R = 0.5
path_out = os.path.join(path, "Models/") + str(R)

metric_name = 'f1_score'
ratio = {'hold_out_ratio': R}
dictionary_out = pickle.load(open(os.path.join('./FinalDBPreprocessed/Experiments/Fodder/Models/0.5/', 'dictionary_output_' + metric_name + '.pickle'), 'rb'))

numeric_values = dictionary_out['plot']
#numeric_values = numeric_values.iloc[[0,2,4],:]
numeric_values['RF'] = numeric_values['RF'].astype(np.float)
numeric_values['ENSEMBLE'] = numeric_values['ENSEMBLE'].astype(np.float)
numeric_values['OCSVM'] = numeric_values['OCSVM'].astype(np.float)

numeric_values['Teacher'] = gru_results_teacher[metric_name].values
numeric_values['Teacher_XT'] = gru_results_teacher_only[metric_name].values
numeric_values['Student'] = gru_results_student_only[metric_name].values


numeric_values = numeric_values.drop(['RF','ENSEMBLE','OCSVM'],axis = 1)

#plot_results(numeric_values)
print(numeric_values.to_latex())



'''

def launch_experiments(reconstruction_df,
                       rsuffix='tanh',
                       nb_epoch=30,
                       alpha=0.5,
                       name_model = 'GRU_V2_',
                       metric = 'Huber',
                       n_pixels = 300):

    pu_process = supervised_training.PUL_NN(reconstruction_df,
                                            path_experiment=path_experiment,
                                            path_folder_model=output_NN)

    ##############################################################################################
    ##############################################################################################
    ##############################################################################################

    # work well with nunits = 16, ndim = 8, dropout_rate=0.15 + only 20 epochs
    name_model += rsuffix

    pu_process.initialize_output(n_sample=n_sample,
                                 i_exp=i_exp,
                                 name_model=name_model,
                                 metric = metric
                                 )

    pu_process.RunClassifierIteratively(model_AE=model,
                                        classifier_teacher=models.GRUClassifImproved(nunits=64, fcunits=64,
                                                                                     dropout_rate=0.5, l2_reg=0.01),
                                        classifier_student=models.GRUClassifImproved(nunits=64, fcunits=64,
                                                                                     dropout_rate=0.5, l2_reg=0.01),
                                        optimizer=tf.keras.optimizers.Adam(learning_rate=10e-4),
                                        loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False),
                                        nb_epoch=nb_epoch,
                                        input_shape=(25, 6),
                                        alpha=alpha,
                                        n_pixels = n_pixels)


alpha = 0.5

for alpha in [0.5, 1]:
    for n_sample in [60]: #20,40,60, 80, 100
        for i_exp in range(1,11, 2):
            path_file = 'Models/AE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp)
            output_NN = os.path.join(path_file, 'PUL_NN')

            name_model = 'GRU_V3_PL_tanh_64_64_Student_AE_alpha_' + str(alpha)
            filepath = os.path.join(path_file, name_model)
            if not os.path.exists(os.path.join(filepath, name_model + '_teacher_CE.index')):
                ##############################################################################################
                X_P, Y_P, y_p, X_U, Y_U, y_u, X_T, Y_T, y_t = utils.load_data(path_experiment = path_experiment,
                                                                              n_sample=n_sample,
                                                                              i_exp = i_exp)

                ##############################################################################################
                #path_VAE = os.path.join(path_experiment,'Models/VAE/' + 'N_' + str(n_sample) + '_Exp_' + str(i_exp) + '/Reconstruction')
                path_AE = os.path.join(path_experiment,
                                       os.path.join(path_file,'Reconstruction'))

                dico_tanh, filename, model = utils.read_AE_file(path_out = path_AE,
                                                         hidden_activation='tanh',
                                                         embedding_activation='None',
                                                         output_activation='None',
                                                         name_optimizer='Adam_10e-4',
                                                         name_loss='huber',
                                                         prefix = 'AEGRU_V2_64')

                reconstruction_df_tanh = dico_tanh['Reconstruction']
                reconstruction_df_tanh['binary'] = y_u
                reconstruction_df_tanh['True_class'] = Y_U[:, 0]

                reconstruction_U = model.predict(X_U.reshape(X_U.shape[0], 25, 6), batch_size=1028)
                h = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
                res = h(X_U.reshape(X_U.shape[0], 25, 6), reconstruction_U)
                res = np.mean(res, axis=1)
                reconstruction_df_tanh['Huber'] = res

                #increase alpha : more weights on training
                launch_experiments(reconstruction_df_tanh,
                                   rsuffix = 'tanh_64_64_Student_AE_alpha_' + str(alpha) , #+ '_NOV', #,
                                   alpha = alpha, nb_epoch = 15, name_model = 'GRU_V3_PL_',
                                   metric = 'Huber', n_pixels= int(X_P.shape[0] * 0.1))
            else:
                pass
'''